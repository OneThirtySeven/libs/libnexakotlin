package org.nexa.libnexakotlin

import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import androidx.startup.Initializer
import app.cash.sqldelight.db.QueryResult
import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.db.SqlSchema
import app.cash.sqldelight.driver.android.AndroidSqliteDriver
import java.io.File
import java.lang.RuntimeException
import java.net.InetAddress
import java.net.URLEncoder
import java.util.Date
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec

var androidContext: android.content.Context? = null  // Android application execution context
var backingLog: iLogging? = null

actual fun appContext():Any?
{
    return androidContext
}

actual fun epochSeconds(): Long
{
    return Date().time / 1000L
    // return System.currentTimeMillis()/1000
}

actual fun epochMilliSeconds(): Long
{
    return Date().time
    // return System.currentTimeMillis()/1000
}

actual fun String.urlEncode():String
{
    return URLEncoder.encode(this, "utf-8")
}

actual fun sourceLoc(): String
{
    val fr = Exception().stackTrace[1]
    return "[" + fr.fileName + ":" + fr.lineNumber + "]"
}

actual fun GetLog(module:String): iLogging
{
    backingLog?.let { return it }
    var l:iLogging = AndroidLogging(module)
    try {
        l.error("Android logging initialized")
    }
    catch (e: RuntimeException)  // Log doesn't exist because we are not within the android framework
    {
        // Method i in android.util.Log not mocked. See https://developer.android.com/r/studio-ui/build/not-mocked for details.
        l = MockAndroidLogging(module)
    }
    backingLog = l
    return l
}

class MockAndroidLogging(override val module: String): iLogging
{
    override fun error(s: String) { println("[$module] ERROR:    $s") }
    override fun warning(s:String) { println("[$module] WARNING: $s") }
    override fun info(s:String) { println("[$module] INFO:    $s") }
}

class AndroidLogging(override val module: String): iLogging
{
    override fun error(s: String) { Log.e(module, s) }
    override fun warning(s:String) { Log.w(module, s) }
    override fun info(s:String) { Log.i(module, s) }
}

actual fun generateBip39Seed(wordseed: String, passphrase: String, size: Int): ByteArray
{
    val salt = "mnemonic" + passphrase
    val skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512")
    val secretkey = PBEKeySpec(wordseed.toCharArray(), salt.toByteArray(), 2048, 512)
    val seed = skf.generateSecret(secretkey)
    return seed.encoded.slice(IntRange(0, size - 1)).toByteArray()
}



/** Return false if this node has no internet connection, or true if it does or null if you can't tell */
actual fun iHaveInternet(): Boolean?
{
    val ctxt = androidContext ?: return null
    val connectivityManager = ctxt.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    return connectivityManager.activeNetwork != null
}

actual fun ipAsString(ba:ByteArray):String
{
    val inet = InetAddress.getByAddress(ba)
    val nodeIp: String = inet.hostAddress!!
    return nodeIp
}

/** Return the directory that this application may use for its files */
actual fun getFilesDir(): String?
{
    return null
}

actual fun deleteDatabase(name: String): Boolean?
{
    val c = androidContext
    if (c == null) throw LibNexaException("Initialize the 'org.nexa.libnexakotlin.context' global with the Android application context\n**or run Android tests from the androidInstrumentedTest directory**\n.")
    //val dir: File = androidContext!!.getFilesDir()
    // val file = File(dir, path + ".db")
    val file: File = androidContext!!.getDatabasePath(name + ".db")
    val worked = file.delete()
    return worked
}

actual fun createDbDriver(dbname: String, schema: SqlSchema<QueryResult.Value<Unit>>): SqlDriver
{
    val c = androidContext
    if (c == null) throw LibNexaException("Initialize the 'org.nexa.libnexakotlin.context' global with the Android application context\n**or run Android tests from the androidInstrumentedTest directory**")
    return AndroidSqliteDriver(schema, c, dbname)
}

class Initialiser : Initializer<Unit>
{
    override fun create(ctxt: Context)
    {
        val name = ctxt.applicationContext.applicationInfo.processName
        val clname = ctxt.applicationContext.applicationInfo.className
        Log.i("Nexa", "Nexa Android Initializer: $name running class $clname")
        androidContext = ctxt
    }

    override fun dependencies(): List<Class<out Initializer<*>>> {
        return emptyList()
    }
}

