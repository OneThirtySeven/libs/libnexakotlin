package org.nexa.libnexakotlin

import io.ktor.http.encodeURLParameter
import io.ktor.http.encodeURLPath
import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.alloc
import kotlinx.cinterop.allocArray
import kotlinx.cinterop.get
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.pointed
import kotlinx.cinterop.value
import kotlinx.datetime.*
import platform.SystemConfiguration.SCNetworkReachabilityCreateWithAddress
import platform.SystemConfiguration.SCNetworkReachabilityCreateWithName
import platform.SystemConfiguration.SCNetworkReachabilityFlags
import platform.SystemConfiguration.SCNetworkReachabilityFlagsVar
import platform.SystemConfiguration.SCNetworkReachabilityGetFlags
import platform.SystemConfiguration.kSCNetworkFlagsReachable
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1

class NativeIosLogging(override val module: String):iLogging
{
    override fun error(s: String)
    {
        println("[$module] ERROR:    $s")
    }

    override fun warning(s:String)
    {
        println("[$module] WARNING: $s")
    }
    override fun info(s:String)
    {
        println("[$module] INFO:    $s")
    }
}

/** Returns seconds since the epoch */
actual fun epochSeconds(): Long = Clock.System.now().epochSeconds

/** Returns milliseconds since the epoch */
actual fun epochMilliSeconds(): Long = Clock.System.now().toEpochMilliseconds()

actual fun String.urlEncode(): String
{
    return encodeURLParameter(true)
}

//@OptIn(kotlin.experimental.ExperimentalNativeApi::class)
actual fun sourceLoc(): String
{
    try
    {
        //val s = Exception().getStackTrace()[3]
        //return s.slice(s.lastIndexOf('(')..s.lastIndexOf(')'))
        return ""
    }
    catch(e: Exception)
    {
        return ""
    }
}

actual fun GetLog(module: String): iLogging = NativeIosLogging(module)

actual fun generateBip39Seed(wordseed: String, passphrase: String, size: Int): ByteArray
{
    val salt = ("mnemonic".toCharArray() + passphrase.toCharArray())
    return salt.map { it.code.toByte() }.toByteArray()
        .let { saltb ->
            PBEKeySpecCommon(wordseed.toCharArray(), saltb, 2048, 512).let { pbeKeySpec ->
                SecretKeyFactoryCommon.getInstance("PBKDF2WithHmacSHA512", FallbackProvider()).let { keyFactory ->
                    keyFactory.generateSecret(pbeKeySpec).encoded.also {
                        pbeKeySpec.clearPassword()
                    }
                }
            }
        }
}

/** Return false if this node has no internet connection, or true if it does or null if you can't tell */
@OptIn(ExperimentalForeignApi::class)
actual fun iHaveInternet(): Boolean?
{
    val reach = SCNetworkReachabilityCreateWithName(null,"www.nexa.org")
    if (reach != null)
    {
        memScoped {
            var flags = this.allocArray<SCNetworkReachabilityFlagsVar>(1)
            val ret = SCNetworkReachabilityGetFlags(reach, flags)
            if (ret)
            {
                val tmp = flags.get(0)
                if ((tmp and kSCNetworkFlagsReachable.toUInt()) != 0U) return true
                else return false
            }
        }
    }
    return null
}

private var isInitialized = false
