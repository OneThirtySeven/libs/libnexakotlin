package org.nexa.libnexakotlin

import app.cash.sqldelight.db.QueryResult
import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.db.SqlSchema
import app.cash.sqldelight.driver.jdbc.sqlite.JdbcSqliteDriver
import java.io.File
import java.net.URLEncoder
import java.util.Date
import java.util.logging.FileHandler
import java.util.logging.Level
import java.util.logging.Logger
import java.util.logging.SimpleFormatter
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec

val LOG_SOURCE_LOC = true

actual fun appContext():Any?
{
    return null
}

var backingLog: iLogging? = null

//actual fun getPlatform(): Platform = JvmPlatform()

actual fun epochSeconds(): Long
{
    return Date().time / 1000L
    // return System.currentTimeMillis()/1000
}

actual fun epochMilliSeconds(): Long
{
    return Date().time
    // return System.currentTimeMillis()/1000
}

actual fun String.urlEncode():String
{
    return URLEncoder.encode(this, "utf-8")
}


actual fun sourceLoc(): String
{
    if (LOG_SOURCE_LOC)
    {
        val fr = Exception().stackTrace[1]
        return "[" + fr.fileName + ":" + fr.lineNumber + "]"
    }
    else return ""
}

actual fun GetLog(module:String): iLogging
{
    backingLog?.let { return it }
    val l = JvmLogging(module)
    backingLog = l
    return l
}

class JvmLogging(override val module: String):iLogging
{
    val log = Logger.getLogger(module)
    init {
        log.useParentHandlers = false
        val h = FileHandler("dwally.log", 256*1024*1024, 10, true)
        h.formatter = SimpleFormatter()
        log.addHandler(h)
        log.level = Level.ALL
    }

    override fun error(s: String)
    {
        println(s)
        log.severe(s)
    }
    override fun warning(s:String)
    {
        println(s)
        log.warning(s)
    }
    override fun info(s:String)
    {
        println(s)
        log.info(s)
    }
}

actual fun generateBip39Seed(wordseed: String, passphrase: String, size: Int): ByteArray
{
    val salt = "mnemonic" + passphrase
    val skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512")
    val secretkey = PBEKeySpec(wordseed.toCharArray(), salt.toByteArray(), 2048, 512)
    val seed = skf.generateSecret(secretkey)
    return seed.encoded.slice(IntRange(0, size - 1)).toByteArray()
}


/** Return false if this node has no internet connection, or true if it does or null if you can't tell */
actual fun iHaveInternet(): Boolean?
{
    return null
}

actual fun ipAsString(ba:ByteArray):String
{
    if (ba.size == 4)  // IPv4 address
    {
        return "${ba[0].toPositiveInt().toString()}.${ba[1].toPositiveInt().toString()}.${ba[2].toPositiveInt().toString(10)}.${ba[3].toPositiveInt().toString()}"
    }
    else if (ba.size == 16)
    {
        var str = ""
        for (i in 0 until 16 step 2)
        {
            //val tmp = ba.slice(IntRange(i,i+1))
            val tmp = ba[i].toPositiveInt() * 256 + ba[i+1].toPositiveInt()
            str = str + tmp.toString(16) + if (i<14) ":" else ""
        }
        return str
    }
    else  // some other format
    {
        return "unknown IP address format"
    }
}

actual fun getFilesDir(): String?
{
    return ""
}

actual fun deleteDatabase(name: String):Boolean?
{
    val f = File(name + ".db")
    if (f.isFile)
    {
        return f.delete()
    }
    return false
}


actual fun createDbDriver(dbname: String, schema: SqlSchema<QueryResult.Value<Unit>>): SqlDriver
{
    val driver: SqlDriver = JdbcSqliteDriver("jdbc:sqlite:" + dbname)
    // TODO deal with migrations schema.migrate(...)
    try
    {
        schema.create(driver)
    }
    catch(e: Exception) // org.sqlite.SQLiteException
    {
        val m = e.message
        if (!(m != null && m.contains("already exists")))  // database already created
            throw e
    }
    return driver
}

/*
actual fun createNexaDbDriver(dbname: String): SqlDriver
{
    val driver: SqlDriver = JdbcSqliteDriver(dbname)
    NexaDb.NexaDb.Schema.create(driver)
    return driver
}

actual fun createKvpDbDriver(dbname: String): SqlDriver
{
    val driver: SqlDriver = JdbcSqliteDriver(dbname)
    WalletDb.WalletDb.Schema.create(driver)
    return driver
}

 */