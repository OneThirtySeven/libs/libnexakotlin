// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.nexa.libnexakotlin

import kotlinx.coroutines.*
import kotlinx.serialization.json.JsonObject
import kotlin.coroutines.CoroutineContext
import kotlin.text.StringBuilder
import kotlin.time.TimeSource
import kotlin.math.ceil
import org.nexa.threads.*
import kotlin.concurrent.Volatile
import com.ionspin.kotlin.bignum.decimal.BigDecimal

const val MAX_TX_INPUTS = 256

private val LogIt = GetLog("BU.wallet")

/** Helper class that glues a wallet to a blockchain
 * @property [chain] a reference to an active blockchain object.  The wallet will access this object whenever it needs blockchain data.
 */
@cli(Display.Simple, "Connection between this wallet and its blockchain.")
class GlueWalletBlockchain(val chain: Blockchain) : BCHserializable
{
    val mutex = Mutex()

    companion object
    {
        const val SERIALIZATION_VERSION = 2.toByte()
    }
    /** This wallet has synchronized its balances up to this block height.  Note, use syncedHash to be certain of a sync because the chain could have forks.
     */
    @Volatile
    @cli(Display.Simple, "This wallet has synchronized its balances up to this block height.")
    var syncedHeight: Long = chain.checkpointHeight

    @Volatile
    @cli(Display.Simple, "This wallet has synchronized its balances up to this date (as read from the block syncedHash/syncedHeight).  This is a convenience function so that the block header does not need to be accessed.  syncedHash is authoritative.")
    var syncedDate: Long = chain.checkpointHeight

    /** This wallet has synchronized its balances up to this block hash */
    @Volatile
    @cli(Display.Simple, "This wallet has synchronized its balances up to this block hash.")
    var syncedHash: Hash256 = chain.checkpointId

    @Volatile
    @cli(Display.Simple, "The confirmed balance as of the syncedHash.")
    var balance: Long = 0

    /** Any block before this one cannot have any transactions relevant to this wallet (the wallet was created after this block).
     *  The wallet uses this information to rapidly sync.
     */
    @cli(Display.Simple, "Any block before this date cannot have any transactions relevant to this wallet.")
    var prehistoryDate: Long = 0

    /** Any block before this one cannot have any transactions relevant to this wallet (the wallet was created after this block).
     *  The wallet uses this information to rapidly sync.
     */
    @cli(Display.Simple,
      "Any block before this one cannot have any transactions relevant to this wallet.  If zero we don't know the height.")
    var prehistoryHeight: Long = 0

    /** a handle to this wallet's bloom filter data installed in the Blockchain */
    var filterHandle: Int? = null

    init
    {
        chain.attachWallet()
    }

    @cli(Display.Simple,
      "Return true if this wallet is synchronized with the blockchain")
    fun isSynchronized(maxBlock: Int=0, maxSec: Int=60*60):Boolean
        {
            if (syncedHeight + maxBlock < chain.curHeight) return false  // We cannot be behind in blocks
            if (syncedDate + maxSec < epochSeconds()) return false  // If our last synced block is too old, we assume unsynced (blockchain may have to catch up as well).
            return true
        }


    fun delete()
    {
        mutex.lock {
            chain.detachWallet(filterHandle)
        }
    }

    /** Reset this wallet's blockchain state to the earliest point available in the blockchain */

    fun resetToCheckpoint()
    {
        mutex.lock {
            syncedHeight = chain.checkpointHeight
            syncedHash = chain.checkpointId
            syncedDate = 0
            balance = 0
        }
    }

    /** Reset this wallet's blockchain state to the wallet prehistory */

    fun resetToPrehistory()
    {
        mutex.lock {
            // force reset of prehistory
            //if (prehistoryHeight > 0) prehistoryHeight = 1
            //if (prehistoryDate > 0) prehistoryDate = 1
            if (prehistoryHeight != 0L)  // I know the prehistory block
            {
                syncedHeight = prehistoryHeight
                syncedHash = chain.getBlockHeader(syncedHeight).hash
                syncedDate = 0
                balance = 0
            }
            else resetToCheckpoint()  // I don't know it so reset do checkpoint to discover it by iteration
        }
    }


    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        LogIt.info(sourceLoc() + " " + chain.name + " prehistory written synced is $syncedHeight time is $prehistoryDate height is $prehistoryHeight hash is $syncedHash")
        return mutex.lock {
            BCHserialized(SerializationType.DISK).addUint8(SERIALIZATION_VERSION.toInt()).addUint64(syncedHeight.toULong()).addUint64(prehistoryDate.toULong()).addUint64(prehistoryHeight.toULong()).add(syncedHash).addUint64(syncedDate).addUint64(balance)
        }
    }


    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        return mutex.lock {
            val ver = stream.deuint8()
            if (ver == 2)
            {
                syncedHeight = stream.deuint64()
                prehistoryDate = stream.deuint64()
                prehistoryHeight = stream.deuint64()
                syncedHash = Hash256(stream)
                syncedDate = stream.deuint64()
                balance = stream.deuint64()
                LogIt.info(sourceLoc() + " " + chain.name + " Loaded GlueWalletBlockchain.  prehistory read synced is $syncedHeight time is $prehistoryDate height is $prehistoryHeight hash is $syncedHash")
            }
            else throw DeserializationException("Invalid version for stored TransactionHistory")
            stream
        }
    }
}

fun chainStateDbKey(name: String, chainSelector: ChainSelector) = "wallet_" + name + "_chainstate_" + chainSelector
fun chainSelectorDbKey(name: String) = "wallet_" + name + "_chainselector"
fun txStateDbKey(name: String) = "wallet_" + name + "_txstate"
fun unusedAddressesDbKey(name: String) = "wallet_" + name + "_unusedAddresses"
// fun txHistoryDbKey(name: String) = "wallet_" + name + "_txHistory"
fun identityDomainsDbKey(name: String) = "wallet_" + name + "_identitydomains"
fun identityInfoDbKey(name: String) = "wallet_" + name + "_identityInfo"
fun txoCacheDbKey(name: String) = "wallet_" + name + "_maxTxoCache"
fun txHistoryCacheDbKey(name: String) = "wallet_" + name + "_maxTxCache"
fun txAddrGenChunkDbKey(name: String) = "wallet_" + name + "_txAddrGenChunkDbKey"

/** Delete a wallet on disk */
fun deleteWallet(walletName: String, chainSelector: ChainSelector)
{
    val fname = walletName + "_wallet"
    try
    {
        val db = openWalletDB(fname)
        if (db == null) return
        deleteWallet(db, walletName, chainSelector)
    }
    catch(e: Exception)
    {
        LogIt.info("Wallet $fname: was not able to open it to delete contents, trying to delete entire file")
    }

    // Right now every wallet is stored in a separate database file so I can delete the entire file
    val fileDeleted = deleteDatabase(fname)
    if (fileDeleted != true)
    {
        LogIt.error("Wallet file $fname not deleted: ${fileDeleted}")
    }
}

fun deleteWallet(db: WalletDatabase, walletName: String, chainSelector: ChainSelector)
{
    val dbkey = "wallet_" + walletName + "chainstate_" + chainSelector
    db.delete(dbkey)
    db.delete("bip44wallet_" + walletName)
    db.delete(chainSelectorDbKey(walletName))
    db.delete(chainStateDbKey(walletName, chainSelector))
    db.delete(txStateDbKey(walletName))
    db.delete(unusedAddressesDbKey(walletName))
    db.delete(identityDomainsDbKey(walletName))
    db.delete(identityInfoDbKey(walletName))
    db.tx.clear()
    db.txo.clear()
    db.kvp.clear()

}

// Returns a predicate function that accepts a BCHspendable.  This function returns true if the BCHspendable is in the passed
// group and has properties based on passed flags, and is not already reserved.  If "normal" is true, any non-authority utxo will return true.
// If "authority" is a bitmap, returns true if these bits are set in the authorityFlags.
// If "maskOff" is zero, maskOff is set to "authority" (and so does nothing).  Otherwise all set bits are part of the filter.  This parameter allows the caller require that a bit is zero, by
// setting it in "maskOff", but leaving it cleared in "authority".

fun groupedFilter(
  groupId: GroupId,
  normal: Boolean = true,
  authority: ULong = GroupAuthorityFlags.AUTHORITY,
  maskOff: ULong = 0.toULong()
): (Spendable) -> Long
{
    val mask = if (maskOff == 0.toULong()) authority else maskOff
    // returns a filter function that selected only grouped inputs, either normal or authority (or both) depending on the flags
    return {
        if (it.reserved != 0L) 0
        else
        {
            val groupInfo: GroupInfo? = it.groupInfo()

            if (groupInfo == null) 0
            else
            {
                if (groupInfo.groupId != groupId)
                {
                    // LogIt.info("groupId doesn't match")
                    0
                }
                else
                {
                    if (groupInfo.isAuthority())
                    {
                        // LogIt.info("Auth check: " + groupInfo.authorityFlags.toString(16) + " and " + authority.toString(16))
                        // In the authority case, there is no amount; each authority can be considered 1 unit
                        if ((groupInfo.authorityFlags and mask) == authority) 1
                        else 0
                    }
                    else
                    {
                        if (normal) groupInfo.tokenAmt
                        else 0
                    }
                }
            }
        }
    }
}

/** Calculate the Schnorr signature of the passed transaction and input index
 * This function does not update the transaction.
 * @returns The signature as a byte array (including sighashtype) */
@OptIn(kotlin.ExperimentalUnsignedTypes::class)
fun calcSig(tx: iTransaction, inputIdx: Int, sigHashType: ByteArray, serializedTx: ByteArray? = null): ByteArray
{
    val inp = tx.inputs[inputIdx]
    val spendable = inp.spendable
    val secret = spendable.secret

    val flatTx = serializedTx ?: tx.BCHserialize(SerializationType.NETWORK).toByteArray()

    if (secret == null) throw IllegalArgumentException("Input spendable must have secret set")
    if (secret.getSecret().size == 0) throw IllegalArgumentException("Input spendable must have a nonempty secret")
    val sigSchnorr = libnexa.signTxOneInputUsingSchnorr(flatTx, sigHashType, inputIdx.toLong(), inp.spendable.amount, inp.spendable.priorOutScript.flatten(), secret.getSecret())
    return sigSchnorr
}

/** Sign (and create the satisfier script for) one input of the passed transaction
 * This function modifies the transaction.
 * @returns True if the input was successfully signed
 * */
fun signInput(tx: iTransaction, idx: Long, sigHashType: ByteArray, serializedTx: ByteArray? = null): Boolean
{
    val inp = tx.inputs[idx.toInt()]
    val spendable = inp.spendable
    val secret = spendable.secret

    val flatTx = serializedTx ?: tx.BCHserialize(SerializationType.NETWORK).toByteArray()

    // if we don't know how to sign this input, then don't sign it.
    // this is also how the tx creator communicates to the wallet that this input need not be signed
    if ((secret != null && secret.getSecret().size != 0) || (inp.spendable.backingPayDestination != null))
    {
        /*
            //LogIt.info("Signing tx " + flatTx.ToHex() + ", " + sigHashType + ", " + count + ", " + inp.spendable.amount + ", " + inp.spendable.priorOutScript.flatten().ToHex() + ", " + secret.ToHex())
            val sig = signOneInputUsingECDSA(flatTx, sigHashType, idx, inp.spendable.amount, inp.spendable.priorOutScript.flatten(), secret)
            //val sigSchnorr = signOneInputUsingSchnorr(flatTx, sigHashType, count, inp.spendable.amount, inp.spendable.priorOutScript.flatten(), secret)

            // TODO call a function on the spendable that produces the satisfier script.  For now assume P2PKH
            val pubkey = PayDestination.GetPubKey(secret)
            inp.script = BCHscript(chainSelector, OP.push(sig), OP.push(pubkey))
            // DEBUG break the sig:
            //inp.script = BCHscript(chainSelector, OP.NOP, OP.push(pubkey))
         */
        val pd = inp.spendable.payDestination
        if (pd != null)  // This will be null if we can't understand the script, and therefore can't sign this input
        {
            inp.script = pd.spendScript(flatTx, idx, sigHashType, inp.spendable.amount)
            return true
        }
    }
    return false
}

/** signs a transaction in place, with all inputs that have a BCHspendable with secrets (skips those that do not so others can sign)
 by default the sighashtype is ALL/ALL
*/
fun signTransaction(tx: iTransaction, sigHashType: ByteArray = byteArrayOf())
{
    val txSerialized = tx.BCHserialize(SerializationType.NETWORK)
    val flatTx = txSerialized.toByteArray()
    var changed = false

    for (idx in 0L until tx.inputs.size)
    {
        changed = signInput(tx, idx, sigHashType, flatTx) or changed
    }
    if (changed) tx.changed()  //  Clear out the hash (if it was requested) since it will have changed now that signatures are added.
}

/** This class provides implementations to for functions that are common to many blockchains and wallet types */
@cli(Display.Simple, "Most wallet functionality resides here")
abstract class CommonWallet(override val name: String, chainSelector: ChainSelector) : Wallet(chainSelector)
{
    companion object
    {
        // Constants defining the data version of the wallet
        val CHAIN_STATE_SERIALIZED_VERSION: Byte = 1
        val V1_TX_STATE_SERIALIZED_VERSION: Byte = 1
        val TX_STATE_SERIALIZED_VERSION: Byte = 2
        val UNUSED_ADDRESSES_SERIALIZED_VERSION: Byte = 1
        val TX_HISTORY_SERIALIZED_VERSION: Byte = 1
        val PAYMENT_HISTORY_SERIALIZED_VERSION: Byte = 1
    }

    var absoluteMaxTxoCache = 6000  // During periods of heavy tx creation, we may ramp up the txo cache
    var absoluteMinTxoCache = 100  // During periods of low tx creation, we may ramp down the txo cache
    var maxTxoCache = 2000
    var maxTxCache = 500
    var genAddressChunkSize = DEFAULT_GEN_ADDRESS_CHUNK_SIZE

    /** balance of currently unconfirmed tx (that apply to this wallet) -- confirmed balance is in the chainstate object. */
    var unconfBalance = 0L

    /** rebroadcast historical unconfirmed transactions every hour (and when we first start up) */
    val timeToResendHistoricalTx = Periodically(60L * 60000L)

    /** rebroadcast wallet transaction every minute */
    val timeToResendWalletTx = Periodically(60000)

    val WALLET_FLUSH_PERIOD = 10000   //!< Minimum wallet save interval, for recoverable operations

    /** If the wallet's state changes, it will call this function.  Use [setOnWalletChange]. */
    protected var walletChangeCallback: ((Wallet) -> Unit)? = null

    //  These variables wake up a processing thread to handle periodic tasks like chainstate sync and tx submission.
    //  These tasks are not implemented via co-routines triggered by events because we want both event trigger and periodic checking.
    //w val lock = ReentrantLock()
    //protected val cond = lock.newCondition()
    protected val CHECK_PERIOD: Long = 10000  // 10 seconds

    /**  Set to true to quit */
    protected var done = false

    // Set to true to pause, false to resume
    @Volatile
    override var pause = false

    @Volatile
    var paused = false

    protected val oneTxConstructor = Mutex("1TxConstructor")
    /** protects the next 4 objects */
    protected val dataLock = Gate()

    /** Addresses that have already been generated and are in the bloom filter, but are not yet used. */
    @cli(Display.Dev, "available but unused address list, pair of generated count and actual address.  List should be sorted, the generated count is there to make sure it is.  Remove from index 0 before using, add to the end.  Its important to use in order so that gaps between used addresses remain small.  Otherwise some wallets may not know to look beyond the gap.")
    // TODO protected val unusedAddresses: MutableList<Pair<Int, PayAddress>> = mutableListOf()
    protected val unusedAddresses: MutableList<PayAddress> = mutableListOf()

    /** Every active receive address that has been generated (even if they have not been used or even provided to some external payer).
     * Retired (inactive) addresses will not be available in RAM.  Addresses are retired if they are used for change, or by the user's request.
     * Note that in BIP44 wallets, all private keys remain available.  Other wallets should similarly ensure that private keys of retired addresses are
     * preserved in case someone sends to them and the user needs to recover that payment. */
    @cli(Display.Dev, "Every active or unused receive address that has been generated")
    protected var receiving: MutableMap<PayAddress, PayDestination> = mutableMapOf()

    /** all UTXOs that this wallet is capable of spending (like unspent), indexed by address */
    //@cli(Display.Dev, "All UTXOs that this wallet is capable of spending, indexed by address")
    //protected val unspentByAddress: MutableMap<PayAddress, MutableList<iTxOutpoint>> = mutableMapOf()

    /* all UTXOs that this wallet is capable of spending (and how to spend them) */
    //@cli(Display.Dev, "All UTXOs that this wallet is capable of spending")
    protected var txoCache: HashMap<iTxOutpoint, Spendable> = hashMapOf()

    /** All transactions that interest this wallet -- either sent or received */
    //@cli(Display.User, "All transactions that interest this wallet, sent, received, or externally generated, indexed by transaction hash")
    protected var txHistoryCache: HashMap<Hash256, TransactionHistory> = hashMapOf()

    // offer only a read only version in the Wallet API because unspentByAddress, txHistory, paymentHistory, etc need to be
    // updated when a utxo is spent or received
    //override val txos: Map<iTxOutpoint, Spendable>
    //    get() = allTxos.toMap()

    @cli(Display.User, "wallet history by transaction idem")
    override fun getTxo(txo: iTxOutpoint): Spendable?
    {
        return dataLock.lock {
            val tmp = txoCache.get(txo)
            if (tmp != null)
            {
                // LogIt.info(sourceLoc() + " " + name + " cache hit ${txo.toHex()}")
                tmp
            }
            else
            {
                val ret = walletDb!!.txo.read(txo)
                ret?.let {
                    // LogIt.info(sourceLoc() + " " + name + " DB hit ${txo.toHex()}")
                    it.dirty = false
                    txoCache[txo] = it
                }
                ret
            }
        }
    }

    @cli(Display.User, "delete a txo from the wallet's history.  This should be used if the txo does not exist on the main chain, not when the txo is spent.")
    override fun deleteTxo(vararg txos: iTxOutpoint)
    {
        //dataLock.lock { allTxos.remove(txo) }
        dataLock.lock {
            for(txo in txos)
            {
                txoCache.remove(txo)
                walletDb!!.txo.delete(txo)
            }
        }
    }

    @cli(Display.User, "delete a txo from the wallet's history.  This should be used if the txo does not exist on the main chain, not when the txo is spent.")
    fun deleteTxo(txos: Collection<iTxOutpoint>)
    {
        //dataLock.lock { allTxos.remove(txo) }
        dataLock.lock {
            for(txo in txos)
            {
                txoCache.remove(txo)
                walletDb!!.txo.delete(txo)
            }
        }
    }

    @cli(Display.User, "wallet TXO iterator.")
    override fun forEachTxo(doit: (Spendable) -> Boolean)
    {
        dataLock.lock {
            walletDb!!.txo.forEach {
                // we must use the cached version if it exists in case it was modified
                val cached = txoCache[it.outpoint]
                if (cached != null) doit(cached)
                else
                {
                    val ret = doit(it)
                    // cache any changes or if we have room in the cache
                    if (it.dirty || (it.reserved != 0L) || txoCache.size < maxTxoCache)
                    {
                        val tmp = it.outpoint
                        if (tmp != null) txoCache[tmp] = it
                    }
                    ret
                }
            }
        }
    }


    var numForEachUtxo = 0L
    var utxoFromDisk = 0L
    @cli(Display.User, "wallet UTXO iterator.  For typical wallets this will be all utxos.  And for typical operations, the returned utxos should be enough." +
        "But if you really need to see ALL utxos, pass true.  This is done for efficiency when constructing payments with huge wallets.")
    override fun forEachUtxo(doit: (Spendable) -> Boolean)
    {
        numForEachUtxo++
        dataLock.lock {
            var done = false
            var numCachedUnspent = 0
            // First offer everything in the cache
            val cachedSpent = mutableListOf<iTxOutpoint>()
            val cachedSpentNeedsFlush = mutableListOf<Spendable>()
            for( (k,v) in txoCache)
            {
                if (v.isUnspent)
                {
                    numCachedUnspent++
                    done = doit(v)
                    if (done) break
                }
                else // TODO idea: clean the cache of spent not-dirty elements as part of this search
                {
                    if (v.dirty)
                    {
                        cachedSpentNeedsFlush.add(v)
                    }
                    else
                    {
                        cachedSpent.add(k)
                    }
                }
            }

            if (!done)
            {
                utxoFromDisk++
                // LogIt.info(sourceLoc() + " " + name +": forEachUtxo accessed disk $utxoFromDisk of $numForEachUtxo times (${utxoFromDisk.toFloat()/numForEachUtxo.toFloat()})")
                // Cache optimization: If we have to go to the disk and we don't have many cached unspent, we can clean up the cache
                if (txoCache.size > maxTxoCache-500)  // Arbitrary number
                {
                    for (k in cachedSpent)  // We are holding the dataLock, so this object's state MUST not have changed
                    {
                        txoCache.remove(k)
                    }
                    if (cachedSpentNeedsFlush.size > 200) // Arbitrary number, just make it big enough to be worth a blocking disk access
                    {
                        flushTrimTxo(cachedSpentNeedsFlush)
                    }
                    if (maxTxoCache < absoluteMaxTxoCache) maxTxoCache++
                }

                // Then offer everything NOT in the cache
                walletDb!!.txo.forEachUtxo {
                    val cached = txoCache[it.outpoint]
                    if (cached == null)
                    {
                        if (it.isUnspent)
                        {
                            val ret = doit(it)
                            // cache any changes or if we have room in the cache
                            if (it.dirty || (it.reserved != 0L) || (it.isUnspent && (txoCache.size < maxTxoCache)))
                            {
                                val tmp = it.outpoint
                                if (tmp != null) txoCache[tmp] = it
                            }
                            ret
                        }
                        else
                        {
                            LogIt.info("Should  be unspent but is not: ${it}")
                            LogIt.info("BUG")
                            false
                        }
                    }
                    else false
                }
            }
        }
    }

    @cli(Display.User, "wallet UTXO iterator.  For typical wallets this will be all utxos.  And for typical operations, the returned utxos should be enough." +
        "But if you really need to see ALL utxos, pass true.  This is done for efficiency when constructing payments with huge wallets.")
    fun forEachUtxoWithAddress(addr:PayAddress, doit: (Spendable) -> Boolean)
    {
        dataLock.lock {
            var done = false
            var numCachedUnspent = 0
            // First offer everything in the cache
            for( (k,v) in txoCache)
            {
                if ( v.isUnspent && v.addr == addr)
                {
                    numCachedUnspent++
                    done = doit(v)
                    if (done) break
                }
                else // TODO idea: clean the cache of spent not-dirty elements as part of this search
                {

                }
            }

            if (!done)
            {
                // Then offer everything NOT in the cache
                walletDb!!.txo.forEachUtxoWithAddress(addr) {
                    val cached = txoCache[it.outpoint]
                    if (cached == null)
                    {
                        if (it.isUnspent)
                        {
                            val ret = doit(it)
                            // cache any changes or if we have room in the cache
                            if (it.dirty || (it.reserved != 0L) || txoCache.size < maxTxoCache)
                            {
                                val tmp = it.outpoint
                                if (tmp != null) txoCache[tmp] = it
                            }
                            ret
                        }
                        else
                        {
                            LogIt.info("Should  be unspent but is not: ${it}")
                            false
                        }
                    }
                    else false
                }
            }
        }
    }

    @cli(Display.User, "return the number of currently active ledger entries (UTXOs) in this wallet")
    override fun numUtxos(): Int
    {
        /*  loading from DB does not reflect unspent UTXOs in cache
        return dataLock.lock {
            walletDb!!.txo.numUtxos().toInt()
        }
         */
        var count = 0
        forEachUtxo {count++; false
        }
        return count
    }

    @cli(Display.User, "return the total number of ledger entries (TXOs) this wallet has ever used")
    override fun numTxos(): Int
    {
        // return dataLock.lock { allTxos.size }
        return dataLock.lock {
            walletDb!!.txo.numTxos().toInt()
        }
    }

    fun flushTxo(txo: Array<Spendable?>, count: Int)
    {
        synchronized(dataLock)
        {
            val slice = txo.sliceArray(IntRange(0, count))
            //for (s in slice)
            //{
            //    LogIt.info(sourceLoc() + " " + name + ": write to DB ${s}")
            //}
            walletDb!!.txo.write(*slice)
        }
    }


    /** Internal function to add a Txo to the cache ONLY (when money is received, for example).
     * The caller is expected to call setTxo, flushTxo for this object explicitly later, or to save all dirty Txos in the cache
     */
    protected fun cacheTxo(vararg spendable: Spendable)
    {
        synchronized(dataLock)
        {
           for (sp in spendable)
           {
               sp.dirty = true
               txoCache[sp.outpoint!!] = sp
               insertUnspentByAddress(sp.addr, sp.outpoint!!)
           }
        }
    }

    /** Internal function to add a Txo into this wallet (when money is received, for example).
     * See [injectUnspent] for the external version of this function */
    protected fun setTxo(vararg spendable: Spendable)
    {
        synchronized(dataLock)
        {
            walletDb!!.txo.write(*spendable)
            for (sp in spendable)
            {
                // LogIt.info(sourceLoc() + " " + name + ": write ${sp}")
                sp.dirty = false // I just wrote it to the db above
                txoCache[sp.outpoint!!] = sp
                sp.addr?.let {
                    insertUnspentByAddress(sp.addr, sp.outpoint!!)
                }
            }
            // If too many cached, remove a random one (its a write-thru cache) and put in this one
            trimTxoCache()
        }
    }

    protected fun setTxo(spendable: Collection<Spendable>)
    {
        synchronized(dataLock)
        {
            walletDb!!.txo.write(spendable)
            for (sp in spendable)
            {
                // LogIt.info(sourceLoc() + " " + name + ": write ${sp}")
                sp.dirty = false // I just wrote it to the db above
                txoCache[sp.outpoint!!] = sp
                sp.addr?.let {
                    insertUnspentByAddress(sp.addr, sp.outpoint!!)
                }
            }
            // If too many cached, remove a random one (its a write-thru cache) and put in this one
            trimTxoCache()
        }
    }

    @cli(Display.Simple, "Manually insert spendable coins into this wallet, allowing the wallet to use them for subsequent spends.  This will NOT sweep these coins! This means they will not be restored by recovering the wallet via the recovery key.")
    fun injectUnspent(vararg spend: Spendable)
    {
        // right now there is no difference between injecting your own txo and the internal function that does it in response to some event like a committed tx
        setTxo(*spend)
    }

    var numTxCacheTrims = 0L
    val flushTxArray = Array<TransactionHistory?>(200, {null})

    fun clearTxHistoryCache()
    {
        val flushTxArray = Array<TransactionHistory?>(txHistoryCache.size, { null })
        var numFlush = 0

        // LogIt.info("TXHISTORY clearing ${txHistoryCache.size} items")
        for ((v, h) in txHistoryCache)
        {
            if (h != null)  // don't clean up unconfirmed tx
            {
                if (h.dirty)
                {
                    flushTxArray[numFlush] = h
                    numFlush++
                }
            }
        }
        if (numFlush > 0)
        {
            flushTx(flushTxArray, numFlush)
            for (i in range(0, numFlush)) flushTxArray[i] = null
        }
        //txHistoryCache.clear()

        val keys = txHistoryCache.keys.toList()
        for (v in keys)
        {
            txHistoryCache.remove(v)
        }
    }

    fun trimTxHistoryCache()
    {
        if (txHistoryCache.size > maxTxCache)
        {
            numTxCacheTrims++
            // val amt = txHistoryCache.size - maxTxCache
            var count = 0
            var trimmedAmt = 0
            val keys = txHistoryCache.keys
            var numFlush = 0

            val max = min(txHistoryCache.size/20,200)
            while(count < max && (txHistoryCache.size > maxTxCache-400))
            {
                count++
                val v = keys.random()
                val h = txHistoryCache[v]
                if ((h!= null) && !h.isUnconfirmed())  // don't clean up unconfirmed tx
                {
                    //if (h.dirty) setTx(h, trim=false)
                    if (h.dirty)
                    {
                        flushTxArray[numFlush] = h
                        numFlush++
                    }
                    txHistoryCache.remove(v)
                    trimmedAmt++
                }
            }
            if (numFlush > 0)
            {
                flushTx(flushTxArray, numFlush)
                for (i in range(0,numFlush)) flushTxArray[i] = null

            }

            //LogIt.info(sourceLoc() + " " + name + ": trimed txhistory by ${trimmedAmt}, now ${txHistoryCache.size}")
/*
            if ((numTrims % 32) == 0L)
            {
                var numUnconf = 0
                for ((k,h) in txHistoryCache)
                {
                    if (h.isUnconfirmed())
                    {
                        numUnconf++

                        val tmp = getTxWithoutCaching(h.tx.idem)
                        if (k != h.tx.idem)
                        {
                            LogIt.info("CACHE: bad idem 1 ${k.toHex()} ${h.tx.idem.toHex()} ${h.tx.id.toHex()}")
                        }
                        if (tmp == null)
                        {
                            LogIt.info("CACHE: bad idem 2")
                        }
                        else
                        {
                            if (!tmp.isUnconfirmed())
                            {
                                LogIt.info("CACHE: inconsistent 1")
                            }
                        }
                    }
                }
                LogIt.info(sourceLoc() + " " + name + ": trimmed txhistory by ${trimmedAmt}, now ${txHistoryCache.size}, num unconf ${numUnconf}")
            }
 */
        }
    }

    var numTxoTrims = 0L
    val flushTxoArray = Array<Spendable?>(200, {null})

    fun trimTxoCache()
    {
        if (txoCache.size > maxTxoCache)
        {
            numTxoTrims++
            var count = 0
            var trimmedAmt = 0
            val keys = txoCache.keys
            var numFlush = 0

            val max = min(txoCache.size/20,200)
            while(count < max && (txoCache.size > maxTxoCache-400))
            {
                count++
                val v = keys.random()
                val h = txoCache[v]
                if ((h!= null)&&(h.reserved==0L))
                {
                    if (h.dirty)
                    {
                        flushTxoArray[numFlush] = h
                        h.dirty = false
                        numFlush++
                    }
                    txoCache.remove(v)
                    trimmedAmt++
                }
            }
            if (numFlush > 0)
            {
                flushTxo(flushTxoArray, numFlush)
                for (i in range(0,numFlush)) flushTxArray[i] = null
            }
        }
    }

    /** Flush these Txos and delete from the cache.  Typically used with a collection of Txos you've pulled from the cache already. */
    protected fun flushTrimTxo(sp: Collection<Spendable>)
    {
        synchronized(dataLock)
        {
            walletDb!!.txo.write(sp)
            for(s in sp)
            {
                // LogIt.info(sourceLoc() + " " + name + ": write ${sp}")
                txoCache.remove(s.outpoint)
            }
        }
    }


    @cli(Display.User, "wallet history by transaction idem")
    override public fun getTx(txIdem: Hash256): TransactionHistory?
    {
        return dataLock.lock {
            var ret = txHistoryCache[txIdem]
            if (ret == null)
            {
                ret = walletDb!!.tx.read(txIdem)
                ret?.let {
                    txHistoryCache[it.tx.idem] = it
                }
                ret
            }
            ret
        }
    }

    protected fun getTxWithoutCaching(txIdem: Hash256): TransactionHistory?
    {
        return dataLock.lock {
            walletDb!!.tx.read(txIdem)
        }
    }

    /** Internal function to add a Txo into this wallet (when money is received, for example).
     * See [injectUnspent] for the external version of this function */
    /*
    protected fun setTx(vararg txh: TransactionHistory, trim: Boolean = true)
    {
        synchronized(dataLock)
        {
            walletDb!!.tx.write(*txh)
            for(t in txh)
            {
                val tmp = t.tx.idem
                t.tx.changed()
                if (tmp != t.tx.idem)
                {
                    LogIt.error("TX was changed but not noted!!!")
                }
                txHistoryCache.put(t.tx.idem, t)
            }
        }
        NOT CALLED
        if (trim) trimTxHistoryCache()
    }

     */

    /** Internal function to add a Txo into this wallet (when money is received, for example); writes to cache
     * See [injectUnspent] for the external version of this function */
    protected fun setTx(vararg txh: TransactionHistory, trim: Boolean = true)
    {
        synchronized(dataLock)
        {
            for(t in txh)
            {
                t.dirty = true
                txHistoryCache[t.tx.idem] = t
            }
        }
        if (trim) trimTxHistoryCache()
    }

    /** Internal function to add a Txo into this wallet (when money is received, for example).
     * See [injectUnspent] for the external version of this function */
    protected fun flushTx(txh: Array<TransactionHistory?>, count: Int)
    {
        synchronized(dataLock)
        {
            val slice = txh.sliceArray(IntRange(0, count))
            walletDb!!.tx.write(*slice)
        }
    }



    @cli(Display.User, "wallet history iterator. State may be carried from call to call and then returned")
    override fun forEachTx(doit: (TransactionHistory) -> Boolean)
    {
        dataLock.lock {
            walletDb!!.tx.forEach(doit)
            //for (item in txHistory.values)
            //    doit(item)
        }
    }

    @cli(Display.User, "wallet history iterator, sorted by newest first.")
    override fun forEachTxByDate(doit: (TransactionHistory) -> Boolean)
    {
        dataLock.lock {
            walletDb!!.tx.forEach(doit)  // this for each already sorts by date
            //val lst = txHistory.values.sortedByDescending { it.date }
            //lst.forEach(doit)
        }
    }

    @cli(Display.User, "Wallet history iterator by address.")
    override fun forEachTxByAddress(addr: PayAddress, doit: (TransactionHistory) -> Unit): Unit
    {
        dataLock.lock {
            walletDb!!.tx.forEachWithAddress(addr, doit)
        }
    }


    @cli(Display.User, "wallet history iterator.  If the callback returns a nonnull value, iteration is aborted and that value is returned")
    override fun<T> searchTx(doit: (tx: TransactionHistory) -> T?): T?
    {
        var ret: T? = null
        forEachTx {
            ret = doit(it)
            (ret != null)
        }
        return ret
        /*
        return dataLock.lock {
            var result:T? = null
            for (item in txHistory.values)
            {
                result = doit(item)
                if (result != null) break
            }
            result
        }
        */
    }

    @cli(Display.User, "return number of transactions in this wallet's history")
    override fun numTx(): Int
    {
        return walletDb!!.tx.size().toInt()
    }

    data class WalletStatistics(
      val numUnusedAddrs: Int,
      val numUsedAddrs: Int,
      val numUnspentTxos: Int,
      val totalTxos: Int,
      val numTransactions: Int,
      val firstReceiveHeight: Long,
      val lastReceiveHeight: Long,
      val firstSendHeight: Long,
      val lastSendHeight: Long,
      val txCacheSize: Int,
      val txoCacheSize: Int
    )

    fun statistics(): WalletStatistics
    {
        var unspentTxos = 0
        var firstReceive = Long.MAX_VALUE
        var lastReceive = 0L
        var firstSpend = Long.MAX_VALUE
        var lastSpend = 0L
        var txoCount = 0
        forEachTxo {
            txoCount++
            if (it.spentHeight < 0) unspentTxos += 1
            if (it.commitHeight > 0 && it.commitHeight < firstReceive) firstReceive = it.commitHeight
            if (it.commitHeight > 0 && it.commitHeight > lastReceive) lastReceive = it.commitHeight

            if (it.spentHeight > 0 && it.spentHeight < firstSpend) firstSpend = it.spentHeight
            if (it.spentHeight > 0 && it.spentHeight > lastSpend) lastSpend = it.spentHeight
            false
        }
        // Cross check some stats (for debugging)
        //assert(txoCount == numTxos())
        //assert(unspentTxos == numUtxos())
        //LogIt.info(sourceLoc() + " " + name + ": txoCount: $txoCount numTxos: ${numTxos()} utxoCount: $unspentTxos numUtxos: ${numUtxos()}")
        var txoSize = 0
        var txSize = 0
        dataLock.lock {
            txSize = txHistoryCache.size
            txoSize = txoCache.size
        }
        return WalletStatistics(unusedAddresses.size, receiving.size - unusedAddresses.size, unspentTxos, txoCount, numTx(), firstReceive, lastReceive, firstSpend, lastSpend, txSize, txoSize)
    }

    /** After startup, chainstate should always be set.  It connects this wallet to a particular location in the blockchain */
    @cli(Display.User, "connection between this wallet and its blockchain")
    @Volatile
    var chainstate: GlueWalletBlockchain? = null

    var walletDb: WalletDatabase? = null

    /** Thread that handles keeping wallet synced
     */
    var processingThread: iThread? = null

    /** Get a set of every address in this wallet */
    // w@cli(Display.User, "every address (generated so far) in this wallet")
    val allAddresses: MutableSet<PayAddress>
        get() = receiving.keys

    protected val pendingTxLock = Mutex()
    /** transactions that this wallet has generated that have not yet been confirmed */
    @cli(Display.User, "New transactions that have not yet been confirmed")
    var pendingTx: MutableMap<Hash256, ByteArray> = mutableMapOf()

    override val blockchain: Blockchain
        get() = chainstate!!.chain

    override val syncedHeight: Long
        get() = chainstate?.syncedHeight ?: -1

    /** wake up the processing loop early */
    protected var wakey = Gate()
    @Volatile
    protected var wakeCount = 0L

    /** for inessential wallet operations (like syncing with the blockchain), we allow periodic flush.  If the program fails between flush periods, a few seconds of work must be redone */

    @Volatile protected var lastSavedSyncedHeight: Long = 0
    protected val flushPeriod = Periodically(WALLET_FLUSH_PERIOD.toLong())
    protected fun flushWalletPeriodically(): Boolean
    {
        if (lastSavedSyncedHeight + 50 < syncedHeight || flushPeriod())
        {
            lastSavedSyncedHeight = syncedHeight
            return true
        }
        else return false
    }

    var identityDomain: MutableMap<String, IdentityDomain> = mutableMapOf()
    @Volatile var identityDomainChanged = false

    var identityInfo: MutableMap<PayAddress, IdentityInfo> = mutableMapOf()
    @Volatile var identityInfoChanged = false

    // enable long delays to be changed, primarily for tests where activities happen rapidly
    var LONG_DELAY_INTERVAL = 5000L

    protected val coCtxt: CoroutineContext = newFixedThreadPoolContext(4, "ConnectionManager")
    protected val coScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(coCtxt)

    fun thisLaunch(scope: CoroutineScope? = null, fn: (suspend () -> Unit)?)
    {
        val s = if (scope != null) scope else coScope
        (::launch)(s, fn)
    }

    public override fun toString(): String
    {
        val ptsz = pendingTxLock.lock { pendingTx.size }
        val ret = StringBuilder()
        ret.append("""{ "type":"CommonWallet", "name": "$name", "blockchain" : "$chainSelector", "pause" : "$pause", "timeToResendWalletTx" : $timeToResendWalletTx, """)
        ret.append(""" "unusedAddresses":"[...${unusedAddresses.size}...]", "receiving": "[...${receiving.size}...]", "unspent" : "[...${numUtxos()}...]", "txHistory" : "{...${numTx()}...}", """)
        ret.append(""" "pendingTx":"{...$ptsz...}", "identityDomain":"{...${identityDomain.size}...}", "identityInfo":"{...${identityInfo.size}...}"    }""")
        return ret.toString()
    }

    @cli(Display.Dev, "find and return the secret corresponding to the passed pubkey.")
    override fun pubkeyToSecret(pubkey: ByteArray): Secret?
    {
        for ((_, d) in receiving)
        {
            if ((d.pubkey.contentEquals(pubkey)) && (d.secret != null)) return d.secret
        }
        return null
    }

    /** Forget about all unconfirmed transactions in the wallet.  It the transactions are in the network and are confirmed they will be added to the wallet at that point.
     * This API is used to clear out tx that will never confirm for some reason.
     * This API causes the wallet to forget about the inputs that weren't confirmed.  "Rediscover" can get those back
     */
    @cli(Display.Dev, "Forget about unconfirmed transactions in the wallet")
    fun cleanUnconfirmed(before: Long = Long.MAX_VALUE)
    {
        val forget = mutableListOf<TransactionHistory>()
        forEachTx { hist ->
            if (hist.confirmedHeight <= 0) // this tx is not confirmed yet
            {
                LogIt.info("TX ${hist.tx.idem} is unconfirmed since ${hist.date}")
                if (hist.date < before) forget.add(hist)
            }
            false
        }
        cleanUnconfirmed(forget)
    }

    /** Forget about certain transactions in the wallet.  If the transactions are in the network and are later confirmed they will be added to the wallet then.
     * This API is used to clear out tx that will never confirm for some reason.
     * This API causes the wallet to forget about the inputs that weren't confirmed.  "Rediscover" can get those back
     */
    @cli(Display.Dev, "Forget about unconfirmed transactions in the wallet")
    fun cleanUnconfirmed(lst: MutableList<TransactionHistory>)
    {
        val p = Array<ByteArray>(lst.size) { lst[it].tx.idem.hash }
        walletDb!!.tx.delete(*p)
        for (hist in lst)
        {
            // LogIt.info("Cleaning up ${hist.tx.idem}")
            txHistoryCache.remove(hist.tx.idem)
            pendingTxLock.lock { pendingTx.remove(hist.tx.idem) }
            deleteTxo(*hist.tx.outpoints)
        }
        save()
    }

    /** If you clear the "receiving" addresses map, you need to fill them back up with derived class injected destinations using this function */
    fun fillReceivingWithRetrieveOnly()
    {
        val rods = getRetrieveOnlyDestinations()

        for (r in rods)
        {
            r.address?.let {
                receiving[it] = r
            }
        }
    }

    /** In case someone sends coins to one of your identities, the wallet should be able to spend them
     * It is not recommended to send coins to identities, as that makes the payments identifiable, and
     * also a subsequent payment may mix other incoming payments with identity payments.  So the identity address
     * is never provided as a payment address.  However, someone may use it as such.  */
    fun fillReceivingWithIdentities()
    {
        dataLock.lock {
            // hard code loading the common ID
            if (true)
            {
                val identityDest: PayDestination = destinationFor(Bip44Wallet.COMMON_IDENTITY_SEED)
                identityDest.address?.let {
                    LogIt.info(sourceLoc() + name + ": receiving added " + it.toString())
                    if (!receiving.contains(it))
                    {
                        receiving[it] = identityDest
                    }
                }
            }

            // then get the rest
            for (id in identityInfo)
            {
                id.value.identity?.let {
                    if (!receiving.contains(it))
                    {
                        val identityDest: PayDestination = destinationFor(id.value.identityKey)
                        receiving[it] = identityDest
                    }
                }
            }
        }
    }



    fun launchRediscover(forgetAddresses: Boolean = false, noPrehistory: Boolean = false): Unit
    {
        thisLaunch { rediscover(forgetAddresses, noPrehistory) }
    }

    /** Forget all transaction and blockchain state, and the redo the search for wallet transactions.
     * This is intended for testing and debug
     */
    @cli(Display.Dev, "Forget all transaction and blockchain state, and the redo the search for wallet transactions.")
    override fun rediscover(forgetAddresses: Boolean, noPrehistory: Boolean): Unit
    {
        // We want to pause all blockchain processing while switching the chain tip.  Otherwise we could be in the middle of processing a bunch of tx when we reset
        val origP = pause
        pause = true
        while (!paused) millisleep(100U)

        val cs = chainstate

        if (cs != null)
        {
            if (noPrehistory)
            {
                cs.prehistoryDate = 0
                cs.prehistoryHeight = 0
            }

            synchronized(dataLock)
            {
                pendingTxLock.lock { pendingTx.clear() }
                txHistoryCache.clear()
                txoCache.clear()
                walletDb!!.tx.clear()
                walletDb!!.txo.clear()

                assert(numUtxos() == 0)
                assert(numTx() == 0)

                // unspentByAddress.clear()
                if (forgetAddresses)
                {
                    unusedAddresses.clear()
                    receiving.clear()
                    generateDestinationsInto(unusedAddresses)
                    fillReceivingWithRetrieveOnly()
                    fillReceivingWithIdentities()
                }

                try
                {
                    cs.resetToPrehistory()
                }
                catch (_: Exception)
                {
                    cs.resetToCheckpoint()
                }
            }

            regenerateBloom { }
            save()
            walletChangeCallback?.invoke(this)
            pause = origP
            wakey.wake() { wakeCount++ }  // Wake up wallet processing
        }
    }

    /** Insert a record into the unspentByAddress structure */
    fun insertUnspentByAddress(addr: PayAddress?, outpoint: iTxOutpoint?)
    {
        /*
        if (addr == null) return
        if (outpoint == null) return
        synchronized(dataLock) {
            var lst = unspentByAddress.getOrPut(addr) { mutableListOf() }
            if (!lst.contains(outpoint))
            {
                lst.add(outpoint)
            }
        }

         */
    }

    /** Indicate that an address has been used (maybe it really has, or maybe not, but the point is that its reserved).
     * If we start to run low, we'll also make more */
    fun markAddressUsed(addr: PayAddress)
    {
        // LogIt.info(sourceLoc() + " " + name + ": address used ${addr.toString()}, ${unusedAddresses.size} unused of ${receiving.size} last generated index ${(this as? Bip44Wallet)?.maxAddress}")
        unusedAddresses.remove(addr)
        prepareDestinations(MIN_UNUSED_ADDRESSES, genAddressChunkSize)
    }

    /** Returns whether this address has been provided to the GUI (it may or may not have actually been used by someone) */
    fun isAddressGivenOut(addr: PayAddress): Boolean
    {
        if (receiving.containsKey(addr))
        {
            return !unusedAddresses.contains(addr)
        }
        return false  // its not even ours so not used!
    }

    override fun suggestFee(tx: iTransaction, pad: Int, priority: Int): Long
    {
        // This function just returns the size of the tx for BCH-style 1 sat/byte blockchains, but
        // really it should compute the size and other properties of the tx and pass this information to the underlying blockchain object for fee calculation.

        return tx.size.toLong() + pad
    }

    open fun delete()
    {
        dataLock.lock {
            chainstate?.delete()
            chainstate = null
            deleteWallet(name, chainSelector)
            walletDb = null
        }
    }

    override fun save(force: Boolean)
    {
        dataLock.lock {
            saveWalletTo(walletDb) // TODO , walletHistDb)
        }
    }

    /** Save wallet transaction state to the database */

    open fun saveWalletTo(db: WalletDatabase?)
    {
        if (db == null) return
        dataLock.lock {
            val st = SerializationType.DISK
            chainstate?.let {
                LogIt.info(sourceLoc() + " " + name + ": Save Wallet: Synced Height: ${it.syncedHeight} Hash: ${it.syncedHash.toHex()}  Prehistory Date: ${it.prehistoryDate}, Height: ${it.prehistoryHeight}, txHistory: ${numTx()}")
                if (true)
                {
                    // Save wallet behavior configuration
                    db.set(txoCacheDbKey(name), maxTxoCache)
                    db.set(txHistoryCacheDbKey(name), maxTxCache)
                    db.set(txAddrGenChunkDbKey(name), genAddressChunkSize)

                    // Even though this doesn't use chainstate, don't store it if for some reason we don't have a blockchain yet. Because it makes sense to be consistent
                    // with chainstate
                    val txSer = BCHserialized.uint8(TX_STATE_SERIALIZED_VERSION, st) + synchronized(dataLock) {
                       val v = BCHserialized.map(receiving, { BCHserialized(st).add(it) }, {
                            //BCHserialized(st).addUint8(it.derivedType).add(it)
                           it.serializeTypeAndDerived(st)
                        })
                        v
                    }
                    assert(txSer.format == st)
                    val unusedAddrs = synchronized(dataLock) {
                        BCHserialized.uint8(UNUSED_ADDRESSES_SERIALIZED_VERSION, st)
                            .add(BCHserialized.list(unusedAddresses, SerializationType.DISK))
                    }
                    assert(unusedAddrs.format == st)
                    /*
                    val hist = synchronized(dataLock)
                    {
                        BCHserialized.uint8(TX_HISTORY_SERIALIZED_VERSION, st).add(BCHserialized.map(txHistory, SerializationType.DISK))
                    }
                    assert(hist.format == st)
                     */

                    val numSaves = db.tx.writeDirty(txHistoryCache)
                    // LogIt.info(sourceLoc() + " " + name + ": Save Wallet: DirtyTxHistory $numSaves")

                    db.txo.writeDirty(txoCache)

                    val chainStateData = BCHserialized.uint8(CHAIN_STATE_SERIALIZED_VERSION, st).add(it.BCHserialize(st))
                    assert(chainStateData.format == st)

                    //val gwb = GlueWalletBlockchain(it.chain)
                    //val tmp = gwb.BCHdeserialize(BCHserialized(chainStateData, SerializationType.DISK))  // DEBUG test deserialize right away
                    db.set(chainSelectorDbKey(name), chainToURI[it.chain.chainSelector]!!.toByteArray())
                    db.set(chainStateDbKey(name, chainSelector), chainStateData.toByteArray())
                    db.set(txStateDbKey(name), txSer.toByteArray())
                    db.set(unusedAddressesDbKey(name), unusedAddrs.toByteArray())

                    /*
                    val flathist = hist.toByteArray()
                    val result = db.set(txHistoryDbKey(name), flathist)
                    if (result == false)
                    {
                        LogIt.error(sourceLoc() + name + ": Wallet FAILED TO SAVE history of ${flathist.size} bytes")
                    }
                    else
                    {
                        LogIt.info(sourceLoc() + name + ": Wallet saved history of ${flathist.size} bytes")
                    }
                     */

                    if (identityDomainChanged)
                    {
                        identityDomainChanged = false
                        val identityDomainSerialized: ByteArray =
                            BCHserialized.map(identityDomain, { BCHserialized(SerializationType.DISK).add(it) }, { it.BCHserialize(SerializationType.DISK) }, SerializationType.DISK).toByteArray()
                        db.set(identityDomainsDbKey(name), identityDomainSerialized)

                    }

                    if (identityInfoChanged)
                    {
                        identityInfoChanged = false
                        val identityInfoSerialized: ByteArray =
                            BCHserialized.map(identityInfo, { BCHserialized(SerializationType.DISK).add(it) }, { it.BCHserialize(SerializationType.DISK) }, SerializationType.DISK).toByteArray()
                        db.set(identityInfoDbKey(name), identityInfoSerialized)
                    }
                }
                LogIt.info(sourceLoc() + " " + name + ": Wallet state saved: ${numTxos()} txos,  ${receiving.size} receiving addresses.")
            }
        }
    }

    open fun loadPendingTxFromUnspent(throwIllegal: Boolean = true)
    {
        // need a helper list because I can't nest db transactions, and both the txos and the tx history are db ops
        val unspent = mutableListOf<Spendable> ()
        forEachUtxo { v ->
            if (v.spentHeight == -1L && (v.commitUnconfirmed > 0) && (v.spentUnconfirmed == false))
            {
                unspent.add(v)
            }
            false
        }

        for(v in unspent)
        {
            val idem = v.commitTxIdem
            var tx = getTxWithoutCaching(idem)?.tx
            //var tx = txHistory[idem.data]?.tx
            if (tx != null)
            {
                val txbytes = tx.BCHserialize(SerializationType.NETWORK).toByteArray()
                if (txbytes.size < 100)  // TODO: reference chain consensus parameter rather than 100
                {
                    if (throwIllegal) throw TransactionException("Transaction is too small at ${txbytes.size} bytes")
                }
                else
                {
                    /*
                    LogIt.info(sourceLoc() +": pending TX: ")
                    tx.debugDump()
                    LogIt.info(sourceLoc() +": TX hex: ")
                    val hex = txbytes.toHex()
                    for (c in hex.chunked(900))
                    {
                        LogIt.info(c)
                    }
                     */

                    if (v.commitTxIdem != tx.idem)
                    {
                        LogIt.error(sourceLoc() + " " + name + ": inconsistent data structure: tx and its hash")
                    }

                    pendingTxLock.lock { pendingTx[tx.idem] = txbytes }
                }
            }
            else
            {
                // Should never happen, the database is inconsistent
                LogIt.error(sourceLoc() + " " + name + ": Pending tx idem ${idem.toHex()} unrecoverable, try rediscovering. txhistory size: ${numTx()}")
            }
        }
    }

    fun loadChainState(cs: GlueWalletBlockchain, db: WalletDatabase, key: String)
    {
        val chData = db.get(key)
        val chStream = BCHserialized(chData, SerializationType.DISK)

        val chainstateVer = chStream.debytes(1)[0]
        if (chainstateVer != CHAIN_STATE_SERIALIZED_VERSION) throw DeserializationException("Invalid version in chain state")
        cs.BCHdeserialize(chStream)
        LogIt.info(sourceLoc() + " " + name + ": Load Chain State: Synced Height: ${cs.syncedHeight} Hash: ${cs.syncedHash.toHex()}  Prehistory Date: ${cs.prehistoryDate}, Height: ${cs.prehistoryHeight}")
    }

    /** Load wallet transaction state from the database */

    open fun loadWalletTx(db: WalletDatabase): Boolean
    {
        return dataLock.lock {
            var ret = false
            try
            {
                if (true) // chainstate?.let
                {
                    var corrupt = false

                    // Save wallet behavior configuration

                    maxTxoCache = try { db.getInt(txoCacheDbKey(name)) } catch(e:Exception) { maxTxoCache }
                    maxTxCache = try { db.getInt(txHistoryCacheDbKey(name)) } catch(e:Exception) { maxTxCache }
                    genAddressChunkSize = try { db.getInt(txAddrGenChunkDbKey(name)) } catch(e:Exception) { genAddressChunkSize }

                    try
                    {
                        LogIt.info(sourceLoc() + " " + name + ": Loading wallet")
                        synchronized(dataLock)
                        {
                            val txData = db.get(txStateDbKey(name))
                            val addrData = db.get(unusedAddressesDbKey(name))
                            // val histData = db.get(txHistoryDbKey(name))

                            val addrStream = BCHserialized(addrData, SerializationType.DISK)
                            val txStream = BCHserialized(txData, SerializationType.DISK)
                            // val histStream = BCHserialized(histData, SerializationType.DISK)

                            val unspentVer = txStream.debytes(1)[0]
                            if ((unspentVer != TX_STATE_SERIALIZED_VERSION)&&(unspentVer != V1_TX_STATE_SERIALIZED_VERSION)) throw DeserializationException("Invalid version in transaction state data")

                            // All references to allTxos is removed
                            // allTxos = db.txo.readAll()
                            if (unspentVer == V1_TX_STATE_SERIALIZED_VERSION)
                            {
                                // TODO load old-format wallet
                                val oldAllTxos = txStream.demap({ outpointFor(chainSelector, it) }, { Spendable(chainSelector, it) })
                                LogIt.info(sourceLoc() + " " + name + ": Loaded all ${oldAllTxos.size} outpoints")
                            }
                            receiving = txStream.demap({ PayAddress(it) },
                                {
                                    PayDestination.from(it, chainSelector)
                                    /*
                                    val destType = it.debyte().toInt()
                                    when (destType)
                                    {
                                        Pay2PubKeyTemplateDestination.DEST_TYPE -> Pay2PubKeyTemplateDestination(chainSelector, it)
                                        Pay2PubKeyHashDestination.DEST_TYPE -> Pay2PubKeyHashDestination(chainSelector, it)
                                        // Pay2ScriptHashDestination.DEST_TYPE -> Pay2ScriptHashDestination(chainSelector, it)
                                        // MultisigDestination.DEST_TYPE -> MultisigDestination(chainSelector, it)
                                        else -> throw DeserializationException("destination type not handled")
                                    }
                                     */
                                })
                            LogIt.info(sourceLoc() + " " + name + ": Loaded ${receiving.size} receiving")

                            /*
                            val histVer = histStream.debytes(1)[0]
                            if (histVer != TX_HISTORY_SERIALIZED_VERSION) throw DeserializationException("Invalid version in transaction history")
                            txHistory = histStream.demap({ Hash256(it) }, { TransactionHistory(chainSelector, it) })
                            LogIt.info(sourceLoc() + " " + name + ": Loaded tx history: ${txHistory.size} entries")
                             */

                            //txHistory = db.tx.readAll()

                            unusedAddresses.clear()  // should be empty on load anyway
                            val uaVer = addrStream.debytes(1)[0]
                            if (uaVer != UNUSED_ADDRESSES_SERIALIZED_VERSION) throw DeserializationException("Invalid version in unused addresses")
                            var uaList = addrStream.delist { PayAddress(it) }
                            LogIt.info(sourceLoc() + " " + name + ": Loaded ${uaList.size} unused addresses")
                            unusedAddresses.addAll(uaList)

                            /*
                            unspentByAddress.clear()
                            forEachTxo {
                                // patch up the Spendable transaction info from our tx history so we don't store it in 2 places.
                                //val txh = txHistory[it.commitTxIdem.data]
                                //it.commitTx = txh?.tx
                                insertUnspentByAddress(it.addr, it.outpoint)
                            }
                             */
                            LogIt.info(sourceLoc() + " " + name + ": Wallet state loaded: ${receiving.size} receiving addresses.")
                        }
                    }
                    catch (e: DeserializationException)
                    {
                        LogIt.info(sourceLoc() + name + ": (expected if new wallet) Deserialization exception: " + e.message)
                        handleThreadException(e)
                        corrupt = true
                    }
                    catch (e: UnknownBlockchainException)
                    {
                        LogIt.info(sourceLoc() + name + ": Unknown blockchain: " + e.message)
                        handleThreadException(e)
                        corrupt = true
                    }
                    catch (e: DataMissingException)
                    {
                        LogIt.info(sourceLoc() + name + ": Data missing: " + e.message)
                        // handleThreadException(e)
                        corrupt = true
                    }

                    if (corrupt)
                    {
                        LogIt.info(sourceLoc() + name + ": Stored data is corrupt, rediscovering")
                        launchRediscover()
                    }
                    ret = !corrupt

                    corrupt = false // Identity data failure does not require wallet reset
                    try
                    {
                        val idData = db.get(identityDomainsDbKey(name))
                        val idStream = BCHserialized(idData, SerializationType.DISK)
                        identityDomain = idStream.demap({ val s = it.deString(); LogIt.info(s); s }, { IdentityDomain(it) })
                        identityDomainChanged = false
                    }
                    catch (e: DeserializationException)
                    {
                        LogIt.info("Deserialization exception: " + e.message)
                        handleThreadException(e)
                        corrupt = true
                    }
                    catch (e: UnknownBlockchainException)
                    {
                        LogIt.info("Unknown blockchain: " + e.message)
                        handleThreadException(e)
                        corrupt = true
                    }
                    catch (e: DataMissingException)  // This is normal; identity is linked to only one blockchain
                    {
                        //LogIt.info(sourceLoc() + name + ": Identity data missing: " + e.message)
                        corrupt = true
                    }
                    if (corrupt)
                    {
                        LogIt.info(sourceLoc() + name + ": Identitiy domain data is corrupt or missing, not loading")
                        // not critical info
                    }

                    corrupt = false // Identity data failure does not require wallet reset
                    try
                    {
                        val idData = db.get(identityInfoDbKey(name))
                        val idStream = BCHserialized(idData, SerializationType.DISK)
                        identityInfo = idStream.demap({ val s = PayAddress(it); LogIt.info(s.toString()); s }, { IdentityInfo(it) })
                        identityInfoChanged = false
                    }
                    catch (e: DeserializationException)
                    {
                        logThreadException(e, "Identity Info Deserialization exception", sourceLoc())
                        corrupt = true
                    }
                    catch (e: DataMissingException)
                    {
                        //logThreadException(e, "Identity Info Data Missing", sourceLoc())
                        corrupt = true
                    }
                    if (corrupt)
                    {
                        LogIt.info(sourceLoc() + name + ": Identity info data is corrupt or missing, not loading")
                        // not critical info
                    }
                }

                try
                {
                    loadPendingTxFromUnspent(false)
                }
                catch(e: DeserializationException)
                {
                    LogIt.info(sourceLoc() + name + ": UTXO stored data is corrupt, rediscovering")
                    launchRediscover()
                }
            }
            catch (e: DataMissingException)
            {
                LogIt.warning("missing wallet chain and tx state data.")
                return@lock false
            }
            catch (e: IndexOutOfBoundsException)
            {
                LogIt.warning("corrupt wallet chain and tx state data.")
                return@lock false
            }
            catch (e: PayAddressDecodeException)
            {
                LogIt.warning("corrupt wallet chain and tx state data (PayAddress)")
                return@lock false
            }
            return@lock ret
        }
    }

    /** Install a change handler that will get called whenever this wallet's state changes */
    override fun setOnWalletChange(callback: ((wallet: Wallet) -> Unit)?)
    {
        walletChangeCallback = callback
    }

    override fun send(amountSatoshis: Long, destAddress: String, deductFeeFromAmount: Boolean, sync: Boolean, note: String?, minConfirms: Int): iTransaction
    {
        val dest = PayAddress(destAddress)
        if (dest.blockchain != chainSelector) throw WalletIncompatibleAddress("Cannot send.  The destination address is for a different cryptocurrency")
        return send(amountSatoshis, dest, deductFeeFromAmount, sync, note, minConfirms)
    }

    override fun send(amountSatoshis: Long, destAddress: PayAddress, deductFeeFromAmount: Boolean, sync: Boolean, note: String?, minConfirms: Int): iTransaction
    {
        if (destAddress.blockchain != chainSelector) throw WalletIncompatibleAddress("Cannot send.  The destination address is for a different cryptocurrency")
        return send(amountSatoshis, destAddress.outputScript(), deductFeeFromAmount, sync, note, minConfirms)
    }

    @cli(Display.Simple, "confirmed balance (of ungrouped coins)")
    var crossCheckCount = 0
    override val balanceConfirmed: Long
        get()
        {
            val cs = chainstate
            if (cs == null) throw WalletDisconnectedException()
            return cs.balance
        }

    fun consistencyCheck()
    {
        // Check unconfirmed balance
        var ret = 0.toLong()
        synchronized(dataLock)
        {
            forEachUtxo { u ->
                // Skip if the output was subsequently spent by an unconfirmed tx
                if ((u.spentHeight == -1L) && (u.commitUnconfirmed > 0) && (u.spentUnconfirmed == false)
                    // And if this is not a grouped output (yes this could skip grouped BCH or BCH just in the group output)
                    && (u.groupInfo() == null))
                    ret += u.amount
                false
            }
        }
        if (ret != unconfBalance)
        {
            LogIt.severe(sourceLoc() + " " + name +": Unconfirmed Balance mismatch $unconfBalance != (recalced) $ret")
            unconfBalance = ret
        }

        // Check confirmed balance matches Txos
        val cs = chainstate
        if (cs != null)
        {
            var nowHeight = 0L
            var nowBal = 0L
            var ret = 0L
            var count = 0L
            dataLock.lock {
                nowHeight = cs.syncedHeight
                nowBal = cs.balance
                // LogIt.info(sourceLoc() + " " + name + "   Begin Balance assessment at $nowHeight, expecting $nowBal")
                forEachTxo { u ->
                    count++
                    if (u.groupInfo() == null)
                    {
                        // Committed but not spent
                        if (((u.spentHeight == -1L) || (u.spentHeight > nowHeight)) && (u.commitHeight != -1L) && (u.commitHeight <= nowHeight))
                        {
                            ret += u.amount
                            // LogIt.info(sourceLoc() + " " + name + "   bal: $ret delta ${u.amount} from $u")
                        }
                        else
                        {
                            //  LogIt.info(sourceLoc() + " " + name + "   NOCHANGE delta ${u.amount} from $u")
                        }
                    }
                    false
                }
            }
            if (ret != nowBal)
            {
                LogIt.severe(sourceLoc() + " " + name + ": Balance @$nowHeight mismatch $nowBal != (recalced) $ret  utxos: ${numUtxos()} processed $count")
                cs.balance = ret
            }
            else
            {
                // LogIt.info(sourceLoc() + " " + name +": Balance @$nowHeight $ret utxos: ${numUtxos()} processed $count")
            }
        }
        else throw WalletDisconnectedException()
    }

    @cli(Display.Simple, "unconfirmed balance (of ungrouped coins)")
    override val balanceUnconfirmed: Long
        get() //= unconfBalance
        {
            var ret = 0.toLong()
            synchronized(dataLock)
            {
                forEachTxo { u ->
                    // Skip if the output was subsequently spent by an unconfirmed tx
                    if ((u.spentHeight == -1L) && (u.commitUnconfirmed > 0) && (u.spentUnconfirmed == false)
                      // And if this is not a grouped output (yes this could skip grouped BCH or BCH just in the group output)
                      && (u.groupInfo() == null))
                        ret += u.amount
                    false
                }
            }
            return ret
        }

    @cli(Display.Simple, "total balance (of ungrouped coins)")
    override val balance: Long
        get() //= chainstate!!.balance + unconfBalance  TODO carry the current balance of this wallet
        {
            var ret = 0.toLong()
            forEachTxo { u ->
                    if (u.groupInfo() == null)
                    {
                        if ((u.spentHeight == -1L) && (u.spentUnconfirmed == false))  // unspent
                            ret += u.amount
                    }
                    false
                }

            return ret
        }

    @cli(Display.User, "unconfirmed transaction list")
    fun getUnconfirmedTx(): MutableList<Spendable>
    {
        var ret = mutableListOf<Spendable>()
        synchronized(dataLock)
        {
            forEachTxo { u ->
                // Skip if the output was subsequently spent by an unconfirmed tx
                if ((u.spentHeight == -1L) && (u.commitUnconfirmed > 0) && (u.spentUnconfirmed == false))
                    ret.add(u)
                false
            }
        }
        return ret
    }

    /** Return identity domain data if this domain has previously been used */
    override fun lookupIdentityDomain(name: String): IdentityDomain?
    {
        return identityDomain[name]
    }

    /** Add or update identity domain data */
    override fun upsertIdentityDomain(id: IdentityDomain)
    {
        identityDomainChanged = true
        identityDomain[id.domain] = id
    }

    /** Remove identity domain data */
    override fun removeIdentityDomain(name: String)
    {
        identityDomainChanged = true
        identityDomain.remove(name)
    }

    override fun allIdentityDomains(): Collection<IdentityDomain>
    {
        return identityDomain.values
    }

    /** Return identity domain data if this domain has previously been used */
    override fun lookupIdentityInfo(id: PayAddress): IdentityInfo?
    {
        return identityInfo[id]
    }

    /** Add or update identity domain data */
    override fun upsertIdentityInfo(id: IdentityInfo)
    {
        val tmp = id.identity
        if (tmp != null)
        {
            identityInfoChanged = true
            identityInfo[tmp] = id
        }
    }

    /** Remove identity domain data */
    override fun removeIdentityInfo(id: PayAddress)
    {
        identityInfoChanged = true
        identityInfo.remove(id)
    }

    /** track this transaction and periodically resubmit until it has been committed.
     * @param tx: the wallet transaction to commit
     * @param txbytes: OPTIONAL: the network-serialized wallet transaction (for efficiency if the caller has already serialized it)
     * @throws */
    fun commitWalletTransaction(tx: iTransaction, txbytes: ByteArray? = null, note: String? = null)
    {
        val txb = txbytes ?: tx.BCHserialize(SerializationType.NETWORK).toByteArray()
        pendingTxLock.lock { pendingTx[tx.idem] = txb }
        val req = chainstate?.chain?.req

        if (tx.size < 100)
        {
            throw IllegalStateException("txbytes is too small")
        }

        try
        {
            synchronized(dataLock)
            {
                // notify this wallet first so the note is stored if there is one
                unconfBalance += interestingUnconfirmedTx(mutableListOf(tx), note)
            }
            // other wallets in this device may not get notifications for this tx because I sent it by pushing this into the req mgr, it calls back into ALL the connected wallets
            if (req != null)  // send it out
                req.onUnconfirmedTx(mutableListOf(tx))
        }
        catch(e: Exception)
        {
            handleThreadException(e)
        }
        thisLaunch { save() }  // do this out-of-band so UI response is quicker

        chainstate?.chain?.net?.broadcastTransaction(txb)
    }

    override fun prepareSend(outputs: MutableList<iTxOutput>, minConfirms: Int, deductFeeFromAmount: Boolean): iTransaction
    {
        var total = 0L
        for (out in outputs)
        {
            total += out.amount
        }

        val (signedTx, _) = txConstructor(total, deductFeeFromAmount, minConfirms)
        { tx, _ ->
            assert(deductFeeFromAmount == false)  // TODO: actually deduct
            for (out in outputs)
            {
                tx.add(out)
            }
            deductFeeFromAmount
        }

        return signedTx
    }

    override fun abortTransaction(tx: iTransaction)
    {
        LogIt.info(sourceLoc() + " " + name + ": Transaction ${tx.idem.toHex()} aborted")
        synchronized(dataLock) // Give all the unspent back to the general use pool since we've aborted this inprogress spend
        {
            val sps = tx.inputs.map { it.spendable }
            for (sp in sps)
            {
                if (sp.reserved == 0L)
                    LogIt.warning(sourceLoc() + " " + name + ": Reserved UTXO ${sp.outpoint?.toHex()} was prematurely unreserved (maybe double transaction abort?) ")
                sp.reserved = 0L
            }
            setTxo(sps)
        }
    }


    /** Send funds to multiple destinations.  This function will select input coins from the wallet to fill the passed quantity
     * @param addrAmt Provide a list of how many coins to send to which addresses denominated in the fundamental (smallest possible) unit of this currency
     * @param sync If true, do not return until this transaction has been sent to some nodes
     * @param note Some information the sender may privately associate with this send
     * @return a signed transaction
     * @throws [WalletException] error specific subclasses of [WalletException] are thrown if the transaction cannot be constructed
     */
    override fun send(addrAmt: List<Pair<PayAddress, Long>>, sync: Boolean, note: String?, minConfirms: Int): iTransaction
    {
        var amountSatoshis = 0L
        val minConf = if (minConfirms == -1) 1 else minConfirms
        for (s in addrAmt) amountSatoshis += s.second
        val (signedTx, serializedTx) = try  // Try at least 1 confirmation first
        {
            txConstructor(amountSatoshis, false, minConf) { tx, _ ->
                for (s in addrAmt)
                {
                    val output = txOutputFor(chainSelector)
                    output.amount = s.second
                    output.script = s.first.outputScript()
                    tx.add(output)
                }
                false
            }
        }
        catch (e: WalletNotEnoughBalanceException)  // Try unconfirmed
        {
            if (minConfirms == -1)
            {
                txConstructor(amountSatoshis, false, 0) { tx, _ ->
                    for (s in addrAmt)
                    {
                        val output = txOutputFor(chainSelector)
                        output.amount = s.second
                        output.script = s.first.outputScript()
                        tx.add(output)
                    }
                    false
                }
            }
            else throw e
        }

        LogIt.info(sourceLoc() + " " + name + ": Sending TX " + signedTx.idem.toHex())
        LogIt.info(sourceLoc() + " " + name + ": TX hex " + serializedTx.toHex())
        if (sync)
            commitWalletTransaction(signedTx, serializedTx, note)
        else thisLaunch { commitWalletTransaction(signedTx, serializedTx, note) }
        return signedTx
    }


    //* Send funds to this destination
    override fun send(amountSatoshis: Long, destScript: SatoshiScript, deductFeeFromAmount: Boolean, sync: Boolean, note: String?, minConfirms: Int): iTransaction
    {
        var retry = 10
        var signedTx:iTransaction? = null
        var serializedTx: ByteArray? = null
        var minConf = if (minConfirms == -1) 1 else minConfirms
        while(true)
        {
            try  // Try at least 1 confirmation first
            {
                val tmp = txConstructor(amountSatoshis, deductFeeFromAmount, minConf) { tx, fee ->
                    // Add the output that we are sending to
                    val output = txOutputFor(chainSelector)
                    output.amount = if (deductFeeFromAmount) amountSatoshis - fee else amountSatoshis
                    output.script = destScript
                    tx.add(output)
                    deductFeeFromAmount
                }
                signedTx = tmp.first
                serializedTx = tmp.second
                break
            }
            catch (e: WalletNotEnoughBalanceException)  // Try unconfirmed
            {
                if (minConfirms == -1) minConf = 0
                else throw e
            }
            catch (e: WalletNeedsConsolidation)  // Consolidate first
            {
                if (minConfirms == -1 || minConfirms == 0)
                {
                    minConf = 0 // so we can use the consolidated input
                    val (cTx, csTx) = consolidate()
                    commitWalletTransaction(cTx, csTx)  // If I don't commit, I won't be able to use it in my tx
                    retry--
                    if (retry == 0) throw e
                }
                else throw e
            }
        }

        // LogIt.info(sourceLoc() + " " + name + ": Sending TX " + signedTx.idem.toHex())
        // LogIt.info(sourceLoc() + " " + name + ": TX hex " + serializedTx.toHex())
        if ((signedTx == null)||(serializedTx==null)) throw WalletException("unknown send issue")
        if (sync)
            commitWalletTransaction(signedTx, serializedTx, note)
        else thisLaunch { commitWalletTransaction(signedTx, serializedTx, note) }
        return signedTx
    }

    //* Send funds to this destination
    override fun send(tx: iTransaction, sync: Boolean, note: String?)
    {
        val serializedTx = tx.BCHserialize(SerializationType.NETWORK).toByteArray()
        LogIt.info(sourceLoc() + " " + name + ": Sending TX " + tx.idem.toHex())
        LogIt.info(sourceLoc() + " " + name + ": TX hex " + serializedTx.toHex())
        if (sync)
            commitWalletTransaction(tx, serializedTx, note)
        else thisLaunch { commitWalletTransaction(tx, serializedTx, note) }
    }

    //* publish any unconfirmed transactions to the network
    @cli(Display.User, "sends all tx marked unconfirmed to the network.")
    fun resendUnconfirmedTx()
    {
        val v = chainstate?.chain?.net
        if (v != null)
        {
            val txHist = unconfirmedTx()

            for (i in txHist)
            {
                if (i.confirmedHeight < 1)
                {
                    if (!i.tx.isCoinbase())  // If a coinbase somehow got into our history, if we ever relay it we will be banned.  So don't.
                    {
                        v.broadcastTransaction(i.tx.BCHserialize(SerializationType.NETWORK).toByteArray())
                        LogIt.info("Resending ${i.tx.idem} confirmedHeight is: ${i.confirmedHeight}")
                        LogIt.info("Hex: ${i.tx.toHex()}")
                    }
                }
            }
        }
    }

    /** This function re-checks all tx marked unconfirmed in this wallet to see if they are actually unconfirmed.
     * It uses an electrumx server to do so, so may be unavailable if electrumx is not supported or no servers are available. */
    @cli(Display.User, "re-checks all tx marked unconfirmed in this wallet to see if they are actually unconfirmed.")
    fun reassessUnconfirmedTx()
    {
        val reqMgr = chainstate?.chain?.req ?: throw ElectrumNoNodesException()
        val unTxes = unconfirmedTx()
        val forgetTxes: MutableList<TransactionHistory> = mutableListOf()
        for (unTx in unTxes)
        {
            val retmsg = reqMgr.getTxDetails(unTx.tx.idem)
            val result = (retmsg as JsonObject)["result"] as JsonObject?
            if (result != null)
            {
                val confBlock: String? = result.get("blockhash")?.toString()?.replace(""""""", "")
                val confHeight = result.get("height")?.toString()?.toLong()
                val confTime = result.get("time")?.toString()?.toLong()
                // TODO don't trust the server; get a merkle proof

                if ((confBlock != null) && (confHeight != null) && (confTime != null))
                {
                    interestingConfirmedTx(listOf(unTx.tx), Hash256(confBlock), confHeight, confTime)
                }
                continue
            }
            val error = retmsg["error"] as JsonObject?
            if (error != null)
            {
                LogIt.info("Tx ${unTx.tx.idem} reassess error: " + error["code"])
                if (error["code"].toString() == "-32603")  // This transaction does not exist
                {
                    forgetTxes.add(unTx)
                }
            }
        }

        cleanUnconfirmed(forgetTxes)
    }

    //* get unconfirmed tx sorted by date
    fun unconfirmedTx(): MutableList<TransactionHistory>
    {
        val ret = mutableListOf<TransactionHistory>()
        synchronized(dataLock) {
            // Note all unconfirmed tx are always kept in the cache
            for (i in txHistoryCache.values)
            {
                if (i.confirmedHeight < 1) ret.add(i)
            }
        }
        ret.sortBy { it.date }
        return ret
    }

    // This function analyses a script for template op codes, and fills in wallet specific data where requested.
    fun bindScriptToWallet(s: SatoshiScript, overrideDest: PayDestination? = null): SatoshiScript
    {
        return s.replace {
            if (it.contentEquals(OP.TMPL_PUBKEYHASH.v))
            {
                val d = runBlocking { overrideDest ?: newDestination() }
                SatoshiScript(s.chainSelector) + OP.push(d.pkh()!!).v
            }
            else if (it.contentEquals(OP.TMPL_SCRIPT.v))
            {
                val d = runBlocking { overrideDest ?: newDestination() }
                d.grouplessConstraintScript()
            }
            else if (it.contentEquals(OP.TMPL_PUBKEY.v))
            {
                val d = runBlocking { overrideDest ?: newDestination() }
                SatoshiScript(s.chainSelector) + OP.push(d.pubkey!!).v
            }
            else
            {
                throw NotImplementedError()
            }
        }

    }

    /**
     * Checks the transaction's final fee.  Throws an exception and aborts the transaction if the fee is too big.
     */
    fun txSanityCheck(tx: iTransaction)
    {
        tx.changed()
        val sz = tx.size

        var outputAmount: Long = 0
        for (output in tx.outputs)
        {
            outputAmount += output.amount
        }

        // We don't need to check our input amounts against the blockchain for accuracy because if our values are incorrect then the signature hash that was generated
        // as part of signedTx will be incorrect.
        var inputAmount: Long = 0
        for (input in tx.inputs)
        {
            inputAmount += input.spendable.amount
        }

        val finalFee = inputAmount - outputAmount

        if (finalFee > MaxFee)
        {
            abortTransaction(tx)
            LogIt.error("Wallet fee exception inputs: ${tx.inputs.size} outputs: ${tx.outputs.size} fee: $finalFee")
            throw WalletFeeException(appI18n(RfeeExceedsFlatMax))
        }
        else if (sz * MaxFeePerByte < finalFee)
        {
            abortTransaction(tx)
            throw WalletFeeException(appI18n(RexcessiveFee))
        }
    }

    /**
    Find inputs needed to supply funds for this transaction.
    @param[minConfirms] Only fund with coins that have at least this many confirmations
    @param[inputAmount] If non-null, assume existing inputs supply this number of satoshis (do not look up these inputs)
    @param[flags] See [TxCompletionFlags]
     If change outputs are required, add them.
     If mint baton passing outputs are possible then add them if equalizeAuthorities=true
     If useAuthorities = true, pull in authorities if needed (and available) to handle (mint/melt) operations
     If fund = true, add native crypto inputs to pay for the transaction
    @param[adjustableOutput] Pass an output index if the fee should be deducted from this output.  Otherwise the fee must be taken from extra input
    @throws[WalletException],[WalletNotEnoughBalanceException]
    */
    override fun txCompleter(tx: iTransaction, minConfirms: Int, flags: Int, inputAmount: Long?, adjustableOutput: Int?, destinationAddress:PayAddress?)
    {
        val iSpent = mutableListOf<Spendable>()
        val DUST: Long = dust(chainSelector)  // Below this many satoshis not worth making an output -- just give to miner
        val MAX_FEE_OVERPAY: Long = DUST * 3
        val AVG_OUTPUT_SIZE: Long = 34

        // Track what's coming in and going out for each group
        data class GroupIO(
          var groupId: GroupId, var tokenI: Long, var tokenO: Long, var authorityFlagsI: ULong,
          var authorityFlagsO: ULong
        )

        val groupData = mutableMapOf<GroupId, GroupIO>()

        val destinationOverride: PayDestination? = if (destinationAddress != null)
        {
            val tmp = receiving[destinationAddress]
            if (tmp == null) throw WalletIncompatibleAddress("Not an address from this wallet")
            tmp
        }
        else null

        // Fill any parameterized scripts with addresses from this wallet and
        // Fill the groupData structure with all the output groups and token quantities.
        for (out in tx.outputs)
        {
            val constraint = if ((flags and TxCompletionFlags.BIND_OUTPUT_PARAMETERS) > 0)
            {
                val tmp = bindScriptToWallet(out.script, destinationOverride)
                LogIt.info(sourceLoc() + ": Bind script parameterization: ${out.script.toHex()} -> ${tmp.toHex()}")
                out.script = tmp
                tmp
            }
            else out.script

            val cgdata = constraint.groupInfo(out.amount) ?: continue  // If ungrouped, nothing more to do
            val d = groupData[cgdata.groupId]
            if (d == null) groupData[cgdata.groupId] = GroupIO(cgdata.groupId, 0, cgdata.tokenAmt, 0.toULong(), cgdata.authorityFlags)
            else
            {
                d.tokenO += cgdata.tokenAmt
                d.authorityFlagsO = d.authorityFlagsI or cgdata.authorityFlags
            }

        }

        // Figure out what caller has supplied.
        for (inp in tx.inputs)
        {
            val prevout = inp.spendable
            val cgdata = prevout.priorOutScript.groupInfo(prevout.amount) ?: continue  // continue if ungrouped
            val d = groupData[cgdata.groupId] ?: continue // Well, there's an existing input with no corresponding output... either tx is melting or won't validate (not our problem)

            d.tokenI += cgdata.tokenAmt
            d.authorityFlagsI = d.authorityFlagsI or cgdata.authorityFlags  // if the caller supplied an authority bit
        }

        oneTxConstructor.lock()
        {
            if ((flags and TxCompletionFlags.FUND_GROUPS) > 0)
            {
                // supply inputs and outputs for the difference between what's coming in and going out
                for ((gid, g) in groupData)
                {
                    if (g.tokenI < g.tokenO) // Supply token input or mint authority
                    {
                        dataLock.synchronized {  // we need to make sure that the utxos we've grabbed STAY unreserved
                            val inputs =
                                filterInputs(g.tokenO, minConfirms, groupedFilter(g.groupId, true, GroupAuthorityFlags.NO_AUTHORITY))
                            val gAmt = inputs.fold(0L,
                                { acc, utxo -> acc + utxo.groupInfo()!!.tokenAmt })  // how much do I have available
                            if (gAmt + g.tokenI >= g.tokenO)  // Ok don't need to mint
                            {
                                // TODO quantity-sensitive selection
                                while (g.tokenI < g.tokenO)
                                {
                                    val utxo = inputs.removeAt(0)
                                    assert(utxo.reserved == 0L, "UTXO reservation status should not have changed" )
                                    utxo.reserved = epochSeconds()
                                    val gi = utxo.groupInfo()!!  // filter did not work if null
                                    tx.add(txInputFor(utxo))
                                    g.tokenI += gi.tokenAmt
                                }

                            }
                            else
                            {
                                if ((flags and TxCompletionFlags.USE_GROUP_AUTHORITIES) == 0)
                                {
                                    LogIt.info(sourceLoc() + ": Token ${gid.toString()} (${gid.toHex()}) input qty: ${g.tokenI} output qty: ${g.tokenO} and wallet has not enough balance: ${gAmt}")
                                    throw WalletNotEnoughTokenBalanceException(appI18n(RsendMoreTokensThanBalance))
                                }
                                var mask = GroupAuthorityFlags.MINT
                                if ((flags and TxCompletionFlags.NO_BATON_AUTHORITIES) != 0)  // If baton authorities are not allowed then include BATON in the mask
                                {
                                    mask = mask or GroupAuthorityFlags.BATON
                                }
                                // Looking for at least 1 mint auth
                                var auths = filterInputs(1, minConfirms, groupedFilter(g.groupId, false, GroupAuthorityFlags.MINT, mask))
                                if (auths.isEmpty())
                                {
                                    // If we have no mint auths, look to see if this is a subgroup and whether we have mint through the parent
                                    if (g.groupId.isSubgroup())
                                    {
                                        mask = mask or GroupAuthorityFlags.SUBGROUP
                                        auths = filterInputs(1, minConfirms,
                                            groupedFilter(g.groupId.parentGroup(),
                                                false,
                                                GroupAuthorityFlags.SUBGROUP or GroupAuthorityFlags.MINT, mask))
                                    }
                                    if (auths.isEmpty())
                                    {
                                        LogIt.info(sourceLoc() + ": Token ${gid.toString()} (${gid.toHex()}) input qty: ${g.tokenI} output qty: ${g.tokenO} and wallet has not enough balance: ${gAmt} and no auths")
                                        throw WalletNotEnoughTokenBalanceException(appI18n(RsendMoreTokensThanBalance))
                                    }
                                }

                                val auth = auths[0]
                                assert(auth.reserved == 0L, "UTXO reservation status should not have changed" )
                                auth.reserved = epochSeconds()
                                val authGroupInfo = auth.groupInfo()
                                tx.add(txInputFor(auth))
                                g.authorityFlagsI = g.authorityFlagsI or authGroupInfo!!.authorityFlags  // We just added this into the inputs, so mark that we now have these new authorities
                                iSpent.add(auth)
                                // If this authority can be passed to a child, make an output so we don't lose this ability
                                if ((authGroupInfo.authorityFlags and GroupAuthorityFlags.BATON) > 0.toULong())
                                {
                                    // TODO do not reuse the authority address, note that we need to handle subgroups here when fixing
                                    tx.add(txOutputFor(chainSelector, dust(chainSelector), auth.priorOutScript))
                                }
                                else
                                {
                                    LogIt.info(sourceLoc() + ": consuming authority without creating child authority because baton not set")
                                }
                            }
                        }
                    }
                    // The above code could include too many tokens, meaning we now need to make change,
                    // or the caller supplied tx could have done the same

                    if (g.tokenI > g.tokenO) // Create token change output
                    {
                        val co = createGroupedChangeOutput(gid, g.tokenI - g.tokenO)
                        tx.add(co)
                        g.tokenO = g.tokenI  // In case we use it later
                    }

                    // are any output authorities set that are not provided by the inputs?
                    // If so, we need to supply an authority
                    val missingAuthorities: ULong = g.authorityFlagsI.inv() and g.authorityFlagsO
                    if (missingAuthorities != 0.toULong())
                    {
                        if ((flags and TxCompletionFlags.USE_GROUP_AUTHORITIES) == 0)
                        {
                            throw WalletAuthorityException("API call must use authorities but not authorized to do so by caller")
                        }
                        var mask = missingAuthorities
                        if ((flags and TxCompletionFlags.NO_BATON_AUTHORITIES) != 0)  // If baton authorities are not allowed then include BATON in the mask
                        {
                            mask = mask or GroupAuthorityFlags.BATON
                        }
                        var auths = filterInputs(1, minConfirms, groupedFilter(g.groupId, false, missingAuthorities, mask))
                        if (auths.isEmpty())
                        {
                            // If we have no mint auths, look to see if this is a subgroup and whether we have the authorities through the parent
                            if (g.groupId.isSubgroup())
                            {
                                mask = mask or GroupAuthorityFlags.SUBGROUP
                                auths = filterInputs(1, minConfirms,
                                  groupedFilter(g.groupId.parentGroup(),
                                    false,
                                    GroupAuthorityFlags.SUBGROUP or missingAuthorities, mask))
                            }
                            if (auths.isEmpty())
                            {
                                throw WalletAuthorityException(appI18n(RneedNonexistentAuthority))
                            }
                        }

                        val auth = auths[0]
                        auth.reserved = epochSeconds()
                        val authGroupInfo = auth.groupInfo()
                        tx.add(txInputFor(auth))
                        iSpent.add(auth)
                        // If this authority can be passed to a child, make an output so we don't lose this ability
                        if ((authGroupInfo!!.authorityFlags and GroupAuthorityFlags.BATON) > 0.toULong())
                        {
                            // TODO do not reuse the authority address, note that we need to handle subgroups here when fixing
                            tx.add(txOutputFor(chainSelector, dust(chainSelector), auth.priorOutScript))
                        }
                        else
                        {
                            LogIt.info(sourceLoc() + ": consuming authority without creating child authority because baton not set")
                        }
                    }

                }
            }

            tx.changed()  // recalc size since the above probably changed the tx
            // Now that the groups are equalized, equalize the satoshis
            if ((flags and TxCompletionFlags.FUND_NATIVE) > 0)
            {
                var outAmt = tx.outputs.fold(0L, { a, b -> a + b.amount })
                var inAmt = inputAmount ?: tx.inputs.fold(0L, { a, b -> a + b.spendable.amount })

                // By setting outFee to 0 here if we intend to deduct the fee from an output, we will just fund the outputs without the fee
                var outFee: Long = if ((flags and TxCompletionFlags.DEDUCT_FEE_FROM_OUTPUT) > 0) 0L
                else feeForSize((tx.size + (APPROX_P2PKH_SIG_SCRIPT_LEN * tx.inputs.size)).toLong())

                // We are draining this account, grab all ungrouped UTXOs
                if ((flags and TxCompletionFlags.SPEND_ALL_NATIVE) > 0)
                {
                    val inputs = findInputs(Long.MAX_VALUE, minConfirms)  // findInputs automatically sets the utxos it selects to reserved
                    if (inputs != null)
                    {
                        while (!inputs.isEmpty())
                        {
                            val utxo = inputs.removeAt(0)
                            tx.add(txInputFor(utxo))
                            iSpent.add(utxo)
                            inAmt += utxo.amount
                            outFee += feeForSize((APPROX_P2PKH_SIG_SCRIPT_LEN + TX_SCRIPTLESS_INPUT_SIZE).toLong())
                        }
                    }
                }
                // Spend the right amount of native satoshis
                else if (inAmt < outAmt + outFee)  // Need to pull in more sats
                {
                    while (inAmt < outAmt + outFee)
                    {
                        val inputs = findInputs(outAmt + outFee - inAmt, minConfirms)  // findInputs automatically sets the utxos it selects to reserved

                        if (inputs == null)
                        {
                            LogIt.info(sourceLoc() + ": Wallet cannot find enough sats with ${minConfirms} confirms. Need ${outAmt} + ${outFee} - $inAmt = ${outAmt + outFee - inAmt}")
                            throw WalletNotEnoughBalanceException(appI18n(RsendMoreThanBalance))
                        }

                        while ((inAmt < outAmt + outFee) && !inputs.isEmpty())
                        {
                            val utxo = inputs.removeAt(0)
                            tx.add(txInputFor(utxo))
                            iSpent.add(utxo)
                            inAmt += utxo.amount

                            outFee += feeForSize((APPROX_P2PKH_SIG_SCRIPT_LEN + TX_SCRIPTLESS_INPUT_SIZE).toLong())
                        }

                        // looping again is unlikely; it means that the chosen input was close enough for outFee to exceed it
                        // In this case, just grab another utxo (which might be not-tx-size-efficient in the short run)
                    }
                }

                // Since the fee is increasing while we add inputs, its possible we'll run out of utxos, so double check that we did it
                if (inAmt < outAmt + outFee)
                {
                    LogIt.info(sourceLoc() + ": Even after looping Adding inputs caused too large fee. Provided ${inAmt}.  Needed ${outFee} + ${outAmt} = ${outAmt + outFee}")
                    throw WalletNotEnoughBalanceException(appI18n(RsendMoreThanBalance))
                }

                // Now add some change if needed.  We only add a change output if we aren't adjusting one of the outputs
                if (adjustableOutput == null)
                {
                    if (inAmt > outAmt + outFee + MAX_FEE_OVERPAY)  // TODO dump extra dust into group outputs for later fee paying
                    {
                        if ((flags and TxCompletionFlags.DEDUCT_FEE_FROM_OUTPUT) == 0)
                            outFee += feeForSize(AVG_OUTPUT_SIZE)  // Add fee for the change output
                        val co = createChangeOutput(inAmt - (outAmt + outFee))
                        tx.add(co)
                    }
                }
                else  // if we are adjusting one of the outputs, dump all the extra into there.
                {
                    tx.outputs[adjustableOutput].amount += inAmt - (outAmt + outFee)
                }

            }
        }


        val sighash = if ((flags and TxCompletionFlags.PARTIAL) > 0)
        // BCH: sign with sighash single and anyone can pay. NOTE: doesn't really do what we want -- we want a few outputs signed
        // actually sighash anyonecanpay would work, but payer would have to create 2 tx, the first makes an output with exact change for this tx
        // 0xc3  // actually sighash anyonecanpay would work, but payer would have to create 2 tx, the first makes an output with exact change for this tx
            tx.appendableSighash()
        else
            byteArrayOf() // ALL/ALL

        // Sign the transaction
        if ((flags and TxCompletionFlags.SIGN) > 0)
            signTransaction(tx, sighash)

        // By signing above, we can calculate the exact fee needed, but then we need to re-sign the transaction (which is poor performance)
        // Note that the most common use case for deductFeeFromOutput is to send the entire wallet's contents somewhere else, so performance probably
        // won't matter.
        //
        // Also note that if you don't sign the TX but select deductFeeFromOutput, that doesn't make sense because we don't know how much to deduct.
        if ((flags and TxCompletionFlags.DEDUCT_FEE_FROM_OUTPUT) > 0)
        {
            if (adjustableOutput == null) throw WalletImplementationException("If deducting a fee from an output, you MUST provide the output index")
            tx.changed()  // recalc size since the above probably changed the tx
            val outFee = feeForSize(tx.size)
            tx.outputs[adjustableOutput].amount -= outFee
            if (tx.outputs[adjustableOutput].amount < 0) throw WalletFeeException("Fee exceeds adjustable output's amount")
            if ((flags and TxCompletionFlags.SIGN) > 0)
                signTransaction(tx, sighash)
        }

        txSanityCheck(tx)
    }


    fun consolidate(): Pair<iTransaction, ByteArray>
    {
        val AVG_INPUT_SIZE: Long = 147
        val AVG_OUTPUT_SIZE: Long = 34
        val TX_OVERHEAD_SIZE: Long = 16

        var newTx = txFor(chainSelector)
        var inAmt: Long = 0

        // Accept every non-grouped input, but pretend they are worth 1 so the quantity actually counts inputs not amounts
        var inputs: MutableList<Spendable> = filterInputs(MAX_TX_INPUTS.toLong(), 0) {
            val groupInfo: GroupInfo? = it.groupInfo()
            if (groupInfo != null) 0
            else 1
        }

        // Fill the TX with the chosen inputs
        for (i in inputs)
        {
            // LogIt.info(name + ": TX Input: " + (i.outpoint?.toHex() ?: "null") + "   Amount: " + i.amount)
            var bchinput = txInputFor(i) // a spendable carries with it the ability to sign a tx.  This is how the secret (if one is needed) is communicated to the tx under construction
            bchinput.script = SatoshiScript(chainSelector)
            newTx.add(bchinput)
            inAmt += i.amount
        }

        // How much do we want to pay for this tx?
        var fee = feeForSize(newTx.inputs.size * AVG_INPUT_SIZE + AVG_OUTPUT_SIZE * 2 + TX_OVERHEAD_SIZE)

        var signedTx: iTransaction?
        var serializedTx: ByteArray?
        newTx.outputs.clear()
        val co = createChangeOutput(inAmt - fee)
        newTx.add(co)

        while (true)  // Sign the TX and see if the fee is acceptable to the network for the real size of the tx.  If not try again with the new fee
        {
            signedTx = signTransaction(newTx)
            // SANITY CHECK for extremely large fee and abort if so.
            txSanityCheck(signedTx)
            serializedTx = signedTx.BCHserialize(SerializationType.NETWORK).toByteArray()
            val serializedTxSize = serializedTx.size.toLong()
            val minFee = minFeeForSize(serializedTxSize)

            if (fee < minFee)   // If the fee we chose is lower than the minimum fee for a tx of this size, try again with minfee as the fee.
            {
                fee = minFee
                newTx.outputs[0].amount = inAmt - minFee
            }
            else
            {
                LogIt.info(sourceLoc() + " " + name +": CONSOLIDATE")
                return Pair(signedTx, serializedTx)
            }
        }
    }


    protected fun txConstructor(
      amountSatoshis: Long,
      deductFeeFromAmount: Boolean,
      minConfirms: Int = 1,
      outputFiller: ((iTransaction, Long) -> Boolean)
    ): Pair<iTransaction, ByteArray>
    {
        val AVG_INPUT_SIZE: Long = 147
        val AVG_OUTPUT_SIZE: Long = 34
        val TX_OVERHEAD_SIZE: Long = 16
        val DUST: Long = dust(chainSelector)

        if (amountSatoshis < DUST)
        {
            throw WalletDustException("Sending dust")
        }

        val ret: Pair<iTransaction, ByteArray> =  oneTxConstructor.lock {
            var NinputsGuestimate = 1
            var retin:Pair<iTransaction, ByteArray>? = null

            outerloop@ while (true)
            {
                // Find inputs
                val total =
                  if (deductFeeFromAmount) amountSatoshis else amountSatoshis + feeForSize(AVG_INPUT_SIZE * NinputsGuestimate + AVG_OUTPUT_SIZE * 2 + TX_OVERHEAD_SIZE)
                val inputs = findInputs(total, minConfirms)

                if (inputs != null)
                {
                    var newTx = txFor(chainSelector)
                    var inAmt: Long = 0

                    // Fill the TX with the chosen inputs
                    for (i in inputs)
                    {
                        // LogIt.info(name + ": TX Input: " + (i.outpoint?.toHex() ?: "null") + "   Amount: " + i.amount)
                        var bchinput = txInputFor(i) // a spendable carries with it the ability to sign a tx.  This is how the secret (if one is needed) is communicated to the tx under construction
                        bchinput.script = SatoshiScript(chainSelector)
                        newTx.add(bchinput)
                        inAmt += i.amount
                    }

                    if (inputs.size >= MAX_TX_INPUTS)
                    {
                        abortTransaction(newTx)
                        throw WalletNeedsConsolidation("consolidation needed to produce tx of $amountSatoshis")
                    }

                    // How much do we want to pay for this tx?
                    var fee = feeForSize(newTx.inputs.size * AVG_INPUT_SIZE + AVG_OUTPUT_SIZE * 2 + TX_OVERHEAD_SIZE)

                    var signedTx: iTransaction?
                    var serializedTx: ByteArray?
                    while (true)  // Sign the TX and see if the fee is acceptable to the network for the real size of the tx.  If not try again with the new fee
                    {
                        newTx.outputs.clear()
                        val feeDeducted = outputFiller(newTx, fee)

                        // check the outputFiller to make sure it used what it claimed
                        var actualAmount = 0L
                        for (out in newTx.outputs)
                        {
                            if (out.amount < 0)
                            {
                                abortTransaction(newTx)
                                throw WalletFeeException(appI18n(RdeductedFeeLargerThanSendAmount))
                            }
                            actualAmount += out.amount
                        }
                        var oops = false
                        if (feeDeducted)
                        {
                            if (actualAmount + fee != amountSatoshis) oops = true
                        }
                        else if (actualAmount != amountSatoshis) oops = true
                        if (oops)
                        {
                            abortTransaction(newTx)
                            throw WalletImplementationException("Output filler filled the wrong number of satoshis")
                        }

                        if (inAmt - (actualAmount + fee) < 0)  // The quantity in all the inputs can't afford this tx.  We need to find another input
                        {
                            // By bumping the guess as to the number of inputs, we will increase the amount allocated for fees

                            NinputsGuestimate = max(inputs.size, NinputsGuestimate + 1)
                            abortTransaction(newTx)
                            continue@outerloop
                        }

                        // add change output only if needed
                        if (inAmt - (actualAmount + fee) > DUST)
                        {
                            val co = createChangeOutput(inAmt - amountSatoshis - fee)
                            newTx.add(co)
                        }

                        // TODO shuffle outputs
                        signedTx = signTransaction(newTx)

                        // SANITY CHECK for extremely large fee and abort if so.
                        txSanityCheck(signedTx)

                        serializedTx = signedTx.BCHserialize(SerializationType.NETWORK).toByteArray()
                        val serializedTxSize = serializedTx.size.toLong()

                        val minFee = minFeeForSize(serializedTxSize)

                        if (fee < minFee)   // If the fee we chose is lower than the minimum fee for a tx of this size, try again with minfee as the fee.
                        {
                            fee = minFee
                        }
                        else break  // Ok worked!
                    }

                    if ((signedTx == null) || (serializedTx == null))  // Should never happen
                    {
                        abortTransaction(newTx)
                        throw WalletNotEnoughBalanceException(appI18n(RsendMoreThanBalance))
                    }

                    val outputSize = signedTx.outputs[0].BCHserialize(SerializationType.NETWORK).toByteArray().size
                    val inputSize = signedTx.inputs[0].BCHserialize(SerializationType.NETWORK).toByteArray().size
                    val inputScriptSize = signedTx.inputs[0].script.BCHserialize(SerializationType.NETWORK).toByteArray().size
                    // LogIt.info(sourceLoc() + " " + name + ": TX info: size ${serializedTx.size} fee $fee rate ${fee.toDouble() / serializedTx.size.toDouble()}  inputSize $inputSize (script $inputScriptSize) outputSize $outputSize ")

                    retin = Pair(signedTx, serializedTx)
                    break
                }
                throw WalletNotEnoughBalanceException(appI18n(RsendMoreThanBalance))
            }
            return@lock retin!!
        }
        return ret
    }

    fun signTransaction(tx: iTransaction, sigHashType: ByteArray = byteArrayOf()): iTransaction
    {
        val txSerialized = tx.BCHserialize(SerializationType.NETWORK)
        var count: Long = 0
        val flatTx = txSerialized.toByteArray()

        //LogIt.info(flatTx.ToHex())
        for (inp in tx.inputs)
        {
            signInput(tx, count, sigHashType, serializedTx = flatTx)
            count += 1
        }
        tx.changed()  //  Clear out the hash (if it was requested) since it will have changed now that signatures are added.
        return tx
    }

    @cli(Display.Simple, "sign the provided string with the provided address (this wallet must have the private key for that address), or pass null to use the common identity")
    override fun signMessage(message: ByteArray, addr: PayAddress?): ByteArray
    {
        val identityDest: PayDestination = destinationFor(Bip44Wallet.COMMON_IDENTITY_SEED)

        if (addr == null || addr == identityDest.address)
        {
            // This is a coding bug in the wallet
            val secret = identityDest.secret ?: throw WalletIncompatibleAddress("Wallet failed to provide an identity with a secret")
            return libnexa.signMessage(message, secret.getSecret()) ?: throw WalletIncompatibleAddress("Wallet signing failed")
        }
        else
        {
            val dest: PayDestination? = receiving[addr]
            val secret = identityDest.secret ?: throw WalletIncompatibleAddress("Wallet failed to provide an identity with a secret")
            if (dest == null) throw WalletAddressMissingException("This wallet is unaware of this address")
            return libnexa.signMessage(message, secret.getSecret()) ?: throw WalletIncompatibleAddress("Wallet signing failed")
        }
    }


    fun createChangeOutput(amtSatoshis: Long): iTxOutput
    {
        val d = getNewDestination() //  runBlocking { newDestination() }
        d.oneUse = true
        var ret = txOutputFor(chainSelector)
        ret.amount = amtSatoshis
        ret.script = d.ungroupedOutputScript()
        return ret
    }

    // If the group holds native tokens, amtSatoshis is ignored
    fun createGroupedChangeOutput(groupId: GroupId, groupAmount: Long, amtSatoshis: Long = dust(chainSelector)): NexaTxOutput
    {
        if (!chainSelector.isNexaFamily) throw UnsupportedInBlockchain("Group tokenization accessed in a non-Nexa chain")
        val d = getNewDestination() // runBlocking { newDestination() }
        d.oneUse = true
        val ret = NexaTxOutput(chainSelector)

        if (groupId.isFenced())
        {
            assert(false)  // not implemented
        }
        else
        {
            ret.amount = amtSatoshis
            ret.script = d.groupedOutputScript(groupId, groupAmount)
        }
        return ret
    }

    fun feeForSize(txSize: Long): Long
    {
        return ceil(txSize.toDouble() * DesiredFeeSatPerByte).toLong()  // Guess 1 satoshi per byte
    }

    fun minFeeForSize(txSize: Long): Long
    {
        return ceil(txSize * MinFeeSatPerByte).toLong()
    }

    /** Find inputs in this wallet to use in a new transaction.  This does NOT mark them as used, so cannot be called repeatedly until consumeInputs is called
     * Pass a filter to select a subset of the available UTXOs.
     * This passed function should return the amount "filled" by this input in units of the minAmt parameter.  Return 0 to not use this input.
     * Typically what is returned is satoshis, but if you wanted N inputs (irrespective of the amount in them) you could pass N for minAmt, and return 1 in your function.
     * Or you could filter by tokens and return the token amount rather than the satoshi amount...
     */
    fun filterInputs(minAmt: Long, minConfirms: Int = 0, filter: ((Spendable) -> Long)? = null): MutableList<Spendable>
    {
        val curHeight: Long = chainstate?.syncedHeight ?: if (minConfirms == 0) 0L else throw WalletDisconnectedException()
        var ret: MutableList<Spendable> = mutableListOf()

        var amt = 0L
        var all = true  // (I search thru the cache first) (minAmt == Long.MAX_VALUE)
        forEachUtxo { i ->
            // If an unconfirmed or confirmed spend of this utxo exists, then don't use it.
            if (i.isUnspent && (i.amount > 0) &&
              // subtracting 1 from minConfirms because "0 confirms" means unconfirmed so the "1th" block is the tip, not the "0th"
              ((minConfirms == 0) || ((i.commitHeight != -1L) && (i.commitHeight + (minConfirms - 1) <= curHeight))) &&  // Or we need some number of confirmations
              (i.reserved == 0L) &&
              ((filter != null) || (i.groupInfo() == null)) // if filter is null, then only look for ungrouped coins
            )  // or this inputs has been used by some other in-progress transaction
            {
                val filterResult = if (filter == null) i.amount else filter(i)
                if (filterResult > 0)
                {
                    ret.add(i)
                    amt += filterResult
                }
            }
            (amt >= minAmt)
        }

        return ret
    }

    /** Find inputs in this wallet to use in a new transaction.  This does NOT mark them as used, so cannot be called repeatedly until consumeInputs is called
     * @param amountSatoshis: The quantity of satoshis needed.  Pass LONG.MAX_VALUE to get all of them (otherwise the function will fail if there are not enough)
     * @param minConfirms: Minimum number of confirmations
     * @param filter: You can eliminate some inputs using this filter, by returning false
     Pass a filter to select a subset of the available UTXOs.
    */
    fun findInputs(amountSatoshis: Long, minConfirms: Int = 0, filter: ((Spendable) -> Long)? = null): MutableList<Spendable>?
    {
        // LogIt.info("Looking for: " + amountSatoshis)
        var ret = mutableListOf<Spendable>()
        var currentTotal: Long = 0
        // grab all of our UTXOs as a list that's sorted by quantity of satoshis
        var ok = true
        if (true)
        {
            // Oversearch for utxos so we have choices to pick from.  Handle wrapping (will only really happen if wanting all) by asking for all
            val srchAmt = if (2*amountSatoshis < amountSatoshis) Long.MAX_VALUE else 2*amountSatoshis
            var amounts: MutableList<Spendable> = filterInputs(srchAmt, minConfirms, filter)

            amounts.sortBy { it.amount }

            // Loop, adding one utxo per iteration, until we've found enough satoshis or run out of UTXOs
            while ((currentTotal < amountSatoshis) && (amounts.size > 0))
            {
                var loc = amounts.binarySearchBy(amountSatoshis - currentTotal) { it.amount }
                if (loc < 0)  // means we missed but -loc-1 is near (its the insertion position if the elem was added)
                {
                    loc = -loc - 1
                }

                if (loc >= amounts.size)  // We need more coins than exist in a UTXO
                {
                    loc = amounts.size - 1
                }

                if ((loc < 0) || (loc >= amounts.size))
                {
                    ok =false
                    break
                }  // loc is out of bounds because nothing left in the UTXO


                val utxo = amounts[loc]
                synchronized(dataLock)  // atomically check and set reserved
                {
                    if (utxo.reserved == 0L)  // Final check because we released the lock
                    {
                        // LogIt.info("found amount: " + utxo.amount)
                        ret.add(utxo)                // Add this utxo into our list of inputs
                        utxo.reserved = epochSeconds()
                        currentTotal += utxo.amount  // and add its quantity of satoshis into our running total
                    }
                }
                amounts.removeAt(loc)  // remove this from our list of possible utxos because we've used it
            }
            true
        }
        if (!ok) return null

        // We have enough we are done
        if ((currentTotal >= amountSatoshis)||(amountSatoshis == Long.MAX_VALUE)) return ret

        // We used all the UTXOs and didn't get enough.  Undo the reservations.  This is an uncommon case since the wallet can determine whether there's enough balance before calling findInputs
        synchronized(dataLock)
        {
            for (utxo in ret)
            {
                utxo.reserved = 0L
            }
        }
        return null
    }

    override fun synced(epochTimeinMsOrBlockHeight: Long): Boolean
    {
        val TIME_FUDGE = 60 * 60 * 1000  // 1 hour in milliseconds
        val now = (epochTimeinMsOrBlockHeight == -1L)
        var tim = if (now) millinow() else epochTimeinMsOrBlockHeight
        val bc = chainstate
        if (bc == null) return false  // can't be synced if we don't have a chainstate


        if (tim > 1262350000000) // Time stamp
        {
            if (bc.syncedHeight >= bc.chain.curHeight)  // Ok we are synced with our blockchain
            {
                val tip = bc.chain.nearTip ?: return false
                // Special case the "now" choice -- check if our blockchain up to date?
                if (now && (tip.time * 1000 < tim - TIME_FUDGE)) return false
                return true
            }
            else
            {
                if (now) return false; // we can't be synced if our blocks aren't even synced
                val hdr = bc.chain.getBlockHeader(bc.syncedHeight)
                if (hdr.time >= tim) return true  // Our synced header came in after the queried time
            }
        }
        else
        {
            if (tim <= bc.syncedHeight) return true
        }

        return false
    }

    //? undo 1 block's changes to this wallet
    fun rewind()
    {
        val cs = chainstate ?: throw WalletException("Cannot rewind with no blockchain")
        val syncedHash = cs.syncedHash

        LogIt.info(sourceLoc() + " " + name + ": Rewind away from ${cs.syncedHash.toHex()}:${cs.syncedHeight}")

        val header = cs.chain.blockHeader(syncedHash) ?: throw WalletException("Cannot rewind earlier than our header history")

        // When we rewind a block, we'll search through all TXOs and remove them if they were created in this block,
        // and mark them as unconfirmed unspent if they were spent in this block
        synchronized(dataLock)
        {
            var bal = cs.balance
            val delTxo = mutableListOf<iTxOutpoint>()
            val newlyUnconfTxIdem = mutableSetOf<Hash256>()
            forEachTxo { sp ->
                if (sp.spentBlockHash == syncedHash)
                {
                    sp.dirty = true
                    sp.spentBlockHash = Hash256.ZERO
                    sp.spentHeight = -1

                    // TODO: for now mark the tx completely unspent (forget it existed) rather then trying to reissue it.
                    sp.spentUnconfirmed = false
                    sp.spentTxHash = Hash256.ZERO
                    sp.spentDate = -1  // I'll mark this as completely unspent & but hang onto the tx.  If the tx is reissued properly then it will be marked spentUnconfirmed
                    newlyUnconfTxIdem.add(sp.spentTxHash)
                    bal = bal + sp.amount  // Remove that I spent this money
                    LogIt.info("Rewind spend of ${sp} in tx ${sp.commitTxIdem.toHex()}")
                }
                if (sp.commitBlockHash == syncedHash)  // Rewinding a committed TXO so it gets removed.
                {
                    sp.outpoint?.let { delTxo.add(it) }
                    newlyUnconfTxIdem.add(sp.commitTxIdem)
                    LogIt.info("Rewind receipt of ${sp} in tx ${sp.commitTxIdem.toHex()}")
                    bal = bal - sp.amount  // Remove my receipt of this money
                }
                false
            }
            deleteTxo(delTxo)


            // TODO check each pending tx to see if its valid before attempting to reissue
            /*
            // Add the unwound transactions into our list to be issued.
            // TODO: check for doublespends!
            for(nu in newlyUnconfTxIdem)
            {
                val tx = getTx(nu)
                if (tx != null)
                {
                    pendingTxLock.lock {
                        pendingTx[nu] = tx.toByteArray()
                    }
                }
                else
                {
                    LogIt.error(sourceLoc() +": LOST TX RECORD")
                }
            }
            // timeToResendWalletTx.reset()
             */

            cs.syncedHeight -= 1
            if (cs.prehistoryHeight > cs.syncedHeight) cs.prehistoryHeight = cs.syncedHeight  // When we rock forward our new history will start where ever we had to rewind back to.
            if (cs.prehistoryDate > header.time) cs.prehistoryDate = header.time - PREHISTORY_SAFEFTY_FACTOR  // we set prehistory to the current block because we have it, but rewind from there.
            cs.syncedHash = header.hashPrevBlock
            cs.balance = bal  // update the current balance WRT the rewind
        }
        walletChangeCallback?.invoke(this)  // Update the GUI
    }

    fun generateDestinationsInto(newUnusedAddresses: MutableList<PayAddress>, minAmt: Int = genAddressChunkSize)
    {
        for (i in 0..minAmt)
        {
            val dest = generateDestination()
            val destaddr = dest.address
            //LogIt.info(i.toString() + ": destaddr: " + destaddr + " dest pubkey:" + dest.pubkey?.toHex())
            if (destaddr == null) throw WalletImplementationException("Generated destination needs to be representable as an address")
            synchronized(dataLock) {
                receiving[destaddr] = dest
                newUnusedAddresses.add(destaddr)  // I cannot add these addresses to the unused pile until they are installed into my bloom filters in connected nodes
            }
        }
        LogIt.info(sourceLoc() + name + ": receiving into ${receiving.size} addresses")
    }

    val preparingDestinationsSync = Mutex()
    var waitingForBloom = false
    override fun prepareDestinations(minAmt: Int, chunk: Int)
    {
        preparingDestinationsSync.trylock {
            if (!waitingForBloom)
            {
                // Only let one execution of this function happen at a time; ignore other calls to it
                val newUnusedAddresses = mutableListOf<PayAddress>()
                if (unusedAddresses.size < minAmt)
                {
                    while (unusedAddresses.size + newUnusedAddresses.size < minAmt)
                    {
                        // Put all the new addrs into a temporary array so nobody uses them yet
                        generateDestinationsInto(newUnusedAddresses, chunk)
                    }
                    waitingForBloom = true
                    // Hand the new addrs to our connected nodes in the form of a bloom filter.
                    regenerateBloom() {
                        // OK bloom is installed so finally we can release these addresses.
                        // this is a continuation function so these aren't actually locked
                        preparingDestinationsSync.lock {
                            dataLock.lock {
                                for (a in newUnusedAddresses) unusedAddresses.add(a)
                            }
                            waitingForBloom = false
                        }
                    }
                }
            }
        }
    }

    override suspend fun newDestination(): PayDestination
    {
        var result: PayDestination? = null
        var prep = false
        while (result == null)
        {
                synchronized(dataLock) {
                    if (unusedAddresses.size > 0)
                    {
                        val ret = unusedAddresses.removeAt(0)
                        // The address must exist because all destinations are put into 'receiving at the time of creation
                        result = receiving[ret]
                        if (result == null) LogIt.severe(sourceLoc() + " " + name + " BUG: Unused Address $ret is not in receiving array (size ${receiving.size})")
                    }
                    if (result == null)
                    {
                        if (!prep) { prep = true; prepareDestinations(1, genAddressChunkSize); }
                    }
                }
                if (result == null) delay(20)  // Wait for destinations to be created
        }
        // LogIt.info(sourceLoc() + " " + name + " Returning new address " + result?.address.toString())
    return result!!
    }

    //* create a new address that funds can be sent to.  This function actually returns an existing pre-generated address, and generates new addresses in chunks of 'GEN_ADDRESS_CHUNK_SIZE when running out
    // This function makes sure that the destination is installed into bloom filters of connected nodes before it returns.  This adds complexity to this function, but eliminates a lot of problems where
    // a transaction is not seen because addresses are used before added to the bloom filter.
    override fun getNewDestination(): PayDestination
    {
        var result: PayDestination? = null
        var prep = false
        while (result == null)
        {
            synchronized(dataLock) {
                if (unusedAddresses.size > 0)
                {
                    val ret = unusedAddresses.removeAt(0)
                    // The address must exist because all destinations are put into 'receiving at the time of creation
                    result = receiving[ret]
                    if (result == null) LogIt.severe(sourceLoc() + " " + name + " BUG: Unused Address $ret is not in receiving array (size ${receiving.size})")
                }
                if (result == null)
                {
                    if (!prep) { prep = true; prepareDestinations(1, genAddressChunkSize); }
                }
            }
            if (result == null) millisleep(50U)  // Wait for destinations to be created
        }
        // LogIt.info(sourceLoc() + " " + name + " Returning new address " + result?.address.toString())
        return result!!
    }


    // This function keeps this wallet synced up with the chainstate
    fun run()
    {
        try
        {
            var priorChainTip = Hash256()
            while (!done)
            {
                // Implements blockchain processing pause feature at the API level.  Normally no pause here.
                // Also allows us to stop processing when making a major change to the blockchain (like rewind or rediscover)
                while (pause)
                {
                    paused = true
                    wakey.delayuntil(100L) { pause == false }
                }
                paused = false
                LogIt.info(sourceLoc() + " " + name + ": wallet analysis at: " + (chainstate?.chain?.nearTip?.hash?.toHex() ?: "unavailable") + ":" + (chainstate?.chain?.nearTip?.height ?: "unavailable"))
                try
                {
                    val cs = chainstate
                    if (cs != null)
                    {
                        if (cs.syncedHash == Hash256())  // I'm not sure if the chain knows about my start location yet
                        {
                            val hdr = cs.chain.findOrRewindTo(cs.syncedHeight)
                            cs.syncedHash = hdr.hash  // ok progress forwards...
                        }

                        // Find the blockchain's most difficult tipFrac
                        cs.chain.nearTip?.let { tip: iBlockHeader ->
                            if (tip.hash != priorChainTip)  // Update UI elements that show the wallet's blockchain since it has changed.
                            {
                                priorChainTip = tip.hash
                                walletChangeCallback?.invoke(this)
                            }

                            if (tip.hash != cs.syncedHash)
                            {
                                LogIt.info(sourceLoc() +" " + name + ": Moving wallet to " + tip.hash.toHex() + ":" + tip.height + " from: " + cs.syncedHash.toHex() + ":" + cs.syncedHeight)
                                try  // Rewind to get onto the path to the most difficult tip
                                {
                                    // The if check makes this code work for regtest when the wallet starts not knowing the genesis block
                                    // And for when you "rediscover" the blockchain
                                    if (cs.syncedHeight != -1L)
                                    {
                                        while (!cs.chain.isInMostWorkChain(cs.syncedHeight, cs.syncedHash))
                                        {
                                            LogIt.info("rewind")
                                            try
                                            {
                                                rewind()
                                            }
                                            catch (e: WalletException)
                                            {
                                                LogIt.info("Wallet Exception:" + e.toString())
                                                millisleep(100U)
                                                break
                                            }
                                        }
                                    }
                                }
                                catch (e: RequestedPrehistoryHeader)  // Rewound everything; move forward from here
                                {
                                    logThreadException(e, "Prehistory header requested", sourceLoc())
                                }

                                val syncedH = cs.syncedHeight
                                if (cs.chain.curHeight > syncedH) // I need to sync with additional blocks
                                {
                                    // Load the headers from storage
                                    //var count = (syncedH.toInt() + 1000) and 511.inv()  // move by powers of 2 because ancestorchain prefers that
                                    //count = count - syncedH.toInt()
                                    //if (count <= 0) count = 1000
                                    val count = 237  // a weird number makes the pauses seem random.  This cannot be too big or a busy wallet maxxes out the full node send buffer.
                                    // LogIt.info("wallet loading headers from ${syncedH}")
                                    val hdrs = cs.chain.getHeaderChain(syncedH + 1, cs.syncedHash, tip.hash, count)
                                    //LogIt.info("header load completed")

                                    // If nothing is going on let's bring the cache down
                                    if ((hdrs.size == 0)&&(maxTxoCache > absoluteMinTxoCache)) maxTxoCache -= 10

                                    while ((hdrs.size > 0) && (hdrs[0].height < syncedH + 1)) // too many returned, chop what I don't want
                                    {
                                        // LogIt.info("Header at ${hdrs[0].height}")
                                        hdrs.removeAt(0)
                                    }

                                    if (hdrs.size > 0 && hdrs[0].height != syncedH + 1)
                                    {
                                        LogIt.error(sourceLoc() + " " + name + ": Out of order headers. Asking starting at ${syncedH + 1}, loaded ${hdrs.size} headers starting at ${hdrs[0].height} (${hdrs[0].hash.toHex()})")
                                        //it.chain.reacquireHeaders(it.syncedHeight+1, hdrs[0].hash)
                                    }
                                    else
                                    {
                                        val earlyReq = mutableListOf<Hash256>()
                                        for (hdr in hdrs)  // Skip early request for prehistory blocks
                                        {
                                            if (!((hdr.time <= cs.prehistoryDate) || (hdr.height <= cs.prehistoryHeight))) earlyReq.add(hdr.hash)
                                            else LogIt.info(sourceLoc() + " " + name + ": Skipping prehistory block time ${hdr.time} <= ${cs.prehistoryDate} or height ${hdr.height} <= ${cs.prehistoryHeight}")
                                        }
                                        thisLaunch { cs.chain.earlyRequestTxInBlock(earlyReq) }

                                        for (hdr in hdrs)
                                        {
                                            val hdrHash = Hash256(hdr.hash.hash)
                                            if ((hdr.time <= cs.prehistoryDate) || (hdr.height <= cs.prehistoryHeight))  // skip accessing any blocks that happened before this wallet was created
                                            {
                                                LogIt.info("prehistory")
                                                cs.prehistoryHeight = max(cs.prehistoryHeight, hdr.height)
                                                onSync(hdrHash, hdr.height, hdr.time, 0)
                                                walletChangeCallback?.invoke(this)
                                            }
                                            else
                                            {
                                                if (hdr.height != cs.syncedHeight + 1)  // ensure that hdrs is in order
                                                {
                                                    LogIt.error(sourceLoc() + " " + name + ": Out of order headers received ${hdr.height} expecting ${cs.syncedHeight + 1}")
                                                    if (hdr.height > cs.syncedHeight + 1) break // and if they are beyond what we need then abort this work
                                                }
                                                else
                                                {
                                                    val txInBlock = cs.chain.getTxInBlock(hdrHash)
                                                    if (txInBlock.size > 0)
                                                    {
                                                        LogIt.info(sourceLoc() + " " + name + ": Syncing Wallet to ${hdr.height} with: " + txInBlock.size + " interesting tx. block hash: " + hdr.hash.toHex())

                                                        synchronized(dataLock)
                                                        {
                                                            val balDelta = interestingConfirmedTx(txInBlock, hdrHash, hdr.height, hdr.time * 1000)
                                                            onSync(hdr.hash, hdr.height, hdr.time, cs.balance + balDelta)
                                                        }
                                                    }
                                                    else
                                                    {
                                                        LogIt.info(sourceLoc() + " " + name + ": Synced Wallet at ${cs.syncedHeight} with: " + txInBlock.size + " interesting tx. block " + hdr.height + " hash: " + hdr.hash.toHex())
                                                        synchronized(dataLock)
                                                        {
                                                            onSync(hdr.hash, hdr.height, hdr.time, cs.balance + 0)  // Nothing to do but bump the synced at number
                                                        }
                                                    }
                                                    if (txInBlock.size > 0 || (hdr.height and 0x15L) == 0L || cs.syncedHeight >= cs.chain.curHeight)
                                                        walletChangeCallback?.invoke(this)
                                                    // Write the chainstate data so we can pick up where we left off if the app stops and restarts
                                                    if (flushWalletPeriodically()) save()
                                                }
                                            }
                                            // abort this sync if something else is going on (we'll just re-request the headers we skipped)
                                            if (pause) break
                                        }
                                        // Write the chainstate data so we can pick up where we left off if the app stops and restarts
                                        // either periodically, or at the end of sync
                                        if ((hdrs.size > 0) && flushWalletPeriodically() || (hdrs.size >= 16 && cs.syncedHeight >= cs.chain.curHeight)) save()
                                    }
                                }
                            }
                        }
                    }
                }
                catch (e: RequestedPrehistoryHeader)  // Blockchain tells us that it doesn't keep data that far back, so wallets must assume that they also have no old activity
                {
                    LogIt.info(sourceLoc() + " " + name + ": wallet asked for blockchain prehistory block.  Moving wallet to ${e.lastBlock?.height}")
                    if (e.lastBlock != null)
                    {
                        chainstate?.syncedHeight = e.lastBlock.height
                        chainstate?.syncedHash = e.lastBlock.hash
                        chainstate?.balance = 0
                    }
                }
                catch (e: BlockNotForthcoming)  // Nodes may not provide blocks to us if we request ones that are not on the main chain.  In that case loop around so that we move the wallet to the main chain
                {
                    LogIt.info(sourceLoc() + " " + name + ": Block is not being supplied, ${e.toString()}")
                    //if (blockchain.net.numPeers() > 0)  // no blocks makes sense  if we have no peers
                    //    chainstate?.chain?.findAnotherTip()  // This fixes a weird, likely never to be seen in mainnet, case where there's a tie but one of the blocks was invalidated and yet we keep requesting that block rather than the active one.
                }
                catch (e: P2PNoNodesException)  // No connectivity; nothing to do but wait
                {
                    val tmp = wakeCount
                    wakey.delayuntil(LONG_DELAY_INTERVAL) { wakeCount != tmp }
                }

                // Periodically resend all unconfirmed transactions to random nodes, most especially this happens once the wallet gets connected up
                val v = chainstate?.chain?.net
                if ((v != null) && (v.size > 0) && timeToResendHistoricalTx())
                {
                    resendUnconfirmedTx()
                }

                // Periodically resend pending transactions to random nodes
                // keep track of which nodes have INVed me with the tx, and don't send it to them
                if ((v != null) && (v.size > 0))
                {
                    if (timeToResendWalletTx())
                    {
                        // Send all transactions to one node in case they are dependant
                        val pt = mutableListOf<ByteArray>()
                        pendingTxLock.lock {
                            for ((_, tx) in pendingTx) pt.add(tx)
                        }

                        if (pt.size > 0)
                        {
                            try
                            {
                                v.sendTransactions(pt)
                            }
                            catch (e: P2PDisconnectedException)  // Nothing to do but wait for another opportunity to send
                            {
                                timeToResendWalletTx.reset()
                            }
                            catch (e: P2PNoNodesException)
                            {
                                timeToResendWalletTx.reset()
                            }
                        }
                    }
                }

                val tmp = wakeCount
                wakey.delayuntil(5000)  { wakeCount != tmp } // Periodic delay before rechecking
            }
        }
        catch (e: Exception)
        {
            handleThreadException(e, sourceLoc() + " " + name)
        }
        done = true  // If the wallet aborts from its processing loop, set the done indicator
    }

    /** @return the current token balance
     * @param groupId The group identifier for this token
     * @param dest If non-null, only return balances in this address
     * @param minConfirms If nonzero, the funding transaction must be at least this deep.
     * @param unspent If unspent is true, the current balance is provided.  Otherwise the total ever received is provided.
     */
    @cli(Display.Simple, "Get the balance of the supplied token (at the supplied address).  If minConfirms is nonzero, the funding transaction must be at least this deep. If unspent is true, the current balance is provided.  Otherwise the total ever received is provided.")
    override fun getBalanceIn(groupId: GroupId, dest: PayAddress?, minConfirms: Int, unspent: Boolean): Long
    {
        return synchronized(dataLock)
        {
            val curHeight = syncedHeight
            var ret = 0L
            forEachTxo { sp ->
                // Check that unconfirmed are allowed or that the right number of confirms have passed
                if ((sp.commitHeight == -1L && minConfirms == 0) || (sp.commitHeight + minConfirms) >= curHeight)
                {
                    val grp = sp.groupInfo()
                    // group id must match
                    if ((grp != null) && !grp.isAuthority() && (grp.groupId == groupId))
                    {
                        if (dest == null || (dest == sp.addr))  // If an address is supplied it must match
                        {
                            if (unspent)
                            {
                                if (sp.isUnspent)
                                    ret += grp.tokenAmt
                            }
                            else
                            {
                                ret += grp.tokenAmt
                            }
                        }
                    }
                }
                false
            }
            ret
        }
    }

    override fun getBalanceIn(dest: PayAddress, unspent:Boolean): Long
    {
        return synchronized(dataLock)
        {
            var ret = 0L
            if (unspent == true)
            {
                /*
                if (dest in unspentByAddress)
                {
                    unspentByAddress[dest]?.let {
                        for (outpt in it)
                        {
                            val sp = getTxo(outpt)
                            if (sp != null)
                            {
                                if ((unspent == false) || ((sp.spentHeight <= 0) && (sp.spentUnconfirmed == false)))
                                    ret += sp.amount
                            }
                        }
                    }
                }
                 */
                forEachUtxoWithAddress(dest) {
                    ret += it.amount
                    false
                }

            }
            else
            {
                val os = dest.outputScript()
                forEachTxo { txo ->
                    if (txo.priorOutScript contentEquals os)
                    {
                        ret += txo.amount
                    }
                    false
                }
            }
            ret
        }
    }

    override fun isUnspentWalletAddress(dest: PayAddress): Boolean
    {
        return synchronized(dataLock)
        {
            var ret = true
            for ((k,v) in txoCache)
            {
                if (v.isUnspent)
                {
                    ret = false
                    break
                }
            }
            if (ret == true)
            {
                walletDb!!.txo.forEachUtxoWithAddress(dest) {
                    ret = false
                    true
                }
            }
            ret

            //val d = unspentByAddress[dest]
            //(d != null)
        }
    }

    override fun isWalletAddress(dest: PayAddress): Boolean
    {
        return synchronized(dataLock)
        {
            val d = receiving[dest]
            (d != null)
        }
    }

    /** @return Return the PayDestination if passed address belongs to this wallet
     */
    @cli(Display.Simple, "Return the PayDestination if passed address belongs to this wallet")
    override fun walletDestination(dest: PayAddress): PayDestination?
    {
        return synchronized(dataLock)
        {
            receiving[dest]
        }
    }

    /** Retire destination removes a PayDestination from the list of destinations we scan for payments.
     * A typical use is for change addresses, since we know that that address will only be used once. */
    fun retireDestination(pd: PayDestination)
    {
        val addr = pd.address
        if (addr != null)
        {
            // LogIt.info(sourceLoc() + " " + name + ": Retiring address " + addr)
            dataLock.lock {
                receiving.remove(addr)
                unusedAddresses.remove(addr)  // should be removed already but we need to be sure because this address will no longer be in the bloom filter.
            }
        }
    }

    fun regenerateBloom(onBloomInstalled: (() -> Unit)?)
    {
        // without a backing blockchain, there's no reason for a bloom, so this function is a no-op
        val cs = chainstate
        if (cs == null)
        {
            onBloomInstalled?.invoke()
            return
        }

        val data = synchronized(dataLock)
        {
            val utxoSz = numUtxos()
            val data = Array<Any>(receiving.size + utxoSz, { Unit })
            var idx = 0
            // Install all known addresses, so we find incoming money
            LogIt.info(sourceLoc() + name + ": Regenerate bloom with ${receiving.size} addresses and $utxoSz UTXOs")
            for (r in receiving)
            {
                r.value.bloomFilterData?.let {
                    data[idx] = it
                    // need to create the log string here because it uses receiving
                    // logStr = name + ": Installing bloom with " + receiving.size + " elements: " + receiving.map { it.key.toString() }.joinToString() + "bloom size: " + bloom.size
                    assert(r.key == r.value.address)
                    // LogIt.info(sourceLoc() + name + ": bloom addr " + idx + " is " + r.value.address + " or " + it.toHex())
                    idx++
                }
            }

            var numUtxo = 0
            // Install all known outpoints so that we find spends
            forEachUtxo { u ->
                val outpt = u.outpoint
                if (u.isUnspent)
                {
                    numUtxo++
                    if (outpt != null)
                    {
                        data[idx] = outpt.BCHserialize(SerializationType.NETWORK).toByteArray()
                        idx++
                    }
                }
                false
            }

            // if a receiving object can't resolve to some bloom filter data this assertion could fail.  But that should never happen for a wallet (that must understand all its own scripts)
            // LogIt.info("UTXO size: $utxoSz $numUtxo")
            assert(idx == receiving.size + utxoSz)  // TODO setFilterObjects can't handle nulls so make sure there are none
            data
        }

        // LogIt.info(sourceLoc() + name + ": setFilterObjects")
        cs.filterHandle = cs.chain.setFilterObjects(data, cs.filterHandle, onBloomInstalled)
        LogIt.info(sourceLoc() + name + ": Regenerated bloom filter handle ${cs.filterHandle} with ${receiving.size} addresses and ${numTxos()} outpoints")
    }

    fun addBlockchain(chain: Blockchain, earliestHeight: Long?, startPlace: Long?)
    {
        val cs = GlueWalletBlockchain(chain)
        val dbkey = chainStateDbKey(name, chain.chainSelector)
        var badChainState = false
        var noData = false
        try
        {
            loadChainState(cs, walletDb!!, dbkey)
        }
        catch (e: DataMissingException)
        {
            badChainState = true
            noData = true
        }
        catch (e: IndexOutOfBoundsException)
        {
            LogIt.info(sourceLoc() + " " + name + ": DB deserialization error for key: " + dbkey)
            walletDb?.delete(dbkey)
            badChainState = true
        }
        catch (e: IllegalArgumentException)  // illegal capacity
        {
            LogIt.warning(sourceLoc() + " " + name + ": database corruption " + dbkey + ". Deleting record")
            walletDb?.delete(dbkey)
            badChainState = true
        }
        catch (e: DeserializationException)
        {
            LogIt.info(sourceLoc() + " " + name + ": DB deserialization error for key: " + dbkey)
            walletDb?.delete(dbkey)
            badChainState = true
        }
        LogIt.info(sourceLoc() + " " + name + ": Loaded wallet blockchain state at: ${cs.syncedHash}:${cs.syncedHeight}")

        if (badChainState)
        {
            if (noData)
            {
                if ((startPlace == null)||(earliestHeight == null))
                {
                    cs.prehistoryDate = 0
                    cs.prehistoryHeight = 1
                }
                else
                {
                    // last prehistory block is 1 less than the first activity...
                    val ph = if (earliestHeight>0) earliestHeight-1 else 0
                    cs.prehistoryDate = startPlace
                    cs.prehistoryHeight = ph
                    cs.syncedHash = Hash256()  // I don't know it yet
                    cs.syncedHeight = ph
                    cs.balance = 0
                }
            }
            LogIt.info(sourceLoc() + " " + name + ": Bad or no persisted chainstate, assuming wallet needs recovery and setting prehistory to " + cs.prehistoryDate + "(" + epochToDate(cs.prehistoryDate) + ")")
            if (cs.prehistoryHeight <= 0.toLong())  // If we couldn't set prehistoryHeight any other way, set it from the checkpoint
            {
                synchronized(dataLock)
                {
                    cs.prehistoryHeight = chain.checkpointHeight
                    cs.syncedHash = chain.checkpointId
                    cs.syncedHeight = chain.checkpointHeight
                    cs.balance = 0
                }
            }
        }

        // Unconfirmed tx are communicated via callback.  Confirmed tx are handled by requesting a block and processing the result.
        chain.req.addUnconfirmedTxHandler({ txs -> interestingUnconfirmedTx(txs, null) })

        chainstate = cs
        try
        {
            if (receiving.size == 0)  // you didn't loadWalletTx yet!
            {
                LogIt.error(sourceLoc() + name + ": Load wallet addresses and transactions before adding a blockchain")
                throw WalletException("Load wallet addresses and transactions before adding a blockchain")
            }
            else
            {
                // Have a bunch of unused addresses ready and installed in the bloom filter
                if (unusedAddresses.size < MIN_UNUSED_ADDRESSES) generateDestinationsInto(unusedAddresses, MIN_UNUSED_ADDRESSES - unusedAddresses.size)
                // Restart will start the processing thread that will catch this wallet up to the current tip.  Don't start doing that until the bloom filter is properly installed or we might miss tx
                thisLaunch { regenerateBloom({}) }
            }
        }
        catch (e: DataMissingException)
        {
            launchRediscover(true)
        }

        restart()
    }

    fun restart()
    {
        if (processingThread == null)
        {
            processingThread = Thread(name + "_wallet") { run() }
        }
    }

    fun stop()
    {
        done = true
    }

    protected fun onSync(blockHash: Hash256, blockHeight: Long, blockTime: Long, newBalance: Long)
    {
        // LogIt.info(sourceLoc() + " " + "onSync set synced height to:" + blockHeight + " time: " + blockTime)
        chainstate?.syncedHeight = blockHeight
        chainstate?.syncedHash = blockHash
        chainstate?.syncedDate = blockTime
        chainstate?.balance = newBalance
        if (blockHeight == blockchain.curHeight) // we caught up so force the wallet to be saved
        {
            save(true)
        }

        walletChangeCallback?.invoke(this)  // UPDATE the GUI
    }


    /** You can only give the wallet confirmed tx for the next block.
     * Otherwise portions of the transaction graph could be lost since we do not know to look for a certain Outpoint without previously seeing the output.
     * @return delta balance for this wallet
     */
    protected fun interestingConfirmedTx(txes: List<iTransaction>, blockHash: Hash256, blockHeight: Long, msSinceEpoch: Long): Long
    {
        // LogIt.info(sourceLoc() + name + ": block $blockHeight: ${blockHash.toHex()} Interesting confirmed TX: ${txes.map {it.idem.toHex()}.joinToString("") }")
        if (chainstate!!.syncedHash == blockHash)
        {
            LogIt.severe(sourceLoc() + name + ": DOUBLE Interesting confirmed TX: ${txes.map {it.idem.toHex()}.joinToString("") }")
        }
        if (chainstate!!.syncedHeight != blockHeight-1)
        {
             LogIt.severe(sourceLoc() + name + ": out of order tx handling }")
        }
        // You can only give the wallet confirmed tx for the next block.
        // Otherwise portions of the transaction graph could be lost since we do not know to look for a certain Outpoint without previously seeing the output
        val (ret, unconfXform) = interestingTx(txes, blockHash, blockHeight, msSinceEpoch, null)
        unconfBalance -= unconfXform  // Adjust the running unconf balance for everything that was confirmed that I knew about
        pendingTxLock.lock {
            for (tx in txes)
                pendingTx.remove(tx.idem)  // If one of our transactions was confirmed, then remove it from the pending list so we know not to keep broadcasting it
        }
        return ret
    }

    fun interestingUnconfirmedTx(txs: List<iTransaction>, note: String?): Long
    {
        // LogIt.info(sourceLoc() + name + ": Interesting Unconfirmed TX: ${txs.map {it.idem.toHex()}.joinToString("") }")
        val (ret, _ ) = interestingTx(txs, null, null, null, note)
        return ret
    }

    /** Get the price of this crypto's finest unit in fiat, using some external service */
    fun getPrice(whenInMsSinceEpoch: Long, currencyCode: String): BigDecimal
    {
        val now = millinow()

        try
        {
            if (now - whenInMsSinceEpoch < 24 * 60 * 60 * 1000) // 24 hours (historical price doesn't work for today)
            {
                val spfn = spotPrice
                if (spfn != null)
                {
                    return (spfn(currencyCode))
                }
            }
            else
            {
                val hfn = historicalPrice
                if (hfn != null)
                {
                    return hfn(currencyCode, whenInMsSinceEpoch / 1000)
                }
            }
        }
        catch (e: Exception)
        {
            LogIt.info(sourceLoc() + " " + name + " " + ": Cannot get price in " + currencyCode + " Error: " + e.toString())
        }

        // I can't get the price
        LogIt.info(sourceLoc() + " " + name + " " + ": Cannot get price in " + currencyCode)
        return BigDecimal.ZERO
    }

    fun recoverDtorOrder(txUnsorted: List<iTransaction>): List<iTransaction>
    {
        return txUnsorted  // TODO
    }

    // Collect all the data we will need to figure out the money flow
    //class HRecord(val inflow: MutableList<iTxOutpoint> = mutableListOf(), val outflow: MutableList<PaymentHistory> = mutableListOf(), val spent: MutableList<PaymentHistory> = mutableListOf())

    /** returns the balance change for this wallet */
    fun interestingTx(txUnsorted: List<iTransaction>, blockHash: Hash256?, blockHeight: Long?, msSinceEpoch: Long?, note: String?): Pair<Long,Long>
    {
        var changed = false
        var idx: Long
        var flushTxos = mutableListOf<Spendable>()
        var bal = 0L
        var confFromUnconfBal = 0L  // Tracks the balance that moved from unconfirmed (that we knew about) to confirmed
        synchronized(dataLock)
        {
            val txs = recoverDtorOrder(txUnsorted)

            // Filter out any repeats
            val filteredTxes = mutableListOf<iTransaction>()

            // These are transactions that I may add to this wallet history by the end of this alg
            val possHistory: MutableMap<Hash256,TransactionHistory> = mutableMapOf()

            // Create a UTXO map for the outputs in all transactions
            // We use this to look up other tx that spend this.  These Tx are not necessarily mine, but we cant tell yet because we haven't analyzed the parent tx which could be
            // anywhere in this block.
            val blockUtxos: MutableMap<iTxOutpoint, iTransaction> = mutableMapOf()
            for (tx in txs)
            {
                tx.changed()  // force recalc of hash just to be sure
                var txoutidx = 0
                for (txo in tx.outputs)
                {
                    blockUtxos[outpointFor(chainSelector, tx.idem, txoutidx.toLong())] = tx
                    txoutidx++
                }
            }

            for (tx in txs)
            {
                /*
                if (blockHash != null)
                {
                    LogIt.info(sourceLoc() +" " + name + ": Confirmed tx " + tx.idem.toHex() + " in block " + blockHash.toHex() + ":" + blockHeight)
                }
                else
                {
                    LogIt.info(sourceLoc() +" " + name + ": Unconfirmed tx " + tx.idem.toHex())
                }
                 */

                // Let's see if we already have this tx in our history
                val handledTx = getTx(tx.idem)

                var process = if (handledTx == null) true  // We've never seen this transaction
                else if (handledTx.confirmedHeight > 0.toLong()) false  // We already have a confirmation of this tx
                else if (blockHash != null) true  // This is confirmed, and we only have unconfirmed
                else false // double unconfirmed

                // If we've already put this tx in our history, populate our possible history cache with it so that we can just use possHistory as a superset of all relevant history
                // that is, possHistory may have extra tx we don't need, but it is guaranteed to have all the tx we may need.
                if (handledTx != null)
                {
                    if (handledTx.tx.idem != tx.idem)
                    {
                        LogIt.error("BUG")
                        handledTx.tx = tx
                    }
                    possHistory[tx.idem] = handledTx
                }

                // Discover if this tx really has something to do with this wallet, or is it a false positive or a different wallet's tx.
                var relevantToMe = false
                if (process)
                {
                    // Is this transaction spending a UTXO tracked by this wallet, or a UTXO created in this block?  (the wallet is sending money)
                    for (i in tx.inputs)
                    {
                        val unsp = i.spendable.outpoint?.let { getTxo(it) }
                        if (unsp != null)
                        {
                            // LogIt.info(sourceLoc() +" " + name + ": block tx ${tx.idem.toHex()} spends my UTXO ${i.spendable.dump()}")
                            relevantToMe = true; break;
                        }

                        val blockSpend = blockUtxos[i.spendable.outpoint]  // If its spending a utxo in this block we need to do the OTI alg with it
                        if (blockSpend != null)
                        {
                            // LogIt.info(sourceLoc() +" " + name + ": block tx ${tx.idem.toHex()} spends a UTXO created in the same block")
                            relevantToMe = true; break;
                        }
                    }
                }
                // If we haven't found a wallet input, then look to see if this tx outputs to this wallet (the wallet is receiving money)
                if (process && !relevantToMe)
                {
                    for (i in tx.outputs)
                    {
                        if (i.amount > 0)  // Ignore anything unspendable
                        {
                            val addr = i.script.address
                            if (addr == null)
                            {
                                LogIt.info(sourceLoc() +name + ": cannot understand tx: " + tx.idem.toHex() + " output: " + i.script.toHex())
                            }
                            else
                            {
                                val dest = receiving[addr]
                                if (dest != null)
                                {
                                    relevantToMe = true
                                    // LogIt.info(sourceLoc() + name + ": block tx ${tx.idem} sending to my address " + addr)
                                    break
                                }
                                else
                                {
                                    // LogIt.info(sourceLoc() + name + ": block tx ${tx.idem} sending to foreign address " + addr)
                                }
                            }
                        }
                    }
                }

                if (process && relevantToMe)
                {
                    // Add this tx into the list that needs wallet processing
                    filteredTxes.add(tx)

                    // Add this tx into the possible history. I can't add it to the history yet, because it might just spend another tx in this block (this other tx or may not be mine)
                    if (handledTx == null)
                    {
                        val entry = TransactionHistory(chainSelector, tx)
                        if (blockHeight != null) entry.confirmedHeight = blockHeight
                        if (blockHash != null) entry.confirmedHash = blockHash
                        // In general, take "now" as the transaction time since it is more accurate than the block time (we'll create this record for unconfirmed tx)
                        // unless the current time is well after the block time.  In that case, assume we are doing an IBD and so use the block time rather than the current time
                        if ((msSinceEpoch != null) && (entry.date - msSinceEpoch > 2 * 60 * 60 * 1000)) // 2 hours
                        {
                            entry.date = msSinceEpoch
                        }
                        val cs = chainstate
                        // If this is the first transaction relevant to this wallet, set the prehistory marker.  Note order is important: numTx is expensive
                        if ((cs != null)&&(cs.prehistoryHeight == 0L)&&(blockHeight != null)&&(numTx() == 0))
                        {
                            // if the prehistory height is unspecified, set it to the block before this first transaction
                            cs.prehistoryHeight = blockHeight-1
                        }

                        if (note != null) entry.note = note
                        possHistory[tx.idem] = entry
                    }
                    else
                    {
                        if (blockHeight != null)
                        {
                            handledTx.confirmedHeight = blockHeight
                            handledTx.dirty = true
                        }
                        if (blockHash != null)
                        {
                            handledTx.confirmedHash = blockHash
                            handledTx.dirty = true
                        }

                    }
                }
            }

            // Now process all the tx in the list

            //val hData: MutableMap<Hash256, HRecord> = mutableMapOf()

            // TX don't come in in dependency order, so need to do outs then inputs algorithm (OTI)
            for (tx in filteredTxes)
            {
                var incomingAmt = 0L
                val walletIncomingIdxes = mutableListOf<Long>()
                idx = 0

                //val hrecord = HRecord()
                //hData[tx.idem] = hrecord

                //LogIt.info(name + ": looking for wallet output given ${receiving.size} addresses")
                for (utxo in tx.outputs)
                {
                    val addr = utxo.script.address  // If we can parse this UTXO into an address, then maybe its being sent to this wallet
                    val payDest = receiving[addr]

                    if (addr != null)  // If its a nonstandard payment give up any analysis
                    {
                        if (payDest != null)  // This utxo is interesting to us (its one of this wallet's destinations)
                        {
                            val outpoint = outpointFor(chainSelector, tx.idem, idx)

                            if (true)
                            {
                                var created = false
                                // If it doesn't exist, initialize it, assume its an unconfirmed and we'll modify later if confirmed
                                var tmp = getTxo(outpoint)

                                val sp = tmp ?: run {
                                    created = true
                                    val sp1 = Spendable(chainSelector)
                                    sp1.outpoint = outpoint
                                    sp1
                                }

                                assert(sp.outpoint == outpoint)
                                sp.amount = utxo.amount

                                incomingAmt += sp.amount

                                if (utxo.script.groupInfo(sp.amount) == null)
                                {
                                    // If its set to something, we've already handled this tx at some other time
                                    if (blockHash == null)  // unconfirmed
                                    {
                                        bal += sp.amount
                                    }
                                    else
                                    {
                                        bal += sp.amount
                                        // this txo is tranforming from a known unconfirmed to confirmed
                                        if (sp.commitUnconfirmed == 1L)  confFromUnconfBal += sp.amount
                                    }
                                }

                                if (blockHash != null)
                                { // This is a confirmed spend.  Overwrite any unconfirmed data with that of the confirmed tx, just in case malleated tx or doublespend
                                    //if (sp.commitHeight > 0) LogIt.info("RECONFIRM")
                                    sp.commitUnconfirmed = 0  // We got confirmed
                                    sp.commitBlockHash = blockHash
                                    sp.commitHeight = blockHeight!!  // blockHeight must be something if blockHash is something
                                    msSinceEpoch?.let { sp.commitDate = it }
                                    // LogIt.info(sourceLoc() + " " + name + ": committed ${sp}")
                                }
                                else
                                {
                                    sp.commitUnconfirmed = 1
                                    if (created == false)
                                    {
                                        // if (sp.commitHeight <= 0) LogIt.info("RE-UNCONFIRMED")
                                        // else LogIt.info("UNCONFIRMED notification of CONFIRMED TX")
                                    }
                                }

                                sp.backingPayDestination = payDest
                                sp.redeemScript = SatoshiScript(chainSelector) // TODO
                                assert(sp.chainSelector == utxo.script.chainSelector)
                                sp.priorOutScript = utxo.script  // Hang onto the output script since its part of the sighash when spending
                                // Note we could check for a double spend by comparing this to an existing value
                                //if ((sp.commitTx == null) || (blockHash != null))  // If there's no info yet, or if this is the tx being commited to the blockchain then update the tx
                                //    sp.commitTx = tx
                                sp.commitTxIdem = tx.idem
                                sp.dirty = true
                                walletIncomingIdxes.add(idx)

                                // LogIt.info(name + ": Wallet tx received " + sp.amount + " in block " + (blockHash?.toHex() ?: "Unconfirmed") + " TX " + tx.idem.toHex() + " outpoint " + sp.outpoint!!.toHex())

                                if (sp.spentBlockHash == Hash256.ZERO)
                                    insertUnspentByAddress(addr, sp.outpoint)
                                else
                                {
                                    // this happens when the tx arrive out of dependency order, which is pretty common with the CTOR block ordering
                                    // LogIt.info(sourceLoc() + " " + name + ": Output was already spent when received")
                                }

                                // Remove this address from the unused addresses list
                                markAddressUsed(addr)
                                // LogIt.info("Output UTXO: ${sp.dump()}")
                                // Write this TXO to the wallet as one we can spend (or have spent)
                                flushTxos.add(sp)
                                cacheTxo(sp)
                                //hrecord.inflow.add(outpoint)
                            }
                            changed = true
                        }
                        else  // If this UTXO is unknown to this wallet, then it might be an outgoing payment if we signed the inputs of the tx.
                        {     // But we can't figure that out until later, so create outgoing histories but keep them locally
                            //val outpay = updateOutgoingPaymentHistory(tx, idx, outpoint, utxo.amount, addr, msSinceEpoch, note)
                            //hrecord.outflow.add(outpay)
                        }
                    }
                    idx += 1
                }

                if (incomingAmt > 0)
                {
                    // LogIt.info(sourceLoc() + " " + name + ": adding ${tx.idem.toHex()} to history because received ${incomingAmt}")
                    val history = possHistory[tx.idem]
                    if (history != null)  // its got to be in this map b/c I added it just above
                    {
                        // if we know the block time now, then use it if its earlier than the first moment we became aware of this tx
                        if (msSinceEpoch != null && msSinceEpoch < history.date) history.date = msSinceEpoch
                        history.incomingAmt = incomingAmt
                        history.incomingIdxes.clear()
                        history.incomingIdxes.addAll(walletIncomingIdxes)
                        if (note != null) history.note = note
                        history.dirty = true
                        setTx(history)  // This is a deposit into this wallet (that is, output is one of ours) so its part of the history
                    }
                    else
                    {
                        LogIt.info(sourceLoc() +name + " BUG should NEVER HAPPEN!")
                    }
                }
            }

            // We added all outputs, so now we can look for any inputs.  This catches any cases where we receive and then send in the same block and the txes are in the list in reverse causal order
            for (tx in filteredTxes)
            {
                val walletOutgoingIdxes = mutableListOf<Long>()
                val walletOutgoingOutputs = mutableListOf<iTxOutput>()

                //val hrecord = hData[tx.idem]!!  // Must be non-null because we create it in the loop above
                //val spentHistory = hrecord.spent

                // go thru outputs again tabulating what I received vs what someone else received
                var Ireceived = 0L
                var Oreceived = 0L
                for (utxo in tx.outputs)
                {
                    val addr = utxo.script.address
                    val payDest = receiving[addr]
                    if (payDest != null)
                    {
                        Ireceived += utxo.amount  // This wallet received
                    }
                    else Oreceived += utxo.amount  // Other wallet received
                }

                // Go through all the inputs, confirming that we spent our UTXOs
                var Ispent = 0L  // This wallet spent
                var Ospent = 0L  // Other wallet spent
                idx = 0
                var spendsMyUtxo = false
                for (spent in tx.inputs)
                {
                    synchronized(dataLock)
                    {
                        val outpoint = spent.spendable.outpoint

                        // This code relies on us getting the output tx before we get the spend.  This may not be the case for unconfirmed tx at least in theory
                        val v: Spendable? = if (outpoint != null) getTxo(outpoint) else null
                        if (v != null) // One of our inputs was spent
                        {
                            spendsMyUtxo = true
                            Ispent += v.amount
                            // LogIt.info(name + ": Wallet tx spent " + v.amount + " in block " + (blockHash?.toHex() ?: " Unconfirmed") + " TX " + tx.idem.toHex() + " outpoint " + spent.spendable.outpoint!!.toHex())

                            walletOutgoingIdxes.add(idx)
                            walletOutgoingOutputs.add(v.prevout)
                            changed = true
                            if (blockHash == null)  // This is an unconfirmed tx
                            {
                                // TODO check other stuff like that spentHeight == -1 (not yet spent)
                                bal -= v.amount
                                v.spentUnconfirmed = true
                                v.spentTxHash = tx.idem
                                v.spentDate = millinow()
                                v.reserved = 0 // Once we note where this was spent (in what block, block height, or tx), its no longer a reserved UTXO, its a spent TXO.
                            }
                            else  // confirmation that our input was spent
                            {
                                // this input txo is tranforming from a known unconfirmed to confirmed
                                if (v.spentUnconfirmed == true)  confFromUnconfBal -= v.amount

                                v.spentUnconfirmed = false
                                bal -= v.amount
                                v.spentBlockHash = blockHash
                                msSinceEpoch?.let { v.spentDate = it }
                                v.spentHeight = blockHeight!!
                                v.spentTxHash = tx.idem
                                v.reserved = 0 // Once we note where this was spent (in what block, block height, and tx), its no longer a reserved UTXO, its a spent TXO.
                                assert(v.spentBlockHash  != v.spentTxHash)

                                val pd = v.payDestination
                                if (pd != null)
                                {
                                    if (pd.oneUse)
                                    {
                                        retireDestination(pd)
                                    }
                                }
                            }
                            v.dirty = true
                            flushTxos.add(v)
                            cacheTxo(v)
                            // We should have prior payment history on every payment because the OTI (outs-then-ins) algorithm added all payments
                            //paymentHistory[outpoint]?.let { spentHistory.add(it) }
                        }
                        else
                        {
                            Ospent += spent.spendable.amount
                        }
                    }
                    idx += 1
                }

                if (spendsMyUtxo)
                {
                    // LogIt.info(sourceLoc() + " " + name + ": adding ${tx.idem.toHex()} to history because spent ${Ispent}")
                    val history = possHistory[tx.idem]
                    if (history != null) // its got to be in this map b/c I added it in the processing before the OTI step
                    {
                        history.outgoingAmt = Ispent
                        history.outgoingIdxes.clear()
                        history.outgoingIdxes.addAll(walletOutgoingIdxes)
                        history.spentTxos.clear()
                        history.spentTxos.addAll(walletOutgoingOutputs)
                        history.dirty = true
                        if (note != null) history.note = note
                        setTx(history)
                    }
                    else
                    {
                        LogIt.info(sourceLoc() +name + " BUG should NEVER HAPPEN!  (All relevant tx should have been put into the history)")
                    }
                }
            }

            // we want to track the cost basis for every tx (and only those tx) relevant to this wallet, which is anything that either the input or output processing above decided to put into
            // the wallet.
            for (tx in filteredTxes)
            {
                // TODO
                txHistoryCache[tx.idem]?.let {
                    trackCostBasis(it)
                }
            }

        }

        if (flushTxos.size > 0)
        {
            try
            {
                setTxo(flushTxos)
            }
            catch(e: Exception) // it should not fail to write, but if it does I suppose we just need to keep going
            {
                logThreadException(e)
            }
        }
        if (changed) walletChangeCallback?.let { it(this) }
        return Pair(bal, confFromUnconfBal)
    }

    // TODO: This entire code is broken by partial transactions because it assumes that any tx I've signed are entirely "mine" (all outputs are change or outgoing flows)
    // and all inputs are mine if any are.
    fun trackCostBasis(th:TransactionHistory?)
    {
        return
        /*
        var giveUp = 0
        // Figure out what this transaction actually did and update the relevant history records
        // This code assumes that if this wallet spent any coins (i.e. signed any inputs) then self outputs are change

        while (hData.size > 0)
        {
            var progress = 0
            val txdataIter = hData.iterator()
            for (txdata in txdataIter)
            {
                // Look for a transaction whose inputs are all resolved with cost basis information
                var inputsResolved = true
                var inputAmount = 0L
                var totalPrice = BigDecimal(0, currencyMath).setScale(currencyScale)
                var priceWhatFiat = "USD"
                for (inp in txdata.value.spent)
                {
                    val priorSpend:PaymentHistory? = inp.outpoint?.let { paymentHistory[it] }
                    if (priorSpend == null) inputsResolved = false
                    else
                    {
                        if (priorSpend.priceWhatFiat == "") inputsResolved = false  // I haven't figured out the cost basis yet
                        else
                        {
                            priceWhatFiat = priorSpend.priceWhatFiat  // TODO make sure same fiat
                            inputAmount += priorSpend.amount
                            val basisOverride = priorSpend.basisOverride

                            if (basisOverride != null)
                            {
                                totalPrice += basisOverride
                            }
                            else
                            {
                                totalPrice = totalPrice + (priorSpend.priceWhenIssued * BigDecimal(priorSpend.amount))
                            }
                        }
                    }
                    if (!inputsResolved) break
                }

                // We have enough information to resolve this whole tx
                if (inputsResolved)
                {
                    if (inputAmount > 0)  // Wallet paid someone
                    {
                        val avgPrice = totalPrice / BigDecimal(inputAmount)  // fiat/satoshi

                        for (outflow in txdata.value.outflow)  // The outputs that actually went to someone
                        {
                            outflow.basisOverride = avgPrice * BigDecimal(outflow.amount)
                            outflow.priceWhatFiat = priceWhatFiat
                            outflow.priceWhenIssued = getPrice(outflow.date, priceWhatFiat)
                            paymentHistory[outflow.outpoint!!] = outflow  // Since this wallet signed some inputs in this tx, I'm going to assume that all the outputs are this wallet spending
                        }

                        for (inflow in txdata.value.inflow)  // The outputs that went to myself (change) should get the cost basis of the inputs
                        {
                            val inflowRecord = paymentHistory[inflow]!!
                            inflowRecord.priceWhenIssued = avgPrice
                            inflowRecord.priceWhatFiat = priceWhatFiat
                            inflowRecord.isChange = true
                            // TODO inherit the most recent date of all parents
                        }
                    }
                    else  // Wallet received money
                    {
                        for (inflow in txdata.value.inflow)  // The outputs that went to myself (change) should get the cost basis of the inputs
                        {
                            val inflowRecord = paymentHistory[inflow]!!
                            inflowRecord.priceWhenIssued = getPrice(inflowRecord.date, priceWhatFiat)
                            inflowRecord.priceWhatFiat = priceWhatFiat
                            inflowRecord.isChange = false
                        }
                    }

                    // remove this tx from txdata since its finished processing
                    txdataIter.remove()
                    progress += 1
                }

            }
            if (progress == 0)
            {
                LogIt.warning("No progress")
                giveUp += 1
                if (giveUp == 5)
                {
                    // TODO (doesn't work with complex tx, see top comment)
                    LogIt.severe("No progress resolving wallet incoming/outgoing flows: cost basis will be inaccurate")
                    return
                }
            }
        }

         */
    }

    fun statsDump():String
    {
        val stats = statistics()
        val ret = StringBuilder()
        ret.append(name + " Balance: ${balance} (${balanceConfirmed}.${balanceUnconfirmed}) -- Transactions: ${stats.numTransactions}  Txos: ${stats.totalTxos}  Utxos: ${numUtxos()}  Used Addresses: ${stats.numUsedAddrs}  Unused Addresses: ${stats.numUnusedAddrs}  Tx Cache: ${stats.txCacheSize} of ${maxTxCache}  Txo Cache: ${stats.txoCacheSize} of ${maxTxoCache}")
        return ret.toString()
    }

    /** print debugging data to the log */
    fun debugDump()
    {
        val s = StringBuilder()
        synchronized(dataLock) {
            s.append("Wallet " + name + " Balance: Confirmed: " + balanceConfirmed + " Unconfirmed: " + balanceUnconfirmed + "\n")
            s.append("  " + "Unspent:\n")
            forEachTxo {
                s.append("    " + it.amount + " on " + it.addr + " outpoint " + it.outpoint?.toHex() + "\n")
                false
            }

            s.append("  " + "Unconfirmed:\n")

            forEachTxo { v ->
                // Skip if the output was subsequently spent by an unconfirmed tx
                if ((v.spentHeight == -1L) && (v.commitUnconfirmed > 0) && (v.spentUnconfirmed == false))
                {
                    var tx = getTx(v.commitTxIdem)?.tx
                    s.append("    " + v.amount + " on " + v.addr + " outpoint " + v.outpoint?.toHex() + " prevout " + v.priorOutScript.toString())
                    if (tx != null)
                    {
                        val SperB = CurrencyDecimal(tx.fee) / CurrencyDecimal(tx.size)
                        val feeWarning = if (SperB < BigDecimal.fromDouble(MinFeeSatPerByte)) " !!UNRELAYABLE FEE!! " else ""
                        s.append(" fee: " + tx.fee + " sat/byte: " + NexaFormat.format(SperB) + feeWarning + "\n")
                        s.append("      TX: " + tx.idem + " inputs: " + tx.inputs.size + " outputs: " + tx.outputs.size)
                        val txhex = tx.toHex()
                        s.append("      TX hex (sz ${txhex.length}: $txhex")
                    }
                    s.append("\n")
                }
                false
            }
        }

        // Java log line is limited to 1000 chars
        val logLines = s.toString().split("\n")
        for (line in logLines)
        {
            for (chunkedLine in line.chunked(900))  // Log max length is 1000 bytes, this gives room for the log prefix
                LogIt.info(chunkedLine)
        }
    }
}


sealed class WalletStartup
object NEW_WALLET : WalletStartup()
//object LOAD_WALLET : WalletStartup()

/** Helper class that saves/loads data needed by the Bip44Wallet */
class Bip44WalletData() : BCHserializable
{
    var id: String = ""

    var secretWords: String = ""

    //var secret: ByteArray = byteArrayOf()
    var maxAddress: Int = 0

    var chainSelector: ChainSelector = ChainSelector.NEXA

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        return BCHserialized(format).add(id).add(secretWords).addUint32(maxAddress.toLong()).addUint16(chainSelector.v.toLong())
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        id = stream.deString()
        secretWords = stream.deString()
        maxAddress = stream.deint32()
        try  // TODO remove after a bit
        {
            chainSelector = ChainSelectorFromValue(stream.deuint16().toByte())
        }
        catch (e: Exception)
        {
            // old save format so use default chainselector
        }
        return stream
    }
}

fun GetWalletData(name: String, db: WalletDatabase): Bip44WalletData
{
    //LogIt.info("deserialize " + name)
    val wd: Bip44WalletData = Bip44WalletData()
    wd.BCHdeserialize(BCHserialized(db.get("bip44wallet_" + name), SerializationType.DISK))
    //LogIt.info("done deserialize " + name)
    return wd
}


/** Return the Bip44 address number based on this blockchain */
fun Bip44AddressDerivationByChain(chainSelector: ChainSelector): Long
{
    return when (chainSelector)
    {
        ChainSelector.NEXA -> AddressDerivationKey.NEXA
        ChainSelector.NEXATESTNET -> AddressDerivationKey.NEXA
        ChainSelector.NEXAREGTEST -> AddressDerivationKey.NEXA

        ChainSelector.BCH -> AddressDerivationKey.BCH
        ChainSelector.BCHTESTNET -> AddressDerivationKey.BCH
        ChainSelector.BCHREGTEST -> AddressDerivationKey.BCH
    }
}

/** This wallet uses a single piece of random data, deriving new private keys and addresses using the technique described in [BIP-0032](https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki) with key derivation paths
 *  specified by [BIP-043](https://github.com/bitcoin/bips/blob/master/bip-0043.mediawiki) and [BIP-044](https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki).
 *  In essence, key derivation is:  m/<purpose=44>'/<coinType=0x91 for BCH>'/<account>'/<change=0 or 1>/index
 */
class Bip44Wallet(name: String, chainSelector: ChainSelector, var wdb: WalletDatabase) : CommonWallet(name, chainSelector)
{
    companion object
    {
        val COMMON_IDENTITY_SEED = ""

        /** The seed used to specify an identity used across all web sites where the user didn't choose a unique identity */
        val SECRET_SIZE = 16
        val SEED_SIZE = 64
        val MIN_RESAVE_TIME = 10000
    }

    /** Wallet secret seed */
    public var secretWords: String = ""

    /** User enters this to unlock the wallet */
    var passCode: String = ""

    /** Wallet seed derived from words + password */
    var secret: ByteArray = ByteArray(0) // initialize with a secret of the wrong size so it can't be accidentally used uninitialized // TODO keep this encrypted until needed

    /** No addresses have be calculated beyond this index */
    var maxAddress: Int = 0

    /** when was this wallet last saved */
    @OptIn(kotlin.time.ExperimentalTime::class)
    public var lastSave = TimeSource.Monotonic.markNow()

    var addressDerivationCoin: Long = Bip44AddressDerivationByChain(chainSelector)


    data class HdDerivationPath(val secret: ByteArray?, val purpose: Long, val coinType: Long, val account: Long, val change: Boolean, var index: Int)

    /** This variable captures HD derivation paths that should be monitored and spent but never offered as new payment destinations */
    val retrieveOnlyDerivationPaths = mutableListOf<HdDerivationPath>()

    public override fun toString(): String
    {
        val ret = StringBuilder(super.toString())
        ret.append(", maxAddress=$maxAddress\n")
        return ret.toString()
    }

    init
    {
        walletDb = wdb
    }

    /** Load an existing wallet */
    constructor(wdb: WalletDatabase, wd: Bip44WalletData) : this(wd.id, wd.chainSelector, wdb)
    {
        secretWords = wd.secretWords
        maxAddress = wd.maxAddress
        secret = generateBip39Seed(secretWords, passCode)
        loadWalletTx(wdb)
        // This is a patch that puts all identity addresses into the receiving because that was not happening upon creation
        fillReceivingWithIdentities()
    }

    /** Load an existing wallet */
    constructor(wdb: WalletDatabase, name: String) : this(wdb, GetWalletData(name, wdb))

    /** Create a new wallet with a random secret */
    constructor(wdb: WalletDatabase, name: String, chainSelector: ChainSelector, wop: WalletStartup) : this(name, chainSelector, wdb)
    {
        when (wop)
        {
            is NEW_WALLET ->
            {
                secretWords = GenerateBip39SecretWords(GenerateEntropy(SECRET_SIZE * 8))
                secret = generateBip39Seed(secretWords, passCode)
                fillReceivingWithRetrieveOnly()
                fillReceivingWithIdentities()
                saveBip44Wallet()
            }
            else ->
            {
                throw WalletException("Do not use this constructor unless you are making a new wallet")
            }
        }


        if (DEBUG)
        {
            // LogIt.info(sourceLoc() + " " + name + ": secret words: '" + secretWords + "' secret: " + secret.toHex())
        }
    }

    /** Create a new wallet given secret words */
    constructor(wdb: WalletDatabase, name: String, chainSelector: ChainSelector, secretWordList: String, maxAddr: Int = -1) : this(name, chainSelector, wdb)
    {
        secretWords = secretWordList
        secret = generateBip39Seed(secretWords, passCode)
        if (maxAddr == -1)  // TODO Search for addresses
        {
            maxAddress = 0
        }
        else
        {
            maxAddress = maxAddr
            // TODO add all prior addresses into this wallet and find balances
        }
        fillReceivingWithRetrieveOnly()
        fillReceivingWithIdentities()
        saveBip44Wallet()
    }

    public fun saveBip44Wallet()
    {
        val wd = Bip44WalletData()
        wd.id = name
        wd.secretWords = secretWords
        wd.maxAddress = maxAddress
        wd.chainSelector = chainSelector
        wdb.set("bip44wallet_" + name, wd.BCHserialize(SerializationType.DISK).toByteArray())  // walletDb must be inited by the app before any wallets are created
    }

    override fun getRetrieveOnlyDestinations(): MutableList<PayDestination>
    {
        val ret = mutableListOf<PayDestination>()
        for (path in retrieveOnlyDerivationPaths)
        {
            for (idx in 0..path.index)
            {
                val secret = path.secret ?: secret  // If the secret isn't specified, then use this account's secret
                val newSecret = libnexa.deriveHd44ChildKey(secret, path.purpose, path.coinType, path.account, path.change, idx)
                val dest = if (chainSelector.hasTemplates)
                    Pay2PubKeyTemplateDestination(chainSelector, UnsecuredSecret(newSecret.first), idx.toLong())
                else Pay2PubKeyHashDestination(chainSelector, UnsecuredSecret(newSecret.first), idx.toLong())
                LogIt.info(sourceLoc() + " " + name + ": retrieve only: " + dest.address.toString())
                ret.add(dest)
            }
        }
        return ret
    }

    @OptIn(kotlin.time.ExperimentalTime::class)
    override fun save(force: Boolean)
    {
        dataLock.lock {
            if (force || lastSave.elapsedNow().inWholeMilliseconds > MIN_RESAVE_TIME)
            {
                lastSave = TimeSource.Monotonic.markNow()
                saveBip44Wallet()
                super.save(force)
            }
        }
    }

    /** Forget all transaction and blockchain state, and the redo the search for wallet transactions.
     */
    @cli(Display.User, "Forget all transaction and blockchain state, and the redo the search for wallet transactions.")
    override fun rediscover(forgetAddresses: Boolean, noPrehistory: Boolean): Unit
    {
        synchronized(dataLock) {
            if (forgetAddresses) maxAddress = 0
        }
        super.rediscover(forgetAddresses, noPrehistory)

    }

    /** Forget all transaction and blockchain state, regenerate the first N addresses
     */
    public fun rediscoverAddresses(lastAddr: Long): Unit
    {
        LogIt.info(sourceLoc() + ": Generating ${lastAddr} addresses")
        synchronized(dataLock)
        {
            maxAddress = 0
            for (i in 0..lastAddr)
            {
                val dest = generateDestination()
                val destaddr = dest.address
                if (destaddr == null) throw WalletImplementationException("Generated destination needs to be representable as an address")
                receiving[destaddr] = dest
            }
            maxAddress = lastAddr.toInt()
        }
        LogIt.info(sourceLoc() + ": rediscovering tx in blockchain")
        rediscover(false)
    }

    override fun generateDestination(): PayDestination
    {
        val tmp = maxAddress
        maxAddress += 1
        // LogIt.info(sourceLoc() + " " + name + ": secret " + secret.toHex())
        val newSecret = libnexa.deriveHd44ChildKey(secret, AddressDerivationKey.BIP44, addressDerivationCoin, 0, false, tmp)
        val dest = if (chainSelector.hasTemplates)
            Pay2PubKeyTemplateDestination(chainSelector, UnsecuredSecret(newSecret.first), tmp.toLong())
        else Pay2PubKeyHashDestination(chainSelector, UnsecuredSecret(newSecret.first), tmp.toLong())  // Note, if multiple destination types are allowed, the wallet load/save routines must be updated
        // LogIt.info(sourceLoc() + " $name: New Destination ${newSecret.second}")
        return dest
    }

    override fun destinationFor(seed: String): PayDestination
    {
        val index = if (seed == COMMON_IDENTITY_SEED) 0  // Common identity uses index 0
            else  // Otherwise create a probabilistically unique identity for this seed
            {
                // To prevent someone from engineering a collision with a particular site, we get some info that is unique (and secret) to this wallet
                val uniquifier = libnexa.deriveHd44ChildKey(secret, AddressDerivationKey.BIP44, AddressDerivationKey.ANY, 0, false, 0x7fffffff)
                // Then we hash that data with the seed string & grab 4 bytes from that to get the unique identity
                val hash = libnexa.sha256(seed.toByteArray() + uniquifier.first)
                hash[0].toPositiveLong() + hash[1].toPositiveLong()*256L + hash[2].toPositiveLong()*256L*256L + (hash[3].toPositiveLong() and 0x7f) * 256L*256L*256L
            }

        return getDestinationAtIndex(index.toInt())
    }

    public fun getDestinationAtIndex(addrIndex: Int): PayDestination
    {
        assert(addrIndex >= 0)
        val privateKey = libnexa.deriveHd44ChildKey(secret, AddressDerivationKey.BIP44, AddressDerivationKey.ANY, 0, false, addrIndex)

        // We should use P2PKH destinations for now because 3rd party (BTC and BCH) message signature creation/verification libraries want P2PKH
        //if (chainSelector.hasTemplates)
        //  return Pay2PubKeyTemplateDestination(chainSelector, UnsecuredSecret(privateKey))
        //else

        return Pay2PubKeyHashDestination(chainSelector, UnsecuredSecret(privateKey.first), addrIndex.toLong())
    }


    /** Search a variety of derivation paths for unspent utxos and add them into this wallet if discovered.
     * These utxos are not swept, so may be forgotten if you recreate this wallet from a recovery key.
     */
    @cli(Display.Simple, "Search various derivation paths and add unspent coins into the wallet.  Provide the quantity (and starting point) of accounts and addresses.")
    @OptIn(kotlin.ExperimentalUnsignedTypes::class)
    fun recoverUnspent(wallet: Wallet, numAddrToSearch: Int, numAccountToSearch: Int = 1, startAddrIndex: Int = 0, startAccountIndex: Int = 0): List<Spendable>
    {
        val w = wallet as CommonWallet
        val hdw = wallet as Bip44Wallet

        val ec = w.blockchain.net.getElectrum()

        val ret = mutableListOf<Spendable>()

        for (actI in IntRange(startAccountIndex, startAccountIndex + numAccountToSearch - 1))
        {
            for (index in IntRange(startAddrIndex, startAddrIndex + numAddrToSearch - 1))
            {
                val act = actI.toLong()
                // println("account $act index $index")
                val derivs = listOf(
                  Pair("BIP44 hardened BTC (m/44'/0'/$act'/0/$index)", { idx: Int ->
                      libnexa.deriveHd44ChildKey(hdw.secret, AddressDerivationKey.BIP44, AddressDerivationKey.BTC, act, false, idx)
                  }),
                  Pair("BIP44 hardened BTC change (m/44'/$act'/0'/1/$index)", { idx: Int ->
                      libnexa.deriveHd44ChildKey(hdw.secret, AddressDerivationKey.BIP44, AddressDerivationKey.BTC, act, true, idx)
                  }),
                  Pair("BIP44 hardened common", { idx: Int ->  // Identity derivation
                      libnexa.deriveHd44ChildKey(hdw.secret, AddressDerivationKey.BIP44, AddressDerivationKey.ANY, act, false, idx)
                  }),
                  Pair("BIP44 hardened common", { idx: Int ->  // Identity change
                      libnexa.deriveHd44ChildKey(hdw.secret, AddressDerivationKey.BIP44, AddressDerivationKey.ANY, act, true, idx)
                  }),
                  Pair("BIP44 hardened BCH (m/44'/${AddressDerivationKey.BCH}'/$act'/0/$index)", { idx: Int ->
                      libnexa.deriveHd44ChildKey(hdw.secret, AddressDerivationKey.BIP44, AddressDerivationKey.BCH, act, false, idx)
                  }),
                  Pair("BIP44 hardened BCH change (m/44'/${AddressDerivationKey.BCH}'/$act'/1/$index)", { idx: Int ->
                      libnexa.deriveHd44ChildKey(hdw.secret, AddressDerivationKey.BIP44, AddressDerivationKey.BCH, act, true, idx)
                  }),
                )
                for (derivation in derivs)
                {
                    while (true)
                    {
                        val sk = derivation.second(index).first
                        val dest = Pay2PubKeyHashDestination(w.chainSelector, UnsecuredSecret(sk), index.toLong())
                        try
                        {
                            val utxos: List<Spendable> = ec.listUnspent(dest, 60000)

                            if (utxos.size > 0)
                            {
                                println("Found ${utxos.size} coins at derivation path: ${derivation.first} index: $index address: ${dest.address}")
                                ret += utxos
                            }
                            break
                        }
                        catch (e: ElectrumRequestTimeout)
                        {
                            println("Index $index, electrum timeout for ${dest.address}, retrying")
                        }
                    }
                }
            }
        }

        w.injectUnspent(*ret.toTypedArray())
        return ret
    }
}

