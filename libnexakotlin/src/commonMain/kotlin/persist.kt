package org.nexa.libnexakotlin

import BchDb.BchBlockHeaderTbl
import BchDb.BchDb
import KvDb.KvDb
import NexaDb.NexaBlockHeaderTbl
import NexaDb.NexaDb
import WalletDb.TxHistory
import KvDb.KvpDb
import WalletDb.WalletDb
import app.cash.sqldelight.TransactionWithoutReturn
import app.cash.sqldelight.db.QueryResult
import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.db.SqlSchema
import com.ionspin.kotlin.bignum.integer.BigInteger
import com.ionspin.kotlin.bignum.integer.Sign


private val LogIt = GetLog("BU.persist")

val CHAINTIP_MARKER:ByteArray = byteArrayOf(0)

fun BigInteger.toSizedByteArray(sz:Int):ByteArray
{
    val ba = this.toByteArray()
    if (ba.size == sz) return ba
    if (ba.size > sz) throw IllegalArgumentException("Desired byte array of $sz bytes cannot store BigInteger $this")
    val ret = ByteArray(sz)
    ba.copyInto(ret, sz-ba.size)
    return ret
}

interface KvpDatabase
{
    /** update or insert a key value pair into the database */
    fun set(key: ByteArray, value: ByteArray):Boolean

    /** look up the passed key, throwing DataMissingException if it does not exist */
    fun get(key: ByteArray): ByteArray

    /** look up the passed key, returning the value or null if it does not exist */
    fun getOrNull(key: ByteArray): ByteArray?

    /** update or insert a key value pair into the database */
    fun set(key: String, value: ByteArray) = set(key.encodeUtf8(), value)

    /** look up the passed key, returning the value or throwing DataMissingException */
    fun get(key: String): ByteArray = get(key.encodeUtf8())

    /** look up the passed key, returning the value or null if it does not exist */
    fun getOrNull(key: String): ByteArray? = getOrNull(key.encodeUtf8())

    /** delete a record */
    fun delete(key: String) = delete(key.encodeUtf8())

    /** delete a record */
    fun delete(key: ByteArray)

    /** Delete all items */
    fun clear()
}

interface TxDatabase
{
    fun delete(vararg idem: ByteArray)
    fun delete(vararg idem: Hash256)
    {
        val p = Array<ByteArray>(idem.size) { idem[it].hash }
        delete(*p)
    }

    fun size():Long

    /** Read one item */
    fun read(idem: Hash256):TransactionHistory?

    fun write(vararg txh: TransactionHistory?)

    /** Read all items and return a map of each entry */
    fun readAll():MutableMap<Hash256, TransactionHistory>

    /** Insert or overwrite all items in the provided map into the database */
    fun writeAll(fullmap:MutableMap<Hash256, TransactionHistory>)

    /** Insert or overwrite all changed items in the provided map into the database */
    fun writeDirty(fullmap:MutableMap<Hash256, TransactionHistory>): Int

    /** Delete all items */
    fun clear()

    fun forEachWithAddress(addr: PayAddress, doit: (TransactionHistory) -> Unit)

    fun forEach(doit: (TransactionHistory) -> Boolean)
}

interface TxoDatabase
{
    fun delete(outpoint: ByteArray)
    fun delete(outpoint: iTxOutpoint)

    fun delete(outpoints: Collection<iTxOutpoint>)

    fun read(outpoint: ByteArray): Spendable?
    fun read(outpoint: iTxOutpoint): Spendable?

    /** Read all items and return a map of each entry */
    fun readAll():MutableMap<iTxOutpoint, Spendable>

    /** Insert or overwrite all items in the provided map into the database */
    fun writeAll(fullmap:MutableMap<iTxOutpoint, Spendable>)

    /** Insert or overwrite all changed items in the provided map into the database */
    fun writeDirty(fullmap:MutableMap<iTxOutpoint, Spendable>): Int

    /** Insert or overwrite the passed txos into the database */
    fun write(splist: Collection<Spendable?>)
    fun write(vararg splist: Spendable?)

    /** Delete all items */
    fun clear()

    fun forEach(doit: (Spendable) -> Boolean)
    fun forEachUtxo(doit: (Spendable) -> Boolean)

    fun forEachWithAddress(addr: PayAddress, doit: (Spendable) -> Boolean)
    fun forEachUtxoWithAddress(addr: PayAddress, doit: (Spendable) -> Boolean)

    fun numUtxos():Long
    fun numTxos():Long
}

interface WalletDatabase
{
    val kvp: KvpDatabase
    val tx: TxDatabase
    val txo: TxoDatabase

    fun delete(key: ByteArray) = kvp.delete(key)
    fun delete(key: String) = kvp.delete(key)

    fun set(key: String, value: Int)
    {
        kvp.set(key.encodeUtf8(), BCHserialized(SerializationType.DISK, sizeHint = 8).addInt64(value.toLong()).toByteArray())
    }
    fun getInt(key: String):Int
    {
        return BCHserialized(SerializationType.DISK,get(key.encodeUtf8())).deint64().toInt()
    }
    fun set(key: String, value: Long)
    {
        kvp.set(key.encodeUtf8(), BCHserialized(SerializationType.DISK, sizeHint = 8).addInt64(value).toByteArray())
    }
    fun getLong(key: String):Long
    {
        return BCHserialized(SerializationType.DISK,get(key.encodeUtf8())).deint64()
    }


    /** update or insert a key value pair into the database */
    fun set(key: String, value: ByteArray) = kvp.set(key.encodeUtf8(), value)
    /** look up the passed key, returning the value or throwing DataMissingException */
    fun get(key: String): ByteArray = kvp.get(key.encodeUtf8())

    /** update or insert a key value pair into the database */
    fun set(key: ByteArray, value: ByteArray):Boolean = kvp.set(key,value)

    /** look up the passed key, throwing DataMissingException if it does not exist */
    fun get(key: ByteArray): ByteArray = kvp.get(key)

    /** look up the passed key, returning the value or null if it does not exist */
    fun getOrNull(key: ByteArray): ByteArray? = kvp.get(key)



}

// common type for Nexa and Bch header DAOs
interface BlockHeaderDatabase
{
    /** Get a main chain (a chain with the most work) header by its height */
    fun getHeader(height: Long): iBlockHeader?
    /** If a header identified by this hash does not exist, return null */
    fun getHeader(hash: ByteArray): iBlockHeader?
    /** Multiple chains may exist at this height so a list is returned */
    fun getHeadersAtHeight(height: Long): List<iBlockHeader>
    // Multiple chain tips might have the same total work so a list is returned
    fun getMostWorkHeaders(): List<iBlockHeader>
    // If a header identified by this hash does not exist, return null
    fun getHeader(hash: Hash256): iBlockHeader?

    // For quick access, the current main chain tip can be specially stored
    fun setCachedTipHeader(header: iBlockHeader)
    // Get the current main chain tip.
    fun getCachedTipHeader(): iBlockHeader?
    // Add a header into the DB.
    fun insertHeader(header: iBlockHeader)

    // Update or insert multiple headers into the database
    fun upsert(headers: Collection<iBlockHeader>)

    // Add a header into the DB, update the existing header if it does not match what is passed.  This limits database writes.
    fun diffUpsert(header: iBlockHeader)   // Read header, if different or nonexistent insert or update
    // Delete all headers.
    fun clear()

    // close this database, releasing any resources
    fun close()
}

val ZERO_BA32 = ByteArray(32, {0})
fun NexaBlockHeader.Companion.fromDb(db: NexaBlockHeaderTbl):NexaBlockHeader
{
    val ret = NexaBlockHeader()
    if (db.hash.size == 32) ret.assignHashData(Hash256(db.hash))  // otherwise its probably the tip -- anyway its bad and needs recalc.
    ret.hashAncestor = Hash256(db.hashAncestor ?: ZERO_BA32)
    ret.hashPrevBlock = Hash256(db.hashPrevBlock ?: ZERO_BA32)
    ret.diffBits = db.diffBits
    ret.hashMerkleRoot = Hash256(db.hashMerkleRoot ?: ZERO_BA32)
    ret.time = db.time
    ret.height = db.height
    ret.chainWork =  BigInteger.fromByteArray(db.chainWork, Sign.POSITIVE)
    ret.size = db.size
    ret.txCount = db.txCount
    ret.feePoolAmt = db.feePoolAmt
    ret.utxoCommitment = db.utxoCommitment  ?: byteArrayOf()
    ret.minerData = db.minerData ?: byteArrayOf()
    ret.nonce = db.nonce ?: byteArrayOf()
    return ret
}

fun BchBlockHeader.Companion.fromDb(db: BchBlockHeaderTbl):BchBlockHeader
{
    val ret = BchBlockHeader()
    ret.assignHashData(Hash256(db.hash))
    ret.hashAncestor = Hash256(db.hashAncestor ?: ZERO_BA32)
    ret.hashPrevBlock = Hash256(db.hashPrevBlock ?: ZERO_BA32)
    ret.diffBits = db.diffBits
    ret.hashMerkleRoot = Hash256(db.hashMerkleRoot ?: ZERO_BA32)
    ret.time = db.time
    ret.height = db.height
    ret.chainWork =  BigInteger.fromByteArray(db.chainWork, Sign.POSITIVE)
    ret.size = db.size
    ret.txCount = db.txCount
    ret.nonce = db.nonce
    return ret
}

class NexaBlockHeaderDb(val name: String, val sql: SqlDriver): BlockHeaderDatabase
{
    val db = NexaDb(sql)

    protected val EMPTY_BA_STAND_IN = byteArrayOf(0)
    // https://github.com/touchlab/SQLiter/issues/42
    // https://github.com/touchlab/SQLiter/pull/102/files
    fun avoid42(header: iBlockHeader):NexaBlockHeader
    {
        val h = header as NexaBlockHeader
        h.calcHash()  // we need to calc the real hash (if needed) before we put fake values in the empty fields
        val ret = NexaBlockHeader(h)
        if (ret.nonce.size == 0) ret.nonce = EMPTY_BA_STAND_IN
        if (ret.utxoCommitment.size == 0) ret.utxoCommitment = EMPTY_BA_STAND_IN
        if (ret.minerData.size == 0) ret.minerData = EMPTY_BA_STAND_IN
        return ret
    }

    fun undoAvoid42(bh: NexaBlockHeader)
    {
        if (bh.nonce contentEquals EMPTY_BA_STAND_IN) bh.nonce = byteArrayOf()
        if (bh.utxoCommitment contentEquals EMPTY_BA_STAND_IN) bh.utxoCommitment = byteArrayOf()
        if (bh.minerData contentEquals EMPTY_BA_STAND_IN) bh.minerData = byteArrayOf()
    }

    override fun getHeader(height: Long): iBlockHeader?
    {
        val q = db.nexaHeaderDatabaseQueries.getAtHeight(height)
        val item = q.executeAsOneOrNull()
        if (item == null) return null
        val ret = NexaBlockHeader.fromDb(item)
        undoAvoid42(ret)
        return ret
    }

    override fun getHeader(hash: ByteArray): iBlockHeader?
    {
        //LogIt.info("get db ${hash.toHex()}")
        val q = db.nexaHeaderDatabaseQueries.getByHash(hash)
        val item = q.executeAsOneOrNull()
        if (item == null) return null
        val ret = NexaBlockHeader.fromDb(item)
        undoAvoid42(ret)
        return ret
    }

    override fun getHeader(hash: Hash256): iBlockHeader? = getHeader(hash.hash)

    override fun getHeadersAtHeight(height: Long): List<iBlockHeader>
    {
        val q = db.nexaHeaderDatabaseQueries.getAtHeight(height)
        val lst =  q.executeAsList().map { NexaBlockHeader.fromDb(it)}
        lst.forEach { undoAvoid42(it)}
        return lst
    }

    override fun getMostWorkHeaders(): List<iBlockHeader>
    {
        val q = db.nexaHeaderDatabaseQueries.getByMostWork()
        val lst =  q.executeAsList().map { NexaBlockHeader.fromDb(it)}
        lst.forEach { undoAvoid42(it)}
        return lst
    }

    override fun setCachedTipHeader(header: iBlockHeader)
    {
        val n = avoid42(header)
        db.nexaHeaderDatabaseQueries.upsert(CHAINTIP_MARKER, n.hashPrevBlock.hash, n.diffBits, n.hashAncestor.hash, n.hashTxFilter.hash, n.time, n.height, n.chainWork.toSizedByteArray(32), n.size, n.feePoolAmt, n.utxoCommitment, n.minerData, n.nonce, n.hashMerkleRoot.hash, n.txCount)
    }

    override fun getCachedTipHeader(): iBlockHeader?
    {
        val q = db.nexaHeaderDatabaseQueries.getByHash(CHAINTIP_MARKER)
        val item = q.executeAsOneOrNull()
        if (item == null) return null
        val ret = NexaBlockHeader.fromDb(item)
        undoAvoid42(ret)
        return ret
    }

    override fun insertHeader(header: iBlockHeader)
    {
        //LogIt.info("insert db ${(header as NexaBlockHeader).hashData?.toHex() ?: 0} ")
        val n = avoid42(header)
        // do not recalc the hash for n, because avoid42 has to stick fake data in to avoid a bug
        // (hash, hashPrevBlock, diffBits, hashAncestor, hashTxFilter, time, height, chainWork, size, feePoolAmt, utxoCommitment, minerData, nonce, hashMerkleRoot, txCount)
        db.nexaHeaderDatabaseQueries.insert(header.hash.hash, n.hashPrevBlock.hash, n.diffBits, n.hashAncestor.hash, n.hashTxFilter.hash, n.time, n.height, n.chainWork.toSizedByteArray(32), n.size, n.feePoolAmt, n.utxoCommitment, n.minerData, n.nonce, n.hashMerkleRoot.hash, n.txCount)
    }

    override fun upsert(headers: Collection<iBlockHeader>)
    {
        db.nexaHeaderDatabaseQueries.transaction {
            for (h in headers)
            {
                val n = avoid42(h)
                db.nexaHeaderDatabaseQueries.upsert(n.hash.hash, n.hashPrevBlock.hash, n.diffBits, n.hashAncestor.hash, n.hashTxFilter.hash, n.time, n.height, n.chainWork.toSizedByteArray(32), n.size, n.feePoolAmt, n.utxoCommitment, n.minerData, n.nonce, n.hashMerkleRoot.hash, n.txCount)

            }
        }
    }

    override fun diffUpsert(header: iBlockHeader)
    {
        val n = avoid42(header)
        db.nexaHeaderDatabaseQueries.upsert(header.hash.hash, n.hashPrevBlock.hash, n.diffBits, n.hashAncestor.hash, n.hashTxFilter.hash, n.time, n.height, n.chainWork.toSizedByteArray(32), n.size, n.feePoolAmt, n.utxoCommitment, n.minerData, n.nonce, n.hashMerkleRoot.hash, n.txCount)
    }

    override fun clear()
    {
        db.transaction {
            db.nexaHeaderDatabaseQueries.deleteAll()
        }
    }

    override fun close()
    {
    }

}

class BchBlockHeaderDb(val name: String, val sql: SqlDriver): BlockHeaderDatabase
{
    val db = BchDb(sql)

    protected val EMPTY_BA_STAND_IN = byteArrayOf(0)
    // https://github.com/touchlab/SQLiter/issues/42
    // https://github.com/touchlab/SQLiter/pull/102/files
    fun avoid42(header: iBlockHeader):BchBlockHeader
    {
        val h = header as BchBlockHeader
        return h
        //val ret = BchBlockHeader(h)
        //if (ret.nonce.size == 0) ret.nonce = EMPTY_BA_STAND_IN
        //if (ret.utxoCommitment.size == 0) ret.utxoCommitment = EMPTY_BA_STAND_IN
        //if (ret.minerData.size == 0) ret.minerData = EMPTY_BA_STAND_IN
        // if (ret..size == 0) ret. = EMPTY_BA_STAND_IN
        //return ret
    }

    fun undoAvoid42(bh: BchBlockHeader)
    {
        //if (bh.nonce contentEquals EMPTY_BA_STAND_IN) bh.nonce = byteArrayOf()
        //if (bh.utxoCommitment contentEquals EMPTY_BA_STAND_IN) bh.utxoCommitment = byteArrayOf()
        //if (bh.minerData contentEquals EMPTY_BA_STAND_IN) bh.minerData = byteArrayOf()
    }

    override fun getHeader(height: Long): iBlockHeader?
    {
        val q = db.bchHeaderDatabaseQueries.getAtHeight(height)
        val item = q.executeAsOneOrNull()
        if (item == null) return null
        val ret = BchBlockHeader.fromDb(item)
        undoAvoid42(ret)
        return ret
    }

    override fun getHeader(hash: ByteArray): iBlockHeader?
    {
        val q = db.bchHeaderDatabaseQueries.getByHash(hash)
        val item = q.executeAsOneOrNull()
        if (item == null) return null
        val ret = BchBlockHeader.fromDb(item)
        undoAvoid42(ret)
        return ret
    }

    override fun getHeader(hash: Hash256): iBlockHeader? = getHeader(hash.hash)

    override fun getHeadersAtHeight(height: Long): List<out iBlockHeader>
    {
        val q = db.bchHeaderDatabaseQueries.getAtHeight(height)
        val lst =  q.executeAsList().map { BchBlockHeader.fromDb(it)}
        lst.forEach { undoAvoid42(it)}
        return lst
    }

    override fun getMostWorkHeaders(): List<iBlockHeader>
    {
        val q = db.bchHeaderDatabaseQueries.getByMostWork()
        val lst =  q.executeAsList().map { BchBlockHeader.fromDb(it)}
        lst.forEach { undoAvoid42(it)}
        return lst
    }

    override fun setCachedTipHeader(header: iBlockHeader)
    {
        val n = avoid42(header)
        db.bchHeaderDatabaseQueries.upsert(CHAINTIP_MARKER, n.hashPrevBlock.hash, n.diffBits, n.hashAncestor.hash, n.time, n.height, n.chainWork.toSizedByteArray(32), n.size, n.nonce, n.hashMerkleRoot.hash, n.txCount)
    }

    override fun getCachedTipHeader(): iBlockHeader?
    {
        val q = db.bchHeaderDatabaseQueries.getByHash(CHAINTIP_MARKER)
        val item = q.executeAsOneOrNull()
        if (item == null) return null
        val ret = BchBlockHeader.fromDb(item)
        undoAvoid42(ret)
        return ret
    }

    override fun insertHeader(header: iBlockHeader)
    {
        val n = avoid42(header)
        val tmp = n.calcHash()
        // (hash, hashPrevBlock, diffBits, hashAncestor, hashTxFilter, time, height, chainWork, size, feePoolAmt, utxoCommitment, minerData, nonce, hashMerkleRoot, txCount)
        db.bchHeaderDatabaseQueries.insert(tmp.hash, n.hashPrevBlock.hash, n.diffBits, n.hashAncestor.hash, n.time, n.height, n.chainWork.toSizedByteArray(32), n.size, n.nonce, n.hashMerkleRoot.hash, n.txCount)
    }

    override fun upsert(headers: Collection<iBlockHeader>)
    {
        TODO("Not yet implemented")
    }

    override fun diffUpsert(header: iBlockHeader)
    {
        val n = avoid42(header)
        val tmp = n.calcHash()
        db.bchHeaderDatabaseQueries.upsert(tmp.hash, n.hashPrevBlock.hash, n.diffBits, n.hashAncestor.hash, n.time, n.height, n.chainWork.toSizedByteArray(32), n.size, n.nonce, n.hashMerkleRoot.hash, n.txCount)
    }

    override fun clear()
    {
        db.transaction {
            db.bchHeaderDatabaseQueries.deleteAll()
        }
    }

    override fun close()
    {
    }

}


class SqldelightKvpDatabase(val name: String, val sql: SqlDriver): KvpDatabase
{
    val db = WalletDb(sql)
    override fun set(key: ByteArray, value: ByteArray): Boolean
    {
        db.wKvpQueries.upsert(key, value)
        return true
    }

    override fun get(key: ByteArray): ByteArray
    {
        val v = db.wKvpQueries.get(key).executeAsOneOrNull()
        if (v == null) throw DataMissingException(key.toHex())
        return v.valu
    }

    override fun getOrNull(key: ByteArray): ByteArray?
    {
        val v = db.wKvpQueries.get(key).executeAsOneOrNull()
        if (v == null) return null
        return v.valu
    }

    override fun delete(key: ByteArray)
    {
        db.wKvpQueries.delete(key)
    }

    override fun clear()
    {
        db.transaction {
            db.wKvpQueries.deleteAll()
        }
    }
}

class SqldelightKvDb(val name: String, val sql: SqlDriver): KvpDatabase
{
    val db = KvDb(sql)

    init {
        KvDb.Schema.create(sql)  // since the actual creation in the .sq file says IF NOT EXISTS, this will not recreate the table
    }

    override fun set(key: ByteArray, value: ByteArray): Boolean
    {
        db.kvpDbQueries.upsert(key, value)
        return true
    }

    override fun get(key: ByteArray): ByteArray
    {
        val v = db.kvpDbQueries.get(key).executeAsOneOrNull()
        if (v == null) throw DataMissingException(key.toHex())
        return v.valu
    }

    override fun getOrNull(key: ByteArray): ByteArray?
    {
            val query = db.kvpDbQueries.get(key)
            val v = query.executeAsOneOrNull()
            if (v == null) return null
        return v.valu
    }

    override fun delete(key: ByteArray)
    {
        db.kvpDbQueries.delete(key)
    }

    override fun clear()
    {
        db.transaction {
            db.kvpDbQueries.deleteAll()
        }
    }
}

class SqldelightTxoDatabase(val name: String, val sql: SqlDriver): TxoDatabase
{
    val db = WalletDb(sql)

    override fun delete(outpoint: ByteArray)
    {
        db.txoQueries.delete(outpoint)
    }
    override fun delete(outpoint: iTxOutpoint) = delete(outpoint.toByteArray(SerializationType.DISK))

    override fun delete(outpoints: Collection<iTxOutpoint>)
    {
        val tmp = outpoints.map { it.toByteArray(SerializationType.DISK) }
        db.transaction {
            for(outp in tmp)
               db.txoQueries.delete(outp)
        }
    }

    override fun read(outpoint: ByteArray): Spendable?
    {
        var ret:Spendable? = null
        val q = db.txoQueries.get(outpoint)
        q.execute { cursor ->
            if(cursor.next().value)
            {
                val tsv = q.mapper(cursor)
                val sp = Spendable(BCHserialized(tsv.spendable,SerializationType.DISK))
                sp.dirty = false  // I just read this from disk
                ret = sp
            }
            QueryResult.Unit
        }
        return ret
    }
    override fun read(outpoint: iTxOutpoint) = read(outpoint.toByteArray(SerializationType.DISK))

    override fun forEach(doit: (Spendable) -> Boolean)
    {
        val q = db.txoQueries.getAll()
        q.execute { cursor ->
            while(cursor.next().value)
            {
                val tsv = q.mapper(cursor)
                try
                {
                    val sp = Spendable(BCHserialized(tsv.spendable, SerializationType.DISK))
                    if (doit(sp) == true) break
                }
                catch(e: DeserializationException)
                {
                    // should I just delete this spendable, rediscover the wallet? or what
                    logThreadException(e)
                }
            }
            QueryResult.Unit
        }
    }

    override fun forEachUtxo(doit: (Spendable) -> Boolean)
    {
        val q = db.txoQueries.getAllUtxo()
        q.execute { cursor ->
            while(cursor.next().value)
            {
                val tsv = q.mapper(cursor)
                val sp = Spendable(BCHserialized(tsv.spendable,SerializationType.DISK))
                if (doit(sp)==true) break
            }
            QueryResult.Unit
        }
    }

    override fun forEachWithAddress(addr: PayAddress, doit: (Spendable) -> Boolean)
    {
        val q = db.txoQueries.getByAddress(addr.toString())
        q.execute { cursor ->
            while(cursor.next().value)
            {
                val tsv = q.mapper(cursor)
                val sp = Spendable(BCHserialized(tsv.spendable,SerializationType.DISK))
                if (doit(sp)==true) break
            }
            QueryResult.Unit
        }
    }
    override fun forEachUtxoWithAddress(addr: PayAddress, doit: (Spendable) -> Boolean)
    {
        val q = db.txoQueries.getUtxoByAddress(addr.toString())
        q.execute { cursor ->
            while(cursor.next().value)
            {
                val tsv = q.mapper(cursor)
                val sp = Spendable(BCHserialized(tsv.spendable,SerializationType.DISK))
                if (doit(sp)==true) break
            }
            QueryResult.Unit
        }
    }

    override fun readAll(): MutableMap<iTxOutpoint, Spendable>
    {
        val ret = mutableMapOf<iTxOutpoint, Spendable>()
        val q = db.txoQueries.getAll()
        q.execute { cursor ->
            while(cursor.next().value)
            {
                val tsv = q.mapper(cursor)
                val sp = Spendable(BCHserialized(tsv.spendable,SerializationType.DISK))
                sp.dirty = false  // I just read this from disk
                val spo = sp.outpoint
                if (spo == null) throw DeserializationException("Spendable is null")
                ret[spo] = sp
            }
            QueryResult.Unit
        }
        return ret
    }

    override fun numUtxos():Long
    {
        val q = db.txoQueries.utxoSize()
        val result =  q.executeAsOne()

        val q2 = db.txoQueries.getAllUtxo()
        var tallied = 0
        var actuallyUnspent = 0
        q2.execute { cursor ->
            while(cursor.next().value)
            {
                val tsv = q2.mapper(cursor)
                val sp = Spendable(BCHserialized(tsv.spendable,SerializationType.DISK))
                if (!sp.isUnspent)
                {
                    LogIt.info("getAllUtxo gave me a SPENT UTXO")
                }
                else actuallyUnspent++
                if (tsv.spentDate >= 0) LogIt.info("getAllUtxo gave me a UTXO with spentDate >= 0")
                tallied++
            }
            QueryResult.Unit
        }
        LogIt.info(sourceLoc() +": UTXOS $result $tallied $actuallyUnspent ")
        return result
    }
    override fun numTxos():Long
    {
        val q = db.txoQueries.size()
        return q.executeAsOne()
    }

    // For efficiency duplicate the code in the next two routines rather than copying the array into a collection
    override fun write(vararg splist: Spendable?)
    {
        db.transaction {
            for (sp in splist)
            {
                if (sp!=null)
                {
                    val id = sp.outpoint
                    if (id != null)
                    {
                        db.txoQueries.upsert(id.toByteArray(SerializationType.DISK), sp.toByteArray(SerializationType.DISK),
                            sp.commitDate,
                            sp.spentDate,
                            sp.amount,
                            sp.groupInfo?.tokenAmt ?: 0,
                            sp.groupInfo?.groupId?.toByteArray(),
                            sp.addr?.toString() ?: ""
                        )
                    }
                }
            }
        }
    }

    override fun write(splist: Collection<Spendable?>)
    {
        db.transaction {
            for (sp in splist)
            {
                if (sp!=null)
                {
                    val id = sp.outpoint
                    if (id != null)
                    {
                        db.txoQueries.upsert(id.toByteArray(SerializationType.DISK), sp.toByteArray(SerializationType.DISK),
                            sp.commitDate,
                            sp.spentDate,
                            sp.amount,
                            sp.groupInfo?.tokenAmt ?: 0,
                            sp.groupInfo?.groupId?.toByteArray(),
                            sp.addr?.toString() ?: ""
                        )
                    }
                }
            }
        }
    }

    override fun writeAll(fullmap: MutableMap<iTxOutpoint, Spendable>)
    {
        db.transaction {
            for (tx in fullmap)
            {
                val sp = tx.value
                val id = tx.key
                //assert(h.chainSelector == chainSelector, "chainselector mismatch ${h.chainSelector} vs $chainSelector")

                db.txoQueries.upsert(id.toByteArray(SerializationType.DISK), sp.toByteArray(SerializationType.DISK),
                    sp.commitDate,
                    sp.spentDate,
                    sp.amount,
                    sp.groupInfo?.tokenAmt ?: 0,
                    sp.groupInfo?.groupId?.toByteArray(),
                    sp.addr?.toString() ?: ""
                    )
            }
        }
    }

    override fun writeDirty(fullmap: MutableMap<iTxOutpoint, Spendable>): Int
    {
        var recordsWritten = 0
        db.transaction {
            for (tx in fullmap)
            {
                val sp = tx.value
                val id = tx.key
                if (sp.dirty)
                {
                    //assert(h.chainSelector == chainSelector, "chainselector mismatch ${h.chainSelector} vs $chainSelector")
                    db.txoQueries.upsert(id.toByteArray(SerializationType.DISK), sp.toByteArray(SerializationType.DISK),
                        sp.commitDate,
                        sp.spentDate,
                        sp.amount,
                        sp.groupInfo?.tokenAmt ?: 0,
                        sp.groupInfo?.groupId?.toByteArray(),
                        sp.addr?.toString() ?: ""
                    )
                    sp.dirty = false
                    recordsWritten++
                }
            }
        }
        return recordsWritten
    }

    override fun clear()
    {
        db.transaction {
            db.txoQueries.deleteAll()
        }
        assert(numTxos() == 0L)
    }

}


fun TransactionWithoutReturn.thUpsert(db: WalletDb, idem: ByteArray, h: TransactionHistory)
{
    val histSer = h.toByteArray(SerializationType.DISK)
    db.txHistoryQueries.upsert(idem, histSer,
                        h.date,
                        h.confirmedHeight,
                        h.confirmedHash?.hash, h.incomingAmt - h.outgoingAmt, h.note, h.priceWhatFiat, h.priceWhenIssued.toPlainString())

    for (inp in h.tx.inputs)
    {
        val tmp = inp.script.address
        if (tmp != null)
            db.txHistoryQueries.txInvolvesAddress(idem, tmp.data)
    }
    for (outp in h.tx.outputs)
    {
        val tmp = outp.script.address
        if (tmp != null)
            db.txHistoryQueries.txInvolvesAddress(idem, tmp.data)
    }
}


class SqldelightTxDatabase(val name: String, val sql: SqlDriver): TxDatabase
{
    var chainSelector: ChainSelector? = null
    val db = WalletDb(sql)

    override fun size():Long
    {
        return db.txHistoryQueries.size().executeAsOne()
    }
    override fun clear()
    {
        db.txHistoryQueries.deleteAll()
    }

    override fun delete(vararg idems: ByteArray)
    {
        db.transaction {
            for (idem in idems)
                db.txHistoryQueries.delete(idem)
        }
    }

    // implement full map load/save to maintain func but switch to this underlying db
    override fun writeAll(fullmap:MutableMap<Hash256, TransactionHistory>)
    {
        db.transaction {
            for (tx in fullmap)
            {
                val h = tx.value
                val idem = tx.key.hash
                assert(h.chainSelector == chainSelector, "chainselector mismatch ${h.chainSelector} vs $chainSelector")
                thUpsert(db,tx.key.hash,h)
            }
        }
    }

    override fun writeDirty(fullmap:MutableMap<Hash256, TransactionHistory>): Int
    {
        var ret = 0
        db.transaction {
            for (tx in fullmap)
            {
                val h = tx.value
                val idem = tx.key.hash

                if (h.dirty)
                {
                    assert(h.chainSelector == chainSelector, "chainselector mismatch ${h.chainSelector} vs $chainSelector")
                    thUpsert(db,tx.key.hash,h)
                    h.dirty = false
                    ret++
                }
            }
        }

        return ret
    }

    override fun read(idem: Hash256):TransactionHistory?
    {
        val q = db.txHistoryQueries.getByIdem(idem.hash)
        val tsv = q.executeAsOneOrNull()
        if (tsv!=null) return TransactionHistory(chainSelector!!, BCHserialized(tsv.txData, SerializationType.DISK))
        return null
    }

    override fun write(vararg txh: TransactionHistory?)
    {
        db.transaction {
            for (h in txh)
            {
                if (h != null)
                {
                    val idem = h.tx.idem.hash
                    assert(h.chainSelector == chainSelector, "chainselector mismatch ${h.chainSelector} vs $chainSelector")
                    thUpsert(db, idem, h)
                }
            }
        }
    }

    override fun readAll():MutableMap<Hash256, TransactionHistory>
    {
        val ret = mutableMapOf<Hash256, TransactionHistory>()
        val q = db.txHistoryQueries.getAll()
        q.execute { cursor ->
            while(cursor.next().value)
            {
                val tsv = q.mapper(cursor)
                ret[Hash256(tsv.idem)] = TransactionHistory(chainSelector!!, BCHserialized(tsv.txData, SerializationType.DISK))
            }
            QueryResult.Unit
        }
        return ret
    }

    override fun forEach(doit: (TransactionHistory) -> Boolean)
    {
        val q = db.txHistoryQueries.getAllByDate()
        q.execute { cursor ->
            while(cursor.next().value)
            {
                val tsv = q.mapper(cursor)
                val tmp = TransactionHistory(chainSelector!!, BCHserialized(tsv.txData, SerializationType.DISK))
                if (doit(tmp)) break
            }
            QueryResult.Unit
        }
    }

    override fun forEachWithAddress(addr: PayAddress, doit: (TransactionHistory) -> Unit)
    {
        db.transaction {
            val q = db.txHistoryQueries.txWithAddress(addr.data)
            q.execute { cursor ->
                while (cursor.next().value)
                {
                    val twa = q.mapper(cursor)
                    val idem = twa.txIdem
                    if (idem != null)
                    {
                        val result = db.txHistoryQueries.getByIdem(idem)
                        result.execute { cursor2 ->
                            while (cursor2.next().value)
                            {
                                val tsv = result.mapper(cursor2)
                                val txh = TransactionHistory(chainSelector!!, BCHserialized(tsv.txData, SerializationType.DISK))
                                doit(txh)
                            }
                            QueryResult.Unit
                        }

                    }

                }
                QueryResult.Unit
            }
        }
    }
}

class SqldelightWalletDatabase(val name: String, val drvr: SqlDriver, chainSelector: ChainSelector? = null): WalletDatabase
{
    override val kvp: KvpDatabase = SqldelightKvpDatabase(name, drvr)
    override val tx: TxDatabase = SqldelightTxDatabase(name, drvr)
    override val txo: TxoDatabase = SqldelightTxoDatabase(name, drvr)

    init
    {
        val key = chainSelectorDbKey(name)
        if (chainSelector == null)  // if you are going to create a wallet db, you must specify the chain
        {
            val key = chainSelectorDbKey(name)
            println("key: $key")
            val csName = kvp.get(key).decodeToString()
            println("csName: $csName")
            (tx as SqldelightTxDatabase).chainSelector = uriToChain[csName]!!
        }
        else
        {
            (tx as SqldelightTxDatabase).chainSelector = chainSelector
            kvp.set(key, chainToURI[chainSelector]!!.encodeToByteArray())
        }
    }

}

/*  ROOM db type converters kept here in case they are needed at some point
class Hash256Converters
{
    @TypeConverter
    fun fromByteArray(value: ByteArray?): Hash256?
    {
        return value?.let { Hash256(it) }
    }

    @TypeConverter
    fun toByteArray(bid: Hash256?): ByteArray?
    {
        return bid?.hash
    }
}

class BigIntegerConverters
{
    @TypeConverter
    fun fromByteArray(value: ByteArray?): BigInteger?
    {
        return value?.let {
            var ret = 0.toBigInteger()
            for (b in it)
            {
                ret = ret.shiftLeft(8)
                ret += b.toPositiveInt().toBigInteger()
            }
            ret
        }
    }

    @TypeConverter
    fun toByteArray(bid: BigInteger?): ByteArray?
    {
        if (bid == null) return null
        var ret = ByteArray(32)
        var value: BigInteger = bid
        for (i in 1..32)  // By converting by hand we are sure that the byte order means that lexicographical compare is equivalent to numerical compare
        {
            ret[32 - i] = value.and(255.toBigInteger()).toByte()
            value = value.shiftRight(8)

        }
        return ret
    }
}

 */

expect fun createDbDriver(dbname: String, schema: SqlSchema<QueryResult.Value<Unit>>): SqlDriver
//expect fun createNexaDbDriver(dbname: String): SqlDriver
//expect fun createKvpDbDriver(dbname: String): SqlDriver
fun openNexaDB(name: String): BlockHeaderDatabase?
{
    // println("opening nexa db ${name}.db")
    val drvr = createDbDriver(name + ".db", NexaDb.Schema)
    return  NexaBlockHeaderDb(name, drvr)
}
fun openBchDB(name: String): BlockHeaderDatabase?
{
    val drvr = createDbDriver(name + ".db", BchDb.Schema)
    return  BchBlockHeaderDb(name, drvr)
}

/** If you are going to create a wallet DB, you must specify the chain.  Otherwise its optional (will be loaded from the db) */
fun openWalletDB(name: String, chainSelector: ChainSelector? = null): WalletDatabase?
{
    val drvr = createDbDriver(name + ".db", WalletDb.Schema)
    return  SqldelightWalletDatabase(name, drvr, chainSelector)
}


/** Open a plain key/value pair database for general storage */
fun openKvpDB(name: String): KvpDatabase?
{
    val drvr = createDbDriver(name + ".db", KvDb.Schema)
    return SqldelightKvDb(name, drvr)
}
