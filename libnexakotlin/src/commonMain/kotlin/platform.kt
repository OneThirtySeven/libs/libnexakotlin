package org.nexa.libnexakotlin

import com.ionspin.kotlin.bignum.decimal.BigDecimal
import kotlinx.datetime.Clock

interface iLogging
{
    val module: String

    fun severe(s:String) = error(s)
    fun error(s:String)
    fun warning(s:String)
    fun info(s:String)
}

/** Provide the line separator for this platform */
expect val lineSeparator: String

/** In platforms that use an application context object (Android), return that object.
 * This is meant to be called only in platform specific upper layers, so each platform specific
 * code should "know" what to cast the returned Any into.
 * @return null or the platform specific application context variable
 * */
expect fun appContext():Any?

/** Returns seconds since the epoch */
expect fun epochSeconds(): Long

/** Returns milliseconds since the epoch */
expect fun epochMilliSeconds(): Long

expect fun String.urlEncode(): String

/** Convert a byte array that contains a utf8 string into a String */
expect fun ByteArray.decodeUtf8():String

/** Convert a string into a utf-8 encoded byte array. */
expect fun String.encodeUtf8():ByteArray


/** Return the source location of the caller (for debugging and logging) */
expect fun sourceLoc(): String

/** get the logging object for this platform */
expect fun GetLog(module: String): iLogging

expect fun generateBip39Seed(wordseed: String, passphrase: String, size: Int = 64): ByteArray

expect fun initializeLibNexa(variant: String? = ""): LibNexa

expect class DecimalFormat constructor (fmtSpec: String)
{
    fun format(num:BigDecimal) : String
}

/** convert a domain name into an address
 * @return The resolved name.  The type of the resolution e.g. ipv4, ipv6, tor, is implied by its length. */
expect fun resolveDomain(domainName: String, port: Int?=null): List<ByteArray>

/** Return the epoch time in milliseconds */
fun millinow() = Clock.System.now().toEpochMilliseconds()

/** Return false if this node has no internet connection, or true if it does or null if you can't tell */
expect fun iHaveInternet():Boolean?

/** Return the directory that this application may use for its files */
@Deprecated("all file io should be already relative to this")
expect fun getFilesDir(): String?

/** Path should not contain extension (will be added) & directories may be ignored if the platform places its databases in a standard location.
 *  @return true if something was actually deleted, false if not, null if don't know */
expect fun deleteDatabase(name: String):Boolean?

/** given a byte array, convert it into some string form of IP address
 * Should support ipv4 and ipv6 at a minimum
 */
expect fun ipAsString(ba:ByteArray):String

expect fun epochToDate(epochSeconds: Long): String

