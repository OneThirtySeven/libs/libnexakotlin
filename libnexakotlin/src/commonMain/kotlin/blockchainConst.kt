package org.nexa.libnexakotlin

const val MinFeeSatPerByte = 1.01
const val DesiredFeeSatPerByte = 1.1
const val MaxFeePerByte = 5.0
var MaxFee = 50000  //!< Sanity check by refusing to create a tx bigger than this fee

//!< If we fall below this many unused addresses, make more.  This happens whenever the wallet "sees" an address being used by a transaction.
// In the situation where a foreign same-seed wallet is spending, we might miss some transactions if more than this number of addresses are
// used in a block.  The situation occurs because tx are not committed in creation order in blocks, so if we are tracking N unused addresses,
// the N+1th unused address could appear in the block first, but we haven't yet started tracking it.  Ofc, no money is lost, the SPV wallet
// just doesn't 'see' the deposit/UTXO.
// Set this number high enough that such a situation would be extremely unlikely.
// Also  to avoid race conditions when installing the bloom filter and using an address, it makes sense to generate the new ones well before
// they are used.
const val MIN_UNUSED_ADDRESSES = 200

//!< How many addresses to generate in a single "chunk".  Every time we generate a new address, we need to send a new bloom filter to nodes.  So
// let's not do that one at a time.
const val DEFAULT_GEN_ADDRESS_CHUNK_SIZE = 20
const val PREHISTORY_SAFEFTY_FACTOR = 60 * 60 * 2 //!< 2 hours in seconds

// Approximate size of signature in a script -- used for guessing fees
const val APPROX_P2PKH_SIG_SCRIPT_LEN = 65 + 33; // sig + pubkey
const val TX_SCRIPTLESS_INPUT_SIZE = 32 + 4 + 4  // prevout hash + prevout index + sequence

const val BCH_SIGHASH_ALL = 0x41
const val BCH_SIGHASH_SINGLE = 0x43
const val BCH_SIGHASH_NONE = 0x42
const val BCH_SIGHASH_ANYONECANPAY = 0x80

const val BLOCK_REQ_TIMEOUT = 5000.toLong()
const val ELECTRUM_REQ_TIMEOUT = 5000

const val NUM_BLOCKCHAIN_PROCESSING_THREADS = 2

const val BCHtestnetPort = 18333
const val BCHregtestPort = 18444
const val BCHregtest2Port = 8334  // for multicurrency tests, you can create 2 separate regtest chains on different ports
val BCHmainnetPort = 8333
const val NexaPort = 7228
const val NexaTestnetPort = 7230
const val NexaRegtestPort = 18444
const val NexaRegtestRpcPort = 18332

//val CASH_MAGIC_BYTES = hashMapOf("mainnet" to byteArrayOf(0xe3.toByte(),0xe1.toByte(),0xf3.toByte(),0xe8.toByte() ) )

//val BCH_CASH_MAGIC_TESTNET3 = byteArrayOf(0xf4.toByte(), 0xe5.toByte(), 0xf3.toByte(), 0xf4.toByte())
val BCH_CASH_MAGIC_TESTNET4 = byteArrayOf(0xe2.toByte(), 0xb7.toByte(), 0xda.toByte(), 0xaf.toByte())
val BCH_CASH_MAGIC_REGTEST = byteArrayOf(0xda.toByte(), 0xb5.toByte(), 0xbf.toByte(), 0xfa.toByte())
val REGTEST_NET_ID = byteArrayOf(0xea.toByte(), 0xe5.toByte(), 0xef.toByte(), 0xea.toByte())
val BCH_CASH_MAGIC_MAINNET = byteArrayOf(0xe3.toByte(), 0xe1.toByte(), 0xf3.toByte(), 0xe8.toByte())
val NEXA_NET_ID = byteArrayOf(0x72.toByte(), 0x27.toByte(), 0x12.toByte(), 0x21.toByte())
val NEXA_TESTNET_NET_ID = byteArrayOf(0x72.toByte(), 0x27.toByte(), 0x12.toByte(), 0x22.toByte())

val PROTOCOL_VERSION = 80003
val CLIENT_SUBVERSION = "nexalight:0.1.5.1"

val MAX_QUIET_TIME_SEC = 120
val PING_QUIET_TIME_SEC = 35

val BlockchainNetMagic = mapOf<ChainSelector, ByteArray>(
    ChainSelector.BCH to BCH_CASH_MAGIC_MAINNET, ChainSelector.BCHTESTNET to BCH_CASH_MAGIC_TESTNET4, ChainSelector.BCHREGTEST to BCH_CASH_MAGIC_REGTEST,
    ChainSelector.NEXA to NEXA_NET_ID, ChainSelector.NEXAREGTEST to REGTEST_NET_ID, ChainSelector.NEXATESTNET to NEXA_TESTNET_NET_ID)
val BlockchainPort = mapOf<ChainSelector, Int>(
    ChainSelector.NEXA to NexaPort, ChainSelector.NEXAREGTEST to NexaRegtestPort, ChainSelector.NEXATESTNET to NexaTestnetPort,
    ChainSelector.BCH to BCHmainnetPort, ChainSelector.BCHTESTNET to BCHtestnetPort, ChainSelector.BCHREGTEST to BCHregtestPort

)

fun dust(chain: ChainSelector): Long
{
    // All the supported chains currently have the same dust limit
    return 546L
}