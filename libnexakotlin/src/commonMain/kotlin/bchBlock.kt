// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.nexa.libnexakotlin

private val LogIt = GetLog("BU.bchblock")


/** Header of a block for bitcoin-family cryptocurrencies */
@cli(Display.Simple, "Bitcoin Cash Block header")
open class BchBlockHeader() : CommonBlockHeader()
{
    @cli(Display.Simple, "block version")
    var version: Int = 0

    //@cli(Display.Simple, "previous block hash")
    //var hashPrevBlock = Hash256()

    //@cli(Display.Simple, "merkle root hash")
    //var hashMerkleRoot = Hash256()

    //@cli(Display.Simple, "block timestamp")
    //var time: Long = 0L

    //@cli(Display.Simple, "difficulty in 'bits' representation")
    //var diffBits: Long = 0L // nBits

    @cli(Display.Simple, "nonce")
    var nonce: Long = 0L

    // renamed txCount in base class
    // This data is not part of the consensus header
    //@cli(Display.Simple, "number of tx in this block")
    //var numTx: Long = -1

    // renamed size in base class
    //@cli(Display.Simple, "block size in bytes")
    //var blockSize: Long = -1

    // renamed chainWork in base class
    //@cli(Display.Simple, "cumulative work")
    //var cumulativeWork: BigInteger = 0.toBigInteger()

    //@cli(Display.Simple, "block height")
    //var height: Long = -1

    //@cli(Display.Simple, "ancestor")
    //var hashAncestor = Hash256() // For more rapid rewind, an ancestor is stored when this block is saved

    companion object
    {
        fun fromHex(hex: String, serializationType: SerializationType = SerializationType.UNKNOWN): BchBlockHeader
        {
            val bytes = hex.fromHex()
            var header = BchBlockHeader()
            header.deserHeaderFields(BCHserialized(bytes, serializationType))
            return header
        }
    }

    override fun equals(b:Any?): Boolean
    {
        if (!(b is BchBlockHeader)) return false
        if (hash == b.hash) return true
        return false
    }

    /** Force recalculation of hash. To access the hash just use #hash */
    override fun calcHash(): Hash256
    {
        // call header serialize explicitly in case derived class has overridden BCHserialize
        var ser = BchSerializeHeader(SerializationType.HASH)
        val h = Hash256(libnexa.hash256(ser.toByteArray()))
        hashData = h
        return h
    }

    override fun validate(cs: ChainSelector): Boolean
    {
        // TODO:  For now we will assume that BCH blocks are correct
        return true
    }

    /** assignment constructor */
    constructor(_hash: Hash256, _version: Int, _time: Long, _diffBits: Long, _nonce: Long, _hashMerkleRoot: Hash256, _hashPrevBlock: Hash256) : this()
    {
        assignHashData(_hash)

        version = _version
        time = _time
        diffBits = _diffBits
        nonce = _nonce
        hashMerkleRoot = _hashMerkleRoot
        hashPrevBlock = _hashPrevBlock

    }

    constructor(stream: BCHserialized) : this() //!< deserializing constructor
    {
        BCHdeserialize(stream)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized = BchSerializeHeader(format) //!< Serializer

    fun BchSerializeHeader(format: SerializationType): BCHserialized
    {
        var serialized = BCHserialized(format).addInt32(version).add(hashPrevBlock).add(hashMerkleRoot).addInt32(time).addUint32(diffBits).addUint32(nonce)

        if (format != SerializationType.HASH)
            BCHserialized.compact(txCount)

        if (format == SerializationType.DISK)
        {
            serialized.add(BCHserialized.compact(size))
            serialized.add(hashAncestor)
            serialized.add(BCHserialized.uint256(chainWork))
        }
        return serialized
    }

    /** Deserialize just the header fields (used when this header is included in other objects/messages */
    fun deserHeaderFields(stream: BCHserialized): BCHserialized
    {
        // header fields
        version = stream.deint32()
        hashPrevBlock.BCHdeserialize(stream)
        hashMerkleRoot.BCHdeserialize(stream)
        time = stream.deuint32()
        diffBits = stream.deuint32()
        nonce = stream.deuint32()
        return stream
    }

    /** Deserializer
    Note that this includes the number of tx in NETWORK serialization as a legacy of the P2P network protocol.
    In DISK serialization, the block size in bytes is included.

    When the header is part of the block, the numTx is not included as an independent field so use deserHeaderFields() API or HASH SerializationType
     */
    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        deserHeaderFields(stream)

        if (stream.format == SerializationType.NETWORK) txCount = stream.decompact()  // P2P Network only

        if (stream.format == SerializationType.DISK)
        {
            size = stream.decompact()
            hashAncestor.BCHdeserialize(stream)
            chainWork = stream.deuint256()
        }
        return stream
    }
}

/** A block that contains a subset of the total number of transactions, and includes a merkle proof that the provided transactions are part of the block */
class BchMerkleBlock(val chainSelector: ChainSelector) : BchBlockHeader(), iMerkleBlock
{
    // Hashes of either transactions or merkle sub-trees
    //var hashes: List<Hash256>? = null
    // Bit data that describes how the hashes are used to form a merkle proof.
    //var merkleProofPath: ByteArray? = null

    //* the ids of the transactions provided by this merkle block
    override val txHashes: MutableSet<Hash256> = mutableSetOf()

    //* the actual transactions provided by this merkle block
    override val txes: MutableList<BchTransaction> = mutableListOf()

    val lock = org.nexa.threads.Mutex()

    /** Determine whether all matching transactions have been acquired.
     * @return true if all needed transactions are available in this merkle block */
    override fun complete(): Boolean
    {
        return lock.synchronized {
                if (txHashes.size == 0)
                {
                    //LogIt.finer("Merkle block provided ${txes.size} of ${numTx} total transactions")
                    true
                }  // If we are out of txHashes, txes should be full
                else
                {
                    assert(txes.size < txCount) // otherwise it should not be full
                    false
                }
            }
    }

    /** Call to offer a transaction that this merkleblock might contain.
     * @return true if this merkleblock consumed this transaction */
    override fun txArrived(tx: iTransaction): Boolean
    {
        return lock.synchronized {
            if (txHashes.contains(tx.id))
            {
                txes.add(tx as BchTransaction)
                txHashes.remove(tx.id)
                //LogIt.finer(sourceLoc() + ": Merkle block ${hash.toHex()} tx arrived ${tx.hash.toHex()}.  ${txHashes.size} left to find (of ${numTx}).")
                true
            }
            else false
        }
    }

    constructor(chainSelector: ChainSelector, stream: BCHserialized) : this(chainSelector) //!< stream constructor
    {
        BCHdeserialize(stream)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized //!< Serializer
    {
        throw NotImplementedError()
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized //!< Deserializer
    {
        deserHeaderFields(stream)
        txCount = stream.deuint32()
        val h: Array<ByteArray> = stream.delist { Hash256(it).hash }.toTypedArray()  // The merkle block proof data, containing either inner hashes or tx hashes
        val mpp = stream.deByteArray()

        val result = libnexa.extractFromMerkleBlock(txCount.toInt(), mpp, h)  // Validate the merkle block proof, extracting the transaction hashes
        if ((result == null) || (result.size == 0) || (Hash256(result[0]) != hashMerkleRoot))
        {
            throw DeserializationException("Merkle block inconsistent", "network error")
        }

        //val logstr = StringBuilder()
        //logstr.append("Deserialize merkle block: ${hash.toHex()}, requires ${result.size-1} (${numTx}) TX: ")
        // Fill with the hashes we need.  result[0] is the merkle root
        for (i in 1 until result.size)
        {
            txHashes.add(Hash256(result[i]))
            //logstr.append(Hash256(result[i]).toHex())
            //logstr.append(" ")
        }

        //LogIt.finer(sourceLoc() + " " + logstr.toString())
        return stream
    }
}

class BchBlock(val chainSelector: ChainSelector) : iBlock, BchBlockHeader()
{
    override var txes: MutableList<BchTransaction> = mutableListOf()

    constructor(chainSelector: ChainSelector, stream: BCHserialized) : this(chainSelector)
    {
        BCHdeserialize(stream)
    }


    override fun BCHserialize(format: SerializationType): BCHserialized //!< Serializer
    {
        // serialization for hash calc is only the header, not the tx.  merkle root in header captures tx entropy
        val data = if (format == SerializationType.HASH)
            super.BCHserialize(format)
        else super.BCHserialize(format) + txes
        return data
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized //!< Deserializer
    {
        deserHeaderFields(stream)
        txes = stream.delist { BchTransaction(chainSelector, it) }
        txCount = txes.size.toLong()
        return stream
    }
}
