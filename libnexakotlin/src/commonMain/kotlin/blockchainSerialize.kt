// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.nexa.libnexakotlin

import org.nexa.libnexakotlin.libnexa
import com.ionspin.kotlin.bignum.integer.BigInteger
import com.ionspin.kotlin.bignum.integer.Sign
import com.ionspin.kotlin.bignum.integer.util.fromTwosComplementByteArray
import okio.ArrayIndexOutOfBoundsException
import okio.Buffer
import okio.internal.commonToUtf8String

val CHOP_SIZE: Int = 8192

open class DeserializationException(msg: String, shortMsg: String? = null) : LibNexaException(msg, shortMsg, ErrorSeverity.Expected)


enum class SerializationType
{
    NETWORK, DISK, HASH, SCRIPTHASH, UNKNOWN
}

class Codec
{
    companion object
    {
        /** Convert a byte array to a string using 64 values per character.  This format is used in Bitcoin to convert message signatures to a printable representation */
        fun encode64(data: ByteArray): String = libnexa.encode64(data)

        /** Convert an encoded string (using 64 values per character) to bytes.  This format is used in Bitcoin to convert message signatures to a printable representation */
        fun decode64(data: String): ByteArray = libnexa.decode64(data)
    }
}

/** This class behaves like a function and its sole purpose is to wrap a bytearray into another class so that when we serialize it we know not to include the length */
class exactBytes(val data: ByteArray)

/** This class behaves like a function and its sole purpose is to wrap a bytearray into another class so that when we serialize it we know to include the length */
class variableSized(val data: ByteArray)

// Base class allowing derived classes to specify BCH serialization capability
// For deserialization, create a constructor that accepts a BCHSerialized object
interface BCHserializable
{
    // Store this object into a BCHserialized stream and return it
    fun BCHserialize(format: SerializationType = SerializationType.UNKNOWN): BCHserialized
    // Load this object with data from the passed stream
    fun BCHdeserialize(stream: BCHserialized): BCHserialized

    fun toByteArray(format: SerializationType = SerializationType.UNKNOWN): ByteArray
    {
        return BCHserialize(format).toByteArray()
    }
}

class ByteArraySlice(val data: ByteArray, val start: Int, val size: Int)
{
    fun joinToString(separator: CharSequence=", ", prefix: CharSequence="", postfix: CharSequence="", limit: Int = -1, truncated: CharSequence="...", transform: ((Byte) -> CharSequence)?): String
    {
        val ret = StringBuilder()
        ret.append(prefix)
        var first = true
        for(index in range(start, size))
        {
            val item = data[index]
            if (limit>=0 && (index-start)==limit) { ret.append(truncated); break }
            if (first) first = false
            else ret.append(separator)
            ret.append(if (transform!=null) transform(item) else item.toString())
        }
        ret.append(postfix)
        return ret.toString()
    }

    fun toHex(): String
    {
        return joinToString("") { it.toUByte().toString(radix = 16).padStart(2, '0') }
    }
}

// This class serializes or deserializes bitcoin cash data
class BCHserialized(_format: SerializationType, notCopiedData: ByteArray? = null, sizeHint:Int = 1024) // = SerializationType.UNKNOWN)
{
    //var data = if (notCopiedData == null) MutableList<ByteArray>(0, { _ -> ByteArray(0) }) else MutableList<ByteArray>(1, { _ -> notCopiedData })
    var data = if (notCopiedData == null) ByteArray(sizeHint) else notCopiedData
    var format: SerializationType = _format
    var serPos: Int = if (notCopiedData == null) 0 else notCopiedData.size
    var deserPos: Int = 0

    init {
        if (notCopiedData != null)
        {
            serPos = notCopiedData.size
        }
    }


    constructor(contents: ByteArray, _format: SerializationType) : this(_format, contents) // =SerializationType.UNKNOWN): this()
    {
        format = _format
        // data.add(contents)
    }

    constructor(contents: MutableList<ByteArray>, _format: SerializationType) : this(_format, contents.join())
    {
    }

    constructor(contents: BCHserialized) : this(contents.format, contents.toByteArray())
    {
    }

    /** Return how many bytes are left to be read */
    fun availableBytes(): Int = serPos - deserPos

    /** If we happen to be at the end of the received data, rewind to the beginning */
    fun lazyReset()
    {
        if (serPos == deserPos)
        {
            deserPos = 0
            serPos = 0
        }


    }

    /** Return an object that allows access to the unconsumed data */
    fun slice(): ByteArraySlice
    {
        return ByteArraySlice(data, deserPos, serPos - deserPos)
    }

    // Converts the internal representation to a single ByteArray
    fun toByteArray(): ByteArray
    {
        if ((serPos==0) && (deserPos==data.size)) return data  // the serialized buffer is exactly what we need
        return data.sliceArray(0 until serPos)
        /*
        if (data.size > 1)
            data = MutableList<ByteArray>(1, { _ -> data.join() })
        if (data.size == 0) return ByteArray(0)

        if (deserPos > CHOP_SIZE)
        {
            // reclaim memory by chopping off data that we've already consumed
            data[0] = data[0].sliceArray(deserPos..(data[0].size - 1))
            deserPos = 0
            size = data[0].size
        }
        return data[0]

         */
    }

    fun expand(amt:Int = 0)
    {
        val newsz = if (amt == 0) 2*data.size else data.size + amt
        val d = ByteArray(newsz)
        data.copyInto(d)
        data = d
    }

    fun reserve(amt: Int)
    {
        if (serPos + amt > data.size) expand(max(data.size, amt))
    }

    // Serialize a 32 bit integer into this object
    fun addUint8(num: Int): BCHserialized
    {
        reserve(1)
        data[serPos] = num.toByte()
        serPos++
        return this
    }

    // Serialize a 32 bit integer into this object
    fun addUint8(num: Long): BCHserialized
    {
        reserve(1)
        data[serPos] = num.toByte()
        serPos++
        return this
    }


    /** Append a single character to this object */
    fun add(c: Char): BCHserialized
    {
        reserve(1)
        data[serPos] = c.code.toByte()
        serPos++
        return this
    }

    fun add(v: Byte): BCHserialized
    {
        reserve(1)
        data[serPos] = v
        serPos++
        return this
    }

    // Serialize a 32 bit integer into this object
    fun addUint16(num: Long): BCHserialized
    {
        reserve(2)
        var cur = num
        for (i in 0..1)
        {
            data[serPos] = (cur and 0xFF).toByte()
            cur = cur ushr 8
            serPos++
        }
        return this
    }

    fun addUint16(num: Int): BCHserialized
    {
        reserve(2)
        var cur = num
        for (i in 0..1)
        {
            data[serPos] = (cur and 0xFF).toByte()
            cur = cur ushr 8
            serPos++
        }
        return this
    }

    // Serialize a 32 bit integer into this object
    fun addInt16(num: Long): BCHserialized
    {
        reserve(2)
        var cur = num
        for (i in 0..1)
        {
            data[serPos] = (cur and 0xFF).toByte()
            cur = cur ushr 8
            serPos++
        }
        return this
    }

    /** Serialize a 32 bit integer into this object */
    fun addInt16(num: Int): BCHserialized
    {
        reserve(2)
        var cur = num
        for (i in 0..1)
        {
            data[serPos] = (cur and 0xFF).toByte()
            cur = cur ushr 8
            serPos++
        }
        return this
    }


    // Serialize a 32 bit integer into this object
    fun addUint32(num: Long): BCHserialized
    {
        reserve(4)
        var cur = num
        for (i in 0..3)
        {
            data[serPos] = (cur and 0xFF).toByte()
            cur = cur ushr 8
            serPos++
        }
        return this
    }

    // Serialize a 32 bit integer into this object
    fun addInt32(num: Long): BCHserialized
    {
        reserve(4)
        var cur = num
        for (i in 0..3)
        {
            data[serPos] = (cur and 0xFF).toByte()
            cur = cur ushr 8
            serPos++
        }
        return this
    }

    /** Serialize a 32 bit integer into this object */
    fun addInt32(num: Int): BCHserialized
    {
        reserve(4)
        var cur = num
        for (i in 0..3)
        {
            data[serPos] = (cur and 0xFF).toByte()
            cur = cur ushr 8
            serPos++
        }
        return this
    }

    /** Serialize a 64 bit integer into this object */
    fun addUint64(num: ULong): BCHserialized = addInt64(num.toLong())
    /** Serialize a 64 bit integer into this object */
    fun addUint64(num: Long): BCHserialized = addInt64(num)

    /** Serialize a 64 bit integer into this object */
    fun addInt64(num: Long): BCHserialized
    {
        reserve(8)
        var cur = num
        for (i in 0..7)
        {
            data[serPos] = (cur and 0xFF).toByte()
            cur = cur ushr 8
            serPos++
        }
        return this
    }

    /** Append a serialized object to this serialized object */
    fun add(contents: BCHserializable) = add(contents.BCHserialize(this.format))

    /** Append a serialized object to this serialized object
     * Network formats do not support this, but occasionally we serialize into a network format to store as a binary blob on disk
     * */
    fun addNullable(contents: BCHserializable?): BCHserialized
    {
        if (contents == null) addUint8(0)
        else
        {
            addUint8(1)
            add(contents)
        }
        return this
    }

    /** Append a serialized object to this serialized object */
    fun add(contents: BCHserialized): BCHserialized
    {
        assert((this.format == SerializationType.UNKNOWN) || (contents.format == SerializationType.UNKNOWN) || (contents.format == this.format))
        if (this.format == SerializationType.UNKNOWN) this.format = contents.format  // grab a known format if I don't know mine
        reserve(contents.serPos)
        try
        {
            contents.data.copyInto(data, serPos, 0, contents.serPos)
        }
        catch (e:ArrayIndexOutOfBoundsException)
        {
            println("BUG")
            reserve(contents.serPos)
        }
        serPos += contents.serPos
        return this
    }

    /** Append a serialized object to this serialized object */
    fun plusAssign(contents: BCHserialized) = add(contents)

    fun addExactBytes(contents: AbstractCollection<ByteArray>): BCHserialized
    {
        val spaceNeeded = contents.fold(0, { inp, item -> inp+item.size})
        reserve(spaceNeeded)
        contents.forEach { this.addExactBytes(it) }
        return this
    }

    // Append the serialization of the parameter into this object and return this object
    fun addlist(b: MutableList<out BCHserializable>): BCHserialized
    {
        add(BCHserialized.compact(b.size.toLong()))
        for (i in b)
        {
            add(i)
        }
        return this
    }

    /** Add these exact bytes to the serialization. (NO LENGTH FIELD : this does not add a serialized byte array) */
    fun addExactBytes(newdata: ByteArray): BCHserialized
    {
        reserve(newdata.size)
        newdata.copyInto(data, serPos,0, newdata.size)
        serPos+=newdata.size
        return this
    }

    // add some new data on to the end of this buffer
    fun addExactBytes(newdata: ByteArray, len: Int): BCHserialized
    {
        reserve(len)
        newdata.copyInto(data, serPos,0, len)
        serPos+=len
        return this
    }

    fun add(b: Boolean): BCHserialized
    {
        reserve(1)
        data[serPos] = if (b) 1 else 0
        serPos++
        return this
    }

    /** Serialize a bitcoin-style "compact" integer */
    fun addCompact(num: Long): BCHserialized
    {
        if (num < 253)
        {
            return addUint8(num)
        }
        if (num < 0x10000)
        {
            return addUint8(253).addUint16(num)
        }
        if (num < 0x100000000)
        {
            return addUint8(254).addUint32(num)
        }
        return addUint8(255).addInt64(num)
    }

    fun addCompact(num: Int): BCHserialized = addCompact(num.toLong())

    // Start deserialization at a specified position (by default, restart)
    fun deserializeReset(pos: Int = 0)
    {
        deserPos = pos
    }

    /** Deserialize certain number of bytes as a ByteArray */
    fun debytes(len: Long): ByteArray
    {
        val tmp = deserPos
        deserPos += len.toInt()
        try
        {
            return data.sliceArray(IntRange(tmp, (tmp + len - 1).toInt()))
        }
        catch (e: IndexOutOfBoundsException)
        {
            throw DeserializationException(e.toString())
        }
    }

    /** Deserialize certain number of bytes as a ByteArray */
    fun debytes(into:ByteArray, len: Int, start: Int=0)
    {
        val tmp = deserPos
        deserPos += len
        try
        {
            data.copyInto(into,start, tmp, tmp+len)
        }
        catch (e: IndexOutOfBoundsException)
        {
            throw DeserializationException(e.toString())
        }
    }

    fun deboolean(): Boolean
    {
        val v: Byte = data[deserPos]
        deserPos += 1
        return v != 0.toByte()
    }

    /** Deserialize an 8 bit integer */
    fun deuint8(): Int
    {
        val ret: Int = data[deserPos].toPositiveInt()
        deserPos += 1
        return ret
    }
    /** Deserialize an 8 bits (as a signed value) */
    fun debyte(): Byte
    {
        val ret: Byte = data[deserPos]
        deserPos += 1
        return ret
    }

    /** Deserialize a character */
    fun dechar(): Char
    {
        val ret: Byte = data[deserPos]
        deserPos += 1
        return Char(ret.toInt())
    }

    /** Deserialize a 16 bit integer */
    fun deuint16(): Int
    {
        var ret: Int = 0
        for (i in 1 downTo 0)
        {
            val piece = data[deserPos + i].toPositiveInt()
            ret = (ret shl 8) or piece
        }
        deserPos += 2
        return ret
    }

    /** Deserialize a 16 bit BigEndian unsigned integer */
    fun deBigEndianUint16(): UInt
    {
        var ret: UInt = (data[deserPos].toUByte().toUInt() shl 8) + data[deserPos + 1].toUByte()
        deserPos += 2
        return ret
    }

    /** Deserialize a 32 bit integer */
    fun deint32(): Int
    {
        var ret: Int = 0
        for (i in 3 downTo 0)
        {
            val piece = data[deserPos+i].toPositiveInt()
            ret = (ret shl 8) or piece
        }
        deserPos+=4
        return ret
    }

    /** Deserialize a 32 bit unsigned integer -- must be returned as a long since kotlin integers are signed */
    fun deuint32(): Long
    {
        var ret: Long = 0
        for (i in 3 downTo 0)
        {
            val piece = data[deserPos + i].toPositiveLong()
            ret = (ret shl 8) or piece
        }
        deserPos+=4
        return ret
    }

    // Deserialize a 64 bit unsigned integer
    fun deuint64(): Long
    {
        var ret: Long = 0
        for (i in 7 downTo 0)
        {
            val piece = data[deserPos + i].toPositiveLong()
            ret = (ret shl 8) or piece
        }
        deserPos += 8
        return ret
    }

    /** Deserialize a 64 bit signed integer */
    fun deint64(): Long = deuint64()

    fun deuint256(): BigInteger
    {
        val tmp = debytes(32)
        tmp.reverse()
        return BigInteger.fromByteArray(tmp, sign = Sign.POSITIVE)  // converts from a big-endian byte array
    }

    fun addUint256(num: BigInteger): BCHserialized
    {
        val data = num.toByteArray()  // Note that this is not necessarily 32 bytes -- its the smallest # of bytes that the # fits in
        data.reverse()
        addExactBytes(data)  // append the actual number
        addExactBytes(ByteArray(32-data.size))  // add any MSB 0s to make it 32 bytes
        return this
    }

    // Deserialize a Bitcoin-style "compact" integer
    fun decompact(): Long
    {
        var sz: Int = data[deserPos].toPositiveInt()
        deserPos++
        if (sz < 253)
        {
            return sz.toLong()
        }
        if (sz == 253)
        {
            return deuint16().toLong()
        }
        if (sz == 254)
        {
            return deuint32().toLong()
        }
        else return deuint64()
    }

    fun devarint(): Long
    {
        var ret = 0L
        while (true)
        {
            var chData: Long = deuint8().toLong()
            ret = (ret shl 7) or (chData and 0x7f)
            if (chData >= 0x80)
            {
                ret++;
            }
            else
            {
                return ret;
            }
        }
    }

    fun addVarint(num: Long): BCHserialized
    {
        var tmp = ByteArray((8*8 + 6)/7)
        var n = num
        var pos:Int = 0
        while (true)
        {
            tmp[pos] = ((n and 0x7F) or ( if (pos != 0) 0x80L else 0L)).toByte()
            if (n <= 0x7F) break
            n = (n shr 7) - 1
            pos += 1
        }
        // Now write it backwards
        val tmp1:ByteArray = tmp.slice(pos downTo 0).toByteArray()
        addExactBytes(tmp1)
        return this
    }

    fun deString(): String
    {
        val length = decompact()
        val ba = debytes(length)
        return ba.decodeUtf8()
    }

    fun denullString(): String?
    {
        val length = decompact()
        if (length == 0L) return null
        val buf = Buffer()
        buf.write(debytes(length))
        return buf.readUtf8()
    }

    /** Deserialize a list of some object.  You must pass a factory function that takes a buffer
    and returns an instance of the object (consuming some of the buffer) */
    fun <T> delist(Tfactory: (BCHserialized) -> T): MutableList<T>
    {
        val len = decompact().toInt()
        var ret = MutableList<T>(len, { _ -> Tfactory(this) })
        return ret
    }

    /** Deserialize a map of key value pair objects.  You must pass 2 factory functions that each take a buffer
    and returns an instance of the key or value object (consuming some of the buffer) */
    fun <K, V> demap(Kfactory: (BCHserialized) -> K, Vfactory: (BCHserialized) -> V): MutableMap<K, V>
    {
        val len = decompact().toInt()
        var ret: MutableMap<K, V> = mutableMapOf()
        var count = 0
        while (count < len)
        {
            count += 1
            val k = Kfactory(this)
            val v = Vfactory(this)
            ret[k] = v
        }
        return ret
    }
    fun <K, V> demap(ret: AbstractMutableMap<K, V>, Kfactory: (BCHserialized) -> K, Vfactory: (BCHserialized) -> V): AbstractMutableMap<K, V>
    {
        val len = decompact().toInt()
        var count = 0
        while (count < len)
        {
            count += 1
            val k = Kfactory(this)
            val v = Vfactory(this)
            ret[k] = v
        }
        return ret
    }


    /** Deserialize a variable length array (vector) of bytes
     * Note that the opposite of this (serializing a array of bytes) is ambiguous
     * Do you want to serialize the exact bytes passed, or serialize an array of bytes
     * (that is, indicate the length in the serialization).
     * To serialize an array of bytes (the opposite of this) use the "variableSized" object like this:
     * satoshiSerializedObject + variableSized(yourByteArray)
     * */
    fun deByteArray(): ByteArray
    {
        val len = decompact().toLong()
        var ret = debytes(len)
        return ret
    }

    /** calculate the double SHA256 of the data in this object */
    fun hash256():Hash256
    {
        return Hash256(libnexa.hash256(this.toByteArray()))
    }
    /** calculate the SHA256 of the data in this object */
    fun sha256():Hash256
    {
        return Hash256(libnexa.sha256(this.toByteArray()))
    }

    companion object
    {
        fun wrap(contents: ByteArray, _format: SerializationType):BCHserialized
        {
            return BCHserialized(_format, contents)
        }

        fun <T> list(lst: List<T>, Tserializer: (T) -> BCHserialized, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var ret = BCHserialized.compact(lst.size, format)
            for (i in lst)
            {
                ret.add(Tserializer(i))
            }
            return ret
        }

        fun list(lst: Collection<BCHserializable>, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var ret = BCHserialized.compact(lst.size, format)
            for (i in lst)
            {
                ret.add(i.BCHserialize(format))
            }
            return ret
        }

        /** Serializes a map object with the provided key and value serialization functions */
        fun <K, V> map(mp: Map<K, V>, Kserializer: (K) -> BCHserialized, Vserializer: (V) -> BCHserialized, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var ret = BCHserialized.compact(mp.size, format)
            for (i in mp)
            {
                ret.add(Kserializer(i.key))
                ret.add(Vserializer(i.value))
            }
            return ret
        }

        /** Serializes a map object that contains serializable keys and values */
        fun map(mp: Map<out BCHserializable, BCHserializable>, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var ret = BCHserialized.compact(mp.size, format)
            for (i in mp)
            {
                ret.add(i.key.BCHserialize(format))
                ret.add(i.value.BCHserialize(format))
            }
            return ret
        }

        /** Serializes a map object that contains serializable keys and values */
        fun mutableMap(mp: MutableMap<out BCHserializable, out BCHserializable>, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var ret = BCHserialized.compact(mp.size, format)
            for (i in mp)
            {
                ret.add(i.key.BCHserialize(format))
                ret.add(i.value.BCHserialize(format))
            }
            return ret
        }

        // Serialize into a 32 bit integer
        fun int32(num: Long, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var data = ByteArray(4)
            var cur = num
            for (i in 0..3)
            {
                data[i] = (cur and 0xFF).toByte()
                cur = cur ushr 8;
            }
            return BCHserialized(data, format)
        }

        fun int32(num: Int, format: SerializationType = SerializationType.UNKNOWN): BCHserialized = int32(num.toLong(), format)

        // Serialize into a 16 bit integer
        fun uint16(num: Long, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var data = ByteArray(2)
            var cur = num
            for (i in 0..1)
            {
                data[i] = (cur and 0xFF).toByte()
                cur = cur ushr 8;
            }
            return BCHserialized(data, format)
        }

        fun boolean(b: Boolean): BCHserialized
        {
            var data = byteArrayOf(0)
            if (b) data[0] = 1
            return BCHserialized(data, SerializationType.UNKNOWN)
        }

        /** Serialize into an unsigned 8 bit number */
        fun uint8(num: Long): BCHserialized
        {
            var data = ByteArray(1)

            data[0] = (num and 0xFF).toByte()
            return BCHserialized(data, SerializationType.UNKNOWN)
        }
        /** Serialize into an unsigned 8 bit number */
        fun uint8(num: Int) = uint8(num.toLong())

        /** Serialize into an 8 bit number */
        fun uint8(num: Byte, format: SerializationType = SerializationType.UNKNOWN): BCHserialized = BCHserialized(byteArrayOf(num), format)

        /** Serialize into a 32 bit unsigned number */
        fun uint32(num: Long, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            return int32(num, format)
        }

        fun uint256(num: BigInteger, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            val data = num.toByteArray()
            data.reverse()
            val ret = BCHserialized(data, format).addExactBytes(ByteArray(32-data.size))
            return ret
        }

        /** Serialize a bitcoin-style "compact" integer */
        fun compact(num: Long, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            if (num < 253)
            {
                return uint8(num.toByte(), format)
            }
            if (num < 0x10000)
            {
                return uint8(253.toByte(), format) + uint16(num)
            }
            if (num < 0x100000000)
            {
                return uint8(254.toByte(), format) + uint32(num)
            }
            return uint8(255.toByte()).addInt64(num)
        }

        fun compact(num: Int, format: SerializationType = SerializationType.UNKNOWN): BCHserialized = compact(num.toLong(), format)

        fun varint(num: Long, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            return BCHserialized(format).addVarint(num)
        }
    }

    /** Return a hex string of this serialized data */
    fun toHex(): String
    {
        return data.sliceArray(0 until serPos).toHex()
        /*
        val ret = StringBuilder()
        for (array in data)
        {
            for (b in array)
            {
                //ret.append(String.format("%02x", b))
                ret.append(HEX_CHARS[b.toPositiveInt() shr 4])
                ret.append(HEX_CHARS[b.toPositiveInt() and 0xf])
            }
        }
        return ret.toString()
         */
    }

    @Suppress("UNUSED_PARAMETER")
    @Deprecated("Ambiguous: Use '+ exactBytes(array)' to append the ByteArray directly into the serialization, or '+ variableSized(array)' to append as a sized array", level = DeprecationLevel.ERROR)
    operator fun plus(b: ByteArray): BCHserialized
    {
        assert(false);
        return this
    }

    /** Return a new serialization object that contains this and the passed parameter serialized as a 64 bit number */
    // operator fun plus(num: Long): BCHserialized = plus(num.toULong())

    /** @Deprecated Inefficient.  Use addXXX
     * Return a new serialization object that contains this and the passed parameter serialized as a 64 bit number */
    /*
    operator fun plus(num: ULong): BCHserialized
    {
        var data = ByteArray(8)
        var cur = num
        for (i in 0..7)
        {
            data[i] = (cur and 0xFF.toULong()).toByte()
            cur = cur shr 8
        }
        var ret = BCHserialized(this)
        ret.data.add(data)
        return ret
    }
     */

    /** Return a new serialization object that contains this and the passed parameter serialized as a 32 bit number */
    //operator fun plus(num: Int): BCHserialized
    //{
    //    return BCHserialized(this) + BCHserialized.int32(num.toLong(), this.format)
    //}

    /** Return a new serialization object that contains this and the passed parameter serialized as a 32 bit number */
    //operator fun plus(num: UInt): BCHserialized
    //{
    //    return BCHserialized(this) + BCHserialized.uint32(num.toLong(), this.format)
    //}

    /** Return a new serialization object that contains this and the passed parameter serialized as a 32 bit number */
    //operator fun plus(value: Boolean): BCHserialized
    //{
    //    return BCHserialized(this) + BCHserialized.uint8(if (value == true) 1 else 0, this.format)
    //}

    /** Construct containing a serialized string */
    // ambiguous with accidentally passing a hex buffer to the constructor (use add(s))
    //constructor(s: String, format: SerializationType) : this(format)
    //{
    //    this += s
    //}

    /** Append a serialized string to this object */
    operator fun plusAssign(s: String): Unit
    {
        add(s)
    }

    /** Append a serialized string to this object */
    fun add(s: String?): BCHserialized
    {
        if (s == null)
            add(compact(0))
        else
        {
            val ba = s.encodeUtf8()
            add(compact(ba.size))
            addExactBytes(ba)
        }
        return this
    }


    /** Return a new serialization object that contains this and the passed parameter serialized */
    operator fun plus(s: String?): BCHserialized
    {

        var ret = BCHserialized(this)
        if (s==null) ret.plusAssign("")
        else ret.plusAssign(s)
        return ret
    }

    /** Return a new serialization object that contains this and the passed parameter serialized */
    operator fun plus(s: variableSized): BCHserialized
    {
        var ret = BCHserialized(this)
        ret.addCompact(s.data.size)
        ret.addExactBytes(s.data)
        return ret
    }

    fun add(s: variableSized): BCHserialized
    {
        addCompact(s.data.size)
        addExactBytes(s.data)
        return this
    }

    fun addVariableSized(data: ByteArray): BCHserialized
    {
        addCompact(data.size)
        addExactBytes(data)
        return this
    }

    /** Return a new serialization object that contains this and the passed parameter */
    operator fun plus(b: BCHserialized): BCHserialized
    {
        // Get the format from the incoming object if it has one and I don't
        if (this.format == SerializationType.UNKNOWN) this.format = b.format
        assert((b.format == SerializationType.UNKNOWN) || (b.format == this.format))
        var ret = BCHserialized(this)
        ret.add(b)
        return ret
    }

    /** Return a new serialization object that contains this and the passed parameter */
    operator fun plus(b: BCHserializable): BCHserialized = this + b.BCHserialize(format)

    /** Return a new serialization object that contains this and the serialization of an array of serializable objects */
    operator fun plus(b: Array<out BCHserializable>): BCHserialized
    {
        var ret = BCHserialized(this)
        ret = ret + BCHserialized.compact(b.size.toLong())
        for (i in b)
        {
            ret = ret + i.BCHserialize(format)
        }
        return ret
    }

    // Return a new serialization object that contains this and the serialization of
    // a MutableList of serializable objects
    operator fun plus(b: MutableList<out BCHserializable>): BCHserialized
    {
        var ret = BCHserialized(this)
        ret = ret + BCHserialized.compact(b.size.toLong())
        for (i in b)
        {
            ret = ret + i.BCHserialize(format)
        }
        return ret
    }

    // Return a new serialization object that contains this and the serialization of
    // a MutableList of serializable objects
    fun add(b: MutableList<out BCHserializable>): BCHserialized
    {
        addCompact(b.size.toLong())
        for (i in b)
        {
            add(i.BCHserialize(format))
        }
        return this
    }

    //operator fun plus(b: Byte): BCHserialized
    //{
    //    return BCHserialized(this) + BCHserialized.uint8(b, this.format)
    //}



}

/** Deserialize any object that is nullable, but prepending a byte indicating whether this object is null or not.
 * This API should only be used for DISK serialization because this nullable technique is not part of the network protocol.
 * However, in one case we store to disk the network serialization of an object (if it exists).
 * */
fun <T : BCHserializable> BCHserialized.deNullable(Tfactory: (BCHserialized) -> T): T?
{
    if (deuint8() == 0) return null
    val ret = Tfactory(this)
    return ret
}

