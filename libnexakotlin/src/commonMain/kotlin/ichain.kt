package org.nexa.libnexakotlin

import com.ionspin.kotlin.bignum.integer.BigInteger
import com.ionspin.kotlin.bignum.integer.Sign
import com.ionspin.kotlin.bignum.integer.toBigInteger

private val LogIt = GetLog("BU.ichain")

interface iBlockHeader: BCHserializable
{
    /** Return this block's hash.  Uses a cached value if one exists.  If you change the block be sure to call calcHash() or invalidateHash() to update this value either greedily or lazily respectively */
    @cli(Display.Simple, "block hash")
    val hash: Hash256

    // Implement an equals in every concrete instance
    // override fun equals(b:Any?): Boolean

    /** Force recalculation of hash. To access the hash just use #hash, to force recalc call "changed()" */
    abstract fun calcHash(): Hash256

    /** Communicate that this object has been changed, so recalculation of cached data is needed */
    abstract fun changed()

    /** run checks (such as verifying POW claim) & return true if the block header is consistent */
    abstract fun validate(cs: ChainSelector): Boolean

    @cli(Display.Simple, "previous block hash")
    var hashPrevBlock: Hash256

    @cli(Display.Simple, "difficulty in 'bits' representation")
    var diffBits: Long

    @cli(Display.Simple, "ancestor")
    var hashAncestor: Hash256 // For more rapid rewind, an ancestor is stored when this block is saved

    @cli(Display.Simple, "merkle root hash")
    var hashMerkleRoot: Hash256

    @cli(Display.Simple, "block timestamp")
    var time: Long

    @cli(Display.Simple, "block height")
    var height: Long

    @cli(Display.Simple, "cumulative work in the chain")
    var chainWork: BigInteger

    @cli(Display.Simple, "block size in bytes")
    var size: Long

    @cli(Display.Simple, "number of transactions in block")
    var txCount: Long

    @cli(Display.Simple, "expected # of hashes to solve this block")
    val work: BigInteger
        get()
        {
            val work = libnexa.getWorkFromDifficultyBits(diffBits)
            return BigInteger.fromByteArray(work, Sign.POSITIVE)
        }
}

abstract class CommonBlockHeader: iBlockHeader
{
    constructor() : super()
    constructor(h: CommonBlockHeader):this()
    {
        hashData = h.hashData
        hashPrevBlock = h.hashPrevBlock
        diffBits = h.diffBits
        hashAncestor = h.hashAncestor
        hashMerkleRoot = h.hashMerkleRoot
        time = h.time
        height = h.height
        chainWork = h.chainWork
        size = h.size
        txCount = h.txCount
    }

    //protected var hexHash = String()
    //protected
    var hashData: Hash256? = null

    /** Return this block's hash.  Uses a cached value if one exists.  If you change the block be sure to call calcHash() or invalidateHash() to update this value either greedily or lazily respectively */
    @cli(Display.Simple, "block hash")
    override val hash: Hash256
        get()
        {
            var temp = hashData
            if (temp == null) // ((temp == null) || (temp == Hash256()))
            {
                temp = calcHash()
                hashData = temp
            }
            return temp
        }

    /** Communicate that this object has been changed, so recalculation of cached data is needed */
    override fun changed()
    {
        hashData = null
    }

    // For efficiency only, this manually assigns the hash rather than recalculating it.  Used in disk load functions
    fun assignHashData(h: Hash256) { hashData = h }


    @cli(Display.Simple, "previous block hash")
    override var hashPrevBlock = Hash256()

    @cli(Display.Simple, "difficulty in 'bits' representation")
    override var diffBits: Long = 0L // nBits

    @cli(Display.Simple, "ancestor")
    override open var hashAncestor = Hash256() // For more rapid rewind, an ancestor is stored when this block is saved

    @cli(Display.Simple, "merkle root hash")
    override var hashMerkleRoot = Hash256()

    @cli(Display.Simple, "block timestamp")
    override var time: Long = 0L

    @cli(Display.Simple, "block height")
    override var height: Long = -1L

    @cli(Display.Simple, "cumulative work in the chain")
    override var chainWork: BigInteger = 0.toBigInteger()

    @cli(Display.Simple, "block size in bytes")
    override var size: Long = -1L

    @cli(Display.Simple, "number of transactions in block")
    override var txCount: Long = 0L
}

interface iMerkleBlock: iBlockHeader
{
    abstract val txHashes: MutableSet<Hash256>
    abstract val txes: MutableList<out iTransaction>
    @cli(Display.Simple, "Returns true if all transactions defined in this merkle block have arrived (via calls to txArrived())")
    abstract fun complete(): Boolean

    fun txArrived(tx: iTransaction): Boolean
}

/** Reference to a UTXO */
@cli(Display.Simple, "UTXO reference")
interface iTxOutpoint : BCHserializable
{
    @cli(Display.Simple, "return the serialized hex representation")
    abstract fun toHex(): String
}

/** defines new UTXOs */
interface iTxOutput : BCHserializable
{
    var amount: Long //!< Amount in satoshis assigned to this output
    var script: SatoshiScript  // !< Constraints to spend this output
}

/** defines what UTXOs are being spent and proves ability to spend */
interface iTxInput : BCHserializable
{
    var spendable: Spendable
    var script: SatoshiScript

    /** Make a shallow copy of this object */
    fun copy(): iTxInput
}

interface iTransaction: BCHserializable
{
    @cli(Display.Simple, "Which blockchain is this transaction for")
    /** Which blockchain is this transaction for */
    val chainSelector: ChainSelector

    /** inputs to this transaction */
    @cli(Display.Simple, "transaction inputs")
    val inputs:MutableList<out iTxInput>

    /** Add an input to this transaction (at the end) */
    fun add(input: iTxInput):iTransaction

    /** Overwrite all inputs with the provided list */
    fun setInputs(inputs: MutableList<out iTxInput>):iTransaction

    /** outputs of this transaction */
    @cli(Display.Simple, "transaction outputs")
    val outputs: MutableList<out iTxOutput>

    /** Add an output to this transaction (at the end) */
    fun add(output: iTxOutput):iTransaction

    /** Overwrite all outputs with the provided list */
    fun setOutputs(outputs: MutableList<out iTxOutput>):iTransaction

    /** transaction outpoints (handles to the UTXO entries the outputs will generate) */
    @cli(Display.Dev, "transaction outpoints")
    val outpoints: Array<out iTxOutpoint>

    /** transaction version */
    @cli(Display.Simple, "version")
    var version: Int

    /** transaction lock time */
    @cli(Display.Simple, "transaction lock time")
    var lockTime: Long

    /** transaction size in bytes */
    @cli(Display.Simple, "transaction size in bytes")
    val size: Long

    /** Return this transaction's idem. If you change the transaction be sure to call changed() to update this value.
     * Returns the id for blockchains that do not have an idem. */
    @cli(Display.Simple, "transaction idem")
    val idem: Hash256

    /** Return this transaction's id. If you change the transaction be sure to call changed() to update this value */
    @cli(Display.Simple, "transaction id")
    val id: Hash256

    /** Communicate that this transaction has been changed, so recalculation of id and idem are needed */
    abstract fun changed()

    /** return true if this is a coinbase transaction */
    @cli(Display.Simple, "return true if this is a coinbase transaction")
    abstract fun isCoinbase(): Boolean

    /** Return true if this is an ownership challenge transaction.  This means that this transaction is invalid and so can never be actually committed to the blockchain. */
    @cli(Display.Simple, "Return true if this is an ownership challenge transaction.  This means that this transaction is invalid and so can never be actually committed to the blockchain.")
    abstract fun isOwnershipChallenge(): Boolean

    /** Return the serialized hex representation */
    @cli(Display.Simple, "Return the serialized hex representation")
    abstract fun toHex(): String

    /** Serialize this transaction into a byte array (using network serialization) */
    fun toByteArray(): ByteArray = BCHserialize(SerializationType.NETWORK).toByteArray()

    /** What this transaction will be paying in transaction fees (might be calculated each time) */
    @cli(Display.Simple, "transaction fee")
    abstract val fee: Long

    /** Transaction fee rate in satoshi/byte */
    @cli(Display.Simple, "transaction fee rate in satoshi/byte")
    val feeRate: Double
        get()
        {
            return fee.toDouble() / size.toDouble()
        }

    /** Total quantity of satoshis spent (input into this transaction), including grouped satoshis */
    val inputTotal: Long
        get()
        {
            var total = 0L
            for (i in inputs) total += i.spendable.amount
            return total
        }

    /** Total quantity of satoshis sent (output from this transaction), including grouped satoshis */
    val outputTotal: Long
        get()
        {
            var total = 0L
            for (i in outputs) total += i.amount
            return total
        }


    fun amountSentTo(dest: PayDestination): Long
    {
        var total = 0L
        val destScript = dest.outputScript()
        for (i in outputs)
        {
            if (i.script == destScript) total += i.amount
        }
        return total
    }

    fun amountSentTo(addr: PayAddress): Long
    {
        var total = 0L
        for (i in outputs)
        {
            if (i.script.address == addr) total += i.amount
        }
        return total
    }

    /** Return the sighash that allows new inputs and/or outputs to be appended to the transaction (but commits to all existing inputs and outputs) */
    abstract fun appendableSighash(extendInputs:Boolean = true, extendOutputs:Boolean=true): ByteArray

    /** Dump this transaction to stdout */
    abstract fun debugDump()

}

/** This represents something that can be spent.  It may not be spendable by ME, in which case the secret and perhaps other items will be unknown */
class Spendable(var chainSelector: ChainSelector) : BCHserializable
{
    val V1_SPENDABLE_ID = 1
    val SPENDABLE_ID = 2
    var outpoint:iTxOutpoint? = null
    var priorOutScript = SatoshiScript(chainSelector)   // The script that constrains spending these coins
    var amount: Long = -1
    var redeemScript = SatoshiScript(chainSelector) //ByteArray( 0)

    /** RAM only: has this been changed since last saved to disk */
    var dirty: Boolean = true

    val prevout:iTxOutput
        get() = txOutputFor(chainSelector, amount, priorOutScript)

    val groupInfo: GroupInfo?
        get() = priorOutScript.groupInfo(amount)

    val secret: Secret?
        get() = payDestination?.secret

    val addr: PayAddress?
        get() = payDestination?.address

    var backingPayDestination: PayDestination? = null
    val payDestination: PayDestination?
        get()
        {
            // P2SH multisig is an interesting problem here.  The P2SH address will depend on external data (the pubkeys of cooperating wallets)
            // so the wallet cannot determine if an address is spendable by this wallet without knowing cooperating wallets' pubkeys.

            // Allow override
            if (backingPayDestination != null) return backingPayDestination

            /*  There must be a backingPayDestination now..
            // Otherwise try to figure out the script type to construct the appropriate destination
            // We can short-cut this if we already know the address
            var a = addr
            if (a == null)
            {
                a = priorOutScript.address  // We can attempt to recognize the prior out script.  In this case we can extract an address from it.
            }

            if (a != null)  // Now use that address to determine the script type to create a payment destination
            {
                val sec = secret ?: return null // Can't build a payment if don't know the secret
                if (a.type == PayAddressType.P2PKH)
                {
                    backingPayDestination = Pay2PubKeyHashDestination(chainSelector, sec, -1.toLong())  // I don't know the index
                    return backingPayDestination
                }
                if (a.type == PayAddressType.P2PKT)
                {
                    backingPayDestination = Pay2PubKeyTemplateDestination(chainSelector, sec, -1.toLong())
                    return backingPayDestination
                }

                // We can't construct the other types because we don't know info (like the redeem script)
                // We could guess the redeem script if there are P2SH redeem script types.  One common type that would be useful is to wrap every P2PKH into P2SH
                // to increase payment anonymity for those using p2sh (and make the UTXO set a tiny bit smaller per UTXO)
            }

             */
            return null
        }

    var commitHeight: Long = -1
    var commitDate: Long = -1
    // The hash of the block that committed this Spendable to the blockchain
    var commitBlockHash: Hash256 = Hash256.ZERO
    var commitTxIdem: Hash256 = Hash256.ZERO
    var commitUnconfirmed: Long = 0 // > 0 if this UTXO is unconfirmed.  Number indicates the length of the longest spend chain
    // var commitTx: iTransaction? = null   // Get the TX from the wallet's tx history by the commitTxIdem

    var spentHeight: Long = -1
    /** spentDate is when the transaction was created (if unconfirmed) or when the transaction was confirmed, in epoch milliseconds.
     * It MUST have a value if this TXO is spent! */
    var spentDate: Long = -1
    var spentBlockHash: Hash256 = Hash256.ZERO
    var spentTxHash: Hash256 = Hash256.ZERO
    var spentUnconfirmed: Boolean = false  // True if an unconfirmed spend exists. False if spend tx exists and is confirmed or if spend tx does not exist.

    /** Has an in-progress (being readied by this wallet) transaction used this?  0 means no, > 0 means yes.  RAM ONLY -- anything that is reserved WILL NOT be flushed */
    var reserved: Long = 0

    /** Return true if this txo is unspent */
    val isUnspent: Boolean
        get() {
            return ((spentUnconfirmed == false)&&(spentHeight==-1L)&&(spentBlockHash== Hash256.ZERO)&&(spentTxHash == Hash256.ZERO))
        }

    constructor(chainSelector: ChainSelector, out_point: iTxOutpoint, amount: Long) : this(chainSelector)
    {
        // Throw something if the wrong outpoint type is given
        if (chainSelector.isNexaFamily)
        {
            (out_point as NexaTxOutpoint)
        }
        if (chainSelector.isBchFamily)
        {
            (out_point as BchTxOutpoint)
        }

        outpoint = out_point
        this.amount = amount
        // No PayDestination so I cannot spend this
    }

    // Serialization
    constructor(chainSelector: ChainSelector, data: BCHserialized) : this(chainSelector)
    {
        BCHdeserialize(data)
    }

    // Serialization
    constructor(data: BCHserialized) : this(ChainSelector.NEXA)  // Will be overwritten by deserialization
    {
        BCHdeserialize(data)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized //!< Serializer
    {
        if ((format == SerializationType.NETWORK) || (format == SerializationType.HASH))
        {
            return outpoint!!.BCHserialize(format)
        }
        else if (format == SerializationType.DISK)
        {
            //val sec = secret?.getSecret() ?: byteArrayOf()
            //val a = addr ?: PayAddress(chainSelector, PayAddressType.NONE, byteArrayOf())
            val ret = BCHserialized(format).addUint8(SPENDABLE_ID)
            ret.add(chainSelector.v)
            //ret.add(variableSized(sec))
            ret.addNullable(backingPayDestination)
            ret.add(outpoint!!.BCHserialize(format))
            ret.add(priorOutScript)
            ret.addInt64(amount)
            ret.add(redeemScript)
            ret.addInt64(commitHeight)
            ret.addInt64(commitDate)
            ret.add(commitBlockHash)
            ret.add(commitTxIdem)
            assert(commitBlockHash == Hash256.ZERO || commitBlockHash != commitTxIdem)
            assert(spentBlockHash == Hash256.ZERO || spentBlockHash != spentTxHash)
            /* commitTx +  since the commitTx contains this BCHinput, that would recurse -- when loading you need to restore this from an alternate source using the commitTxIdem */
            ret.addUint64(spentHeight).addInt64(spentDate).add(spentBlockHash)
            ret.add(spentTxHash)
            ret.add(spentUnconfirmed)
            ret.addUint16(commitUnconfirmed)
            assert(ret.format == format)
            return ret
        }
        else
        {
            throw NotImplementedError()
        }
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized //!< Deserializer
    {
        if (stream.format == SerializationType.NETWORK)
        {
            outpoint = outpointFor(chainSelector, stream)
        }
        else if (stream.format == SerializationType.DISK)
        {
            val id = stream.deuint8()
            if ((id != SPENDABLE_ID)&&(id != V1_SPENDABLE_ID))
            {
                // LogIt.info("deserialization corruption!")
                throw DeserializationException("Spendable identifier incorrect")
            }
            if (id != V1_SPENDABLE_ID)
            {
                // LogIt.info("V2 SPENDABLE")
                chainSelector = ChainSelector.from(stream.debyte())
                backingPayDestination = stream.deNullable { PayDestination.from(it, chainSelector) }
                // LogIt.info(backingPayDestination.toString())
            }
            var secret: Secret? = null
            if (id == V1_SPENDABLE_ID)
            {
                LogIt.info("V1 SPENDABLE")
                val secBytes = stream.deByteArray()
                if (secBytes.size != 0) secret = UnsecuredSecret(secBytes)
                else secret = null
            }
            // LogIt.info("outpoint")
            outpoint = outpointFor(chainSelector, stream)
            // LogIt.info("priorOutScript")
            priorOutScript.BCHdeserialize(stream)
            // LogIt.info("amount")
            amount = stream.deuint64()
            redeemScript.BCHdeserialize(stream)
            commitHeight = stream.deuint64()
            if (id == V1_SPENDABLE_ID) commitDate = -2  // -2 indicates that this needs to be fixed up
            else commitDate = stream.deint64()
            commitBlockHash = Hash256(stream)
            commitTxIdem = Hash256(stream)
            spentHeight = stream.deuint64()
            if (id == V1_SPENDABLE_ID) spentDate = -2  // -2 indicates that this needs to be fixed up
            else spentDate = stream.deint64()
            spentBlockHash = Hash256(stream)
            spentTxHash = Hash256(stream)
            spentUnconfirmed = stream.deboolean()
            commitUnconfirmed = stream.deuint16().toLong()
            reserved = 0
            dirty = false  // just loaded
            if (id == V1_SPENDABLE_ID)
            {
                val p = PayAddress(stream)
                val addr = if (p.type != PayAddressType.NONE) p else null
                if (addr == null)
                {
                    backingPayDestination = Pay2PubKeyTemplateDestination(chainSelector, secret!!, -1)
                }
                else
                {
                    backingPayDestination = when (addr.type)
                    {
                        PayAddressType.TEMPLATE -> Pay2PubKeyTemplateDestination(chainSelector, secret!!, -1)
                        PayAddressType.P2PKH -> Pay2PubKeyHashDestination(chainSelector, secret!!, -1)
                        else -> throw DeserializationException("conversion from V1 Spendable to V2")
                    }
                }
            }
        }
        else
        {
            throw NotImplementedError()
        }
        return stream
    }

    override fun toString(): String
    {
        return amount.toString() + " from UTXO " + outpoint.toString() + " created in " + commitTxIdem.toHex() +
            (if (commitBlockHash == Hash256.ZERO) " unconfirmed " else " (block ${commitHeight}:${commitBlockHash.toHex()})") +
            if (isUnspent) " UNSPENT " else (" spent in ${spentTxHash} " + if (spentBlockHash == Hash256.ZERO) " unconfirmed " else " (block ${spentHeight}:${spentBlockHash.toHex()})")
    }

    fun dump(): String
    {
        val s = StringBuilder()

        s.append("${amount} from UTXO ${outpoint}: Unspent: ${isUnspent}  committed: ${commitHeight}:${commitBlockHash.toHex()}  spent: ")
        if (spentUnconfirmed)
        {
            s.append("unconfirmed ")
        }
        else
        {
            s.append("${spentHeight}:${spentBlockHash.toHex()} in tx ${spentTxHash.toHex()} ")
        }
        return s.toString()
    }

    @cli(Display.Simple, "Return group token information, or null if not grouped")
    fun groupInfo(): GroupInfo? = priorOutScript.groupInfo(amount)
}


interface iBlock: iBlockHeader
{
    /** list of transactions in the block */
    val txes: List<iTransaction>
}

// common type for Nexa and Bch header DAOs
interface BlockHeaderPersist
{
    /** Get a main chain (a chain with the most work) header by its height */
    fun getHeader(height: Long): iBlockHeader?
    /** If a header identified by this hash does not exist, return null */
    fun getHeader(hash: ByteArray): iBlockHeader?
    /** Multiple chains may exist at this height so a list is returned */
    fun getHeadersAtHeight(height: Long): List<iBlockHeader>
    // Multiple chain tips might have the same total work so a list is returned
    fun getMostWorkHeaders(): List<iBlockHeader>
    // If a header identified by this hash does not exist, return null
    fun getHeader(hash: Hash256): iBlockHeader?

    // For quick access, the current main chain tip can be specially stored
    fun setCachedTipHeader(header: iBlockHeader)
    // Get the current main chain tip.
    fun getCachedTipHeader(): iBlockHeader?
    // Add a header into the DB.
    fun insertHeader(header: iBlockHeader)
    // Add a header into the DB, update the existing header if it does not match what is passed.  This limits database writes.
    fun diffUpsert(header: iBlockHeader)   // Read header, if different or nonexistent insert or update
    // Delete all headers.
    fun clear()
}