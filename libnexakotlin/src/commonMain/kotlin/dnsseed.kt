// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.nexa.libnexakotlin

// TODO: when we get to API level 29
// import android.net.DnsResolver

fun withPeersFrom(seedername: String, callback: (Set<String>) -> Unit)
{
    /*
    val addrs = InetAddress.getAllByName(seedername).toMutableSet()
    if (MY_CNXNS_ONLY)
        addrs.clear()

    // Testnet seeders and nodes are unreliable.  Nodes have short lifetimes so seeder data is stale.
    // Place a known good node into the seeder output
    if (seedername.contains("test"))
    {
        //addrs.add(InetAddress.getByName("192.168.1.100"))
        //addrs.add(InetAddress.getByName("192.168.1.155"))
        addrs.add(InetAddress.getByName("159.65.163.15"))
        if (!MY_CNXNS_ONLY)
        {
            addrs.add(InetAddress.getByName("194.14.247.130"))
            addrs.add(InetAddress.getByName("194.14.247.131"))
        }
    }
    else if (!seedername.contains("10.0.2.2"))  // Not Regtest
    {
        // force BU's high perf host to be on the list so if no other node is good we have something
        addrs.add(InetAddress.getByName("bch1.bitcoinunlimited.net"))
    }
     callback(addrs)
     */
    callback(setOf<String>(seedername))
}