package org.nexa.libnexakotlin


/** change this function to install your own translation routine */
public var appI18n: ((Int) -> String) = { v -> "STR" + v.toString(16) }

// It is necessary to abstract the R.string. resource identifier because they don't exist on other platforms
// You might overwrite these values with real string resource ids on platforms that support resource ids
var RinsufficentBalance:Int = 0xf00d + 0 // R.string.insufficentBalance
var RbadWalletImplementation:Int = 0xf00d + 1 // R.string.badWalletImplementation
var RdataMissing:Int = 0xf00d + 2 // R.string.dataMissing
var RwalletAndAddressIncompatible:Int = 0xf00d + 3 // R.string.chainIncompatibleWithAddress
var RnotSupported:Int = 0xf00d + 4 // R.string.notSupported
var Rexpired:Int = 0xf00d + 5 // R.string.expired
var RsendMoreThanBalance:Int = 0xf00d + 6 // R.string.sendMoreThanBalance
var RbadAddress:Int = 0xf00d + 7 // R.string.badAddress
var RblankAddress:Int = 0xf00d + 8 // R.string.blankAddress
var RblockNotForthcoming:Int = 0xf00d + 9 // R.string.blockNotForthcoming
var RheadersNotForthcoming:Int = 0xf00d + 10 // R.string.headersNotForthcoming
var RbadTransaction:Int = 0xf00d + 11 // R.string.badTransaction
var RfeeExceedsFlatMax:Int = 0xf00d + 12 // R.string.feeExceedsFlatMax
var RexcessiveFee:Int = 0xf00d + 13 // R.string.excessiveFee
var Rbip70NoAmount:Int = 0xf00d + 14 //  R.string.badAmount
var RdeductedFeeLargerThanSendAmount:Int = 0xf00d + 15 // R.string.deductedFeeLargerThanSendAmount
var RwalletDisconnectedFromBlockchain:Int = 0xf00d + 16 // R.string.walletDisconnectedFromBlockchain
var RsendDust:Int = 0xf00d + 17 // R.string.sendDustError
var RnoNodes:Int = 0xf00d + 18 // R.string.noNodes
var RbadCryptoCode:Int = 0xf00d + 19
var RneedNonexistentAuthority:Int = 0xf00d + 20
var RwalletAddressMissing:Int = 0xf00d + 21
var RunknownCryptoCurrency:Int = 0xf00d + 22
var RsendMoreTokensThanBalance:Int = 0xf00d + 23 // R.string.sendMoreTokensThanBalance


//* Indicates how severe the exceptions is
public enum class ErrorSeverity
{
    Expected,  //* This is a normal error that should be handled programatically.  No logging necessary
    Abnormal,  //* This is unexpected but worst case means that minor functionality is unavailable.  Log it
    Severe,    //* Causes major functionality to be unavailable. Log it
}

/**
 * @property msg Full description of the problem.
 * @property errCode Numerical problem identifier -- it is recommended that this be the string resource id for the shortMsg on platforms that support resources.
 * @property shortMsg The exception "title". Useful for displaying in a the title bar
 * @property severity Indicates how the error should be logged and handled.
 */
public open class LibNexaException(msg: String, public val shortMsg: String? = null, public val severity: ErrorSeverity = ErrorSeverity.Abnormal, val errCode:Int=-1) : Exception(msg)

public open class BadCryptoException(msg: String = "bad crypto code") : LibNexaException(msg, appI18n(RbadCryptoCode))

public open class UnimplementedException(msg: String, shortMsg: String? = null) : LibNexaException(msg, shortMsg, ErrorSeverity.Abnormal)

public open class PermissionDeniedException(msg: String, shortMsg: String? = null) : LibNexaException(msg, shortMsg, ErrorSeverity.Abnormal)
public open class InUseException(msg: String, shortMsg: String? = null) : LibNexaException(msg, shortMsg, ErrorSeverity.Abnormal)


public open class PayAddressException(msg: String, shortMsg: String? = null, severity: ErrorSeverity = ErrorSeverity.Expected) : LibNexaException(msg, shortMsg, severity)
public open class PayAddressDecodeException(msg: String) : PayAddressException(msg, appI18n(RbadAddress))
public open class PayAddressBlankException(msg: String) : PayAddressException(msg, appI18n(RblankAddress), ErrorSeverity.Expected)

open class DataMissingException(msg: String) : LibNexaException(msg, appI18n(RdataMissing))

open class UnknownBlockchainException() : LibNexaException(
  "Could not determine the a blockchain from the input",
  "Unknown cryptocurrency/blockchain", ErrorSeverity.Expected, RunknownCryptoCurrency)

// Wallet exceptions
open class WalletException(msg: String, shortMsg: String? = null, severity: ErrorSeverity = ErrorSeverity.Abnormal) : LibNexaException(msg, shortMsg, severity)

open class WalletFeeException(msg: String, severity: ErrorSeverity = ErrorSeverity.Abnormal) : WalletException(msg, appI18n(RexcessiveFee), severity)
open class WalletAuthorityException(msg: String, severity: ErrorSeverity = ErrorSeverity.Abnormal) : WalletException(msg, appI18n(RexcessiveFee), severity)
open class WalletDustException(msg: String) : WalletException(msg, appI18n(RsendDust))
open class WalletNeedsConsolidation(msg: String) : WalletException(msg, appI18n(RsendDust))
open class WalletNotEnoughBalanceException(msg: String) : WalletException(msg, appI18n(RinsufficentBalance), ErrorSeverity.Expected)
open class WalletImplementationException(msg: String) : WalletException(msg, appI18n(RbadWalletImplementation), ErrorSeverity.Abnormal)
open class WalletIncompatibleAddress(msg: String) : WalletException(msg, appI18n(RwalletAndAddressIncompatible), ErrorSeverity.Expected)
open class WalletNotSupportedException(msg: String) : WalletException(msg, appI18n(RnotSupported))
open class WalletAddressMissingException(msg: String) : WalletException(msg, appI18n(RnotSupported))
open class WalletDisconnectedException() : WalletException("", appI18n(RwalletDisconnectedFromBlockchain), ErrorSeverity.Abnormal)
open class WalletNotEnoughTokenBalanceException(msg: String) : WalletException(msg, appI18n(RinsufficentBalance), ErrorSeverity.Expected)


// Network exceptions

open class NetException(msg: String) : Exception(msg)

open class UnknownHostException(msg: String) : Exception(msg)
open class P2PException(msg: String) : NetException(msg)
open class P2PDisconnectedException(msg: String) : P2PException(msg)
open class P2PNoNodesException : P2PException("no nodes")
open class ElectrumNoNodesException : ElectrumException(appI18n(RnoNodes))
open class NetMsgTypeCodingError(msg: String) : P2PException(msg)