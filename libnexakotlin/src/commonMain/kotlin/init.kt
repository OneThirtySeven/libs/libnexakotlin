package org.nexa.libnexakotlin
import com.ionspin.kotlin.bignum.integer.toBigInteger
import kotlinx.coroutines.runBlocking
import org.nexa.threads.*
private val LogIt = GetLog("BU.init")

var runningTheTests = false
val SimulationHostIP = "10.0.2.2"
var defaultDbPrefix = if (runningTheTests) "guitest_" else if (REG_TEST_ONLY == true) "unittest_" else "blkhdr_"

/** add or remove elements in this list to control the servers that we use to get electrum data by default */
val nexaElectrum = mutableListOf(IpPort("electrum.nexa.org", DEFAULT_NEXA_TCP_ELECTRUM_PORT))

var cnxnMgrLock = Mutex()
var cnxnMgrs: MutableMap<ChainSelector, CnxnMgr> = mutableMapOf()

var blockchainLock = Mutex()
var blockchains: MutableMap<ChainSelector, Blockchain> = mutableMapOf()

fun GetCnxnMgr(chain: ChainSelector, name: String? = null, start:Boolean = true): CnxnMgr
{
    return cnxnMgrLock.synchronized {
        LogIt.info(sourceLoc() + " " + "Get Cnxn Manager")
        val existing = cnxnMgrs[chain]
        if (existing != null) return@synchronized existing

        val n = name ?: chainToURI[chain] ?: "unknown"
        val result = when (chain)
        {
            ChainSelector.NEXATESTNET ->
            {
                val cmgr = MultiNodeCnxnMgr(n, ChainSelector.NEXATESTNET, arrayOf("68.183.223.81", "testnetseeder.nexa.org"))
                cmgr.desiredConnectionCount = 1 // nexa testnet won't have lots of nodes
                cmgr
            }
            ChainSelector.NEXAREGTEST ->
            {
                val cmgr = MultiNodeCnxnMgr(n, ChainSelector.NEXAREGTEST, arrayOf("127.0.0.1", SimulationHostIP))
                cmgr.desiredConnectionCount = 1 // Regtest will just be running on 1 node
                cmgr
            }
            ChainSelector.NEXA ->
            {
                val cmgr = MultiNodeCnxnMgr(n, ChainSelector.NEXA, arrayOf("seeder.nexa.org","68.183.223.81","159.203.179.109"))
                cmgr.desiredConnectionCount = 2
                cmgr
            }
            ChainSelector.BCHTESTNET ->
            {
                val cmgr = MultiNodeCnxnMgr(n, ChainSelector.NEXATESTNET, arrayOf("testnet4-seed-bch.bitcoinforks.org", "testnet4-seed-bch.toom.im", "seed.tbch4.loping.net"))
                cmgr.desiredConnectionCount = 2
                cmgr
            }
            ChainSelector.BCHREGTEST ->
            {
                val cmgr = MultiNodeCnxnMgr(n, ChainSelector.BCHREGTEST, arrayOf()) // arrayOf(SimulationHostIP, LanHostIP)
                cmgr.desiredConnectionCount = 1 // Regtest will just be running on 1 node
                cmgr
            }
            ChainSelector.BCH ->
            {
                val cmgr = MultiNodeCnxnMgr(n, ChainSelector.BCH, arrayOf("seed.bitcoinunlimited.info","seed.bchd.cash"))
                cmgr.desiredConnectionCount = 2
                cmgr
            }
            // ^ is exhaustive:  else -> throw BadCryptoException("bad crypto code")
        }
        result.getElectrumServerCandidate = { chain -> ElectrumServerOn(chain) }
        if (start) result.start()
        cnxnMgrs[chain] = result
        result
    }
}

fun ElectrumServerOn(chain: ChainSelector): IpPort
{
    return when (chain)
    {
        ChainSelector.BCH -> IpPort("electrum.seed.bitcoinunlimited.net", DEFAULT_BCH_TCP_ELECTRUM_PORT)
        ChainSelector.BCHREGTEST -> IpPort(SimulationHostIP, 60401)
        ChainSelector.NEXAREGTEST -> IpPort(SimulationHostIP, 30401)
        // TODO: point these IPs to a seeder
        ChainSelector.NEXA -> nexaElectrum.random()
        ChainSelector.NEXATESTNET -> IpPort(SimulationHostIP, DEFAULT_NEXATEST_TCP_ELECTRUM_PORT)
            //IpPort("electrumserver.seeder.nexa.org", 7229)
        else -> throw BadCryptoException()
    }
}

fun GetBlockchain(chainSelector: ChainSelector, cnxnMgr: CnxnMgr, name: String? = null, start:Boolean = true): Blockchain
{
    return blockchainLock.synchronized {
        LogIt.info(sourceLoc() + " " + "Get Blockchain")
        val existing = blockchains[chainSelector]
        if (existing != null) return@synchronized existing
        val nexaRegTestGb = Hash256("d71ee431e307d12dfef31a6b21e071f1d5652c0eb6155c04e3222612c9d0b371")
        val nexaTestnetGb = Hash256("508c843a4b98fb25f57cf9ebafb245a5c16468f06519cdd467059a91e7b79d52")
        val result = when (chainSelector)
        {
            ChainSelector.BCHTESTNET -> Blockchain(
              ChainSelector.BCHTESTNET,
              name ?: "TBCH",
              cnxnMgr,
              Hash256("000000000933ea01ad0ee984209779baaec3ced90fa3f408719526f8d77f4943"),
              Hash256("000000000003cab8d8465f4ea4efcb15c28e5eed8e514967883c085351c5b134"),
              Hash256("000000000005ae0f3013e89ce47b6f949ae489d90baf6621e10017490f0a1a50"),
              1348366,
              "52bbf4d7f1bcb197f2".toBigInteger(16),
              defaultDbPrefix
            )

            // Regtest for use alongside testnet
            ChainSelector.BCHREGTEST -> Blockchain(
              ChainSelector.BCHREGTEST,
              name ?: "RBCH",
              cnxnMgr,
              // If checkpointing the genesis block, set the prior block id to the genesis block as well
              Hash256("0f9188f13cb7b2c71f2a335e3a4fc328bf5beb436012afca590b1a11466e2206"),
              Hash256("0f9188f13cb7b2c71f2a335e3a4fc328bf5beb436012afca590b1a11466e2206"),
              Hash256("0f9188f13cb7b2c71f2a335e3a4fc328bf5beb436012afca590b1a11466e2206"),
              0,
              0.toBigInteger(),
              defaultDbPrefix
            )

            // Bitcoin Cash mainnet chain
            ChainSelector.BCH -> Blockchain(
              ChainSelector.BCH,
              name ?: "BCH",
              cnxnMgr,
              genesisBlockHash = Hash256("0f9188f13cb7b2c71f2a335e3a4fc328bf5beb436012afca590b1a11466e2206"),
              checkpointPriorBlockId = Hash256("000000000000000002195635b4b14a0054eeaf6d219c521078e1297425381c3a"),
              checkpointId = Hash256("000000000000000004c366e89454a7c071e6293d398b45652128a10d38d21675"),
              checkpointHeight = 750500,
              checkpointWork = "19bba64c36dab2acd254f1c".toBigInteger(16),
              defaultDbPrefix
            )

            // Nexa regtest chain
            ChainSelector.NEXAREGTEST -> Blockchain(
              ChainSelector.NEXAREGTEST,
              name ?: "RNEX",
              cnxnMgr,
              genesisBlockHash = nexaRegTestGb,
              checkpointPriorBlockId = Hash256(),
              checkpointId = nexaRegTestGb,
              checkpointHeight = 0,
              checkpointWork = 0.toBigInteger(),
              defaultDbPrefix
            )
            /*
            // Nexa testnet chain
            ChainSelector.NEXATESTNET -> Blockchain(
              ChainSelector.NEXATESTNET,
              name ?: "TNEX",
              cnxnMgr,
              genesisBlockHash = nexaTestnetGb,
              checkpointPriorBlockId = Hash256("a37262e06459fb1c2e9ede06d2f040566c1428b53a40034734c6b3ba286d0f8d"),
              checkpointId = Hash256("b927b4ad1db13c2c32124f833cabeb0a69c61502ad388ebd7f596fa883adb3ec"),
              checkpointHeight = 130000,
              checkpointWork = "484b4becf5".toBigInteger(16),
              context = context,
              dbPrefix
            )
             */
            // Nexa testnet chain
            ChainSelector.NEXATESTNET -> Blockchain(
                ChainSelector.NEXATESTNET,
                name ?: "TNEX",
                cnxnMgr,
                genesisBlockHash = nexaTestnetGb,
                checkpointPriorBlockId = Hash256("da5dd88a7f9fd1ad999feda2029191fd9b427705b2ca21f74db0f5440a8cd4dc"),
                checkpointId = Hash256("5c37182874070b6ed339ea303febbbb7671f39ae5e24b8f49fa3636ecbf12f27"),
                checkpointHeight = 439252,
                checkpointWork = "0346c474a2b73".toBigInteger(16),
                defaultDbPrefix
            )

            ChainSelector.NEXA -> Blockchain(
                ChainSelector.NEXA,
                name ?: "NEX",
                cnxnMgr,
                genesisBlockHash = Hash256("edc7144fe1ba4edd0edf35d7eea90f6cb1dba42314aa85da8207e97c5339c801"),
                checkpointPriorBlockId = Hash256("0350f5eb119ce9242a3e3858970d088d96be3d13e5c11671dbc8f29063ba126b"),
                checkpointId = Hash256("cb8f70486577e89c997c631870f38ed5ef01157732e10f4afab1eb212deee0d2"),
                checkpointHeight = 424704,
                checkpointWork = "014214a3f56da44fc08".toBigInteger(16),
                defaultDbPrefix
            )

            // ^ is exhaustive so no need for: else -> throw BadCryptoException()
        }

        if (start) result.start()
        blockchains[chainSelector] = result
        result
    }
}

/*
fun checkWalletFilename(name: String, allowNonExistent: Boolean = false, mustNotExist: Boolean = false):File
{
    var f = File(name)
    val fNoExt:File = if (f.extension == ".db")
    {
        File(f.nameWithoutExtension)  // opening is going to add it on
    }
    else
    {
        f
    }
    if (mustNotExist)
    {
        val fWithExt = File(fNoExt.absolutePath + ".db")
        if (fWithExt.exists())
        {
            throw Error("Wallet file already exists: " + fWithExt.absolutePath)
        }
    }
    if (allowNonExistent) return fNoExt
    val fWithExt = File(fNoExt.absolutePath + ".db")
    if (!fWithExt.exists())
    {
        throw Error("File does not exist: " + fWithExt.absolutePath)
    }
    return fNoExt
}

 */

fun connectBlockchain(cs: ChainSelector): Blockchain
    {
        blockchains[cs]?.let { return it }

        val cm = GetCnxnMgr(cs, start = true)
        val bc = GetBlockchain(cs, cm, start = true)
        blockchains[cs] = bc
        return bc
    }

fun newWallet(name: String, bc:Blockchain): Bip44Wallet
{
    val w = runBlocking {
        val wdb: WalletDatabase = openWalletDB(name, bc.chainSelector)!!  // One DB file per wallet for the desktop
        val w =
            try { Bip44Wallet(wdb, name) }
            catch (_: DataMissingException)
            {
                Bip44Wallet(wdb, name, bc.chainSelector, NEW_WALLET)

            }
        w.addBlockchain(bc, bc.checkpointHeight, null)  // Since this is a new ram wallet (new private keys), there cannot be any old blocks with transactions
        w
    }
    return w
}

fun newWallet(name: String, cs:ChainSelector): Bip44Wallet
{
    var bc = blockchains[cs]
    if (bc == null) bc = connectBlockchain(cs)
    return newWallet(name, bc)
}


fun recoverWallet(name: String, recoveryKey: String, bc:Blockchain): Bip44Wallet
{
    val w = runBlocking {
        //val f = checkWalletFilename(name, true)
        val wdb: WalletDatabase = openWalletDB(name)!!  // One DB file per wallet for the desktop
        val w = Bip44Wallet(wdb, name, bc.chainSelector, recoveryKey)
        w.addBlockchain(bc, bc.checkpointHeight, 0)  // Since this is a recovered wallet, look for old tx (start as early as possible)
        w
    }
    return w
}

fun recoverWallet(name: String, recoveryKey: String, cs:ChainSelector): Bip44Wallet
{
    var bc = blockchains[cs]
    if (bc == null) bc = connectBlockchain(cs)
    return recoverWallet(name, recoveryKey, bc)
}

fun openWallet(name: String): Bip44Wallet
{
    val wdb = openWalletDB(name)!!  // One DB file per wallet for the desktop
    val w = try
    {
        Bip44Wallet(wdb, name)
    }
    catch (e: DataMissingException)
    {
        throw DataMissingException("Old, corrupt, or not a wallet file: " + name)
    }
    var bc = blockchains[w.chainSelector]
    if (bc == null) bc = connectBlockchain(w.chainSelector)
    w.addBlockchain(bc, bc.checkpointHeight, null)
    return w
}

/** Open this wallet if it exists, or create a new wallet */
fun openOrNewWallet(name: String, cs:ChainSelector): Bip44Wallet
{
    try
    {
        return openWallet(name)
    }
    catch(e:Exception)
    {
        // println("Creating new wallet: $name")
        return newWallet(name, cs)
    }
}