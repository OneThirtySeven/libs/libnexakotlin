package org.nexa.libnexakotlin

import kotlinx.datetime.LocalDate
import kotlinx.datetime.LocalDateTime

private val LogIt = GetLog("BU.identity")

val nexidParams = arrayOf("attest", "ava", "billing", "dob", "email", "hdl", "realname", "ph", "postal", "sm")
@cli(Display.Simple, "Information that is associated with a particular identity")
class IdentityInfo() : BCHserializable
{
    constructor(stream: BCHserialized):this()
    {
        BCHdeserialize(stream)
    }

    companion object
    {
        const val VERSION: Int = 0
        val COMMON_IDENTITY_SEED = ""
    }

    // have API similar to prefs
    fun getString(k:String, default:String?=null): String?
    {
        when(k)
        {
            "attest" -> return attest
            "ava" -> return ava
            "billing" -> return billing
            "dob" -> return dob.toString()
            "email" -> return email
            "hdl" -> return hdl
            "phone" -> return phone
            "postal" -> return postal
            "realname" -> return realname
            "sm" -> return sm
        }
        return default
    }

    // have API similar to prefs
    fun putString(k: String, value: String)
    {
        when(k)
        {
            "attest" -> attest = value
            "ava" -> ava = value
            "billing" -> billing = value
            "dob" -> dob = LocalDate.parse(value)  // TODO accept human formats
            "email" -> email = value
            "hdl" -> hdl = value
            "phone" -> phone = value
            "postal" -> postal = value
            "realname" -> realname = value
            "sm" -> sm = value
        }
    }

    /* these variables indicate permission to provide certain data to this domain */
    @cli(Display.Simple, "A key or seed that defines this particular identity (typically the domain name of the site this identity is for if a per-site identity is needed)")
    var identityKey: String = COMMON_IDENTITY_SEED
    @cli(Display.Simple, "identity this information corresponds to")
    var identity: PayAddress? = null

    /* these variables indicate permission to provide certain data to this domain */
    @cli(Display.Simple, "attestations")
    var attest: String = ""

    @cli(Display.Simple, "avatar image URL")
    var ava: String = ""

    @cli(Display.Simple, "billing address")
    var billing: String = ""

    @cli(Display.Simple, "birthday")
    var dob: LocalDate = LocalDate(0,1,1)

    @cli(Display.Simple, "email address")
    var email: String = ""

    @cli(Display.Simple, "handle/username")
    var hdl: String = ""

    @cli(Display.Simple, "phone number")
    var phone: String = ""

    @cli(Display.Simple, "postal address")
    var postal: String = ""

    @cli(Display.Simple, "real name")
    var realname: String = ""

    @cli(Display.Simple, "social media")
    var sm: String = ""

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        return BCHserialized(format).addUint8(VERSION)
          .add(identityKey).addNullable(identity).add(attest).add(ava).add(billing).add(dob.toString()).add(email).add(hdl).add(phone).add(postal).add(realname).add(sm)
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        val version = stream.deuint8()
        if (version == VERSION)
        {
            identityKey = stream.deString()
            identity = stream.deNullable { PayAddress(stream)}
            attest = stream.deString()
            ava = stream.deString()
            billing = stream.deString()
            try
            {
                dob = LocalDate.parse(stream.deString())
            }
            catch (e: IllegalArgumentException)  // If the deserialization is garbage it can give the Date ctor garbage, throwing this.
            {
                throw DeserializationException("identity data is bad: invalid date string")
            }
            email = stream.deString()
            hdl = stream.deString()
            phone = stream.deString()
            postal = stream.deString()
            realname = stream.deString()
            sm = stream.deString()
        }
        else
        {
            throw DeserializationException("unknown version")
        }
        return stream
    }
}

@cli(Display.Simple, "An entity that requires identity information")
class IdentityDomain(@cli(Display.Simple, "Address of entity") var domain: String,
  @cli(Display.Simple, "Which crypto public key is used") var useIdentity: Long) : BCHserializable
{
    companion object
    {
        const val IDENTITY_BY_HASH: Long = 0
        const val COMMON_IDENTITY: Long = 1
    }

    /* these variables indicate permission to provide certain data to this domain */
    @cli(Display.Simple, "provide attestations, if requested")
    var attestP: Boolean = false

    @cli(Display.Simple, "provide avatar image, if requested")
    var avaP: Boolean = false

    @cli(Display.Simple, "provide billing address, if requested")
    var billingP: Boolean = false

    @cli(Display.Simple, "provide birthday, if requested")
    var dobP: Boolean = false

    @cli(Display.Simple, "provide email, if requested")
    var emailP: Boolean = false

    @cli(Display.Simple, "provide handle/username, if requested")
    var hdlP: Boolean = false

    @cli(Display.Simple, "provide phone number, if requested")
    var phoneP: Boolean = false

    @cli(Display.Simple, "provide postal address, if requested")
    var postalP: Boolean = false

    @cli(Display.Simple, "provide real name, if requested")
    var realnameP: Boolean = false

    @cli(Display.Simple, "provide social media, if requested")
    var smP: Boolean = false

    /* these variables indicate the requirement from the domain (m=mandatory, r=recommended, o=optional, x not used) */
    @cli(Display.Simple, "attestation requirement")
    var attestR: Char = 'x'

    @cli(Display.Simple, "avatar image requirement")
    var avaR: Char = 'x'

    @cli(Display.Simple, "billing address requirement")
    var billingR: Char = 'x'

    @cli(Display.Simple, "birthday requirement")
    var dobR: Char = 'x'

    @cli(Display.Simple, "email requirement")
    var emailR: Char = 'x'

    @cli(Display.Simple, "username/handle requirement")
    var hdlR: Char = 'x'

    @cli(Display.Simple, "phone number requirement")
    var phoneR: Char = 'x'

    @cli(Display.Simple, "postal address requirement")
    var postalR: Char = 'x'

    @cli(Display.Simple, "real name requirement")
    var realnameR: Char = 'x'

    @cli(Display.Simple, "social media requirement")
    var smR: Char = 'x'

    /** Deserializataion constructor */
    constructor(stream: BCHserialized) : this("", 0)
    {
        BCHdeserialize(stream)
        LogIt.info("identityDomain: " + domain + " " + useIdentity)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        return BCHserialized(format).add(domain).addUint64(useIdentity)
          .add(attestP).add(avaP).add(billingP).add(dobP).add(emailP).add(hdlP).add(phoneP).add(postalP).add(realnameP).add(smP)
          .add(attestR).add(avaR).add(billingR).add(dobR).add(emailR).add(hdlR).add(phoneR).add(postalR).add(realnameR).add(smR)
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        domain = stream.deString()
        useIdentity = stream.deuint64()
        attestP = stream.deboolean()
        avaP = stream.deboolean()
        billingP = stream.deboolean()
        dobP = stream.deboolean()
        emailP = stream.deboolean()
        hdlP = stream.deboolean()
        phoneP = stream.deboolean()
        postalP = stream.deboolean()
        realnameP = stream.deboolean()
        smP = stream.deboolean()
        attestR = stream.dechar()
        avaR = stream.dechar()
        billingR = stream.dechar()
        dobR = stream.dechar()
        emailR = stream.dechar()
        hdlR = stream.dechar()
        phoneR = stream.dechar()
        postalR = stream.dechar()
        realnameR = stream.dechar()
        smR = stream.dechar()
        return stream
    }

    fun getPerms(perms: MutableMap<String, Boolean>)
    {
        perms["attest"] = attestP
        perms["ava"] = avaP
        perms["billing"] = billingP
        perms["dob"] = dobP
        perms["email"] = emailP
        perms["hdl"] = hdlP
        perms["phone"] = phoneP
        perms["postal"] = postalP
        perms["realname"] = realnameP
        perms["sm"] = smP
    }

    fun setPerms(perms: MutableMap<String, Boolean>): Boolean
    {
        var changed = false;

        var tmp = perms["attest"]
        if ((tmp != null) && (tmp != attestP))
        {
            changed = true; attestP = tmp
        }

        tmp = perms["ava"]
        if ((tmp != null) && (tmp != avaP))
        {
            changed = true; avaP = tmp
        }

        tmp = perms["billing"]
        if ((tmp != null) && (tmp != billingP))
        {
            changed = true; billingP = tmp
        }

        tmp = perms["dob"]
        if ((tmp != null) && (tmp != dobP))
        {
            changed = true; dobP = tmp
        }

        tmp = perms["email"]
        if ((tmp != null) && (tmp != emailP))
        {
            changed = true; emailP = tmp
        }

        tmp = perms["hdl"]
        if ((tmp != null) && (tmp != hdlP))
        {
            changed = true; hdlP = tmp
        }

        tmp = perms["phone"]
        if ((tmp != null) && (tmp != phoneP))
        {
            changed = true; phoneP = tmp
        }

        tmp = perms["postal"]
        if ((tmp != null) && (tmp != postalP))
        {
            changed = true; postalP = tmp
        }

        tmp = perms["realname"]
        if ((tmp != null) && (tmp != realnameP))
        {
            changed = true; realnameP = tmp
        }

        tmp = perms["sm"]
        if ((tmp != null) && (tmp != smP))
        {
            changed = true; smP = tmp
        }

        return changed
    }

    fun getReqs(perms: MutableMap<String, String>)
    {
        perms["attest"] = attestR.toString()
        perms["ava"] = avaR.toString()
        perms["billing"] = billingR.toString()
        perms["dob"] = dobR.toString()
        perms["email"] = emailR.toString()
        perms["hdl"] = hdlR.toString()
        perms["phone"] = phoneR.toString()
        perms["postal"] = postalR.toString()
        perms["realname"] = realnameR.toString()
        perms["sm"] = smR.toString()
    }


    fun setReqs(reqs: MutableMap<String, String>): Boolean
    {
        var changed = false;

        var tmp = reqs["attest"]
        if ((tmp != null) && (tmp[0] != attestR))
        {
            changed = true; attestR = tmp[0]
        }

        tmp = reqs["ava"]
        if ((tmp != null) && (tmp[0] != avaR))
        {
            changed = true; avaR = tmp[0]
        }

        tmp = reqs["billing"]
        if ((tmp != null) && (tmp[0] != billingR))
        {
            changed = true; billingR = tmp[0]
        }

        tmp = reqs["dob"]
        if ((tmp != null) && (tmp[0] != dobR))
        {
            changed = true; dobR = tmp[0]
        }

        tmp = reqs["email"]
        if ((tmp != null) && (tmp[0] != emailR))
        {
            changed = true; emailR = tmp[0]
        }

        tmp = reqs["hdl"]
        if ((tmp != null) && (tmp[0] != hdlR))
        {
            changed = true; hdlR = tmp[0]
        }

        tmp = reqs["phone"]
        if ((tmp != null) && (tmp[0] != phoneR))
        {
            changed = true; phoneR = tmp[0]
        }

        tmp = reqs["postal"]
        if ((tmp != null) && (tmp[0] != postalR))
        {
            changed = true; postalR = tmp[0]
        }

        tmp = reqs["realname"]
        if ((tmp != null) && (tmp[0] != realnameR))
        {
            changed = true; realnameR = tmp[0]
        }

        tmp = reqs["sm"]
        if ((tmp != null) && (tmp[0] != smR))
        {
            changed = true; smR = tmp[0]
        }

        return changed
    }
}
