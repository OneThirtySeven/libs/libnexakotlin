package org.nexa.libnexakotlin
private val LogIt = GetLog("BU.dest")

private val EmptyByteArray = byteArrayOf()
private val EmptySecret = UnsecuredSecret(byteArrayOf())  // TODO make a special object that asserts if you use

/** A destination for payments.  This includes all the information needed to send and spend from this destination.
It is often assumed that Bitcoin addresses are payment destinations, but this is not true.  You really need an output script, and also (if P2SH) a redeem script.
The reason why P2PKH addresses "work" as payment destinations is because they imply a specific script.  But "baking in" this assumption will make the wallet a
lot less flexible.
 */
@cli(Display.Simple, "A destination for payments.  This includes all the information needed to send and spend, although you may need to unlock the wallet when spending.  If you do not know the spending information (its someone else's address), use the PayAddress class.")
abstract class PayDestination(val chainSelector: ChainSelector) : BCHserializable
{
    companion object
    {
        fun GetPubKey(privateKey: ByteArray): ByteArray = libnexa.getPubKey(privateKey)

        /** Deserializes a PayDestination of the correct derived class */
        fun from(ser:BCHserialized, chainSelector: ChainSelector):PayDestination
        {
            val destType = ser.debyte().toInt()
            val idx = ser.deint64()
            return when (destType)
            {
                Pay2PubKeyTemplateDestination.DEST_TYPE -> {
                    val ret = Pay2PubKeyTemplateDestination(chainSelector, ser)
                    ret.index = idx
                    ret
                }
                Pay2PubKeyHashDestination.DEST_TYPE ->
                {
                    val ret = Pay2PubKeyHashDestination(chainSelector, ser)
                    ret.index = idx
                    ret
                }
                // Pay2ScriptHashDestination.DEST_TYPE -> Pay2ScriptHashDestination(chainSelector, it)
                // MultisigDestination.DEST_TYPE -> MultisigDestination(chainSelector, it)
                else -> throw DeserializationException("destination type not handled")
            }
        }
    }

    /** serialize any derived class & the type indicator */
    fun serializeTypeAndDerived(st: SerializationType):BCHserialized
    {
        assert(st == SerializationType.DISK)
        return BCHserialized(st).addUint8(derivedType).addInt64(index).add(serializeDerived((st)))
    }
    /** serialize any derived class & the type indicator */
    fun serializeTypeAndDerived(ser: BCHserialized):BCHserialized
    {
        assert(ser.format == SerializationType.DISK)
        return ser.addUint8(derivedType).addInt64(index).add(serializeDerived(ser.format))
    }

    abstract fun serializeDerived(format: SerializationType): BCHserialized

    // Store this object into a BCHserialized stream and return it
    override fun BCHserialize(format: SerializationType): BCHserialized = serializeTypeAndDerived(format)

    // Load this object with data from the passed stream
    override open fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        throw NotImplementedError()
    }

    /** If a Bip44 wallet, this is be the BIP44 index.  Otherwise its the an incrementing integer every time a new destination is created */
    open var index: Long = 0
    /** What type of destination is this? */
    open val derivedType: Int = 0
    /** An indication to retire this address as soon as coins are received (typically used for change). RAM only */
    var oneUse:Boolean = false

    /** Get the [P2SH](%budoc/glossary/P2SH) [redeem script](%budoc/glossary/redeem_script) needed to spend this destination.
     * @return an empty script if this destination is not P2SH */
    @cli(Display.Simple, "Get the [P2SH](%budoc/glossary/P2SH) [redeem script](%budoc/glossary/redeem_script) needed to spend this destination, or an empty script if not P2SH")
    open fun redeemScript(): SatoshiScript = SatoshiScript(chainSelector)

    /** Get the [constraint script](https://bitcoinunlimited.net/glossary/output_script) needed to send coins to this destination.  This script will contain spend constraints in the non-P2SH case, or in the
     * [P2SH](https://bitcoinunlimited.net/glossary/P2SH) case this script only constrains spending to execution of the redeemScript and any additional constraints are located in the redeemScript
     * @return the outputscript needed to constrain coins to this destination */
    open fun outputScript(): SatoshiScript = SatoshiScript(chainSelector)

    /** Get the [constraint script](https://bitcoinunlimited.net/glossary/output_script) needed to send coins to this destination.  This script will contain spend constraints in the non-P2SH case, or in the
     * [P2SH](https://bitcoinunlimited.net/glossary/P2SH) case this script only constrains spending to execution of the redeemScript and any additional constraints are located in the redeemScript
     * @return the outputscript needed to constrain coins to this destination */
    @cli(Display.Simple, """"Get the [constraint script](https://bitcoinunlimited.net/glossary/output_script) needed to send coins to this destination.  This is also called the "output script" or the "scriptPubKey".""")
    fun constraintScript(): SatoshiScript = outputScript()

    /** Get the groupless suffix [constraint script](https://bitcoinunlimited.net/glossary/output_script) needed to send coins to this destination.
     * In other words, this is not a complete output script -- it needs a prefix to either choose no group (NEXA) or a group.
     * It is used as a fragment in script template operations.
     * @return the outputscript needed to constrain coins to this destination */
    open fun grouplessConstraintScript(): SatoshiScript = SatoshiScript(chainSelector)

    /** Get the [output script](https://bitcoinunlimited.net/glossary/output_script) needed to send tokens to this group destination.  This script will contain spend constraints in the non-P2SH case, or in the
     * [P2SH](https://bitcoinunlimited.net/glossary/P2SH) case this script only constrains spending to execution of the redeemScript and any additional constraints are located in the redeemScript
     * @return the outputscript needed to constrain coins to this destination */
    open fun groupedOutputScript(groupId: GroupId, groupAmount: Long): SatoshiScript = throw WalletNotSupportedException("this script destination cannot be grouped")

    /** Get the [output script](https://bitcoinunlimited.net/glossary/output_script) needed to send native coins to this destination.  This script will contain spend constraints in the non-P2SH case, or in the
     * [P2SH](https://bitcoinunlimited.net/glossary/P2SH) case this script only constrains spending to execution of the redeemScript and any additional constraints are located in the redeemScript
     * @return the outputscript needed to constrain coins to this destination */
    open fun ungroupedOutputScript(): SatoshiScript = outputScript()

    @cli(Display.Simple, """"Get the [constraint script](https://bitcoinunlimited.net/glossary/output_script) needed to send tokens and coins to this destination.  This is also called the "output script" or the "scriptPubKey".""")
    fun groupedConstraintScript(groupId: GroupId, groupAmount: Long): SatoshiScript = groupedOutputScript(groupId, groupAmount)


    /** Create a spend (input) script that will satisfy the constraints specified by the [outputScript] and the [redeemScript] (if applicable).  This script will contain the redeemScript in the P2SH case.
     * @param [params] provide needed, script-specific args in the params field, like the signature
     */
    open fun spendScript(vararg params: ByteArray): SatoshiScript = SatoshiScript(chainSelector)

    /** Create a spend (input) script that will satisfy the constraints specified by the [outputScript] and the [redeemScript] (if applicable).  This script will contain the redeemScript in the P2SH case.
     * Many scripts require signatures.  This API provides the data needed for this class to craft signatures, and provides additional derived-class defined params.
     * @param [params] provide needed, script-specific args in the params field
     */
    open fun spendScript(flatTx: ByteArray, inputIdx: Long, sigHashType: ByteArray, inputAmount: Long, vararg params: ByteArray): SatoshiScript = SatoshiScript(chainSelector)


    @cli(Display.Simple, """Get the script that can spend this destination (AKA input script, sigScript, spend script).  Many constraints require the transaction info (for signatures) so this data is supplied explicitly.  Additional script-specific data can be passed in the params field.""")
    fun satisfierScript(flatTx: ByteArray, inputIdx: Long, sigHashType: ByteArray, inputAmount: Long, vararg params: ByteArray): SatoshiScript = spendScript(flatTx, inputIdx, sigHashType, inputAmount, *params)

    /**
     * Calculate the public key hash (PKH)
     */
    @cli(Display.Simple, """Return the public key hash (if this destination has a public key) otherwise return null""")
    fun pkh(): ByteArray?
    {
        pubkey?.let {
            return libnexa.hash160(it)
        }
        return null
    }

    @cli(Display.Simple, """Return the template args hash (if this is a template destination) otherwise return null""")
    open fun argsHash(): ByteArray?
    {
        return null
    }

    /** Get the [P2SH](%budoc/glossary/P2SH) or [P2PKH](%budoc/glossary/P2PKH) address associated with this destination
     *  Note that only a subset of PayDestinations have addresses.  A PayAddress will only exist if this destination constrains spending to require a signature (in the P2PKH case) or a script (P2SH case)
     *  @return Payment address associated with this destination or null if no address exists
     */
    @cli(Display.Simple, """Return the address (if this destination has an address) otherwise return null.""")
    open val address: PayAddress?
        get() = PayAddress(chainSelector, PayAddressType.P2PKH, ByteArray(0))

    /** Return a set of bytes that can be put into a bloom filter to select any transaction that contains this destination */
    open val bloomFilterData: ByteArray?
        get() = null

    /** Get an output (constrained to this destination) suitable for placing in a transaction  */
    @cli(Display.Simple, """Get an output (constrained to this destination) suitable for placing in a transaction""")
    open fun output(satoshis: Long): iTxOutput
    {
        return txOutputFor(chainSelector, satoshis, ungroupedOutputScript())
    }

    /** Get the public key if a single signature is needed to spend this destination
     *   @return If no pubkey exists, returns null
     */
    @cli(Display.Simple, """Get the public key if a single signature is needed to spend this destination, otherwise null""")
    abstract val pubkey: ByteArray?

    /** Get the secret key if a single signature is needed to spend this destination
     *   @return If no secret is needed to spend, return null
     */
    @cli(Display.Simple, """Get the secret if a single signature is needed to spend this destination, otherwise null""")
    abstract val secret: Secret?
}

/** Represents a "standard" P2PKH payment destination
 *
 */
class Pay2PubKeyHashDestination(chainSelector: ChainSelector, override var secret: Secret, idx: Long) : PayDestination(chainSelector)
{
    init
    {
        index = idx
    }
    companion object
    {
        const val DEST_TYPE = 1 // For object identification when serialized
    }

    override val derivedType = DEST_TYPE

    override var pubkey = GetPubKey(secret.getSecret())
    var pubkeyHash = libnexa.hash160(pubkey)

    constructor(chainSelector: ChainSelector, stream: BCHserialized) : this(chainSelector, UnsecuredSecret(stream.deByteArray()),0)
    {
    }

    override fun equals(other: Any?): Boolean
    {
        return contentEquals(other)
    }

    fun contentEquals(other: Any?): Boolean
    {
        if (other == null) return false
        if (other is Pay2PubKeyHashDestination)
        {
            // Check pubkeys here because the secret may require unlocking
            return (pubkey contentEquals other.pubkey)
        }
        return false
    }

    fun updateSecret(s: Secret)
    {
        secret = s
        pubkey = GetPubKey(secret.getSecret())
        pubkeyHash = libnexa.hash160(pubkey)
    }

    override fun serializeDerived(format: SerializationType): BCHserialized
    {
        // check format so the secret isn't accidentally sent over the network
        if (format == SerializationType.DISK)
        {
            return BCHserialized(format) + variableSized(secret.getSecret())  // TODO encrypt secret
        }
        throw NotImplementedError()
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        val s = BCHserialized(SerializationType.UNKNOWN).deByteArray()
        updateSecret(UnsecuredSecret(s))
        return stream
    }

    override fun redeemScript(): SatoshiScript
    {
        return SatoshiScript(chainSelector)
    }

    override fun outputScript(): SatoshiScript
    {
        return SatoshiScript.p2pkh(pubkeyHash, chainSelector)
    }

    override fun grouplessConstraintScript(): SatoshiScript = outputScript()

/*
    fun spendScriptECDSA(flatTx: ByteArray, inputIdx: Long, sigHashType: Int, inputAmount: Long, vararg params: ByteArray): SatoshiScript
    {
        assert(params.size == 0)  // params is unused in this contract
        val sig = libnexa.signBchTxOneInputUsingECDSA(flatTx, sigHashType, inputIdx, inputAmount, ungroupedOutputScript().flatten(), secret.getSecret())
        val pubkey = pubkey // PayDestination.GetPubKey(secret.getSecret())
        return SatoshiScript(chainSelector, SatoshiScript.Type.PUSH_ONLY, OP.push(sig), OP.push(pubkey))
    }
*/
    override fun spendScript(flatTx: ByteArray, inputIdx: Long, sigHashType: ByteArray, inputAmount: Long, vararg params: ByteArray): SatoshiScript
    {
        assert(params.size == 0)  // params is unused in this contract
        val sig = if (chainSelector.isBchFamily)
        {
            if (sigHashType.size != 0) throw UnimplementedException("Only sighash all is supported in BCH chain")
            libnexa.signBchTxOneInputUsingSchnorr(flatTx, BCH_SIGHASH_ALL, inputIdx, inputAmount, ungroupedOutputScript().flatten(), secret.getSecret())
        }
        else
        {
            libnexa.signTxOneInputUsingSchnorr(flatTx, sigHashType, inputIdx, inputAmount, ungroupedOutputScript().flatten(), secret.getSecret())
        }
        assert(pubkey contentEquals PayDestination.GetPubKey(secret.getSecret()))
        return SatoshiScript(chainSelector, SatoshiScript.Type.PUSH_ONLY, OP.push(sig), OP.push(pubkey))
    }

    override val bloomFilterData: ByteArray?
        get() = pubkeyHash

    override val address: PayAddress
        get() = PayAddress(chainSelector, PayAddressType.P2PKH, pubkeyHash)
}

/** Represents a "standard" P2PKT payment destination
 *
 */
class Pay2PubKeyTemplateDestination(chainSelector: ChainSelector, override var secret: Secret, idx: Long) : PayDestination(chainSelector)
{
    init
    {
        index = idx
    }

    companion object
    {
        const val DEST_TYPE = 2 // For object identification when serialized
    }

    override val derivedType = DEST_TYPE

    override var pubkey:ByteArray? = null
        get() {
            if (field == null) field = GetPubKey(secret.getSecret())
            return field
        }
    fun pubkeyOrThrow():ByteArray
    {
        return pubkey ?: GetPubKey(secret.getSecret())
    }

    var groupId: GroupId? = null
    var scriptCode = SatoshiScript(chainSelector, SatoshiScript.Type.SATOSCRIPT, OP.FROMALTSTACK, OP.CHECKSIGVERIFY).flatten()

    constructor(chainSelector: ChainSelector, stream: BCHserialized) : this(chainSelector, EmptySecret, 0)
    {
        BCHdeserialize(stream)
    }

    fun updateSecret(s: Secret, pubK: ByteArray? = null)
    {
        secret = s
        if ((pubK != null) && (pubK.size > 0)) pubkey = pubK
        else pubkey = null
    }

    override fun equals(other: Any?): Boolean
    {
        return contentEquals(other)
    }

    fun contentEquals(other: Any?): Boolean
    {
        if (other == null) return false
        if (other is Pay2PubKeyTemplateDestination)
        {
            // Check pubkeys here because the secret may require unlocking
            return (pubkey contentEquals other.pubkey)
        }
        return false
    }

    override fun serializeDerived(format: SerializationType): BCHserialized
    {
        // check format so the secret isn't accidentally sent over the network
        if (format == SerializationType.DISK)
        {
            // serialize the pubkey in case the secret is encrypted so we don't have continual access to it.
            val pub:ByteArray = pubkey ?: EmptyByteArray
            return BCHserialized(format).add(variableSized(secret.getSecret())).add(variableSized(pub))
        }
        throw NotImplementedError()
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
         if (stream.format == SerializationType.DISK)
         {
             val s = stream.deByteArray()
             val p = stream.deByteArray()
             updateSecret(UnsecuredSecret(s), p)
         }
        else throw NotImplementedError()
        return stream
    }

    override fun redeemScript(): SatoshiScript
    {
        return SatoshiScript(chainSelector)
    }

    override fun outputScript(): SatoshiScript
    {
        return ungroupedOutputScript()
    }

    override fun grouplessConstraintScript(): SatoshiScript = SatoshiScript.p2pktSuffix(chainSelector, pubkeyOrThrow())

    override fun ungroupedOutputScript(): SatoshiScript
    {
        return SatoshiScript.ungroupedP2pkt(chainSelector, pubkeyOrThrow())
    }

    override fun groupedOutputScript(groupId: GroupId, groupAmount: Long): SatoshiScript
    {
        return SatoshiScript.gp2pkt(chainSelector, groupId, groupAmount, pubkeyOrThrow())
    }

    @cli(Display.Simple, """Return the template args hash (if this is a template destination) otherwise return null""")
    override fun argsHash(): ByteArray
    {
        val constraintArgsScript = SatoshiScript(chainSelector, SatoshiScript.Type.PUSH_ONLY, OP.push(pubkeyOrThrow()))
        val constraintArgsHash = libnexa.hash160(constraintArgsScript.toByteArray())
        return constraintArgsHash
    }

    override fun spendScript(flatTx: ByteArray, inputIdx: Long, sigHashType: ByteArray, inputAmount: Long, vararg params: ByteArray): SatoshiScript
    {
        assert(params.size == 0)  // params is unused in this contract
        val sig = libnexa.signTxOneInputUsingSchnorr(flatTx, sigHashType, inputIdx, inputAmount, scriptCode, secret.getSecret())
        val args = SatoshiScript(chainSelector, SatoshiScript.Type.PUSH_ONLY, OP.push(pubkeyOrThrow())).flatten()
        return SatoshiScript(chainSelector, SatoshiScript.Type.PUSH_ONLY, OP.push(args), OP.push(sig))
    }

    override val bloomFilterData: ByteArray?
        get()
        {
            val constraintArgsScript = SatoshiScript(chainSelector, SatoshiScript.Type.PUSH_ONLY, OP.push(pubkeyOrThrow()))
            return libnexa.hash160(constraintArgsScript.toByteArray())
        }

    override val address: PayAddress
        get()
        {
            val gid = groupId
            val scr: SatoshiScript =
              if (gid != null) SatoshiScript.gp2pkt(chainSelector, gid, 0, pubkeyOrThrow())  // group amount 0 in this case implies that the address does not specify an amount (which would be weird)
              else SatoshiScript.ungroupedP2pkt(chainSelector, pubkeyOrThrow())
            return PayAddress(chainSelector, PayAddressType.TEMPLATE, scr.asSerializedByteArray())
        }
}

/** Represents a P2PK (pay to public key) destination wrapped in a P2SH script
 */
class Pay2ScriptPubKeyDestination(chainSelector: ChainSelector, override val secret: Secret, idx: Long) : PayDestination(chainSelector)
{
    init
    {
        index = idx
    }

    override val pubkey = GetPubKey(secret.getSecret())
    val pubkeyHash = libnexa.hash160(pubkey)

    val redeemScriptHash: ByteArray
        get() = libnexa.hash160(redeemScript().flatten())

    override fun equals(other: Any?): Boolean
    {
        return contentEquals(other)
    }

    override fun serializeDerived(format: SerializationType): BCHserialized
    {
        throw NotImplementedError()
    }

    fun contentEquals(other: Any?): Boolean
    {
        if (other == null) return false
        if (other is Pay2ScriptPubKeyDestination)
        {
            // Check pubkeys here because the secret may require unlocking
            return (pubkey contentEquals other.pubkey)
        }
        return false
    }

    override fun redeemScript(): SatoshiScript
    {
        var ret = SatoshiScript(chainSelector, SatoshiScript.Type.SATOSCRIPT, OP.DUP, OP.HASH160, OP.push(pubkeyHash), OP.EQUALVERIFY, OP.CHECKSIG)
        return ret
    }

    override fun outputScript(): SatoshiScript = SatoshiScript.p2sh(redeemScriptHash, chainSelector)
    override fun grouplessConstraintScript(): SatoshiScript = SatoshiScript.p2sh(redeemScriptHash, chainSelector)

    override fun groupedOutputScript(groupId: GroupId, groupAmount: Long): SatoshiScript
    {
        if (chainSelector == ChainSelector.BCH) throw(WalletNotSupportedException("Blockchain does not support group tokens"))

        // If this group holds native tokens, then it must either be an authority or have 0 tokens
        assert(!groupId.isFenced() || (groupAmount <= 0))
        // TODO: NEXA groups
        assert(false)
        //return BCHscript.gp2sh(groupId, groupAmount, redeemScriptHash, chainSelector)
        return SatoshiScript(chainSelector)
    }

    /**
     *  @param [params] Provide a single parameter which is the signature derived from [secret] of the transaction that is including this spend script
     */
    override fun spendScript(vararg params: ByteArray): SatoshiScript
    {
        var redeem = redeemScript().flatten()
        return SatoshiScript(chainSelector, SatoshiScript.Type.PUSH_ONLY, OP.push(redeem).v, pubkey, params[0])
    }

    override val bloomFilterData: ByteArray?
        get() = redeemScriptHash

    override val address: PayAddress
        get() = PayAddress(chainSelector, PayAddressType.P2PKH, pubkeyHash)
}

/** Represents an arbitrary P2SH destination
 */
open class Pay2ScriptHashDestination(chainSelector: ChainSelector, idx: Long) : PayDestination(chainSelector)
{
    init
    {
        index = idx
    }
    companion object
    {
        const val DEST_TYPE = 3 // For object identification when serialized
    }

    override val derivedType = DEST_TYPE

    override var secret: Secret? = null // Not applicable for this destination
    override val pubkey: ByteArray? = null  // Not applicable for this destination

    var redeemScript = SatoshiScript(chainSelector)

    val redeemScriptHash: ByteArray
        get() = libnexa.hash160(redeemScript().flatten())

    constructor(chainSelector: ChainSelector, redeem: SatoshiScript, index: Long) : this(chainSelector, index)
    {
        redeemScript = redeem
    }

    override fun serializeDerived(format: SerializationType): BCHserialized
    {
        throw NotImplementedError()
    }

    override fun equals(other: Any?): Boolean
    {
        return contentEquals(other)
    }

    open fun contentEquals(other: Any?): Boolean
    {
        if (other == null) return false
        if (other is Pay2ScriptHashDestination)
        {
            return (redeemScript contentEquals other.redeemScript)
        }
        return false
    }

    override fun redeemScript(): SatoshiScript
    {
        return redeemScript
    }

    override fun outputScript(): SatoshiScript
    {
        //return BCHscript(chainSelector, OP.HASH160, OP.push(address.data), OP.EQUAL)
        return SatoshiScript.p2sh(address.data, chainSelector)
    }
    override fun grouplessConstraintScript(): SatoshiScript = SatoshiScript.p2sh(redeemScriptHash, chainSelector)


    override fun groupedOutputScript(groupId: GroupId, groupAmount: Long): SatoshiScript
    {
        throw(WalletNotSupportedException("Blockchain does not support group tokens"))  // Must be, because NEXA obsoleted P2SH
    }

    /**
     *  @param [params] Provide all the data needed to spend the redeem script.  Each parameter will be pushed to the stack in the order passed (so the last parameter ends up on the top of the stack).
     */
    override fun spendScript(vararg params: ByteArray): SatoshiScript
    {
        var redeem = redeemScript().flatten()
        val ret = SatoshiScript(chainSelector)
        for (p in params)
        {
            ret.add(OP.push(p).v)
        }
        ret.add(OP.push(redeem).v)
        return ret
    }

    override val bloomFilterData: ByteArray?
        get() = redeemScriptHash

    override val address: PayAddress
        get() = PayAddress(chainSelector, PayAddressType.P2SH, redeemScript().scriptHash160())
}

class MultisigDestination(
  chain: ChainSelector,
  val minSigs: Int,  // mininum number of signatures (n)
  val pubkeys: Array<ByteArray>,    // Order defines order in script, count defines total number of multisig signers (m)
  val privkeys: Array<Secret?>,  // Pass null for privkeys you don't know
  index: Long
) : Pay2ScriptHashDestination(chain, index)
{
    override val derivedType = DEST_TYPE

    init
    {
        redeemScript = redeemScript()
    }

    /*
    companion object
    {
        const val DEST_TYPE = 4 // For object identification when serialized

        @cli(Display.Dev, "Construct a Multisig payment destination from its redeem script and some wallets.  If you do not have enough wallets to fully sign, then the resulting satisfier script will not be fully signed (but could be given to another wallet to finish (TBD)).")
        fun fromRedeem(redeemScript: SatoshiScript, vararg wallets: Wallet): PayDestination
        {
            val redeem = redeemScript.parsed()
            val pubkeys = mutableListOf<ByteArray>()
            val privkeys = mutableListOf<Secret?>()
            val minSigs = scriptNumFrom(redeem[0])!!
            for (item in redeem)  // all data items in the redeem scripts are the wallet pubkeys
            {
                val pubkey = scriptDataFrom(item)
                if (pubkey != null)  // If its null, its not data (not a pubkey)
                {
                    pubkeys.add(pubkey)
                    var found = false
                    for (w in wallets)
                    {
                        val privkey = w.pubkeyToSecret(pubkey)
                        if (privkey != null)
                        {
                            privkeys.add(privkey)
                            found = true
                            break
                        }
                    }
                    if (!found)
                    {
                        privkeys.add(null)
                    }
                }
            }
            return MultisigDestination(redeemScript.chainSelector, minSigs.toInt(), pubkeys.toTypedArray(), privkeys.toTypedArray())
        }
    }
    */

    override fun serializeDerived(format: SerializationType): BCHserialized
    {
        throw NotImplementedError()
    }


    override fun equals(other: Any?): Boolean
    {
        return contentEquals(other)
    }

    override fun contentEquals(other: Any?): Boolean
    {
        if (other == null) return false
        if (other is MultisigDestination)
        {
            // Check pubkeys here because the secret may require unlocking
            if (pubkeys.size != other.pubkeys.size) return false
            for ((p0,p1) in pubkeys zip other.pubkeys)
                return (p0 contentEquals p1)
        }
        return false
    }

    override fun redeemScript(): SatoshiScript
    {
        val ret = SatoshiScript(chainSelector) + OP.push(minSigs.toLong()).v
        for (pk in pubkeys)
            ret.add(OP.push(pk).v)
        ret.add(OP.push(pubkeys.size.toLong()).v)
        ret.add(OP.CHECKMULTISIG.v)
        return ret
    }

    fun spendScriptECDSA(flatTx: ByteArray, inputIdx: Long, sigHashType: Int, inputAmount: Long, vararg params: ByteArray): SatoshiScript
    {
        assert(params.size == 0)  // No params needed for multisig
        val ret = SatoshiScript(chainSelector)

        ret.add(OP.push(0))
        for (priv in privkeys)
        {
            if (priv != null)
            {
                val sc = redeemScript().flatten()
                val secret = priv.getSecret()
                val sig: ByteArray = libnexa.signBchTxOneInputUsingECDSA(flatTx, sigHashType, inputIdx, inputAmount, sc, secret)
                //val sig: ByteArray = Wallet.signOneInputUsingSchnorr(flatTx, sigHashType, inputIdx, inputAmount, sc, secret)
                ret.add(OP.push(sig))
                // println("sighashtype: 0x${sigHashType.toString(16)}, input: ${inputIdx} amount: ${inputAmount} constraintScript: ${sc.toHex()} tx: ${flatTx.toHex()}\npriv: ${secret.toHex()} -> sig: ${sig.toHex()}")
            }
        }

        var redeem = redeemScript().flatten()
        ret.add(OP.push(redeem))
        return ret
    }

    override fun spendScript(flatTx: ByteArray, inputIdx: Long, sigHashType: ByteArray, inputAmount: Long, vararg params: ByteArray): SatoshiScript
    {
        assert(params.size == 0)  // No params needed for multisig
        val ret = SatoshiScript(chainSelector)

        var spendMap = 0
        var privCount = 0
        for (priv in privkeys.reversed())
        {
            spendMap = spendMap shl 1
            if (priv != null)
            {
                spendMap = spendMap or 1
                privCount++
            }
        }

        ret.add(OP.push(spendMap))
        for (priv in privkeys)
        {
            if (priv != null)
            {
                val sc = redeemScript().flatten()
                val secret = priv.getSecret()
                // TODO distinguish NEXA vs BCH
                val sig: ByteArray = libnexa.signTxOneInputUsingSchnorr(flatTx, sigHashType, inputIdx, inputAmount, sc, secret)
                ret.add(OP.push(sig))
                // println("sighashtype: 0x${sigHashType.toString(16)}, input: ${inputIdx} amount: ${inputAmount} constraintScript: ${sc.toHex()} tx: ${flatTx.toHex()}\npriv: ${secret.toHex()} -> sig: ${sig.toHex()}")
            }
        }

        var redeem = redeemScript().flatten()
        ret.add(OP.push(redeem))
        return ret
    }
}
