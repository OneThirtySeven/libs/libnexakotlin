package org.nexa.libnexakotlin

import kotlin.math.absoluteValue
import kotlin.time.ExperimentalTime
import kotlin.time.TimeMark
import kotlin.time.TimeSource
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.*
import org.nexa.threads.iGate

private val LogIt = GetLog("nexa.useful")

/** Helper class to pass around ip and port pairs */
data class IpPort(val ip: String, val port: Int)

/** callback to notify app of any exception that happened within the coroutine launch.
 * Return False to raise the exception, causing program abort.
 * If null, exception is logged and ignored */
var launchExceptionHandler: ((e: Throwable) -> Boolean)? = null
@OptIn(kotlin.experimental.ExperimentalNativeApi::class)
val exceptionHandler = CoroutineExceptionHandler { _, exception ->
    // Handle your exception here
    val stack = exception.stackTraceToString()
    val cause = if (exception.cause != null) "  Cause: ${exception.cause}\n" else ""
    LogIt.error(sourceLoc() + ": CoroutineExceptionHandler: $exception\n${cause}  Message: ${exception.message}\n  Stack:\n  ${stack}")
}


fun min(a: Long, b: Long) = if (a < b) a else b
fun max(a: Long, b: Long) = if (a < b) b else a

// Because kotlin overloading sometimes seems broken
fun min(a: Int, b: Int) = if (a < b) a else b
fun max(a: Int, b: Int) = if (a < b) b else a

/** convert this byte to an int, treating the byte as unsigned
 * I am wrapping toUByte() because something seems broken there -- toUInt().toInt() sign extends, toUByte does not.  This seems inconsistent confusing.
 * */
@Deprecated("use toPositiveInt")
fun Byte.asUnsigned():Int = toPositiveInt()  // toUByte().toInt()

@Deprecated("use toPositiveInt")
fun Byte.toUint():Int = toInt() and 0xFF
@Deprecated("use toPositiveInt")
fun Byte.uAsInt():Int = toInt() and 0xFF
/** convert this byte to an Int, treating the byte as unsigned */
fun Byte.toPositiveInt() = toInt() and 0xFF

/** convert this byte to an unsigned Int, treating the byte as unsigned */
fun Byte.toPositiveUInt() = toUInt() and 0xFFU

/** convert this byte to a Long, treating the byte as unsigned */
fun Byte.toPositiveLong() = toLong() and 0xFF

/** convert this byte to an unsigned Long, treating the byte as unsigned */
fun Byte.toPositiveULong() = toULong() and 0xFFUL

/** Receive from a channel with timeout */
suspend fun <T> Channel<T>.receive(timeout: Long): T?
{
    var ret: T? = null
    try
    {
        withTimeout(timeout.toLong()) {
            ret = receive()
        }
    }
    catch (e: TimeoutCancellationException)
    {
    }
    return ret
}

class EJ(val json: JsonElement?)
{
    operator fun get(s: String): EJ
    {
        if (json == null) throw NoSuchElementException(s)
        val jo = json as JsonObject
        return EJ(jo[s])
    }

    operator fun get(i: Int): EJ
    {
        val jo = json as JsonArray
        return EJ(jo[i])
    }

    val double: Double
        get() = (json as JsonPrimitive).double

    val int: Int
        get() = (json as JsonPrimitive).int

    val long: Long
        get() = (json as JsonPrimitive).long

    val float: Float
        get() = (json as JsonPrimitive).float

    val boolean: Boolean
        get() = (json as JsonPrimitive).boolean

    val doubleOrNull: Double?
        get() = (json as JsonPrimitive).doubleOrNull

    val intOrNull: Int?
        get() = (json as JsonPrimitive).intOrNull

    val longOrNull: Long?
        get() = (json as JsonPrimitive).longOrNull

    val floatOrNull: Float?
        get() = (json as JsonPrimitive).floatOrNull

    val booleanOrNull: Boolean?
        get() = (json as JsonPrimitive).booleanOrNull


    val string: String
        get() = (json as JsonPrimitive).content

    val content: String
        get() = (json as JsonPrimitive).content

    val contentOrNull: String?
        get() = (json as JsonPrimitive).contentOrNull

    override fun toString(): String
    {
        val format = Json { prettyPrint = true }
        return format.encodeToString(json)
    }
}

fun logThreadException(e: Throwable, s: String = "", caughtAt: String? = "")
{
    LogIt.warning(caughtAt + ": " + s + "Expected Exception: " + e.toString())
    LogIt.warning(e.stackTraceToString())
}

fun handleThreadException(e: Throwable, s: String = "", caughtAt: String? = "")
{
    LogIt.warning("$caughtAt: $s.  Unexpected Exception: $e")
    LogIt.warning(e.stackTraceToString())
    launchExceptionHandler?.let { if (!it(e)) throw(e) }
}

fun launch(scope: CoroutineScope? = null, fn: (suspend () -> Unit)?)
{
    val s = scope ?: GlobalScope
    s.launch(exceptionHandler) {
        if (fn != null) fn()
    }
}

fun range(start: Int, count: Int): IntRange
{
    if (count > 0)
        return IntRange(start, start + count - 1)
    else if (count < 0)
        return IntRange(start - count + 1, start)
    //else if (count == 0)
    return IntRange.EMPTY
}

enum class Units {
    BestBytes
}

// TODO make this generic (the problem is converting the constants into numbers of type T
fun Int.toStringU(u:Units): String
{
    //val abs:T = if (this < 0) -1*this else this
    val abs = this.absoluteValue
    if (u == Units.BestBytes)
    {
        // lowercase k,m,b because SI units, not powers of 2
        if (abs < 10000) return this.toString() + "b"
        if (abs < 10000000) return (this/1000).toString() + "kB"
        if (abs < 10000000000) return (this/1000000).toString() + "mB"
        else return (this/1000000000).toString() + "gB"
    }
    return this.toString()
}

// TODO make this generic (the problem is converting the constants into numbers of type T
// Use Number instead of Long?
fun Long.toString(u: Units): String
{
    //val abs:T = if (this < 0) -1*this else this
    val abs = this.absoluteValue
    if (u == Units.BestBytes)
    {
        // lowercase k,m,b because SI units, not powers of 2
        if (abs < 10000L) return this.toString() + "b"
        if (abs < 10000000L) return (this/1000L).toString() + "kB"
        if (abs < 10000000000L) return (this/1000000L).toString() + "mB"
        else return (this/1000000000L).toString() + "gB"
    }
    return this.toString()
}

@OptIn(ExperimentalTime::class)
class Periodically(var periodInMs: Long)
{
    var last: TimeMark? = null
    operator fun invoke(): Boolean
    {
        val lastv = last
        if (lastv == null)
        {
            last = TimeSource.Monotonic.markNow()
            return true
        }
        else
        {
            if (lastv.elapsedNow().inWholeMilliseconds >= periodInMs)
            {
                last = TimeSource.Monotonic.markNow()
                return true
            }
        }
        return false
    }

    /** invoke again right away */
    fun reset()
    {
        last = null
    }

    override fun toString(): String
    {
        val tmp = last
        val lastStr = if (tmp == null) "never" else tmp.elapsedNow().inWholeSeconds.toString() + "sec ago"
        return "Every ${periodInMs} (triggered $lastStr}"
    }
}
class CallAfter(var doneBits: Long = 0, var fn: (() -> Unit)? = null)
{
    var bitmap: Long = 0

    fun reset(undoneBits: Long)
    {
        bitmap = bitmap and undoneBits
    }

    fun setCallback(callwhen: Long, f: (() -> Unit)?)
    {
        doneBits = callwhen
        fn = f
        invoke(0)  // calls the fn if all the bits were already set
    }

    operator fun invoke(completed: Long)
    {
        bitmap = bitmap or completed
        var tmp = bitmap xor doneBits
        fn?.let {
            if (tmp == 0.toLong()) it()
        }
    }
}
/**  Call the passed function after this function has been called 'count times */
class CallAfterN(val fn: (() -> Unit)?, val count: Long)
{
    var cur: Long = 0

    constructor(fn: (() -> Unit)?, c: Int) : this(fn, c.toLong())

    operator fun invoke()
    {
        if (fn == null) return
        cur += 1
        if (cur == count) fn.invoke()
    }
}


/** Behaves like a condition, but for co-routines */
class CoCond<T>(val scope: CoroutineScope)
{
    val ch = Channel<T>()

    //* Wait for a maximum of 'timeout' ms, for this cond to be awakened.  If awakened, invoke 'until' if not null and
    //  return true if 'until' returns true.
    @OptIn(ExperimentalTime::class)
    suspend fun yield(until: ((T) -> Boolean)? = null, timeout: Int = Int.MAX_VALUE): T?
    {

        val start = TimeSource.Monotonic.markNow()
        while (start.elapsedNow().inWholeMilliseconds < timeout)
        {
            try
            {
                val r = withTimeout(timeout - start.elapsedNow().inWholeMilliseconds) {
                    ch.receive()
                }

                if (until == null)
                {
                    //LogIt.info ("yield was triggered")
                    return r
                }
                else if (until(r))
                {
                    //LogIt.info ("yield satisfied predicate")
                    return r
                }
            }
            catch (e: TimeoutCancellationException)
            {
            }
        }
        // LogIt.info ("yield timed out")
        return null
    }

    //? Wake up any coroutine that is yielding on this condition
    fun wake(obj: T)
    {
        scope.launch(exceptionHandler) { ch.send(obj) }
    }
}


/** Create a new map, where the keys and values are swapped */
fun <K, V> Map<K, V>.invert(): Map<V, K>
{
    val ret = mutableMapOf<V, K>()
    for ((k, v) in this) ret[v] = k
    return ret
}

/*
fun List<String>.jsonify():String
{
    if (this.size == 0) return "[]"
    return this.joinToString("\",\"", "[\"", "\"]")
}
 */

fun List<Any?>.jsonify(): String
{
    if (this.size == 0) return "[]"
    val s = StringBuilder("[")
    var first = true
    for (item in this)
    {
        if (!first)
        {
            s.append(",")
        }
        else
        {
            first = false
        }

        val itemStr = when (item)
        {
            null -> "null"
            is String -> "\"" + item + "\""
            is Int -> item.toString()
            is Long -> item.toString()
            is UInt -> item.toString()
            is ULong -> item.toString()
            is Boolean -> if (item) "true" else "false"
            // is Jsonifyable -> item.jsonify() -- TODO add an interface to convert objects to json format
            else -> item.toString()
        }
        s.append(itemStr)
    }
    s.append("]")
    return s.toString()
}

/** split a (IP or FQDN):port formatted string (e.g 192.168.100.10:54 or www.example.com:54) into a string IP address (or FQDN) and an integer port
 * @param s The IP address or FQDN
 * @param defaultPort The port number to use if a port (i.e. a colon) is not part of the string
 * @throws Exception if the input string is bad
 */
fun splitIpPort(s: String, defaultPort: Int): IpPort
{
    val ipAndPort = s.split(":")
    if (ipAndPort.size == 2)
    {
        return IpPort(ipAndPort[0], ipAndPort[1].toInt())
    }
    return IpPort(s, defaultPort)
}
