// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.nexa.libnexakotlin
import org.nexa.threads.*

import io.ktor.network.selector.SelectorManager
import io.ktor.network.sockets.Socket
import io.ktor.network.sockets.aSocket
import io.ktor.network.sockets.openReadChannel
import io.ktor.network.sockets.openWriteChannel
import io.ktor.network.tls.tls
import io.ktor.utils.io.ByteReadChannel
import io.ktor.utils.io.ByteWriteChannel
import io.ktor.utils.io.errors.IOException
import io.ktor.utils.io.readAvailable
import io.ktor.utils.io.write
import io.ktor.utils.io.writeAvailable
import io.ktor.utils.io.writeStringUtf8
import kotlinx.atomicfu.AtomicInt
import kotlinx.atomicfu.AtomicRef
import kotlinx.atomicfu.atomic
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.JsonElement

open class ElectrumException(msg: String) : NetException(msg)

/** Electrum server never replied */
class ElectrumConnectError(node: String, underlyingError:String) : ElectrumException("cannot connect to $node, error $underlyingError")

/** Electrum server never replied */
class ElectrumRequestTimeout() : ElectrumException("timeout")

/** The reply doesn't adhere to the protocol in some manner */
class ElectrumIncorrectReply(what: String) : ElectrumException(what)

/** A passed entity was not found (transaction, block, scripthash, etc). */
class ElectrumNotFound(msg: String) : ElectrumException(msg)

/** The request was not accepted by the server */
class ElectrumIncorrectRequest(what: String) : ElectrumException(what)

private val LogIt = GetLog("BU.electrumclient")

const val HEADER_SIZE_BYTES = 80
const val DEFAULT_BCH_TCP_ELECTRUM_PORT = 50001
const val DEFAULT_BCH_SSL_ELECTRUM_PORT = 50002
const val DEFAULT_NEXA_TCP_ELECTRUM_PORT = 20001
const val DEFAULT_NEXA_SSL_ELECTRUM_PORT = 20002
const val DEFAULT_NEXATEST_TCP_ELECTRUM_PORT = 30001
const val DEFAULT_NEXATEST_SSL_ELECTRUM_PORT = 30002
const val DEFAULT_BCHREG_TCP_ELECTRUM_PORT = 60401
const val DEFAULT_NEXAREG_TCP_ELECTRUM_PORT = 30403

val DefaultElectrumTCP = mapOf(
  ChainSelector.NEXA to DEFAULT_NEXA_TCP_ELECTRUM_PORT,
  ChainSelector.NEXATESTNET to DEFAULT_NEXATEST_TCP_ELECTRUM_PORT,
  ChainSelector.NEXAREGTEST to DEFAULT_NEXAREG_TCP_ELECTRUM_PORT,
  ChainSelector.BCH to DEFAULT_BCH_TCP_ELECTRUM_PORT,
  ChainSelector.BCHTESTNET to 60001,
  ChainSelector.BCHREGTEST to DEFAULT_BCHREG_TCP_ELECTRUM_PORT
)

val DefaultElectrumSSL = mapOf(
  ChainSelector.NEXA to DEFAULT_NEXA_SSL_ELECTRUM_PORT,
  ChainSelector.NEXATESTNET to DEFAULT_NEXATEST_SSL_ELECTRUM_PORT,
  ChainSelector.NEXAREGTEST to -1,
  ChainSelector.BCH to DEFAULT_BCH_SSL_ELECTRUM_PORT,
  ChainSelector.BCHTESTNET to 60002,
)

fun ByteWriteChannel.write(s:String)
{
    //val tmp = s.encodeUtf8()
    runBlocking {
        //writeFully(tmp, 0, tmp.size)
        writeStringUtf8(s)
        flush()
    }
}

const val ELECTRUM_INVALID_PARAMS = -32602

/** Open a JSON-RPC over TCP connection to an Electrum X server */
class ElectrumClient(val chainSelector: ChainSelector, name: String, port: Int = DEFAULT_NEXA_SSL_ELECTRUM_PORT, logName: String = name + ":" + port,
    autostart: Boolean = true, useSSL:Boolean=true, connectTimeoutMs: Long = JsonRpc.CONNECT_TIMEOUT, accessTimeoutMs: Long =  JsonRpc.ACCESS_TIMEOUT):
  JsonRpc(name, port, logName, useSSL = useSSL, connectTimeoutMs, accessTimeoutMs)
{
    // This protocol is specified at:  https://bitcoincash.network/electrum

    @Serializable
    data class ErrorCodeMessage(val code: Int, val message: String)

    @Serializable
    data class ErrorCodeReply(val error: ErrorCodeMessage, val id: Int, val jsonrpc: String)

    @Serializable
    data class ErrorReply(val error: String)

    @Serializable
    data class VersionReply(val result: List<String>)

    @Serializable
    data class StringReply(val result: String)

    @Serializable
    data class HeadersResult(val count: Int, val hex: String, val max: Int)

    @Serializable
    data class HeadersReply(val result: HeadersResult)

    @Serializable
    data class HeaderResult(val height: Long, val hex: String)

    @Serializable
    data class HeaderReply(val result: HeaderResult)

    @Serializable
    data class HeaderNotification(val params: Array<HeaderResult>)

    @Serializable
    data class GetHistoryResult(val height: Int, val tx_hash: String)

    @Serializable
    data class TokenHistoryWithCursor(val cursor: String?, val history: Array<GetHistoryResult>)
    //@Serializable
    //data class ScriptHashHistoryWithCursor(val cursor: String?, val transactions: Array<GetHistoryResult>)
    //@Serializable
    //data class GetHistoryReply(val result: ScriptHashHistoryWithCursor)


    @Serializable
    data class GetHistoryReply(val result: List<GetHistoryResult>)

    @Serializable
    data class GetTokenHistoryReply(val result: TokenHistoryWithCursor)

    @Serializable
    data class FeaturesResult(val genesis_hash: String, val hash_function: String, val server_version: String, val protocol_max: String, val protocol_min: String, val pruning: Int? = null)

    @Serializable
    data class FeaturesReply(val result: FeaturesResult)

    @Serializable
    data class FirstUseResult(val block_hash: String? = null, val block_height: Int? = null, val tx_hash: String? = null)

    @Serializable
    data class FirstUseReply(val result: FirstUseResult)

    @Serializable
    data class BalanceResult(val confirmed: Int = 0, val unconfirmed: Int = 0)

    @Serializable
    data class BalanceReply(val result: BalanceResult)

    @Serializable
    data class GetUtxoSpentInfo(val height: Int?, val tx_hash: String?, val tx_pos: Int?)

    @Serializable
    data class GetUtxoResult(val amount: Long, val height: Int, val scripthash: String, val status: String, val spent: GetUtxoSpentInfo)

    @Serializable
    data class GetUtxoReply(val result: GetUtxoResult)

    @Serializable
    data class TokenGetBalanceResult(val confirmed: Map<String, Long>, val unconfirmed: Map<String, Long>, val cursor: String?)

    @Serializable
    data class TokenGetBalanceReply(val result: TokenGetBalanceResult)

    @Serializable
    data class TokenGenesisInfoReply(val result: TokenGenesisInfo)

    @Serializable
    data class UnspentInfo(
      val group:String, val height: Long, val outpoint_hash: String,
      val token_amount: Long, val token_id_hex: String, val tx_pos:Long, val value: Long, val tx_hash: String
      )
    @Serializable
    data class TokenListUnspentResult(val unspent: Array<UnspentInfo>, val cursor: String?)
    @Serializable
    data class TokenListUnspentReply(val result: TokenListUnspentResult)


    @Serializable
    data class ListUnspentResult(
      val height: Int,
      val tx_pos: Int,
      val tx_hash: String,
      val value: Long)

    @Serializable
    data class ListUnspentReply(val result: Array<ListUnspentResult>)


    /** The version this electrum client supports */
    val MY_VERSION = listOf("4.0.1", "1.4")

    /** how long to wait before giving up on a request */
    var requestTimeout: Int = 5000

    init
    {
        if (autostart) start()
    }

    /** Get the server version */
    fun version(timeoutInMs: Int = requestTimeout): Pair<String, String>
    {
        val ret = call("server.version", MY_VERSION, timeoutInMs)
        if (ret == null)
        {
            throw ElectrumRequestTimeout()
        }
        val parsed = json.decodeFromString(VersionReply.serializer(), ret)
        return Pair(parsed.result[0], parsed.result[1])
    }

    fun features(timeoutInMs: Int = requestTimeout): FeaturesResult
    {
        val ret = call("server.features", MY_VERSION, timeoutInMs)
        if (ret == null) throw ElectrumRequestTimeout()
        // LogIt.info(ret.toString())
        val parsed = json.decodeFromString(FeaturesReply.serializer(), ret)
        return parsed.result
    }

    /* Given an outpoint (txid and index) returns information about that UTXO, including whether it is spent */
    fun getBchUtxo(hexTxHash: String, outIdx: Int, timeoutInMs: Int = requestTimeout): GetUtxoResult
    {
        val ret = call("blockchain.utxo.get", listOf(hexTxHash, outIdx), timeoutInMs)
        if (ret == null) throw ElectrumRequestTimeout()
        // LogIt.info(ret.toString())
        try
        {
            val parsed = json.decodeFromString(GetUtxoReply.serializer(), ret)
            return parsed.result
        }
        catch (e: SerializationException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), ret)
            // LogIt.info("getUtxo error string: " + parsed.error.message)
            if (parsed.error.code == ELECTRUM_INVALID_PARAMS) throw ElectrumNotFound("UTXO not found")
            throw ElectrumIncorrectRequest("getUtxo error")  // Do not expose the untrusted server's error message to higher layers so it is not accidentally shown to end users
        }
    }

    fun getUtxo(outpointHex:String, timeoutInMs: Int = requestTimeout): GetUtxoResult
    {
        val ret = call("blockchain.utxo.get", listOf(outpointHex), timeoutInMs)
        if (ret == null) throw ElectrumRequestTimeout()
        try
        {
            val parsed = json.decodeFromString(GetUtxoReply.serializer(), ret)
            return parsed.result
        }
        catch (e: SerializationException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), ret)
            // LogIt.info("getUtxo error string: " + parsed.error.message)
            if (parsed.error.code == ELECTRUM_INVALID_PARAMS) throw ElectrumNotFound("UTXO not found")
            throw ElectrumIncorrectRequest("getUtxo error")  // Do not expose the untrusted server's error message to higher layers so it is not accidentally shown to end users
        }
    }

     fun getUtxo(inp: iTxOutpoint, timeoutInMs: Int = requestTimeout): GetUtxoResult = getUtxo(inp.toHex(), timeoutInMs)

    fun getUtxo(inp: NexaTxInput, timeoutInMs: Int = requestTimeout) =
        getUtxo(inp.spendable.outpoint ?: throw ElectrumIncorrectRequest("input spendable is null"), timeoutInMs)

    fun getFirstUse(script: SatoshiScript, timeoutInMs: Int = requestTimeout): FirstUseResult = getFirstUse(script.scriptHash(), timeoutInMs)

    /** Get confirmed and unconfirmed activity in a script
     * @param scriptHash the SHA256 hash of the relevant script
     * @param timeoutInMs (optional) Override the default request timeout
     * @return array of (height, transaction hash) pairs
     */
    fun getFirstUse(scriptHash: Hash256, timeoutInMs: Int = requestTimeout): FirstUseResult = getFirstUse(scriptHash.toHex(), timeoutInMs)

    fun getFirstUse(scriptHash: String, timeoutInMs: Int = requestTimeout): FirstUseResult
    {
        val ret = call("blockchain.scripthash.get_first_use", listOf(scriptHash), timeoutInMs)
        if (ret == null) throw ElectrumRequestTimeout()
        // LogIt.info(ret.toString())
        try
        {
            val parsed = json.decodeFromString(FirstUseReply.serializer(), ret)
            return parsed.result
        }
        catch (e: SerializationException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), ret)
            // LogIt.info("getfirstuse error string: " + parsed.error.message)
            if (parsed.error.code == -32004) throw ElectrumNotFound("Scripthash not found")
            throw ElectrumIncorrectRequest("getFirstUse error")
        }
    }

    fun getTokenGenesisInfo(tokenType: GroupId, timeoutInMs: Int = requestTimeout) = getTokenGenesisInfo(tokenType.toString(), timeoutInMs)

    fun getTokenGenesisInfo(tokenType: String, timeoutInMs: Int = requestTimeout): TokenGenesisInfo
    {
        val ret = call("token.genesis.info", listOf(tokenType), timeoutInMs)
        if (ret == null) throw ElectrumRequestTimeout()
        try
        {
            val parsed = json.decodeFromString(TokenGenesisInfoReply.serializer(), ret)
            return parsed.result
        }
        catch (e: SerializationException)
        {
            // LogIt.info("serialization error: " + e.toString())
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), ret)
            // LogIt.info("tokengenesisinfo error string: " + parsed.error.message)
            //if (parsed.error.code == -32004) throw ElectrumNotFound("Scripthash not found")
            throw ElectrumIncorrectRequest("getTokenGenesisInfo error")
        }

    }

    fun getTokenBalance(address: PayAddress, timeoutInMs: Int = requestTimeout) = getTokenBalance(address.toString(), timeoutInMs)

    fun getTokenBalance(address: String, timeoutInMs: Int = requestTimeout): TokenGetBalanceResult
    {
        val ret = call("token.address.get_balance", listOf(address), timeoutInMs)
        if (ret == null) throw ElectrumRequestTimeout()
        try
        {
            val parsed = json.decodeFromString(TokenGetBalanceReply.serializer(), ret)
            return parsed.result
        }
        catch (e: SerializationException)
        {
            // LogIt.info("serialization error: " + e.toString())
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), ret)
            // LogIt.info("gettokenbalance error string: " + parsed.error.message)
            if (parsed.error.code == -32004) throw ElectrumNotFound("Scripthash not found")
            throw ElectrumIncorrectRequest("getTokenBalance error")
        }
    }


    fun getTokenUnspent(address: String, tokenId:String?=null, cursor:String?=null, timeoutInMs: Int = requestTimeout): TokenListUnspentResult
    {
        val ret = if (tokenId != null) call("token.address.listunspent", listOf(address, cursor, tokenId), timeoutInMs)
        else call("token.address.listunspent", listOf(address, cursor), timeoutInMs)
        if (ret == null) throw ElectrumRequestTimeout()
        try
        {
            val parsed = json.decodeFromString(TokenListUnspentReply.serializer(), ret)
            return parsed.result
        }
        catch (e: SerializationException)
        {
            //  LogIt.info("serialization error: " + e.toString())
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), ret)
            // LogIt.info("getTokenUnspent error string: " + parsed.error.message)
            if (parsed.error.code == -32004) throw ElectrumNotFound("Scripthash not found")
            throw ElectrumIncorrectRequest("getTokenUnspent error")
        }

    }


    /** Get the serialized header for the block at the supplied height
     * @param height The height of the desired block header
     * @param serialized header bytes or an empty array if there is no block at that height */
    fun getHeader(height: Int, timeoutInMs: Int = requestTimeout): ByteArray
    {
        val ret = call("blockchain.block.header", listOf(height, 0), timeoutInMs)
        if (ret == null) throw ElectrumRequestTimeout()
        try
        {
            val parsed = json.decodeFromString(StringReply.serializer(), ret)
            return parsed.result.fromHex()
        }
        catch (e: SerializationException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), ret)
            // LogIt.info("getheader error string: " + parsed.error.message)
            throw ElectrumIncorrectRequest("getheader error")
        }
    }

    /** Return serialized headers for the blocks beginning at the supplied height
     * @param height The height of the first block header to return
     * @param count The number of block headers to return
     * @param timeoutInMs (optional) Override the default request timeout
     * @return A list of serialized block headers.  The list will contain fewer than count headers if count exceeds the maximum number of headers this server supports or if the chain has no more headers
     * */
    fun getHeadersFor(cs:ChainSelector, height: Int, count: Int, timeoutInMs: Int = requestTimeout): List<iBlockHeader>
    {
        val ret = call("blockchain.block.headers", listOf(height, count, 0), timeoutInMs)
        if (ret == null) throw ElectrumRequestTimeout()
        val parsed = json.decodeFromString(HeadersReply.serializer(), ret)
        //LogIt.info(parsed.toString())
        //val arr = mutableListOf<ByteArray>()
        //if (parsed.result.hex.length != parsed.result.count * HEADER_SIZE_BYTES * 2) throw ElectrumIncorrectReply("Header count does not match data size")
        val ser = BCHserialized(parsed.result.hex.fromHex(),SerializationType.NETWORK)
        val result = mutableListOf<iBlockHeader>()
        for (i in 0 until parsed.result.count)
        {
            result.add(blockHeaderFor(cs, ser))
        }
        return result
    }

    /** Get the transaction hash at the specified block height and offset.
     * @param height The height of the block that contains the desired transaction
     * @param idx The 0-based index of the transaction in the block
     * @param blockMerkleRoot (optional) If provided, this API will request the merkle proof of this transaction and throw ElectrumIncorrectReply if the proof is incorrect
     * @param timeoutInMs (optional) Override the default request timeout
     * @return transaction hash
     * * */
    fun getTxHashAt(height: Int, idx: Int, blockMerkleRoot: Hash256? = null, timeoutInMs: Int = requestTimeout): Hash256
    {
        if (blockMerkleRoot != null)
        {
            throw UnimplementedException("Merkle proof verification of requested transactions")
        }
        else
        {
            val ret = call("blockchain.transaction.id_from_pos", listOf(height, idx, false), timeoutInMs)
            if (ret == null) throw ElectrumRequestTimeout()

            try
            {
                val parsed = json.decodeFromString(StringReply.serializer(), ret)
                return Hash256(parsed.result)
            }
            catch (e: SerializationException)
            {
                val parsed = json.decodeFromString(ErrorCodeReply.serializer(), ret)
                // LogIt.info("getTxHashAt error string: " + parsed.error.message)
                throw ElectrumIncorrectRequest("getTxHashAt error: " + parsed.error.message)
            }
        }
    }

    /** Get the transaction at the specified block height and offset.  Requires multiple server calls.
     * @param height The height of the block that contains the desired transaction
     * @param idx The 0-based index of the transaction in the block
     * @param blockMerkleRoot (optional) If provided, this API will request the merkle proof of this transaction and throw ElectrumIncorrectReply if the proof is incorrect
     * @param timeoutInMs (optional) Override the default request timeout
     * @return transaction
     */
    fun getTxAt(height: Int, idx: Int, blockMerkleRoot: Hash256? = null, timeoutInMs: Int = requestTimeout): iTransaction
    {
        return getTx(getTxHashAt(height, idx, blockMerkleRoot, timeoutInMs))
    }

    /** Get a transaction by hash
     * @param txHash Transaction ID
     * @param timeoutInMs (optional) Override the default request timeout
     * @return transaction
     */
    fun getTx(txHash: Hash256, timeoutInMs: Int = requestTimeout): iTransaction = getTx(txHash.toHex(), timeoutInMs)

    /** Get a transaction by hash
     * @param txHash Transaction ID as a hex-encoded string
     * @param timeoutInMs (optional) Override the default request timeout
     * @return transaction
     */
    fun getTx(txHash: String, timeoutInMs: Int = requestTimeout): iTransaction
    {
        val ret = call("blockchain.transaction.get", listOf(txHash, false), timeoutInMs)
        if (ret == null) throw ElectrumRequestTimeout()
        try
        {
            val parsed = json.decodeFromString(StringReply.serializer(), ret)
            return txFromHex(chainSelector, parsed.result, SerializationType.NETWORK)
        }
        catch (e: SerializationException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), ret)
            // LogIt.info("getTx error string: " + parsed.error.message)
            if (parsed.error.code == ELECTRUM_INVALID_PARAMS) throw ElectrumNotFound("tx not in blockchain or mempool")
            if (parsed.error.code == 2) throw ElectrumNotFound("tx not in blockchain or mempool")  // Coming from Fulcrum 1.5.0
            throw ElectrumIncorrectRequest("getTx error: " + parsed.error.message)
        }
    }

    @Deprecated("use getTx")
    fun getTxOld(txHash: String, timeoutInMs: Int = requestTimeout): iTransaction
    {
        val ret = call("blockchain.transaction.get", listOf(txHash, false, false), timeoutInMs)
        if (ret == null) throw ElectrumRequestTimeout()
        try
        {
            val parsed = json.decodeFromString(StringReply.serializer(), ret)
            return txFromHex(chainSelector, parsed.result, SerializationType.NETWORK)
        }
        catch (e: SerializationException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), ret)
            // LogIt.info("getTx error string: " + parsed.error.message)
            if (parsed.error.code == ELECTRUM_INVALID_PARAMS) throw ElectrumNotFound("tx not in blockchain or mempool")
            throw ElectrumIncorrectRequest("getTx error: " + parsed.error.message)
        }
    }

    fun getTxDetails(txHash: String, timeoutInMs: Int = requestTimeout): JsonElement
    {
        val result = call("blockchain.transaction.get", listOf(txHash, true), timeoutInMs)
        if (result == null) throw ElectrumRequestTimeout()
        try
        {
            return json.parseToJsonElement(result)
        }
        catch (e: Exception)
        {
            val msg = e.message ?: e.toString()
            throw ElectrumIncorrectReply(msg)
        }
    }

    /** Send out a transaction */
    fun sendTx(serializedTx: ByteArray, timeoutInMs: Int = requestTimeout): String
    {
        val ret = call("blockchain.transaction.broadcast", listOf(serializedTx.toHex()), timeoutInMs)
        // if (ret!=null) LogIt.info(ret)
        if (ret == null) throw ElectrumRequestTimeout()
        try
        {
            val parsed = json.decodeFromString(StringReply.serializer(), ret)
            return parsed.result
        }
        catch (e: SerializationException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), ret)
            // LogIt.info("sendTx error string: " + parsed.error.message)
            throw ElectrumIncorrectRequest("sendTx error: " + parsed.error.message)
        }
    }

    /** Get confirmed and unconfirmed activity in a script
     * @param script the relevant output script
     * @param timeoutInMs (optional) Override the default request timeout
     * @return array of (height, transaction hash) pairs
     */
    fun getHistory(script: SatoshiScript, timeoutInMs: Int = requestTimeout): Array<Pair<Int, Hash256>> = getHistory(script.scriptHash(), timeoutInMs)

    /** Get confirmed and unconfirmed activity in a script
     * @param scriptHash the SHA256 hash of the relevant script
     * @param timeoutInMs (optional) Override the default request timeout
     * @return array of (height, transaction hash) pairs
     */
    fun getHistory(scriptHash: Hash256, timeoutInMs: Int = requestTimeout): Array<Pair<Int, Hash256>> = getHistory(scriptHash.toHex(), timeoutInMs)

    /** Get confirmed and unconfirmed activity in a script
     * @param scriptHash the hex-encoded SHA256 hash of the relevant script
     * @param timeoutInMs (optional) Override the default request timeout
     * @return array of (height, transaction hash) pairs
     */
    fun getHistory(scriptHash: String, timeoutInMs: Int = requestTimeout): Array<Pair<Int, Hash256>>
    {
        val result = call("blockchain.scripthash.get_history", listOf(scriptHash), timeoutInMs)
        if (result == null) throw ElectrumRequestTimeout()
        try
        {
            val parsed = json.decodeFromString(GetHistoryReply.serializer(), result)

            val ret = Array<Pair<Int, Hash256>>(parsed.result.size, { i: Int -> Pair(parsed.result[i].height, Hash256(parsed.result[i].tx_hash)) })
            return ret
        }
        catch (e: SerializationException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), result)
            throw ElectrumIncorrectRequest("getHistory error: " + parsed.error.message)
        }

    }

    /** Get confirmed and unconfirmed activity in a script
     * @param tokenId the address or hex encoded token identifier (group id)
     * @param timeoutInMs (optional) Override the default request timeout
     * @return array of (height, transaction hash) pairs
     */
    fun getTokenHistory(tokenId: GroupId, cursor: String? = null, timeoutInMs: Int = requestTimeout): Array<Pair<Int, Hash256>> =
        getTokenHistory(tokenId.toHex(), cursor, timeoutInMs)

    /** Get confirmed and unconfirmed activity in a script
     * @param tokenId the address or hex encoded token identifier (group id)
     * @param timeoutInMs (optional) Override the default request timeout
     * @return array of (height, transaction hash) pairs
     */
    fun getTokenHistory(tokenId: String, cursor: String? = null, timeoutInMs: Int = requestTimeout): Array<Pair<Int, Hash256>>
    {
        val result = call("token.transaction.get_history", listOf(tokenId, cursor), timeoutInMs)
        if (result == null) throw ElectrumRequestTimeout()
        try
        {
            val parsed = json.decodeFromString(GetTokenHistoryReply.serializer(), result)
            val ret = Array<Pair<Int, Hash256>>(parsed.result.history.size, { i: Int -> Pair(parsed.result.history[i].height, Hash256(parsed.result.history[i].tx_hash)) })
            return ret
        }
        /*  TODO appears to be gone from serialization 1.5.1
        catch (e: MissingFieldException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), result)
            LogIt.info("getheader error string: " + parsed.error.message)
            throw ElectrumIncorrectRequest("getHistory error")
        }

         */
        catch (e: SerializationException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), result)
            throw ElectrumIncorrectRequest("getTokenHistory error:" + parsed.error.message)
        }
    }

    private fun listUnspentInner(scripthash: String, timeoutInMs: Int): List<Spendable>
    {
        val result = call("blockchain.scripthash.listunspent", listOf(scripthash), timeoutInMs)
          ?: throw ElectrumRequestTimeout()

        val toUnconfirmedLength = fun(blockHeight: Int): Long
        {
            if (blockHeight > 0)
            {
                return 0
            }
            if (blockHeight == 0)
            {
                return 1
            }
            assert(blockHeight < 0)
            // We don't know the exact length of the unconfirmed chain, but it's at least 2.
            return 2
        }

        try
        {
            // LogIt.info(result)
            val ret = json.decodeFromString(ListUnspentReply.serializer(), result)

            assert(false) // TODO Nexa
            /*
            return ret.result.map {
                val output = Spendable(
                  chainSelector, it.tx_hash.fromHex(),
                  it.tx_pos.toLong(), it.value.toLong()
                )
                output.spendableUnconfirmed = toUnconfirmedLength(it.height)
                output
            }

             */
            return listOf()
        }
        catch (e: SerializationException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), result)
            // LogIt.info("listUnspent error string: " + parsed.error.message)
            throw ElectrumIncorrectRequest("listUnspent error: " + parsed.error.message)
        }
    }

    fun getBalance(address: PayAddress, timeoutInMs: Int = requestTimeout): BalanceResult
    {
        val scriptHash = address.outputScript().scriptHash().toString()
        val result = call("blockchain.scripthash.get_balance", listOf(scriptHash), timeoutInMs)
          ?: throw ElectrumRequestTimeout()

        try
        {
            val parsed = json.decodeFromString(BalanceReply.serializer(), result)
            return parsed.result
        }
        catch (e: SerializationException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), result)
            if (parsed.error.code == -32004) throw ElectrumNotFound(address.toString())
            // LogIt.info("getBalance error string: " + parsed.error.message)
            throw ElectrumIncorrectRequest("getBalance error:" + parsed.error.message)
        }
    }

    /**
     * Get outputs for a pay destination you don't own.
     */
    /*
    fun listUnspent(address: PayAddress, timeoutInMs: Int = requestTimeout): List<Spendable>
    {
        val scripthash = address.outputScript().scriptHash().toString()
        return this.listUnspentInner(scripthash, timeoutInMs).map {
            it.backingPayDestination = PayDestination.fromAddr(address)
            it.priorOutScript = address.outputScript()
            it
        }
    }

     */

    /**
     * Get spendable output for a pay destination you own.
     *
     * The secret in destination object will be passed on to the
     * BCHspendable instances.
     */
    fun listUnspent(destination: PayDestination, timeoutInMs: Int = requestTimeout): List<Spendable>
    {
        val scripthash = destination.ungroupedOutputScript().scriptHash().toString()
        return this.listUnspentInner(scripthash, timeoutInMs).map { output ->
            output.backingPayDestination = destination
            output.addr?.let {
                output.priorOutScript = it.outputScript()
            }
            output.redeemScript = destination.redeemScript()

            // TODO: What is out.commitHeight? Can we store destination.height in it?
            // TODO: What is out.commitTxHash? Can we store destination.tx_hash in it?
            output
        }

    }

    fun ping(timeoutInMs: Int = requestTimeout)
    {
        call("server.ping", null, timeoutInMs) ?: throw ElectrumRequestTimeout()
    }

    /**
     * Subscribe to blockchain headers. Issues a callback with the blockchain tip when a new block is found.
     */
    fun subscribeHeaders(callback: (iBlockHeader) -> Unit)
    {
        val method = "blockchain.headers.subscribe"
        if (notifications.contains(method))
        {
            error("Only 1 header subscription at a time is supported")
        }
        subscribe(method, null) {
            if (it == null)
            {
                LogIt.info("Received 'null' header notification")
                return@subscribe
            }
            // LogIt.info("Header notification: $it")
            try
            {
                try
                {
                    // First response is wrapped in 'result', so use HeaderReply
                    val parsed = json.decodeFromString(HeaderReply.serializer(), it)
                    val header = blockHeaderFromHex(chainSelector,parsed.result.hex)
                    header.height = parsed.result.height
                    callback(header)
                }
                catch (e: SerializationException)
                {
                    // Other responses are wrapped in params, use 'HeaderNotification'
                    val parsed = json.decodeFromString(HeaderNotification.serializer(), it)
                    val header = blockHeaderFromHex(chainSelector, parsed.params.last().hex)
                    header.height = parsed.params.last().height
                    callback(header)
                }
            }
            catch (e: Exception)
            {
                LogIt.warning("Error when handling block header notification: $e")
            }
        }
    }

    /**
     * Unsubscribes from blockchain headers.
     */
    fun unsubscribeHeaders()
    {
        notifications.remove("blockchain.headers.subscribe")

        // Some servers don't support unsubscribe, we don't really care about the return value either way.
        call("blockchain.headers.unsubscribe", null, requestTimeout)
    }
}

/** Open a JsonRpc communications channel with another node.
 * @throws IllegalStateException if it cannot connect
 */
open class JsonRpc(val name: String, val port: Int, val logName: String = name + ":" + port,
    val useSSL: Boolean = false, connectTimeoutMs: Long = JsonRpc.CONNECT_TIMEOUT, accessTimeoutMs: Long =  JsonRpc.ACCESS_TIMEOUT)
{
    companion object
    {
        public val CONNECT_TIMEOUT = 2000L  //* How long to attempt to connect in milliseconds
        public val ACCESS_TIMEOUT = 10000L  // Long because some rostrum queries can take a long time
        protected val READ_BUF_SIZE = 0x40000
    }

    protected val mutex = Mutex()
    protected var recvProcessor:iThread? = null

    val json = kotlinx.serialization.json.Json { encodeDefaults = true; ignoreUnknownKeys = true }

    //var sock = if (useSSL) SSLSocketFactory.getDefault().createSocket() else Socket()
    //var inp: InputStream
    //var out: OutputStream

    lateinit var sock: Socket
    lateinit var inp: ByteReadChannel
    lateinit var out: ByteWriteChannel

    var reqId = 0

    val outstanding: MutableMap<Int, ((String?) -> Unit)> = mutableMapOf()
    val notifications: MutableMap<String, ((String?) -> Unit)> = mutableMapOf()

    @Serializable
    data class ResultData(val id: Int, val jsonrpc: String)

    @Serializable
    data class NotificationData(val method: String)

    init
    {
        //sock.connect(InetSocketAddress(name, port), CONNECT_TIMEOUT)
        //inp = sock.getInputStream()
        //out = sock.getOutputStream()
        try {
            runBlocking {
                val selectorManager = SelectorManager()
                sock = if(useSSL) withTimeout(connectTimeoutMs)
                {
                    aSocket(selectorManager).tcp().connect(name, port) {
                        socketTimeout = accessTimeoutMs
                    }.tls(this.coroutineContext)
                }
                else withTimeout(connectTimeoutMs)
                {
                    aSocket(selectorManager).tcp().connect(name, port)  {
                        socketTimeout = accessTimeoutMs
                    }
                }
                inp = sock.openReadChannel()
                out = sock.openWriteChannel()
            }
        }
        catch(e:Exception) // could be either IllegalStateException or java.net.ConnectionException depending on the platform
        {
            throw ElectrumConnectError(name + ":" + port.toString(), e.toString())
        }
    }

    fun nextId(): Int
    {
        return mutex.synchronized {
            val ret = reqId
            reqId += 1
            ret
        }
    }

    fun call(method: String, params: List<Any?>?, timeoutInMs: Int): String?
    {
        var ret: String? = null
        val wakey = Gate()
        wakey.wlock {
            call(method, params) {
                wakey.wake {
                    ret = it
                }
            }
            timedwait(timeoutInMs.toLong())
        }
        return ret
    }

    fun <T> parse(method: String, params: List<String>?, serializer: KSerializer<T>, response: (T?) -> Unit)
    {
        call(method, params) {
            if (it == null) response(null)
            else
            {
                try
                {
                    val result = json.decodeFromString<T>(serializer, it)
                    response(result)
                }
                catch (e: SerializationException)
                {
                    LogIt.warning(sourceLoc() + " " + logName + ": Received unparseable reply: " + it)
                    response(null)
                }
            }
        }
    }

    fun subscribe(method: String, params: List<Any?>? = null, response: (String?) -> Unit)
    {
        val cookie = Int.MAX_VALUE / 2 + nextId()


        val sendStr = if (params != null)
        {
            "{\"method\": \"${method}\",\"params\":${params.jsonify()},\"id\": ${cookie}}\n"
        }
        else
        {
            "{\"method\": \"${method}\",\"id\": ${cookie}}\n"
        }

        notifications[method] = response
        // The subscription call itself also responds with current state.
        outstanding[cookie] = response
        LogIt.info(sourceLoc() + " " + logName + ": Issuing request: " + sendStr)
        out.write(sendStr)
    }

    fun call(method: String, params: List<Any?>?, response: (String?) -> Unit)
    {
        mutex.synchronized {
        val cookie = nextId()
        val sendStr = if (params != null)
        {
            """{"method": "${method}","params":${params.jsonify()},"id": ${cookie}}""" + "\n"
        }
        else
        {
            "{\"method\": \"${method}\",\"id\": ${cookie}}\n"
        }
            outstanding[cookie] = response
            // LogIt.info(sourceLoc() + " " + logName + ": Issuing request:" + sendStr)
            out.write(sendStr)
        }
    }

    fun start()
    {
        mutex.synchronized {
            if (recvProcessor == null)
            {
                recvProcessor = Thread(name + "_chain")
                {
                    run()
                }
            }
        }
    }

    fun stop()
    {
        close()  // thread kicks out if the socket closes
    }

    fun run()
    {
        try
        {
            val dataBuf = ByteArray(READ_BUF_SIZE)
            var priorData: String = ""
            while (true)
            {
                val amtRead = runBlocking { inp.readAvailable(dataBuf,0,READ_BUF_SIZE) }
                // LogIt.info("electrum read: " + amtRead)
                if (amtRead > 0)
                {
                    val s = dataBuf.sliceArray(0 until amtRead).decodeUtf8()
                    val responses = (priorData + s).split("\n")
                    // LogIt.info(responses.joinToString("\n"))
                    if (responses.size > 0)
                    {
                        var end = responses.size - 1
                        // The last item will always be the next data fragment -- if the cut was clean then it'll be ""
                        priorData = responses[end]

                        for (idx in 0 until end)
                        {
                            val response = responses[idx]
                            // LogIt.info("JSON response:" + response)
                            val parsedreply = try
                            {
                                val r = json.decodeFromString(ResultData.serializer(), response)
                                // LogIt.info(r.toString())
                                r
                            }
                            catch (e: SerializationException)  // Missing the "id" field -- this is probably a notification
                            {
                                val notificationReply = try
                                {
                                    json.decodeFromString(NotificationData.serializer(), response)
                                }
                                catch (e: SerializationException)
                                {
                                    LogIt.warning(sourceLoc() + " " + logName + ": Received unparseable reply: " + response)
                                    null
                                }
                                if (notificationReply != null)
                                {
                                    val handler = notifications[notificationReply.method]
                                    if (handler == null)
                                    {
                                        LogIt.warning(sourceLoc() + " " + logName + ": Received notification for ${notificationReply.method} that matches no subscription: " + response)
                                    }
                                    else
                                    {
                                        handler(response)
                                    }
                                }

                                null // it was a notification so parsedreply should be null
                            }

                            val handler = mutex.synchronized {
                                if (parsedreply != null)
                                {
                                    val handler = outstanding[parsedreply.id]
                                    if (handler == null)
                                    {
                                        LogIt.warning(sourceLoc() + " " + logName + ": Received reply for ${parsedreply.id} that matches no request: " + response)
                                    }
                                    else  // Call the handler and remove it from the outstanding list
                                    {
                                        if (parsedreply.id < Int.MAX_VALUE / 2)  // Anything above is a subscription
                                            outstanding.remove(parsedreply.id)
                                    }
                                    handler
                                }
                                else null
                            }
                            if (handler != null) handler(response)
                        }

                    }
                }
            }
        }
        catch (e: IOException)  // connection is closed
        {
            handleThreadException(e, sourceLoc() + " " + logName + " electrumClient IOException: ")
        }
        catch (e: Exception)
        {
            handleThreadException(e, sourceLoc() + " " + logName + " electrumClient unknown exception: ")
        }

        abortAllOutstandingRequests() // Tell everyone who is expecting a reply that they aren't going to get one
    }

    fun abortAllOutstandingRequests()
    {
        mutex.synchronized {
            for (out in outstanding)
            {
                out.value(null)
            }
            outstanding.clear()
        }
    }

    fun close()
    {
        try
        {
            sock.close()
        }
        catch (e: IOException)
        {
        }
    }

}
