package org.nexa.libnexakotlin

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.*

@Serializable
data class TokenGenesisInfo(val document_hash: String?, val document_url: String?, val height: Long, val name: String?, val ticker: String?, val token_id_hex: String, val txid: String, val txidem: String, val decimal_places:Int?=null, val op_return:String?=null)

@Serializable
data class TokenDesc(
  val ticker: String,
  val name: String? = null,
  val summary: String? = null,
  val description: String? = null,
  val legal: String? = null,
  val creator: String? = null,
  val category: String? = null,
  val contact: Map<String, String>? = null,
  val icon: String? = null, // absolute or relative url to icon file

  // NFT extensions
  val nftId: String? = null, // hex hash of the NFT data file
  val nftUrl: String? = null, // one location to access the file

  // Won't actually be deserialized in the TDD. Placed here for easy use
  var signedSlice: ByteArray? = null,
  var tddHash: ByteArray? = null,
  var tddSig: ByteArray? = null,
  var pubkey: ByteArray? = null,

  var marketUri: String? = null,  // Derived from the location of the token desc doc or other sources

  var genesisInfo: TokenGenesisInfo? = null
)


/** Decode a token description document JSON file into a TokenDesc structure, the hash of the proper part of the TDD, and its signature bytes.
 * check the signature and fill TokenDesc pubkey with the resulting pubkey if an address is provided, and the signature matches that address. */
fun decodeTokenDescDoc(s: String, addr:PayAddress?):TokenDesc
{
    val js = kotlinx.serialization.json.Json {
        ignoreUnknownKeys = true
    }
    val je = js.decodeFromString(JsonElement.serializer(),s)
    val jsonArray: JsonArray = je.jsonArray
    val tdjo = jsonArray[0].jsonObject
    val td = kotlinx.serialization.json.Json.decodeFromJsonElement(TokenDesc.serializer(), tdjo)
    val tDict = s.slice(IntRange(s.indexOf("{"), s.lastIndexOf("}")))
    val signedSlice = tDict.toByteArray()
    td.signedSlice = signedSlice
    td.tddHash = libnexa.sha256(signedSlice)
    if (jsonArray.size > 1)
    {
        val sigStr: String = jsonArray[1].jsonPrimitive.content
        val sig = Codec.decode64(sigStr)
        td.tddSig = sig
    }
    else td.tddSig = null

    if (addr != null)  // if an address is provided, check for valid signature
    {
        val pub = libnexa.verifyMessage(tDict.toByteArray(), addr.data, td.tddSig!!)  // If you passed an address, you are expecting the TDD to be signed
        td.pubkey = pub
    }
    return td
}