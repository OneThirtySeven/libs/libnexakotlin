// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.nexa.libnexakotlin

private val LogIt = GetLog("BU.primitives")

fun DbgRender(obj: Hash256): String
{
    return obj.toHex()
}

data class Hash256(val hash: ByteArray = ByteArray(32, { _ -> 0 })) : BCHserializable
{
    companion object
    {
        val ZERO = Hash256()
    }
    init
    {
        assert(hash.size == 32)
    }

    constructor(hex: String) : this()
    {
        val hsh = hex.fromHex()
        hsh.reverse()
        assert(hsh.size == 32)
        hsh.copyInto(hash)
    }

    constructor(stream: BCHserialized) : this()
    {
        BCHdeserialize(stream)
    }

    operator fun get(i: Int) = hash[i]
    operator fun set(i: Int, b: Byte)
    {
        hash[i] = b
    }

    /** Convert to the bitcoin standard hex representation */
    fun toHex(): String
    {
        val cpy = hash.copyOf()
        cpy.reverse()
        return cpy.toHex()
    }

    /** The default display will be bitcoin standard hex representation (reversed hex) */
    override fun toString(): String = this.toHex()

    override fun equals(other: Any?): Boolean
    {
        if (other is Hash256) return hash contentEquals other.hash
        return false
    }

    operator fun compareTo(h: Any?): Int
    {
        if (h is Hash256)
        {
            for (i in 0..32)
            {
                if (hash[i] < h.hash[i]) return -1
                else if (hash[i] > h.hash[i]) return 1
            }
            return 0
        }
        return -1
    }

    override fun hashCode(): Int
    {
        return hash[1].toPositiveInt() shl 24 or hash[7].toPositiveInt() shl 16 or hash[17].toPositiveInt() shl 8 or hash[30].toPositiveInt()
    }

    override fun BCHserialize(format: SerializationType): BCHserialized = BCHserialized(hash, format)

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        stream.debytes(32).copyInto(hash)
        return stream
    }
}

fun BCHserialized.denullHash(): Hash256?
{
    val v = Hash256()
    v.BCHdeserialize(this)
    if (v == Hash256()) return null
    return v
}


// Globally unique identifier, implemented as the hash256 of the object
class Guid(var data: Hash256 = Hash256()) : BCHserializable
{
    val GUID_LEN_BYTES: Int = 32

    constructor(dat: String) : this(Hash256(dat.fromHex()))
    {
        assert(dat.length == GUID_LEN_BYTES * 2)
    }

    constructor(dat: ByteArray) : this()
    {
        assert(dat.size == GUID_LEN_BYTES)
        data = Hash256(dat)
    }

    fun toHex(): String = data.toHex()

    // Serialization
    constructor(dat: BCHserialized) : this()
    {
        data = Hash256(dat.debytes(GUID_LEN_BYTES.toLong()))
    }

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        var ret = BCHserialized(format).addExactBytes(data.hash)  //.reversedArray()
        return ret
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        data = Hash256(stream.debytes(GUID_LEN_BYTES.toLong()))
        return stream
    }

    override fun equals(other: Any?): Boolean
    {
        return contentEquals(other)
    }

    fun contentEquals(other: Any?): Boolean
    {
        if (other == null) return false
        if (other is Guid)
        {
            return data.equals(other.data)
        }
        return false
    }

}


/**
An array of block hashes, starting at the current block, and ending at the genesis block.  The creator can skip
blocks, populating whatever hashes he feels is most likely to identify a specific chain, closest to the splitoff point.
Typically, some kind of exponential backoff is used.  For example:
The next 10 hashes are the previous 10 blocks (gap 1).
After these, the gap is multiplied by 2 for each hash, until the genesis block is added
 */
class BlockLocator : BCHserializable
{
    var have: MutableList<Hash256> = mutableListOf()

    fun add(b: Hash256): BlockLocator
    {
        have.add(b)
        return this
    }

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        return BCHserialized.int32(0, format) + have
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        throw UnimplementedException("blockLocator deserialize")  // We only send these locators to full nodes
    }

    override fun toString(): String
    {
        var ret: String = "["
        for (h in have)
        {
            ret += h.toHex() + " "
        }
        ret += "]"
        return ret
    }

}


data class GroupId(var blockchain: ChainSelector, var data: ByteArray) : BCHserializable
{
    companion object
    {
        val GROUP_ID_MIN_SIZE = 32
        val GROUP_ID_MAX_SIZE = 520  // stack size
    }

    init
    {
        assert(data.size >= GROUP_ID_MIN_SIZE)
        assert(data.size <= GROUP_ID_MAX_SIZE)
    }

    // Accept either a hex string or an address format
    constructor(s: String) : this(ChainSelectorFromAddress(s),
      try
      {
          libnexa.groupIdFromAddr(ChainSelectorFromAddress(s), s)
      }
      catch (e: UnknownBlockchainException)  // Its not an address, try hex
      {
          s.fromHex()
      }
      catch (e: LibNexaException)  // decoding from address failed, try hex
      {
          s.fromHex()
      }
    )
    {
    }

    // == compares the groupId bytes for equality
    override fun equals(other: Any?): Boolean
    {
        if (other == null || other !is GroupId || !data.contentEquals(other.data)) return false
        return true
    }

    // Returns true is this group is a subgroup
    fun isSubgroup(): Boolean = data.size > GROUP_ID_MIN_SIZE

    // Returns the parent group of this group (or the group itself if this group is not a subgroup)
    fun parentGroup(): GroupId = GroupId(blockchain, data.sliceArray(0 until GROUP_ID_MIN_SIZE))

    // Returns the unique data associated with this subgroup
    fun subgroupData(): ByteArray = data.sliceArray(GROUP_ID_MIN_SIZE until data.size)

    fun subgroup(subData: ByteArray): GroupId
    {
        return GroupId(blockchain, data + subData)
    }

    /** Returns true if this group is covenanted (grouped output script must = input script) */
    fun isCovenanted(): Boolean
    {
        return (data[GROUP_ID_MIN_SIZE - 1].toPositiveInt() and 1) == 1
    }

    /** Returns true if this group is holding the native crypto rather than tokens */
    @Deprecated("use isFenced()")
    fun isHoldingNative(): Boolean
    {
        return (data[GROUP_ID_MIN_SIZE - 1].toPositiveInt() and 2) == 2
    }

    /** Returns true if this group is holding the native crypto rather than tokens */
    fun isFenced(): Boolean
    {
        return (data[GROUP_ID_MIN_SIZE - 1].toPositiveInt() and 2) == 2
    }

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        if (format == SerializationType.DISK)
        {
            return BCHserialized(format) + variableSized(data)
        }
        else if (format == SerializationType.NETWORK)  // in scripts for example, we just place the raw bytes
        {
            throw NotImplementedError()
        }
        throw NotImplementedError()
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        if (stream.format == SerializationType.DISK)
        {
            data = stream.deByteArray()
        }
        else if (stream.format == SerializationType.NETWORK)  // in scripts for example, we just place the raw bytes
        {
            throw NotImplementedError()
        }
        return stream
    }

    /** convert to hex */
    fun toHex() = data.toHex()

    /** convert to address format */
    override fun toString(): String = libnexa.groupIdToAddr(blockchain, data)

    /** convert to address format, without chainselector prefix */
    fun toStringNoPrefix(): String
    {
        val s = libnexa.groupIdToAddr(blockchain, data)
        val suffix = s.split(":").last()
        return suffix
    }

    fun toByteArray() = data

}

// Need efficient bit manipulation so not using enum
class GroupAuthorityFlags
{
    companion object
    {
        // TODO fix when kotlin figures out ulong
        //const val NO_AUTHORITY = 0x0000000000000000UL
        val NO_AUTHORITY = 0x0000000000000000UL

        //const val AUTHORITY = 0x8000000000000000UL
        val AUTHORITY = 0x8000000000000000UL

        //const val MINT      = 0x4000000000000000L
        val MINT = 0x4000000000000000UL.toULong()

        //const val MELT      = 0x2000000000000000L
        val MELT = 0x2000000000000000UL.toULong()

        //const val BATON     = 0x1000000000000000L
        val BATON = 0x1000000000000000UL.toULong()

        //const val RESCRIPT  = 0x0800000000000000L
        val RESCRIPT = 0x0800000000000000UL.toULong()

        //const val SUBGROUP  = 0x0400000000000000L
        val SUBGROUP = 0x0400000000000000UL.toULong()

        //const val ALL_AUTHORITY_BITS = 0xffff000000000000U
        val ALL_AUTHORITIES = AUTHORITY or MINT or MELT or BATON or RESCRIPT or SUBGROUP

        fun toString(authBits: ULong): String
        {
            val ret = StringJoiner(",")
            val zero = 0L.toULong()
            if ((authBits and GroupAuthorityFlags.AUTHORITY) == zero) return ""
            if ((authBits and MINT) != zero) ret.add("MINT")
            if ((authBits and MELT) != zero) ret.add("MELT")
            if ((authBits and RESCRIPT) != zero) ret.add("RESCRIPT")
            if ((authBits and SUBGROUP) != zero) ret.add("SUBGROUP")
            if ((authBits and BATON) != zero) ret.add("BATON")
            return ret.toString()
        }
    }
}

/** Decoded data about a group that is stored in transactions outputs */
data class GroupInfo(
  /** The group identifier */
  var groupId: GroupId,
  /** How many tokens of this group are involved */
  var tokenAmt: Long,
  /** If this is an authority, what authority flags are set */
  var authorityFlags: ULong = 0.toULong())
{
    /** Returns true is this group is a subgroup */
    fun isSubgroup(): Boolean = groupId.isSubgroup()

    /** Returns true if this group is an authority */
    fun isAuthority(): Boolean = (authorityFlags and GroupAuthorityFlags.AUTHORITY) > 0.toULong()

    override fun toString(): String
    {
        val ret = StringBuilder()
        if (isAuthority())
        {
            // ret.append("authority ") (implied by the flags string)
            ret.append(GroupAuthorityFlags.toString(authorityFlags))
            ret.append(" for group ")
        }
        else
        {
            ret.append(tokenAmt)
            ret.append(" of group ")
        }
        ret.append(groupId)
        return ret.toString()
    }
}
