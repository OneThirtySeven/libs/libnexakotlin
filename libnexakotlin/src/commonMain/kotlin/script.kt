// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.nexa.libnexakotlin
import kotlinx.serialization.Serializable
import okio.Buffer
import kotlin.experimental.and
import kotlin.experimental.or

const val MAX_LONG_LESIGNMAG_SIZE = 9  // This is used when decoding things that might be numbers: 8 bytes + 1 byte sign

open class ScriptException(msg: String, shortMsg: String? = null, severity: ErrorSeverity = ErrorSeverity.Abnormal) :
  LibNexaException(msg, shortMsg, severity)

open class UnsupportedInBlockchain(msg: String) : ScriptException(msg, appI18n(RnotSupported))

private val LogIt = GetLog("BU.script")

data class ScriptTemplate(var groupInfo:GroupInfo?, val templateHash:ByteArray?, val wellKnownId:Long?, val argsHash:ByteArray, val rest:List<ByteArray>)
{
    fun p2pkt(cs: ChainSelector): SatoshiScript
    {
        return SatoshiScript(cs, SatoshiScript.Type.TEMPLATE, OP.PUSHFALSE, P2PKT_ID, OP.push(argsHash))
    }
    fun ungroupedP2t(cs: ChainSelector): SatoshiScript =
        when (wellKnownId)
        {
            null -> SatoshiScript(cs, SatoshiScript.Type.TEMPLATE, OP.PUSHFALSE, OP.push(templateHash!!), OP.push(argsHash))  // if wellKnown is null templatehash must be something
            1L -> p2pkt(cs)
            else -> throw UnsupportedInBlockchain("unknown well known template identifier")
        }

    /* Convert this script template into an output script.  Will group (or not) depending on whether groupInfo is null */
    fun gp2t(cs: ChainSelector): SatoshiScript
    {
        val gi = groupInfo
        if ((gi == null)||(gi.groupId.data.size == 0)) return ungroupedP2t(cs)
        val s = SatoshiScript.grouped(cs, gi.groupId, gi.tokenAmt)
        when (wellKnownId)
        {
            null -> s.add(OP.push(templateHash!!)).add(OP.push(argsHash)).add(rest)
            1L -> s.add(P2PKT_ID).add(OP.push(argsHash)).add(rest)
        }
        return s
    }
}


/** Encode in little-endian sign-magnitude format (like a Nexa script num) */
fun Long.leSignMag(size:Int?=null): ByteArray
{
    // Numbers are encoded to byte arrays in sign-magnitude format.
    // The magnitude is encoded with the least-significant byte first (little-endian).
    // After that, the MSB of the last byte is reused as the sign if the magnitude encoding set it to 0.
    // Otherwise, another (0) byte is appended and then do the previous sentence on it.
    val neg = this < 0
    var abs = if (neg) -1*this else this
    val b = mutableListOf<Byte>()
    while(abs != 0L)
    {
        b.add((abs and 0xff).toByte())
        abs = abs shr 8
    }
    // Pad out to the specified size.  If this happens, a negative will never need to pad because we pushed a 0
    if (size != null) while(b.size < size) b.add(0.toByte())
    // If the last byte has the high bit set, we need to add another byte containing the sign
    if ((b.last() and 0x80.toByte()) != 0.toByte())
    {
        b.add(if (neg) 0x80.toByte() else 0.toByte())
    }
    // otherwise we can just set that last high bit if needed
    else if (neg)
    {
        var tmp = b.last()
        tmp = tmp or 0x80.toByte()
        b[b.size-1] = tmp
    }
    return b.toByteArray()
}

@kotlin.ExperimentalUnsignedTypes
/** Decode a number from little-endian sign-magnitude format (like a Nexa script num) */
fun ByteArray.leSignMag(): Long
{
    // Numbers are encoded to byte arrays in sign-magnitude format.
    // The magnitude is encoded with the least-significant byte first (little-endian).
    // After that, the MSB of the last byte is reused as the sign if the magnitude encoding set it to 0.
    // Otherwise, another (0) byte is appended and then do the previous sentence on it.

    val rev = this.toUByteArray().reversed()

    if (rev.isEmpty()) return 0
    var msb = rev[0]
    val neg = (msb and 0x80.toUByte()) != 0.toUByte()
    msb = msb and 0x7f.toUByte()

    var ret = msb.toLong()

    for (idx in 1 .. rev.lastIndex)
    {
        ret = (ret shl 8) or rev[idx].toLong()
    }

    if (neg) return -1*ret
    return ret
}


// Note enum does not work for these script opcodes because I want the opcodes to be the same type as ByteArray so that data and opcodes can be pushed in the same parameter, list, or varargs
// without qualifying the enum with .ordinal
@Serializable
class OP(val v: ByteArray)
{
    /** PUSH_TX_STATE requires a constant to specify what state is being accessed */
    enum class PushTxState(val v: Int)
    {
        TX_ID(2),
        TX_IDEM(3),
        TX_INCOMING_AMOUNT(5),
        TX_OUTGOING_AMOUNT(6),
        GROUP_INCOMING_AMOUNT(7),
        GROUP_OUTGOING_AMOUNT(8),
        GROUP_INCOMING_COUNT(9),
        GROUP_OUTGOING_COUNT(0xA),
        GROUP_NTH_INPUT(0xB),
        GROUP_NTH_OUTPUT(0xC);
    }

    /** Convert this opcode to human readable assembly */
    fun toAsm(): String
    {
        return OP.toAsm(v)
    }

    /** All of the Script Virtual Machine opcodes, and some helper functions to create opcodes, are defined in the companion object of this class. */
    companion object
    {
        /** The FALSE or 0 opcode.
         * Input: **nothing**, Output: **0** */
        val PUSHFALSE = OP(byteArrayOf(0x0.toByte()))
        /** The TRUE or 1 opcode.
         * Input: **nothing**, Output: **1***/
        val PUSHTRUE = OP(byteArrayOf(0x51.toByte()))

        /** do nothing */
        val NOP = OP(byteArrayOf(0x61.toByte())  )  // control operations
        /** disabled */
        val VER = OP(byteArrayOf(0x62.toByte()))

        /** IF [statements] [ELSE [statements]] ENDIF
         * If the top stack value is not False, the statements are executed. The top stack value is removed.
         */
        val IF = OP(byteArrayOf(0x63.toByte()))

        /** NOTIF [statements] [ELSE [statements]] ENDIF
         * If the top stack value is False, the statements are executed. The top stack value is removed.
         */
        val NOTIF = OP(byteArrayOf(0x64.toByte()))

        /* DO NOT USE */
        val VERIF = OP(byteArrayOf(0x65.toByte()))
        val VERNOTIF = OP(byteArrayOf(0x66.toByte()))

        /** IF [statements] [ELSE [statements]] ENDIF
         * If the preceding OP_IF or OP_NOTIF or OP_ELSE was not executed then these statements are and if the preceding OP_IF or OP_NOTIF or OP_ELSE was executed then these statements are not.
         */
        val ELSE = OP(byteArrayOf(0x67.toByte()))

        /** IF [statements] [ELSE [statements]] ENDIF
         * Ends an if/else block. All blocks must end, or the transaction is marked as invalid. An OP_ENDIF without OP_IF earlier is also invalid.
         */
        val ENDIF = OP(byteArrayOf(0x68.toByte()))

        /** Marks transaction as invalid if top stack value is not true. The top stack value is removed.
         * Input: **true/false**, Output: **nothing/fail**
         */
        val VERIFY = OP(byteArrayOf(0x69.toByte()))

        /** Marks the output as unspendable.  A standard way of attaching extra data to transactions is to add a zero-value output with a scriptPubKey consisting of OP_RETURN followed by data.
         *  Output: **fail**
         */
        val RETURN = OP(byteArrayOf(0x6a.toByte()))

        /** Puts the input onto the top of the alt stack. Removes it from the main stack.
         *  Input: **x1** Output: **(alt) x1**
         */
        val TOALTSTACK = OP(byteArrayOf(0x6b.toByte()) )    // stack operations

        /** Puts the input onto the top of the main stack. Removes it from the alt stack.
         * Input: **(alt) x1**  Output: **x1**
         */
        val FROMALTSTACK = OP(byteArrayOf(0x6c.toByte()))

        /** Removes the top two stack items.
         * Input: **x1 x2** Output: **nothing**
         */
        val DROP2 = OP(byteArrayOf(0x6d.toByte()))

        /** Duplicates the top two stack items.
         * Input: **x1 x2** Output: **x1 x2 x1 x2**
         */
        val DUP2 = OP(byteArrayOf(0x6e.toByte()))

        /** Duplicates the top three stack items.
         * Input: **x1 x2 x3**  Output: **x1 x2 x3 x1 x2 x3**
         */
        val DUP3 = OP(byteArrayOf(0x6f.toByte()))
        /** Copies the pair of items two spaces back in the stack to the front.
         * Input: **x1 x2 x3 x4**  Output: **x1 x2 x3 x4 x1 x2**
         */
        val OVER2 = OP(byteArrayOf(0x70.toByte()))

        /** The fifth and sixth items back are moved to the top of the stack.
         * Input:  **x1 x2 x3 x4 x5 x6** Output: **x3 x4 x5 x6 x1 x2**
         */
        val ROT2 = OP(byteArrayOf(0x71.toByte()))

        /** Swaps the top two pairs of items.
         * Input:  **x1 x2 x3 x4** Output: **x3 x4 x1 x2**
         */
        val SWAP2 = OP(byteArrayOf(0x72.toByte()))

        /** If the top stack value is not 0, duplicate it.
         * Input: **x1**  Output: **x1 or x1 x1**
         */
        val IFDUP = OP(byteArrayOf(0x73.toByte()))

        /** Puts the number of stack items onto the stack.
         * Input: **nothing**   Output: **stack size**
         */
        val DEPTH = OP(byteArrayOf(0x74.toByte()))

        /** Removes the top stack item.
         * Input: **x**   Output: **nothing**
         */
        val DROP = OP(byteArrayOf(0x75.toByte()))

        /** Duplicates the top stack item.
         * Input: **x**   Output: **x x**
         */
        val DUP = OP(byteArrayOf(0x76.toByte()))

        /** Removes the second-to-top stack item.
         * Input: **x1 x2**   Output: **x2**
         */
        val NIP = OP(byteArrayOf(0x77.toByte()))

        /** Copies the second-to-top stack item to the top.
         * Input: **x1 x2**   Output: **x1 x2 x1**
         */
        val OVER = OP(byteArrayOf(0x78.toByte()))

        /** The item n back in the stack is copied to the top.
         * Input: **xn … x2 x1 x0 n**   Output: **xn … x2 x1 x0 xn**
         */
        val PICK = OP(byteArrayOf(0x79.toByte()))

        /** The item n back in the stack is moved to the top.
         * Input: **xn … x2 x1 x0 n**   Output: **x(n-1) … x2 x1 x0 xn**
         */
        val ROLL = OP(byteArrayOf(0x7a.toByte()))

        /** The top three items on the stack are rotated in the direction such that the top item becomes the 2nd to top.
         * Input: **x1 x2 x3**  Output: **x2 x3 x1**
         */
        val ROT = OP(byteArrayOf(0x7b.toByte()))

        /** The top two items on the stack are swapped.
         * Input: **x1 x2**  Output: **x2 x1**
         */
        val SWAP = OP(byteArrayOf(0x7c.toByte()))

        /** The item at the top of the stack is copied and inserted below the second-to-top item.
         * Input: **x1 x2**  Output: **x2 x1 x2**
         */
        val TUCK = OP(byteArrayOf(0x7d.toByte()))

        /** Concatenates two byte sequences
         * Input: **x1 x2**  Output: **out**
         */
        val CAT = OP(byteArrayOf(0x7e.toByte())  )

        /** Splits byte sequence x at position n. x1 contains the byte sequence [0, n), and x2 contains [n, end). For example, ‘abc’ 0 OP_SPLIT produces the stack ‘’, ‘abc’.
         * Input: **x n**  Output: **x1 x2**
         */
        val SPLIT = OP(byteArrayOf(0x7f.toByte()))

        /** Converts numeric value x1 into byte sequence of length n. This operator will handle also BigNum conversion
         * Input: **x1 n**  Output: **out**
         */
        val NUM2BIN = OP(byteArrayOf(0x80.toByte()) ) // number conversion

        /** Converts byte sequence x into a numeric value.
         * Input: **x**  Output: **out**
         */
        val BIN2NUM = OP(byteArrayOf(0x81.toByte()))

        /** Pushes the string length of the top element of the stack (without popping it).
         * Input: **x1**  Output: **x1 size**
         */
        val SIZE = OP(byteArrayOf(0x82.toByte()))

        /* disabled */
        val INVERT = OP(byteArrayOf(0x83.toByte()))

        /** Boolean AND between each bit of the inputs
         * Input: **x1 x2**  Output: **out**
         * */
        val AND = OP(byteArrayOf(0x84.toByte()))

        /** Boolean OR between each bit of the inputs.
         * Input: **x1 x2**  Output: **out**
         */
        val OR = OP(byteArrayOf(0x85.toByte()))

        /** Boolean EXCLUSIVE OR between each bit of the inputs.
         * Input: **x1 x2**  Output: **out**
         */
        val XOR = OP(byteArrayOf(0x86.toByte()))

        /** Returns 1 if the inputs are exactly equal, comparing bits, 0 otherwise.
         * Input: **x1 x2**  Output: **true or false**
         */
        val EQUAL = OP(byteArrayOf(0x87.toByte()))

        /** Same as OP_EQUAL, but runs OP_VERIFY afterward.
         * Input: **x1 x2**  Output: **nothing or fail**
         */
        val EQUALVERIFY = OP(byteArrayOf(0x88.toByte()))

        val RESERVED1 = OP(byteArrayOf(0x89.toByte()))
        val RESERVED2 = OP(byteArrayOf(0x8a.toByte()))

        /** 1 is added to the input.
         * Input: ****  Output: ****
         */
        val ADD1 = OP(byteArrayOf(0x8b.toByte()))

        /**  1 is subtracted from the input.
         * Input: ****  Output: ****
         */
        val SUB1 = OP(byteArrayOf(0x8c.toByte()))


        val MUL2 = OP(byteArrayOf(0x8d.toByte()))
        val DIV2 = OP(byteArrayOf(0x8e.toByte()))

        /** The sign of the input is flipped.
         * Input: ****  Output: ****
         */
        val NEGATE = OP(byteArrayOf(0x8f.toByte()))

        /** The input is made positive.
         * Input: **a b**  Output: **out**
         */
        val ABS = OP(byteArrayOf(0x90.toByte()))

        /** If the input is 0 or 1, it is flipped. Otherwise the output will be 0.
         * Input: **in**  Output: **true/false**
         */
        val NOT = OP(byteArrayOf(0x91.toByte()))

        /** Returns 0 if the input is 0. 1 otherwise.
         * Input: **in**  Output: **true/false**
         */
        val NOTEQUAL0 = OP(byteArrayOf(0x92.toByte()))

        /** a is added to b.
         * Input: **a b**  Output: **out**
         */
        val ADD = OP(byteArrayOf(0x93.toByte()))

        /** b is subtracted from a.
         * Input: **a b**  Output: **out**
         */
        val SUB = OP(byteArrayOf(0x94.toByte()))

        /** a is multiplied by b.
         * Input: **a b**  Output: **out**
         */
        val MUL = OP(byteArrayOf(0x95.toByte()))

        /** a is divided by b.
         * Input: **a b**  Output: **out**
         */
        val DIV = OP(byteArrayOf(0x96.toByte()))

        /** Returns the remainder after a is divided by b.
         * Input: **a b**  Output: **out**
         */
        val MOD = OP(byteArrayOf(0x97.toByte()))

        /** Shifts a left b bits, preserving sign.
         * Input: **a b**  Output: **out**
         */
        val LSHIFT = OP(byteArrayOf(0x98.toByte()))

        /** Shifts a right b bits, preserving sign.
         * Input: **a b**  Output: **out**
         */
        val RSHIFT = OP(byteArrayOf(0x99.toByte()))

        /** If both a and b are not 0, the output is 1. Otherwise 0.
         * Input: **a b**  Output: **true/false**
         */
        val BOOLAND = OP(byteArrayOf(0x9a.toByte()))

        /** If a or b is not 0, the output is 1. Otherwise 0.
         * Input: **a b**  Output: **true/false**
         */
        val BOOLOR = OP(byteArrayOf(0x9b.toByte()))

        /** Returns 1 if the numbers are equal, 0 otherwise.
         * Input: **a b**  Output: **true/false**
         */
        val NUMEQUAL = OP(byteArrayOf(0x9c.toByte()))

        /** Same as OP_NUMEQUAL, but runs OP_VERIFY afterward.
         * Input: **a b**  Output: **nothing or fail**
         */
        val NUMEQUALVERIFY = OP(byteArrayOf(0x9d.toByte()))

        /** Returns 1 if the numbers are not equal, 0 otherwise.
         * Input: **a b**  Output: **true/false**
         */
        val NUMNOTEQUAL = OP(byteArrayOf(0x9e.toByte()))

        /** Returns 1 if a is less than b, 0 otherwise.
         * Input: **a b**  Output: **true/false**
         */
        val LESSTHAN = OP(byteArrayOf(0x9f.toByte()))

        /** Returns 1 if a is greater than b, 0 otherwise.
         * Input: **a b**  Output: **true/false**
         */
        val GREATERTHAN = OP(byteArrayOf(0xa0.toByte()))

        /** Returns 1 if a is less than or equal to b, 0 otherwise.
         * Input: **a b**  Output: **true/false**
         */
        val LESSTHANOREQUAL = OP(byteArrayOf(0xa1.toByte()))

        /** Returns 1 if a is greater than or equal to b, 0 otherwise.
         * Input: **a b**  Output: **true/false**
         */
        val GREATERTHANOREQUAL = OP(byteArrayOf(0xa2.toByte()))

        /** Returns the smaller of a and b.
         * Input: **a b**  Output: **out**
         */
        val MIN = OP(byteArrayOf( 0xa3.toByte()))

        /** Returns the larger of a and b.
         * Input: **a b**  Output: **out**
         */
        val MAX = OP(byteArrayOf( 0xa4.toByte())     )

        /** Hashes input with RIPEMD-160.
         * Input: **in**  Output: **hash**
         */
        val RIPEMD160 = OP(byteArrayOf(0xa6.toByte()))

        /** Hashes input with SHA-1.
         * Input: **in**  Output: **hash**
         */
        val SHA1 = OP(byteArrayOf(0xa7.toByte()))

        /** Hashes input with SHA-256.
         * Input: **in**  Output: **hash**
         */
        val SHA256 = OP(byteArrayOf(0xa8.toByte()))

        /** Hashes input with SHA-256 and then with RIPEMD-160.
         * Input: **in**  Output: **hash**
         */
        val HASH160 = OP(byteArrayOf(0xa9.toByte()))

        /** Hashes input twice with SHA-256.
         * Input: **in**  Output: **hash**
         */
        val HASH256 = OP(byteArrayOf(0xaa.toByte()))

        /** Makes OP_CHECK(MULTI)SIG(VERIFY) use the subset of the script of everything after the most recently-executed OP_CODESEPARATOR when computing the sighash.
         * Input: **nothing**  Output: **nothing**
         */
        val CODESEPARATOR = OP(byteArrayOf(0xab.toByte()))

        /** Sig is a Schnorr signature concatenated to a sighash type specifier. The sighash for this input is calculated based on the sighash type. The validity of the Schnorr signature for this hash and public key is checked. If it is valid, 1 is returned, if it is empty, 0 is returned, otherwise the operation fails.
         * Input: **sig pubkey**  Output: **true or false**
         */
        val CHECKSIG = OP(byteArrayOf(0xac.toByte()))

        /** Same as [CHECKSIG], but [VERIFY] is executed afterward.
         * Input: **sig pubkey**  Output: **nothing or fail**
         */
        val CHECKSIGVERIFY = OP(byteArrayOf(0xad.toByte()))

        /** Signatures are checked against public keys. Signatures must be placed in the unlocking script using the same order as their corresponding public keys were placed in the locking script or redeem script. If all signatures are valid, 1 is returned, 0 otherwise. All elements are removed from the stack. For more information on the execution of this opcode, see (Multisignature)[https://spec.nexa.org/blockchain/cryptography/multisignature].
         * Input: **bitmap sig1 sig2 … #-of-sigs pub1 pub2 … #-of-pubkeys**  Output: **true or false**
         */
        val CHECKMULTISIG = OP(byteArrayOf(0xae.toByte()))

        /** Same as OP_CHECKMULTISIG, but OP_VERIFY is executed afterward.
         * Input: **bitmap sig1 sig2 … #-of-sigs pub1 pub2 … #-of-pubkeys**  Output: **nothing or fail**
         */
        val CHECKMULTISIGVERIFY = OP(byteArrayOf(0xaf.toByte()))

        /** Marks transaction as invalid if the top stack item is greater than the transaction’s nLockTime field, otherwise script evaluation continues as though an OP_NOP was executed. Transaction is also invalid if 1. the stack is empty; or 2. the top stack item is negative; or 3. the top stack item is greater than or equal to 500000000 while the transaction’s nLockTime field is less than 500000000, or vice versa; or 4. the input’s nSequence field is equal to 0xffffffff. The precise semantics are described in BIP65.
         * Input: **x**  Output: **x or fail**
         */
        val CHECKLOCKTIMEVERIFY = OP(byteArrayOf(0xb1.toByte()))

        /** Marks transaction as invalid if the relative lock time of the input (enforced by BIP68 with nSequence) is not equal to or longer than the value of the top stack item. The precise semantics are described in BIP112.
         * Input: **x**  Output: **x or fail**
         */
        val CHECKSEQUENCEVERIFY = OP(byteArrayOf(0xb2.toByte()))

        /** Check if signature is valid for message and a public key. See [checkdatasig](https://spec.nexa.org/forks/op_checkdatasig)
         * Input: **sig msg pubkey**  Output: **true or false**
         */
        val CHECKDATASIG = OP(byteArrayOf(0xba.toByte()))

        /** Same as [CHECKDATASIG], but runs [VERIFY] afterward.
         * Input: **sig msg pubkey**  Output: **nothing or fail**
         */
        val CHECKDATASIGVERIFY = OP(byteArrayOf(0xbb.toByte()))

        /** Reverses the order of the bytes in byte sequence x so that the first byte is now its last byte, the second is now its second-to-last, and so forth.
         * Input: **x**  Output: **out**
         */
        val REVERSE = OP(byteArrayOf(0xbc.toByte()))

        /** The next byte contains the number of bytes to be pushed onto the stack.
         */
        val PUSHDATA1 = OP(byteArrayOf(0x4c.toByte()))

        /** The next two little-endian bytes contain the number of bytes to be pushed onto the stack.
         */
        val PUSHDATA2 = OP(byteArrayOf(0x4d.toByte()))

        /** The next four bytes form a little-endian number of bytes to be pushed onto the stack in little endian order.
         */
        val PUSHDATA4 = OP(byteArrayOf(0x4e.toByte()))

        /** Pushes 0 onto the stack
         * Input: **nothing**  Output: **0**
         */
        val C0 = PUSHFALSE

        /** Pushes -1 onto the stack
         * Input: **nothing**  Output: **-1**
         */
        val CNEG1 = OP(byteArrayOf(0x4f.toByte()))

        /** Pushes 1 onto the stack
         * Input: **nothing**  Output: **1**
         */
        val C1 = PUSHTRUE

        /** Pushes 2 onto the stack
         * Input: **nothing**  Output: **2**
         */
        val C2 = OP(byteArrayOf(0x52.toByte()))

        /** Pushes 3 onto the stack
         * Input: **nothing**  Output: **3**
         */
        val C3 = OP(byteArrayOf(0x53.toByte()))

        /** Pushes 4 onto the stack
         * Input: **nothing**  Output: **4**
         */
        val C4 = OP(byteArrayOf(0x54.toByte()))

        /** Pushes 5 onto the stack
         * Input: **nothing**  Output: **5**
         */
        val C5 = OP(byteArrayOf(0x55.toByte()))

        /** Pushes 6 onto the stack
         * Input: **nothing**  Output: **6**
         */
        val C6 = OP(byteArrayOf(0x56.toByte()))

        /** Pushes 7 onto the stack
         * Input: **nothing**  Output: **7**
         */
        val C7 = OP(byteArrayOf(0x57.toByte()))

        /** Pushes 8 onto the stack
         * Input: **nothing**  Output: **8**
         */
        val C8 = OP(byteArrayOf(0x58.toByte()))

        /** Pushes 9 onto the stack
         * Input: **nothing**  Output: **9**
         */
        val C9 = OP(byteArrayOf(0x59.toByte()))

        /** Pushes 10 onto the stack
         * Input: **nothing**  Output: **10**
         */
        val C10 = OP(byteArrayOf(0x5a.toByte()))

        /** Pushes 11 onto the stack
         * Input: **nothing**  Output: **11**
         */
        val C11 = OP(byteArrayOf(0x5b.toByte()))

        /** Pushes 12 onto the stack
         * Input: **nothing**  Output: **12**
         */
        val C12 = OP(byteArrayOf(0x5c.toByte()))

        /** Pushes 13 onto the stack
         * Input: **nothing**  Output: **13**
         */
        val C13 = OP(byteArrayOf(0x5d.toByte()))

        /** Pushes 14 onto the stack
         * Input: **nothing**  Output: **14**
         */
        val C14 = OP(byteArrayOf(0x5e.toByte()))

        /** Pushes 15 onto the stack
         * Input: **nothing**  Output: **15**
         */
        val C15 = OP(byteArrayOf(0x5f.toByte()))

        /** Pushes 16 onto the stack
         * Input: **nothing**  Output: **16**
         */
        val C16 = OP(byteArrayOf(0x60.toByte()))

        // Introspection
        /** Push the index of the input being evaluated to the stack as a Script Number.
         * Input: **nothing**  Output: **number**
         */
        val INPUTINDEX = OP(byteArrayOf(0xc0.toByte()))

        /** Push the bytecode currently being evaluated, beginning after the last executed OP_CODESEPARATOR, to the stack1. For Pay-to-Script-Hash (P2SH) evaluations, this is the redeem bytecode of the Unspent Transaction Output (UTXO) being spent; for all other evaluations, this is the locking bytecode of the UTXO being spent.
         * Input: **nothing**  Output: **number**
         */
        val ACTIVEBYTECODE = OP(byteArrayOf(0xc1.toByte()))

        /** Push the version of the current transaction to the stack as a Script Number.
         * Input: **nothing**  Output: **number**
         */
        val TXVERSION = OP(byteArrayOf(0xc2.toByte()))

        /** Push the count of inputs in the current transaction to the stack as a Script Number.
         * Input: **nothing**  Output: **number**
         */
        val TXINPUTCOUNT = OP(byteArrayOf(0xc3.toByte()))

        /** Push the count of outputs in the current transaction to the stack as a Script Number.
         * Input: **nothing**  Output: **number**
         */
        val TXOUTPUTCOUNT = OP(byteArrayOf(0xc4.toByte()))

        /** Push the locktime of the current transaction to the stack as a Script Number.
         * Input: **nothing**  Output: **number**
         */
        val TXLOCKTIME = OP(byteArrayOf(0xc5.toByte()))

        /** Pop the top item from the stack as an input index (Script Number). Push the value (in satoshis) of the Unspent Transaction Output (UTXO) spent by that input to the stack as a Script Number.
         * Input: **index**  Output: **number**
         */
        val UTXOVALUE = OP(byteArrayOf(0xc6.toByte()))

        /** Pop the top item from the stack as an input index (Script Number). Push the full locking bytecode of the Unspent Transaction Output (UTXO) spent by that input to the stack.
         * Input: **index**  Output: **script**
         */
        val UTXOBYTECODE = OP(byteArrayOf(0xc7.toByte()))

        /** Pop the top item from the stack as an input index (Script Number). From that input, push the outpoint transaction hash - the hash of the transaction which created the Unspent Transaction Output (UTXO) which is being spent - to the stack in OP_HASH256 byte order.
         * Input: **index**  Output: **hash**
         */
        val OUTPOINTTXHASH = OP(byteArrayOf(0xc8.toByte()))

        /** Pop the top item from the stack as an input index (Script Number). Push the unlocking bytecode of the input at that index to the stack.
         * Input: **index**  Output: **script**
         */
        val INPUTBYTECODE = OP(byteArrayOf(0xca.toByte()))

        /** Pop the top item from the stack as an input index (Script Number). Push the sequence number of the input at that index to the stack as a Script Number.
         * Input: **index**  Output: **number**
         */
        val INPUTSEQUENCENUMBER = OP(byteArrayOf(0xb.toByte()))

        /** Pop the top item from the stack as an output index (Script Number). Push the value (in satoshis) of the output at that index to the stack as a Script Number.
         * Input: **index**  Output: **number**
         */
        val OUTPUTVALUE = OP(byteArrayOf(0xcc.toByte()))

        /**  Pop the top item from the stack as an output index (Script Number). Push the locking bytecode of the output at that index to the stack.
         * Input: **index**  Output: **script**
         */
        val OUTPUTBYTECODE = OP(byteArrayOf(0xcd.toByte()))

        // Nextchain
        /** Copies item count postion back in the stack. This opcode is the inverse of [PICK]
         * Input: **item n**  Output: **nothing**
         */
        val PLACE = OP(byteArrayOf(0xe9.toByte()))
        /** Place information about the current transaction onto the stack, see detailed description (here)[https://spec.nexa.org/nexa/op_push_tx_state.md]
        * Input: **dataSpecifier**  Output: **data**
         */
        val PUSH_TX_STATE = OP(byteArrayOf(0xea.toByte()))
        /** Sets d as the BigNum modulo divisor see detailed description (here)[https://spec.nexa.org/nexa/op_setbmd.md]
        * Input: **d**  Output: **nothing**
         */
        val SETBMD = OP(byteArrayOf(0xeb.toByte()))
        /** Convert a byte string a to a big number, see detailed description (here)[https://spec.nexa.org/nexa/op_bin2bignum.md]
        * Input: **little-endian-sign-mag-byte-array**  Output: **big number**
         */
        val BIN2BIGNUM = OP(byteArrayOf(0xec.toByte()))

        /** OP_EXEC executes a subscript that is presented as data in a script, see (OP_EXEC description)[https://spec.nexa.org/nexa/op_exec] for more details
         * Input: **code param1…paramN N_Params M_Returns** Output: **M values**
         */
        val EXEC = OP(byteArrayOf(0xed.toByte()))

        // Not real opcodes -- used to match data types in script identification comparisons
        val TMPL_BIGINTEGER = OP(byteArrayOf(0xf0.toByte()))
        val TMPL_DATA = OP(byteArrayOf(0xf1.toByte()))
        val TMPL_SCRIPT = OP(byteArrayOf(0xf2.toByte()) ) // replace with any constraint script
        val TMPL_SMALLINTEGER = OP(byteArrayOf(0xfa.toByte()))
        val TMPL_PUBKEYS = OP(byteArrayOf(0xfb.toByte()))
        val TMPL_PUBKEYHASH = OP(byteArrayOf(0xfd.toByte()))
        val TMPL_PUBKEY = OP(byteArrayOf(0xfe.toByte()))

        val LAST_DATAPUSH = 0x4b

        /** Return TRUE if the passed byte is a constant quantity pushdata opcode */
        fun isPushData0(v: Byte): Int
        {
            if ((v >= 1) && (v <= LAST_DATAPUSH)) return v.toPositiveInt()
            return 0
        }

        /** Convert from an opcode into a assembly instruction */
        data class ParsedInst(val inst:ByteArray, val data:ByteArray?, val number: Long?)
        /** Parse a byte array containing 1 instruction into an easy to use structure */
        fun parse(b: ByteArray): ParsedInst
        {
            val inst = b[0]
            val iba = byteArrayOf(inst)
            return when (inst)
            {
                in 1..75 -> ParsedInst(iba, scriptDataFrom(b), scriptNumFrom(b))
                PUSHFALSE.v[0] -> ParsedInst(iba, null, 0)
                PUSHTRUE.v[0] -> ParsedInst(iba, null, 1)
                CNEG1.v[0] -> ParsedInst(iba, null, -1)
                C2.v[0] -> ParsedInst(iba, null, 2)
                C3.v[0] -> ParsedInst(iba, null, 3)
                C4.v[0] -> ParsedInst(iba, null, 4)
                C5.v[0] -> ParsedInst(iba, null, 5)
                C6.v[0] -> ParsedInst(iba, null, 6)
                C7.v[0] -> ParsedInst(iba, null, 7)
                C8.v[0] -> ParsedInst(iba, null, 8)
                C9.v[0] -> ParsedInst(iba, null, 9)
                C10.v[0] -> ParsedInst(iba, null, 10)
                C11.v[0] -> ParsedInst(iba, null, 11)
                C12.v[0] -> ParsedInst(iba, null, 12)
                C13.v[0] -> ParsedInst(iba, null, 13)
                C14.v[0] -> ParsedInst(iba, null, 14)
                C15.v[0] -> ParsedInst(iba, null, 15)
                C16.v[0] -> ParsedInst(iba, null, 16)
                PUSHDATA1.v[0], PUSHDATA2.v[0], PUSHDATA4.v[0] -> ParsedInst(iba, scriptDataFrom(b), scriptNumFrom(b))
                else -> ParsedInst(iba, null, null)
            }
        }

        /** decompile a byte array (that is Nexa script) into a list of opcodes */
        fun decompile(scriptBytes:ByteArray): List<OP>
        {
            val ret = mutableListOf<OP>()
            val tokens = tokenizeScriptBin(scriptBytes)
            for (t in tokens) ret.add(OP(t))
            return ret
        }

        /** compile list of opcodes into a binary program */
        fun compile(opcodes: List<OP>):ByteArray
        {
            val ret = Buffer()
            for (op in opcodes)
            {
                ret.write(op.v)
            }
            return ret.readByteArray()
        }


        /** Convert from an opcode into a assembly instruction */
        fun toAsm(b: ByteArray): String
        {
            val inst = b[0]
            val ret = when (inst)
            {
                in 1..75 -> b.drop(1).toHex() + "h"
                PUSHFALSE.v[0] -> "0"
                PUSHTRUE.v[0] -> "1"
                NOP.v[0] -> "NOP"
                VER.v[0] -> "VER(REMOVED)"
                IF.v[0] -> "IF"
                NOTIF.v[0] -> "NOTIF"
                VERIF.v[0] -> "VERIF(REMOVED)"
                VERNOTIF.v[0] -> "VERNOTIF(REMOVED)"
                ELSE.v[0] -> "ELSE"
                ENDIF.v[0] -> "ENDIF"
                VERIFY.v[0] -> "VERIFY"
                RETURN.v[0] -> "UNSPENDABLE"
                
                TOALTSTACK.v[0] -> "TOALTSTACK"
                FROMALTSTACK.v[0] -> "FROMALTSTACK"
                DROP2.v[0] -> "DROP2"
                DUP2.v[0] -> "DUP2"
                DUP3.v[0] -> "DUP3"
                OVER2.v[0] -> "OVER2"
                ROT2.v[0] -> "ROT2"
                SWAP2.v[0] -> "SWAP2"
                IFDUP.v[0] -> "IFDUP"
                DEPTH.v[0] -> "DEPTH"
                DROP.v[0] -> "DROP"
                DUP.v[0] -> "DUP"
                NIP.v[0] -> "NIP"
                OVER.v[0] -> "OVER"
                PICK.v[0] -> "PICK"
                ROLL.v[0] -> "ROLL"
                ROT.v[0] -> "ROT"
                SWAP.v[0] -> "SWAP"
                TUCK.v[0] -> "TUCK"
                
                CAT.v[0] -> "CAT"
                SPLIT.v[0] -> "SPLIT"
                NUM2BIN.v[0] -> "NUM2BIN"
                BIN2NUM.v[0] -> "BIN2NUM"
                SIZE.v[0] -> "SIZE"
                
                INVERT.v[0] -> "INVERT(REMOVED)"
                AND.v[0] -> "AND"
                OR.v[0] -> "OR"
                XOR.v[0] -> "XOR"
                EQUAL.v[0] -> "EQUAL"
                EQUALVERIFY.v[0] -> "EQUALVERIFY"
                RESERVED1.v[0] -> "RESERVED1"
                RESERVED2.v[0] -> "RESERVED2"
                
                ADD1.v[0] -> "INC"
                SUB1.v[0] -> "DEC"
                MUL2.v[0] -> "MUL2(REMOVED)"
                DIV2.v[0] -> "DIV2(REMOVED)"
                NEGATE.v[0] -> "NEGATE"
                ABS.v[0] -> "ABS"
                NOT.v[0] -> "NOT"
                NOTEQUAL0.v[0] -> "NOTEQUAL0"
                
                ADD.v[0] -> "ADD"
                SUB.v[0] -> "SUB"
                MUL.v[0] -> "MUL"
                DIV.v[0] -> "DIV"
                MOD.v[0] -> "MOD"
                LSHIFT.v[0] -> "LSHIFT"
                RSHIFT.v[0] -> "RSHIFT"

                BOOLAND.v[0] -> "BOOLAND"
                BOOLOR.v[0] -> "BOOLOR"
                NUMEQUAL.v[0] -> "NUMEQUAL"
                NUMEQUALVERIFY.v[0] -> "NUMEQUALVERIFY"
                NUMNOTEQUAL.v[0] -> "NUMNOTEQUAL"
                LESSTHAN.v[0] -> "LESSTHAN"
                GREATERTHAN.v[0] -> "GREATERTHAN"
                LESSTHANOREQUAL.v[0] -> "LESSTHANOREQUAL"
                GREATERTHANOREQUAL.v[0] -> "GREATERTHANOREQUAL"
                MIN.v[0] -> "MIN"
                MAX.v[0] -> "MAX"

                RIPEMD160.v[0] -> "RIPEMD160"
                SHA1.v[0] -> "SHA1"
                SHA256.v[0] -> "SHA256"
                HASH160.v[0] -> "HASH160"
                HASH256.v[0] -> "HASH256"
                CODESEPARATOR.v[0] -> "CODESEPARATOR"
                CHECKSIG.v[0] -> "CHECKSIG"
                CHECKSIGVERIFY.v[0] -> "CHECKSIGVERIFY"
                CHECKMULTISIG.v[0] -> "CHECKMULTISIG"
                CHECKMULTISIGVERIFY.v[0] -> "CHECKMULTISIGVERIFY"

                CHECKLOCKTIMEVERIFY.v[0] -> "CHECKLOCKTIMEVERIFY"
                CHECKSEQUENCEVERIFY.v[0] -> "CHECKSEQUENCEVERIFY"
                CHECKDATASIG.v[0] -> "CHECKDATASIG"
                CHECKDATASIGVERIFY.v[0] -> "CHECKDATASIGVERIFY"
                REVERSE.v[0] -> "REVERSE"

                PUSHDATA1.v[0] -> "PUSH1 " + b.drop(2).toHex() + "h"
                PUSHDATA2.v[0] -> "PUSH2 " + b.drop(3).toHex() + "h"
                PUSHDATA4.v[0] -> "PUSH4 " + b.drop(5).toHex() + "h"

                CNEG1.v[0] -> "-1"

                C2.v[0] -> "2"
                C3.v[0] -> "3"
                C4.v[0] -> "4"
                C5.v[0] -> "5"
                C6.v[0] -> "6"
                C7.v[0] -> "7"
                C8.v[0] -> "8"
                C9.v[0] -> "9"
                C10.v[0] -> "10"
                C11.v[0] -> "11"
                C12.v[0] -> "12"
                C13.v[0] -> "13"
                C14.v[0] -> "14"
                C15.v[0] -> "15"
                C16.v[0] -> "16"


                // Introspection
                INPUTINDEX.v[0] -> "INPUTINDEX"
                ACTIVEBYTECODE.v[0] -> "ACTIVEBYTECODE"
                TXVERSION.v[0] -> "TXVERSION"
                TXINPUTCOUNT.v[0] 	 -> "TXINPUTCOUNT"
                TXOUTPUTCOUNT.v[0] -> "TXOUTPUTCOUNT"
                TXLOCKTIME.v[0] -> "TXLOCKTIME"
                UTXOVALUE.v[0] -> "UTXOVALUE"
                UTXOBYTECODE.v[0] -> "UTXOBYTECODE"
                OUTPOINTTXHASH.v[0] -> "OUTPOINTTXHASH"
                INPUTBYTECODE.v[0] -> "OUTPOINTTXHASH"
                INPUTSEQUENCENUMBER.v[0] -> "INPUTSEQUENCENUMBER"
                OUTPUTVALUE.v[0] -> "OUTPUTVALUE"
                OUTPUTBYTECODE.v[0] -> "OUTPUTBYTECODE"

                // Nexa
                PLACE.v[0] -> "PLACE"
                PUSH_TX_STATE.v[0] -> "PUSHTXSTATE"
                SETBMD.v[0] -> "SETBMD"
                BIN2BIGNUM.v[0] -> "BIN2BIGNUM"
                EXEC.v[0] -> "EXEC"

                // Not real opcodes -- used to match data types in script identification comparisons
                TMPL_BIGINTEGER.v[0] -> "<biginteger>"
                TMPL_DATA.v[0] -> "<bytes>"
                TMPL_SCRIPT.v[0] -> "<script>"
                TMPL_SMALLINTEGER.v[0] -> "<integer>"
                TMPL_PUBKEYS.v[0] -> "<pubkeys>"
                TMPL_PUBKEYHASH.v[0] -> "<pubkeyhash>"
                TMPL_PUBKEYHASH.v[0] -> "<pubkeyhash>"
                TMPL_PUBKEY.v[0] -> "<pubkey>"
                else -> "undefined"
            }
            return ret
        }

        /**
         * If the opcode is a "direct numeric push" (opcodes OP1NEGATE -> OP16),
         * return the value OP code represents as ByteArray. Otherwise, null.
         */
        fun getDirectPush(opcode: Byte): ByteArray?
        {
            when (opcode)
            {
                C0.v[0] ->
                {
                    return ByteArray(0)
                }
                CNEG1.v[0],
                C1.v[0],
                C2.v[0],
                C3.v[0],
                C4.v[0],
                C5.v[0],
                C6.v[0],
                C7.v[0],
                C8.v[0],
                C9.v[0],
                C10.v[0],
                C11.v[0],
                C12.v[0],
                C13.v[0],
                C14.v[0],
                C15.v[0],
                C16.v[0] ->
                {
                    val value: Int = opcode.toPositiveInt() - (C1.v[0].toPositiveInt() - 1)
                    return byteArrayOf(value.toByte())
                }
                else ->
                {
                    return null
                }
            }
        }

        /** Create the appropriate operation to push the passed parameter to the stack */
        fun push(data: ByteArray): OP
        {
            if (data.size == 1)
            {
                when(data[0])
                {
                    0.toByte() -> return OP.C0
                    1.toByte() -> return OP.C1
                    2.toByte() -> return OP.C2
                    3.toByte() -> return OP.C3
                    4.toByte() -> return OP.C4
                    5.toByte() -> return OP.C5
                    6.toByte() -> return OP.C6
                    7.toByte() -> return OP.C7
                    8.toByte() -> return OP.C8
                    9.toByte() -> return OP.C9
                    10.toByte() -> return OP.C10
                    11.toByte() -> return OP.C11
                    12.toByte() -> return OP.C12
                    13.toByte() -> return OP.C13
                    14.toByte() -> return OP.C14
                    15.toByte() -> return OP.C15
                    16.toByte() -> return OP.C16
                    0xFF.toByte() -> return OP.CNEG1
                }
                // If its none of the constants, fall through to the next check
            }
            if (data.size <= LAST_DATAPUSH)
            {
                return OP(byteArrayOf(data.size.toByte()) + data)
            }
            if (data.size <= 0xff)
            {
                return OP(PUSHDATA1.v + data.size.toByte() + data)
            }
            if (data.size <= 0xffff)
            {
                // Size as little endian 16 bits
                val size = ByteArray(2)
                size[0] = (data.size and 0xff).toByte()
                size[1] = (data.size shr 8 and 0xff).toByte()
                return OP(PUSHDATA2.v + size + data)
            }
            else
                throw UnimplementedException("Pushing more data then stack allows")
            //    return PUSHDATA4 + data.size.toPositiveInt() + data
        }

        /** Create the appropriate operation to push the passed parameter to the stack */
        fun push(num: Int): OP = push(num.toLong())
        /** Create the appropriate operation to push the passed parameter to the stack */
        fun push(num: UInt): OP = push(num.toULong())
        /** Create the appropriate operation to push the passed parameter to the stack */
        fun push(num: ULong): OP
        {
            if (num == 0UL) return C0
            if (num <= 16UL)
            {
                var ret = C1.v.copyOf()
                ret[0] = (ret[0].toPositiveULong() + (num - 1UL)).toByte()
                return OP(ret)
            }
            else
            {
                // Numbers are encoded to byte arrays in sign-magnitude format.
                // The magnitude is encoded with the least-significant byte first (little-endian).
                // After that, the MSB of the last byte is reused as the sign if the magnitude encoding set it to 0.
                // Otherwise, another (0) byte is appended and then do the previous sentence on it.
                val neg = false  // keep this implementation exactly the same as the push Long below, but force positive
                var abs = num
                val b = mutableListOf<Byte>()
                while(abs != 0UL)
                {
                    b.add((abs and 0xffUL).toByte())
                    abs = abs shr 8
                }
                // If the last byte has the high bit set, we need to add another byte containing the sign
                if ((b.last() and 0x80.toByte()) != 0.toByte())
                {
                    b.add(if (neg) 0x80.toByte() else 0.toByte())
                }
                // otherwise we can just set that last high bit if needed
                else if (neg)
                {
                    var tmp = b.last()
                    tmp = tmp or 0x80.toByte()
                    b[b.size-1] = tmp
                }
                return push(b.toByteArray())
            }
        }

        /** Create the appropriate operation to push the passed parameter to the stack */
        fun push(num: Long): OP // Pushes a scriptnum
        {
            if (num == 0L) return C0
            if (num > 0 && num <= 16L)
            {
                var ret = C1.v.copyOf()
                ret[0] = (ret[0] + (num - 1)).toByte()
                return OP(ret)
            }
            else
            {
                // Numbers are encoded to byte arrays in sign-magnitude format.
                // The magnitude is encoded with the least-significant byte first (little-endian).
                // After that, the MSB of the last byte is reused as the sign if the magnitude encoding set it to 0.
                // Otherwise, another (0) byte is appended and then do the previous sentence on it.
                val neg = num < 0
                var abs = if (neg) -1*num else num
                val b = mutableListOf<Byte>()
                while(abs != 0L)
                {
                    b.add((abs and 0xff).toByte())
                    abs = abs shr 8
                }
                // If the last byte has the high bit set, we need to add another byte containing the sign
                if ((b.last() and 0x80.toByte()) != 0.toByte())
                {
                    b.add(if (neg) 0x80.toByte() else 0.toByte())
                }
                // otherwise we can just set that last high bit if needed
                else if (neg)
                {
                    var tmp = b.last()
                    tmp = tmp or 0x80.toByte()
                    b[b.size-1] = tmp
                }
                return push(b.toByteArray())
            }

        }

        /** Little endian serialization to 2, 4, or 8 bytes as defined in the Group Tokenization Consensus Specification */
        fun groupAmount(value: Long): OP
        {
            if (value >= 0 && value < 0xffffL) return push(byteArrayOf((value and 0xff).toByte(), (value shr 8).toByte()))
            if (value >= 0 && value < 0xffffffffL) return push(byteArrayOf((value and 0xff).toByte(), (value shr 8).toByte(), (value shr 16).toByte(), (value shr 24).toByte()))
            else return push(byteArrayOf((value and 0xff).toByte(), (value shr 8).toByte(), (value shr 16).toByte(), (value shr 24).toByte(),
              (value shr 32).toByte(), (value shr 40).toByte(), (value shr 48).toByte(), (value shr 56).toByte()))
        }

    }
}


/** Parse out a script num from a byte array that corresponds to a script instruction pushing it to the script's stack */
fun scriptNumFrom(scriptBytes: ByteArray): Long?
{
    if (scriptBytes[0] == OP.C0.v[0]) return 0
    if ((scriptBytes[0] >= OP.C1.v[0]) && (scriptBytes[0] <= OP.C16.v[0])) return (scriptBytes[0].toPositiveInt() - (OP.C1.v[0].toPositiveInt() - 1)).toLong()

    // Lop off the push and then call leSignMag to deserialize a script number
    val pushedBytes = scriptDataFrom(scriptBytes)
    if ((pushedBytes == null)||(pushedBytes.size > MAX_LONG_LESIGNMAG_SIZE)) return null
    else return pushedBytes.leSignMag()
    // throw UnimplementedException("bigger scriptnum parsing is not implemented")
}

/** Parse out the data that would be pushed to the script stack from a byte array that corresponds to a script instruction. */

fun scriptDataFrom(scriptBytes: ByteArray): ByteArray?
{
    var i = 0
    val cmd = scriptBytes[i]
    if (cmd >= 1 && cmd <= OP.LAST_DATAPUSH)
    {
        val len = cmd.toPositiveInt()
        val slice = scriptBytes.sliceArray(range(i + 1, len))
        return slice
    }
    else if (cmd == OP.PUSHDATA1.v[0])
    {
        val len = scriptBytes[i + 1].toPositiveInt()
        val slice = scriptBytes.sliceArray(range(i + 2, len))
        return slice
    }
    else if (cmd == OP.PUSHDATA2.v[0])
    {
        val len = scriptBytes[i + 1].toPositiveInt() + (256*scriptBytes[i + 2].toPositiveInt())
        val slice = scriptBytes.sliceArray(range(i + 3, len))
        return slice
    }
    else if (cmd == OP.PUSHDATA4.v[0])
    {
        val len = scriptBytes[i + 1].toPositiveInt() + (256*scriptBytes[i + 2].toPositiveInt())
        + (256*256*scriptBytes[i + 3].toPositiveInt()) + (256*256*256*scriptBytes[i + 4].toPositiveInt())
        val slice = scriptBytes.sliceArray(range(i + 5, len))
        return slice
    }

    return null
}

/**  tokenizeScriptBin is the opposite of flatten in the sense that each element in data will be 1 instruction
 */
fun tokenizeScriptBin(scriptBytes:ByteArray): MutableList<ByteArray>
{
    var i: Int = 0
    val parsed = MutableList<ByteArray>(0, { _ -> ByteArray(0) })
    while (i < scriptBytes.size)
    {
        val cmd = scriptBytes[i]
        if (cmd >= 1 && cmd <= OP.LAST_DATAPUSH)
        {
            val len: Int = cmd.toPositiveInt()
            val slice = scriptBytes.sliceArray(range(i, len + 1))
            parsed.add(slice)
            i = i + len + 1
        }
        else if (cmd == OP.PUSHDATA1.v[0])
        {
            val j = i + 1
            val len = scriptBytes[j].toPositiveInt()
            val slice = scriptBytes.sliceArray(range(i, len + 2))
            parsed.add(slice)
            i = i + len + 2
        }
        else if (cmd == OP.PUSHDATA2.v[0])
        {
            val j = i + 1
            val len = scriptBytes[j].toPositiveInt() + (256*scriptBytes[j+1].toPositiveInt())
            val slice = scriptBytes.sliceArray(range(i, len + 3))  // 1 cmd byte, 2 length bytes, and the length
            parsed.add(slice)
            i = i + len + 3
        }
        else if (cmd == OP.PUSHDATA4.v[0])
        {
            val j = i + 1
            val len = scriptBytes[j].toPositiveInt() + (256*scriptBytes[j+1].toPositiveInt()) + (256*256*scriptBytes[j+2].toPositiveInt()) + (256*256*256*scriptBytes[j+3].toPositiveInt())
            val slice = scriptBytes.sliceArray(range(i, len + 5))  // 1 cmd byte, 4 length bytes, and the length
            parsed.add(slice)
            i = i + len + 5
        }
        else
        {
            parsed.add(byteArrayOf(cmd))
            i++
        }

    }
    return parsed
}


/** A script the can be run in Nexa or BCH transaction validation virtual machine */
@cli(Display.Simple, "Scripts constrain spending an output or satisfy other scripts' constraints")
open class SatoshiScript(val chainSelector: ChainSelector) : BCHserializable
{
    /** Script type */
    enum class Type(val v: Byte)
    {
        /** A normal script to be run in the Script Virtual Machine */
        SATOSCRIPT(0),
        /** This "script" is actually a bunch of data items. These bytes are never run in the Script Virtual Machine */
        TEMPLATE(1),
        /** A script that may be run in the Script Virtual Machine, limited to push operations */
        PUSH_ONLY(2),

        UNKNOWN(-1);

        companion object
        {
            private val map = values().associateBy(Type::v)
            fun fromByte(type: Byte) = map[type]
        }
    }
    /** What type of script is this */
    var type: Type = Type.SATOSCRIPT
    /** The underlying data that make up this script (do not use) */
    protected var data = MutableList<ByteArray>(0, { _ -> ByteArray(0) })

    // Construction
    @cli(Display.User, "Construct a script from a hex string")
    /** Construct a script from a hex string, which is a serialized script */
    constructor(chainSelector: ChainSelector, script: String, typ: Type = Type.SATOSCRIPT) : this(chainSelector)
    {
        data.add(element = script.fromHex())
        type = typ
    }

    @cli(Display.User, "Construct a script from a list of raw bytes")
    /** Construct a script from a list of raw bytes */
    constructor(chainSelector: ChainSelector, exactBytes: MutableList<ByteArray>, typ: Type = Type.SATOSCRIPT) : this(chainSelector)
    {
        for (b in exactBytes)
        {
            data.add(b)
        }
        type = typ
    }

    @cli(Display.User, "Construct a script from raw bytes")
    /** Construct a script from raw bytes */
    constructor(chainSelector: ChainSelector, typ: Type, vararg instructions: ByteArray) : this(chainSelector)
    {
        for (i in instructions)
        {
            data.add(i)
        }
        type = typ
    }

    @cli(Display.User, "Construct a script from individual instructions")
    /** Construct a script from individual instructions */
    constructor(chainSelector: ChainSelector, typ: Type, vararg instructions: OP) : this(chainSelector)
    {
        for (i in instructions)
        {
            data.add(i.v)
        }
        type = typ
    }

    /** The single SHA256 hash of this script.  Used by some services as a global pointer identifying the script */
    @cli(Display.User, "The single SHA256 hash of this script.  Used by some services as a global pointer identifying the script")
    fun scriptHash(): Hash256
    {
        return Hash256(libnexa.sha256(BCHserialize(SerializationType.SCRIPTHASH).toByteArray()))
    }

    @cli(Display.User, "The RIPEMD160 of the SHA256 hash of this script.  Used by P2SH scripts")
    /** The RIPEMD160 of the SHA256 hash of this script.  Used by P2SH scripts */
    fun scriptHash160(): ByteArray
    {
        return libnexa.hash160(BCHserialize(SerializationType.SCRIPTHASH).toByteArray())
    }


    @cli(Display.User, "Are 2 scripts the same?")
    /** Compare whether 2 scripts are the same. */
    infix fun contentEquals(other: SatoshiScript): Boolean
    {
        val a = flatten()
        val b = other.flatten()
        return a.contentEquals(b)
    }

    /** Compare whether 2 scripts are the same.  Same as contentEquals */
    override fun equals(other: Any?): Boolean
    {
        if (other !is SatoshiScript) return false
        return this.contentEquals(other)
    }

    @cli(Display.User, "The length of this script in bytes")
    /** The length of this script in bytes (serialized) */
    val size: Int
        get() = this.flatten().size

    @cli(Display.User, "Make a copy of this script")
    /** Make a copy of this script */
    fun copy(): SatoshiScript
    {
        return SatoshiScript(chainSelector, type, this.flatten().copyOf())
    }

    @cli(Display.User, "Append raw script bytes to this script, returning a new script.  Note! Use  '+ OP.push(...)' to push data into a script")
    /** Append raw script bytes to this script, returning a new script.  Note! Use  '+ OP.push(...)' to push data into a script") */
    operator fun plus(rawscript: ByteArray): SatoshiScript
    {
        var ret = this.copy()
        ret.add(rawscript)
        return ret
    }

    @cli(Display.User, "Append an opcode to this script, returning a new script.")
    /** Append an opcode to this script, returning a new script. */
    operator fun plus(opcode: OP): SatoshiScript
    {
        var ret = this.copy()
        ret.add(opcode.v)
        return ret
    }

    @cli(Display.User, "concatenate a script to the end of this one, returning the joined script")
    /** concatenate a script to the end of this one, returning the joined script */
    operator fun plus(script: SatoshiScript): SatoshiScript
    {
        var ret = this.copy()
        ret.add(script.flatten())
        return ret
    }

    // Composition
    @cli(Display.User, "Append a raw instruction to this script")
    /** Append a raw instruction to this script */
    fun add(cmd: Byte): SatoshiScript
    {
        data.add(ByteArray(1, { _ -> cmd }))
        return this
    }

    @cli(Display.User, "Append new instructions to this existing script")
    /** Append new instructions to this script*/
    fun add(vararg opcodes: OP): SatoshiScript
    {
        for (op in opcodes)
        {
            data.add(op.v)
        }
        return this
    }

    @cli(Display.User, "Append raw script bytes to this script.  Note! Use add(OP.push(...)) to push data into a script")
    /** Append raw script bytes to this script.  Note! Use add(OP.push(...)) to push data into a script */
    fun add(cmds: ByteArray): SatoshiScript
    {
        data.add(cmds)
        return this
    }

    @cli(Display.User, "Append raw script bytes to this script.  Note! Use add(OP.push(...)) to push data into a script")
    /* Append raw script bytes to this script.  Note! Use add(OP.push(...)) to push data into a script */
    fun add(cmds: List<ByteArray>): SatoshiScript
    {
        for (c in cmds)
        {
            data.add(c)
        }
        return this
    }

    @cli(Display.User, "Converts the internal representation to a single ByteArray")
        /** Converts the internal representation to a single ByteArray
         * @return A ByteArray that represents this compiled script
         */
    fun flatten(): ByteArray
    {
        if (data.size == 0) return byteArrayOf()
        if (data.size > 1)
            data = MutableList<ByteArray>(1, { _ -> data.join() })
        return data[0]
    }

    /** Parse is the opposite of flatten in the sense that each element in data will be 1 instruction
     * @return a list of containing each separate instruction.  To parse each instruction into an OP, use [OP.parse]
     */
    fun parsed(): MutableList<ByteArray>
    {
        val scriptBytes = flatten()
        return tokenizeScriptBin(scriptBytes)
    }


    @cli(Display.User, "Create a script from serialized data")
    /** Create this script object from serialized data */
    constructor(chainSelector: ChainSelector, buf: BCHserialized) : this(chainSelector)
    {
        BCHdeserialize(buf)
    }

    @cli(Display.User, "Overwrite this script with serialized data")
    /** Clear any current script bytes, and replace them with the passed serialized script
     * @param stream a stream containing the serialized script as the "next" element */
    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        // Type of the script is serialized whenever the script is stored to disk
        if (stream.format == SerializationType.DISK)
        {
            val sval = stream.deuint8()
            val tmp = Type.fromByte(sval.toByte())
            if (tmp == null)
            {
                throw DeserializationException("Invalid script type")
            }
            type = tmp
        }
        else type = Type.UNKNOWN  // Caller needs to set this type after this call returns (presumably based on the context the script appears in)
        data.clear()
        var len = stream.decompact()
        data.add(stream.debytes(len))
        return stream
    }

    @cli(Display.User, "Serialize this script")
    /** Serialize this script */
    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        var alldata: ByteArray = data.join()
        if (format == SerializationType.SCRIPTHASH)
        {
            return BCHserialized(alldata, format)
        }
        if (format == SerializationType.DISK)
            return BCHserialized.uint8(type.v).add(BCHserialized.compact(alldata.size.toLong(), format)).add(BCHserialized(alldata, format))

        return BCHserialized.compact(alldata.size.toLong(), format).add(BCHserialized(alldata, format))
    }

    @cli(Display.User, "Convert to serialized byte array (suitable for network, execution or hashing)")
    /** Convert this script to a serialized byte array (suitable for network, execution or hashing) */
    fun toByteArray(): ByteArray
    {
        return BCHserialize(SerializationType.SCRIPTHASH).toByteArray()
    }


    /** The result of attempting to match a script to well known script forms, pulling out the variable parts, such as public keys.
     * @see [match] */
    data class MatchResult(
        /** What script form was matched */
        val type: PayAddressType,
        /** The parameters of that script form.  Exactly what these are depends on the matched script form */
        val params: MutableList<ByteArray>,
        /** Does this script contain a group token prefix? */
        val grouped: Boolean = false)
    {
    }

    @cli(Display.User, "Parse common script types (P2PKH and P2SH) and return useful data")
    /** Parse common script forms, P2PKH and P2PKT, and P2SH (in BCH only) and return useful data */
    fun match(): MatchResult?
    {
        matches(P2PKHscriptForm)?.run { return MatchResult(PayAddressType.P2PKH, this) }
        if (chainSelector.isBchFamily)
            matches(P2SHscriptForm)?.run { return MatchResult(PayAddressType.P2SH, this) }
        matches(P2PKTscriptForm)?.run { return MatchResult(PayAddressType.P2PKH, this, true) }
        return null
    }

    /** iterate through the script, looking for replaceable marker opcodes.  If a marker is found, call the callback with the marker.
     * @mapper Supply a callback that returns the actual data when given specific marker opcodes.
     * @return A script with all replacements made. */
    fun replace(mapper: (ByteArray) -> SatoshiScript): SatoshiScript
    {
        val script = flatten()
        var ret = SatoshiScript(chainSelector)
        ret.type = type

        var i = 0 // current location in the script
        while (i < script.size)  // TODO extract the script looper
        {
            if (script[i] == OP.TMPL_PUBKEYHASH.v[0])
            {
                ret += mapper(OP.TMPL_PUBKEYHASH.v)
                i++

            }
            else if (script[i] == OP.TMPL_SCRIPT.v[0])
            {
                ret += mapper(OP.TMPL_SCRIPT.v)
                i++
            }
            // TODO the other template items
            else if (OP.isPushData0(script[i]) > 0)
            {
                val datalen = script[i]
                i++
                ret.add(OP.push(script.sliceArray(i until i + datalen)))
                i += datalen
            }
            else if (script[i] == OP.PUSHDATA1.v[0])
            {
                i++
                val datalen = script[i]
                i++
                ret.add(OP.push(script.sliceArray(i until i + datalen)))
                i += datalen
            }
            else if (script[i] == OP.PUSHDATA2.v[0])  // TODO
            {
                throw UnimplementedException("pushdata 2")
            }
            else if (script[i] == OP.PUSHDATA4.v[0])  // TODO
            {
                throw UnimplementedException("pushdata 4")
            }
            else  // Handle any other opcode
            {
                ret.add(script[i])
                i++
            }
        }
        return ret
    }

    /** Returns true if this script has replacable marker parameters in it (its not a valid executable script yet). */
    fun parameterized(): Boolean
    {
        val script = flatten()
        var i = 0 // current location in the script
        while (i < script.size)  // TODO extract the script looper
        {
            if (script[i] == OP.TMPL_PUBKEYHASH.v[0])
            {
                return true
            }
            else if (script[i] == OP.TMPL_SCRIPT.v[0])
            {
                return true
            }
            // TODO the other template items
            else if (OP.isPushData0(script[i]) > 0)
            {
                val datalen = script[i]
                i++
                i += datalen
            }
            else if (script[i] == OP.PUSHDATA1.v[0])
            {
                i++
                val datalen = script[i]
                i++
                i += datalen
            }
            else if (script[i] == OP.PUSHDATA2.v[0])  // TODO
            {
                throw UnimplementedException("pushdata 2")
            }
            else if (script[i] == OP.PUSHDATA4.v[0])  // TODO
            {
                throw UnimplementedException("pushdata 4")
            }
            else  // Handle any other opcode
            {
                i++
            }
        }
        return false
    }

    @cli(Display.Dev, "If this is a P2SH satisfier script, get its redeem script.\n" +
      "Warning, this just returns the last script data push as a script, so if this is NOT a P2SH satisfier script, you will get weird results.")
        /**
         * If this is a P2SH satisfier script, get its redeem script.
         * Warning, this just returns the last script data push as a script, so if this is NOT a P2SH satisfier script, you will get weird results.
         */
    fun getRedeemFromSatisfier(): SatoshiScript
    {
        val satisfier = parsed()
        val redeemRaw = scriptDataFrom(satisfier[satisfier.size - 1]) ?: byteArrayOf()
        val redeemScript = SatoshiScript(chainSelector, Type.SATOSCRIPT, redeemRaw)
        return redeemScript
    }

    @cli(Display.Dev, "Attempt to match this script (or just the beginning of it) to a template")
        /**
         * Attempt to match this script (or just the beginning of it) to a script form.
         */
    fun matches(template: SatoshiScript, prefix: Boolean = false): MutableList<ByteArray>?
    {
        val script = parsed()
        val tmpl = template.parsed()
        var ret = mutableListOf<ByteArray>()

        var ti = 0 // current location in the template
        var i = 0 // current location in the script
        while ((i < script.size) && (ti < tmpl.size))
        {
            val scrInst = OP.parse(script[i])  // Parse the instructions
            val tmplInst = OP.parse(tmpl[ti])

            if (scrInst.number != null)
            {
                i+=1
                ti+=1
                // If the script has a number, expect the template has the same number or a placeholder for any number
                if ((tmplInst.inst contentEquals OP.TMPL_SMALLINTEGER.v) || (tmplInst.inst contentEquals OP.TMPL_DATA.v))
                {
                    ret.add(byteArrayOf())  // todo figure out what to do with numbers
                    //ret.add(scrInst.data)
                    continue
                }
                else if (tmplInst.number == scrInst.number) continue

            }
            else if (scrInst.data != null)
            {
                i+=1
                ti+=1
                if (tmplInst.inst contentEquals OP.TMPL_PUBKEYHASH.v)
                {
                    if (scrInst.data.size != 20) return null  // Pubkeyhash must be 20 bytes
                    ret.add(scrInst.data)
                    continue
                }

                if ((tmplInst.inst contentEquals OP.TMPL_DATA.v) ||
                 (tmplInst.inst contentEquals OP.TMPL_BIGINTEGER.v) ||
                 (tmplInst.inst contentEquals OP.TMPL_SCRIPT.v) ||
                 (tmplInst.inst contentEquals OP.TMPL_PUBKEY.v))
                {
                    ret.add(scrInst.data)
                    continue
                }
                // TODO: PUBKEYS is trickier because it can match multiple data items but must match at least 1

                // the data in the template must match the script (but the script can have additional data)
                if ((tmplInst.data != null) && (scrInst.data.size >= tmplInst.data.size))
                  if (tmplInst.data contentEquals scrInst.data.sliceArray(0 until tmplInst.data.size)) continue
            }
            else
            {
                i+=1
                ti+=1
                if (tmplInst.inst contentEquals scrInst.inst) continue  // Instructions match
            }

            // Didn't successfully match so abort
            return null
        }

        if (prefix && (ti == tmpl.size)) return ret  // if looking for a prefix, just check that the template ended
        if ((i == script.size) && (ti == tmpl.size)) return ret  // both script and template ended at the same moment, template matches
        return null
    }

    @cli(Display.Dev, "Attempt to match this script (or just the beginning of it) to a script form")
        /**
         * Attempt to match this script (or just the beginning of it) to a script form
         */
    fun matchesByBytes(template: SatoshiScript, prefix: Boolean = false): MutableList<ByteArray>?
    {
        val script = flatten()
        val tmpl = template.flatten()
        var ret = mutableListOf<ByteArray>()

        var ti = 0 // current location in the template
        var i = 0 // current location in the script
        while ((i < script.size) && (ti < tmpl.size))
        {
            if (tmpl[ti] == script[i])
            {
                i += 1; ti += 1; continue
            }
            if (tmpl[ti] == OP.TMPL_PUBKEYHASH.v[0])
            {
                if (OP.isPushData0(script[i]) > 0)  // Handle the direct push case
                {
                    val datalen = script[i]
                    if (datalen != 20.toByte()) return null   // pubkeyhash must be 20 bytes
                    i += 1
                    ret.add(script.sliceArray(IntRange(i, i + 19)))
                    i += 20
                    ti += 1
                    continue
                }

                if (script[i] == OP.PUSHDATA1.v[0]) // Handle the pushdata1 case
                {
                    i += 1
                    val datalen = script[i]
                    if (datalen != 20.toByte()) return null   // pubkeyhash must be 20 bytes
                    i += 1
                    ret.add(script.sliceArray(IntRange(i, i + 19)))
                    i += 20
                    ti += 1
                    continue
                }

                // TODO the other pushdata operations
                //    PUSHDATA2
                //    PUSHDATA4
            }
            if (tmpl[ti] == OP.TMPL_DATA.v[0])
            {
                if (OP.isPushData0(script[i]) > 0)  // Handle the direct push case
                {
                    val datalen = script[i]
                    i += 1
                    ret.add(script.sliceArray(i until i + datalen))
                    i += datalen
                    ti += 1
                    continue
                }

                if (script[i] == OP.PUSHDATA1.v[0]) // Handle the pushdata1 case
                {
                    i += 1
                    val datalen = script[i]
                    i += 1
                    ret.add(script.sliceArray(i until i + datalen))
                    i += datalen
                    ti += 1
                    continue
                }
                // TODO the other pushdata operations
                //    PUSHDATA2
                //    PUSHDATA4
            }

            // TODO the other template items
            return null
        }

        if (prefix && (ti == tmpl.size)) return ret  // if looking for a prefix, just check that the template ended
        if ((i == script.size) && (ti == tmpl.size)) return ret  // both script and template ended at the same moment, template matches
        return null
    }


    // Analysis
    @cli(Display.User, """Parse common script types (P2PKH, P2SH, script template) and return the address or null if the script is not standard.  
        This returns the minimum spendable address extractable from this script.  That is, Group annotations are removed""")
        /**
         * Parse common script types (P2PKH, P2SH, script template) and return the address or null if the script is not standard.
         * @return This returns the minimum spendable address extractable from this script.  That is, Group annotations are removed
         */
    val address: PayAddress? // nullable because its possible that I can't understand this script or that it is an anyone-can-pay (has no constraint)
        get()
        {
            if (type==Type.TEMPLATE)
            {
                val tp = parseTemplate(-1)  // We don't care about a fenced group amount since we are going to ignore group data anyway, so pass a bogus number for the native amount
                if (tp == null) return null
                if (tp.wellKnownId == 1L)  // 1 is well known template P2PKT
                    return PayAddress(chainSelector, PayAddressType.P2PKT, tp.p2pkt(chainSelector).asSerializedByteArray())
                else
                    return PayAddress(chainSelector, PayAddressType.TEMPLATE, tp.ungroupedP2t(chainSelector).asSerializedByteArray())
            }
            else
            {
                matches(P2PKHscriptForm)?.run {
                    return PayAddress(chainSelector, PayAddressType.P2PKH, this[0])
                }

                matches(P2SHscriptForm)?.run {
                    return PayAddress(chainSelector, PayAddressType.P2SH, this[0])
                }
            }
            return null
        }

    /** Turn a byte array (from the script stack) into a group amount
     */
    fun decodeGroupAmount(ser: ByteArray): Long?
    {
        if (!((ser.size == 2) || (ser.size == 4) || (ser.size == 8))) return null  // amount size is incorrect
        if (ser.size == 2)
        {
            return (ser[0].toPositiveULong() + (ser[1].toPositiveULong() shl 8)).toLong()
        }
        if (ser.size == 4)
        {
            return (ser[0].toPositiveULong() + (ser[1].toPositiveULong() shl 8) + (ser[2].toPositiveULong() shl 16) + (ser[3].toPositiveULong() shl 24)).toLong()
        }
        if (ser.size == 8)
        {
            return (ser[0].toPositiveULong() + (ser[1].toPositiveULong() shl 8) + (ser[2].toPositiveULong() shl 16) + (ser[3].toPositiveULong() shl 24) +
              (ser[4].toPositiveULong() shl 32) + (ser[5].toPositiveULong() shl 40) + (ser[6].toPositiveULong() shl 48) + (ser[7].toPositiveULong() shl 56)
              ).toLong()
        }
        return null
    }

    @cli(Display.User, "Return the Group annotation associated with this script, or null if ungrouped")
        /**
         * Return the Group annotation associated with this script, or null if ungrouped
         */
    fun groupInfo(nativeAmount: Long): GroupInfo?
    {
        if (type != Type.TEMPLATE) return null

        val parameters = parsed()
        if (parameters[0] == OP.C0.v)  // OP0 means no group
            return null
        val gid = scriptDataFrom(parameters[0])
        if (gid == null) return null
        if (gid.size < GroupId.GROUP_ID_MIN_SIZE) return null  // TODO return something indicating illegal script?
        if (gid.size > GroupId.GROUP_ID_MAX_SIZE) return null
        val gamt = parameters[1]  // group amount is a pushdata of 2, 4, or 8 bytes
        OP.isPushData0(gamt[0]).let {
            if (!((it == 2) || (it == 4) || (it == 8))) return null
        }
        var amt = decodeGroupAmount(gamt.drop(1).toByteArray()) ?: return null
        var authority = 0.toULong()
        val groupId = GroupId(chainSelector, gid)
        if (amt < 0)   // Handle authority flags
        {
            authority = amt.toULong()
            amt = 0
        }
        else
        {
            if (groupId.isFenced())
            {
                if (amt != 0L) return null // return something indicating illegal script?
                amt = nativeAmount
            }
        }
        return GroupInfo(GroupId(chainSelector, gid), amt, authority)
    }

    @cli(Display.User, "Given an output script in template format, parse its data items")
        /**
         * Given an output script in script template format, parse its data items
         */
    fun parseTemplate(nativeAmount: Long): ScriptTemplate?
    {
        if (type != Type.TEMPLATE) return null

        try
        {
            var curParam = 0
            val parameters = parsed()
            val groupInfo: GroupInfo? = if (parameters[curParam] contentEquals OP.C0.v)  // OP0 means no group
            {
                curParam++
                null
            }
            else
            {
                curParam += 2
                groupInfo(nativeAmount)
            }

            var wellKnownId: Long? = null
            val templateHash = scriptDataFrom(parameters[curParam])
            if (templateHash != null)
            {
                // bad hash size
                if (!((templateHash.size == 20) || (templateHash.size == 32))) return null
            }
            else
            {
                wellKnownId = scriptNumFrom(parameters[curParam])
            }

            curParam++
            val argsHash = scriptDataFrom(parameters[curParam])
            if (argsHash == null) return null
            curParam++
            return ScriptTemplate(groupInfo, templateHash, wellKnownId, argsHash, parameters.drop(curParam))
        }
        catch (e: IndexOutOfBoundsException)  // If the template script is incorectly formatted, we can't return information about it
        {
            return null
        }
    }

    /** If this script is a script template output, put a token group and token amount into the script template output */
    fun grouped(grpId: GroupId, tokenAmt: Long): SatoshiScript
    {
        if (!chainSelector.hasGroupTokens) throw UnsupportedInBlockchain("group tokens")
        val gid = grpId.toByteArray()
        // A GroupId object programatically can't actually be an invalid group (if you stay within the GroupId API) so these checks are for misbehaving code
        if (gid.size == 0) throw UnimplementedException("implement no group as native coin xfer")
        assert(gid.size >= 32)  // GroupIds must be 32 bytes or more
        val tmpl = parseTemplate(0)
        if (tmpl == null) throw UnsupportedOperationException("cannot group this script")  // If you are grouping a script it MUST be a template, but its not
        tmpl.groupInfo = GroupInfo(grpId, tokenAmt)
        return tmpl.gp2t(chainSelector)
    }

    @cli(Display.User, "Return the P2SH constraint (output) script corresponding to this redeem script")
    /** Return the P2SH constraint (output) script corresponding to this redeem script */
    fun P2SHconstraint(): SatoshiScript
    {
        return SatoshiScript(chainSelector, Type.SATOSCRIPT, OP.HASH160, OP.push(scriptHash160()), OP.EQUAL)
    }

    @cli(Display.User, "Return the P2SH part of the satisfier (input) script corresponding to this redeem script.  You must subsequently append a script that actually satisfies the redeem script.")
    /** Return the P2SH part of the satisfier (input) script corresponding to this redeem script.  You must subsequently append a push-only script that provides the arguments required by the redeem script. */
    fun P2SHsatisfier(): SatoshiScript
    {
        return SatoshiScript(chainSelector, Type.SATOSCRIPT, OP.push(toByteArray()))
    }

    @cli(Display.Simple, "Convert to hex notation")
    /** Convert to hex */
    fun toHex(): String
    {
        return flatten().toHex()
    }

    /** Replace any data currently in this script with the passed hex-encoded binary script.
     * Note that this function does NOT check to ensure that the passed hex-encoded binary data is a valid script.
     * @return this object */
    fun fromHex(s: String): SatoshiScript
    {
        val d = s.fromHex()
        data.clear()
        data.add(d)
        return this
    }

    @cli(Display.Simple, "Convert to a list of opcodes")
    /** Convert to a list of opcodes */
    fun toOPs(): List<OP>
    {
        val ret = mutableListOf<OP>()
        val ps = parsed()  // break into instructions
        for (i in ps)
        {
            ret.add(OP(i))
        }
        return ret
    }

    @cli(Display.Simple, "Convert to assembly code")
    /** Convert to assembly code */
    fun toAsm(separator: String = " "): String
    {
        val ps = parsed()  // break into instructions
        val ret = StringJoiner(separator)
        for (i in ps)
        {
            ret.add(OP.toAsm(i))
        }
        return ret.toString()
    }

    @cli(Display.User, "Take a copy of the first pushed stack item as it would appear on the stack, returning the copy and the range copied from. Throws if first OP is not a PUSH operation.")
    fun copyFront(): Pair<ByteArray, IntRange>
    {
        return copyStackElementAt(0)
    }

    @cli(Display.User, "Take a copy of stack element at offset as it would appear on the stack, returning the copy and the range copied from. Offset MUST point to a PUSH operation.")
        /**
         * Take a copy of stack element at offset as it would appear on the stack, returning the copy and the range copied from.
         * Offset MUST point to a PUSH operation.
         */
    fun copyStackElementAt(offset: Int): Pair<ByteArray, IntRange>
    {
        val script = flatten()
        var i = offset
        val pushOP = script.getOrNull(i++) ?: throw IllegalArgumentException("Offset $offset is out of bounds. Size of script is ${script.size}.")
        OP.getDirectPush(pushOP)?.let {
            return Pair(it, IntRange(offset, offset + it.size))
        }
        if (OP.isPushData0(pushOP) != 0)
        {
            val range = IntRange(i, i + pushOP.toPositiveInt() - 1)
            val stackItem = script.sliceArray(range)
            return Pair(stackItem, range)
        }
        if (pushOP == OP.PUSHDATA1.v[0])
        {
            val stackItemLen = script[i++].toPositiveInt()
            val range = IntRange(i, i + stackItemLen - 1)
            val stackItem = script.sliceArray(range)
            return Pair(stackItem, range)
        }
        if (pushOP == OP.PUSHDATA2.v[0])
        {
            val stackItemLen: ByteArray = script.copyOfRange(i++, ++i)

            val stackItemLenBE: Int = (stackItemLen[1].toPositiveInt() shl 8) or stackItemLen[0].toPositiveInt()

            val range = IntRange(i, i + stackItemLenBE - 1)
            val stackItem = script.sliceArray(range)
            return Pair(stackItem, range)
        }
        if (pushOP == OP.PUSHDATA4.v[0])
        {
            TODO("PUSHDATA4")
        }
        throw IllegalArgumentException("Element at offset ${offset} is not a PUSH operation (found ${pushOP.toPositiveInt()})")
    }

    /** Wraps this script into a serialized byte array.  That is, a byte array of [compact int script length] [script bytes].
     * This is different than toByteArray which just returns the script bytes. */
    fun asSerializedByteArray(st: SerializationType = SerializationType.UNKNOWN):ByteArray
    {
        return (BCHserialized(st).add(variableSized(flatten()))).toByteArray()
    }

    /** convert this script to a human readable format */
    override fun toString(): String = when (type)
    {
        Type.SATOSCRIPT -> "SatoScript(\"" + toAsm() + "\")"
        Type.TEMPLATE ->
        {
            val ret = StringBuilder()
            ret.append("TemplateScript")
            val gi = groupInfo(0)  // not going to use the native amount anyway
            if (gi != null)
            {
                ret.append(" " + gi + " ")
            }
            ret.append("(\"" + toAsm() + "\")")
            ret.toString()
        }
        Type.PUSH_ONLY -> "DataScript(\"" + toAsm() + "\")"
        else -> "Script(\"" + toAsm() + "\")"
    }

    // Execution
    companion object
    {
        // Right now, the chain is ignored in these templates since multiple blockchains support the same script templates
        val P2PKHscriptForm = SatoshiScript(ChainSelector.NEXA, Type.SATOSCRIPT, OP.DUP, OP.HASH160, OP.TMPL_PUBKEYHASH, OP.EQUALVERIFY, OP.CHECKSIG)
        val P2SHscriptForm = SatoshiScript(ChainSelector.BCH, Type.SATOSCRIPT, OP.HASH160, OP.TMPL_PUBKEYHASH, OP.EQUAL)
        // Pay to pub key template
        val P2PKTscriptForm = SatoshiScript(ChainSelector.NEXA, Type.TEMPLATE, OP.TMPL_DATA, OP.TMPL_DATA,OP.TMPL_DATA)  // TODO
        /*  TODO Identify template contract output
        // These do not match that the group data is the correct size so more checking is needed
        val GroupPrefixTemplate = BCHscript(ChainSelector.BCHMAINNET, OP.TMPL_DATA, OP.TMPL_DATA, OP.GROUP)
        val GroupP2PKHtemplate = GroupPrefixTemplate + P2PKHtemplate
        val GroupP2SHtemplate = GroupPrefixTemplate + P2SHtemplate

        init
        {
            GroupP2PKHtemplate.flatten()
            GroupP2SHtemplate.flatten()
        }
         */

        val asm2Bin:MutableMap<String, OP> = mutableMapOf()
        @cli(Display.User, "Compile text asm into a script")
        fun fromAsm(asm: Array<String>, chainSelector: ChainSelector = ChainSelector.NEXA): SatoshiScript
        {
            val prog = SatoshiScript(chainSelector)
            if (asm2Bin.size == 0)  // populate the first time
            {
                for (i in 0 .. 256)
                {
                    val tgt = byteArrayOf(i.toByte())
                    val asm2 = OP.toAsm(tgt)
                    if (!asm2.startsWith("0x"))
                    {
                        asm2Bin[asm2] = OP(tgt)
                    }
                }
                // Now add a few hard-coded synonyms
                asm2Bin["FALSE"] = OP.PUSHFALSE
                asm2Bin["TRUE"] = OP.PUSHTRUE
                asm2Bin["-1"] = OP.CNEG1
            }

            for(i in asm)
            {
                if (i.uppercase() in asm2Bin)
                {
                    prog.add(asm2Bin[i.uppercase()]!!.v)
                }
                else
                {
                    var str=i
                    println(str)
                    if (str.startsWith("0x") || str.startsWith("0X"))
                    {
                        str = str.drop(2)  // Remove the prefix
                        val pushdata: ByteArray = str.fromHex()
                        println(pushdata.size)
                        println(pushdata.toHex())
                        prog.add(OP.push(pushdata))
                    }
                    else
                    {
                        var v = i.toLongOrNull()  // Try to convert to a decimal number
                        if (v != null)
                        {
                            prog.add(OP.push(v))
                        }
                        else
                        {
                            throw ScriptException("instruction not understood: $i", "bad $i")
                        }
                    }
                }
            }
            return prog
        }

        @cli(Display.Simple, "Create a pay-to-public-key-hash constraint script")
        fun p2pkh(rawAddr: ByteArray, chainSelector: ChainSelector = ChainSelector.NEXA): SatoshiScript
        {
            assert(rawAddr.size == 20)  // Otherwise this p2pkh is unspendable
            return SatoshiScript(chainSelector, Type.SATOSCRIPT, OP.DUP, OP.HASH160, OP.push(rawAddr), OP.EQUALVERIFY, OP.CHECKSIG)
        }

        @cli(Display.Simple, "Create a BCH pay-to-script-hash constraint script")
        fun p2sh(rawAddr: ByteArray, chainSelector: ChainSelector = ChainSelector.BCH): SatoshiScript
        {
            assert(rawAddr.size == 20)  // Otherwise this p2sh is unspendable
            return SatoshiScript(chainSelector, Type.SATOSCRIPT, OP.HASH160, OP.push(rawAddr), OP.EQUAL)
        }

        @cli(Display.Simple, "Create a grouped pay-to-public-key-template constraint script.")
        fun gp2pkt(chainSelector: ChainSelector, grpId: GroupId, tokenAmt: Long, pubkey: ByteArray): SatoshiScript
        {
            val gid = grpId.toByteArray()
            // A GroupId object programatically can't actually be an invalid group (if you stay within the GroupId API) so these checks are for misbehaving code
            if (gid.size == 0) return ungroupedP2pkt(chainSelector, pubkey)
            assert(gid.size >= 32)  // GroupIds must be 32 bytes or more
            if (!chainSelector.hasGroupTokens) throw UnsupportedInBlockchain("group tokens")
            val constraintArgsScript = SatoshiScript(chainSelector, Type.PUSH_ONLY, OP.push(pubkey))
            val constraintArgsHash = libnexa.hash160(constraintArgsScript.toByteArray())
            return SatoshiScript(chainSelector, Type.TEMPLATE, OP.push(gid), OP.groupAmount(tokenAmt), P2PKT_ID, OP.push(constraintArgsHash))
        }

        @cli(Display.Simple, "Create a grouped output script prefix (you need to append the script hash, args hash, etc to create a correct output script template).")
        fun grouped(chainSelector: ChainSelector, grpId: GroupId, tokenAmt: Long): SatoshiScript
        {
            val gid = grpId.toByteArray()
            if (!chainSelector.hasGroupTokens) throw UnsupportedInBlockchain("group tokens")
            return SatoshiScript(chainSelector, Type.TEMPLATE, OP.push(gid), OP.groupAmount(tokenAmt))
        }

        @cli(Display.Simple, "Create a pay-to-public-key-template output script suffix -- group information must be prepended")
        fun p2pktSuffix(chainSelector: ChainSelector, pubkey: ByteArray): SatoshiScript
        {
            if (!chainSelector.hasTemplates) throw UnsupportedInBlockchain("templates")
            val constraintArgsScript = SatoshiScript(chainSelector, Type.PUSH_ONLY, OP.push(pubkey))
            val constraintArgsHash = libnexa.hash160(constraintArgsScript.toByteArray())
            return SatoshiScript(chainSelector, Type.TEMPLATE, P2PKT_ID, OP.push(constraintArgsHash))
        }

        @cli(Display.Simple, "Create a raw nexa coin (no group) pay-to-public-key-template output script")
        fun ungroupedP2pkt(chainSelector: ChainSelector, pubkey: ByteArray): SatoshiScript
        {
            if (!chainSelector.hasTemplates) throw UnsupportedInBlockchain("templates")
            val constraintArgsScript = SatoshiScript(chainSelector, Type.PUSH_ONLY, OP.push(pubkey))
            val constraintArgsHash = libnexa.hash160(constraintArgsScript.toByteArray())
            return SatoshiScript(chainSelector, Type.TEMPLATE, OP.PUSHFALSE, P2PKT_ID, OP.push(constraintArgsHash))
        }

        @cli(Display.Simple, "Create a pay-to-template output script. If grpId is null, or not provided, nexa coin is used")
        fun p2t(chainSelector: ChainSelector, templateScriptHash: ByteArray, constraintArgsHash: ByteArray, constraintPublicArgs: SatoshiScript?=null, grpId: GroupId? = null, tokenAmt: Long? = null): SatoshiScript
        {
            if ((grpId == null) xor (tokenAmt == null)) throw ScriptException("invalid args combination.  Must provide both group and amount or neither")
            if (!chainSelector.hasTemplates) throw UnsupportedInBlockchain("templates")
            assert(templateScriptHash.size == 20 || templateScriptHash.size == 32)
            assert(constraintArgsHash.size == 20 || constraintArgsHash.size == 32)
            val ret = if (grpId != null)
            {
                SatoshiScript(chainSelector, Type.TEMPLATE, OP.push(grpId.toByteArray()), OP.groupAmount(tokenAmt!!), OP.push(templateScriptHash), OP.push(constraintArgsHash))
            }
            else
            {
                SatoshiScript(chainSelector, Type.TEMPLATE, OP.PUSHFALSE, OP.push(templateScriptHash), OP.push(constraintArgsHash))
            }
            if (constraintPublicArgs != null) ret.add(constraintPublicArgs.flatten())
            return ret
        }

    }


}

//* Well known template type identifier: pay-to-public-key-template
val P2PKT_ID = OP.C1
