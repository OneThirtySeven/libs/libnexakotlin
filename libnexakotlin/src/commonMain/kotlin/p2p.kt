// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.nexa.libnexakotlin

import io.ktor.network.selector.SelectorManager
import io.ktor.network.sockets.InetSocketAddress
import io.ktor.network.sockets.Socket
import io.ktor.network.sockets.SocketTimeoutException
import io.ktor.network.sockets.aSocket
import io.ktor.network.sockets.isClosed
import io.ktor.network.sockets.openReadChannel
import io.ktor.network.sockets.openWriteChannel
import io.ktor.utils.io.ByteReadChannel
import io.ktor.utils.io.ByteWriteChannel
import io.ktor.utils.io.charsets.Charset
import io.ktor.utils.io.errors.IOException
import kotlinx.coroutines.*
import kotlinx.datetime.Clock
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.*
import org.nexa.threads.Gate
import org.nexa.threads.millisleep
import kotlin.concurrent.Volatile

import kotlin.coroutines.CoroutineContext
import kotlin.math.min
import kotlin.time.DurationUnit
import kotlin.time.TimeMark
import kotlin.time.ExperimentalTime
import kotlin.time.TimeSource

private val LogIt = GetLog("BU.p2p")

enum class NetMsgType
{
    INVALID, VERSION, VERACK, ADDR, INV, GETDATA, MERKLEBLOCK, GETBLOCKS, TX, HEADERS, BLOCK, THINBLOCK, XTHINBLOCK, SENDCMPCT,
    XBLOCKTX, GET_XBLOCKTX, GET_XTHIN, GETADDR, MEMPOOL, PING, PONG, NOTFOUND, FILTERLOAD, FILTERADD, FILTERCLEAR,
    FILTERSIZEXTHIN, FEEFILTER, REJECT, GETHEADERS, SENDHEADERS, XPEDITEDREQUEST, XPEDITEDBLK, XPEDITEDTXN, BUVERSION, BUVERACK, XVERSION, XUPDATE,
    REQ_TXVAL, RES_TXVAL,

    // SV
    PROTOCONF;

    companion object
    {
        fun ba(sz: Int, s: String) = s.toByteArray().copyOf(sz)

        val VERSION_STR = ba(12, "version")
        val VERACK_STR = ba(12, "verack")
        val BUVERSION_STR = ba(12, "buversion")
        val BUVERACK_STR = ba(12, "buverack")
        val XVERSION_STR = ba(12, "xversion")
        val XUPDATE_STR = ba(12, "xversion")
        val SENDCMPCT_STR = ba(12, "sendcmpct")
        val ADDR_STR = ba(12, "addr")
        val INV_STR = ba(12, "inv")
        val GETDATA_STR = ba(12, "getdata")
        val NOTFOUND_STR = ba(12, "notfound")  // response to getdata if some data is not found


        val MERKLEBLOCK_STR = ba(12, "merkleblock")
        val BLOCK_STR = ba(12, "block")

        val FILTERLOAD_STR = ba(12, "filterload")
        val FILTERADD_STR = ba(12, "filteradd")
        val FILTERCLEAR_STR = ba(12, "filterclear")
        val FEEFILTER_STR = ba(12, "feefilter")

        val TX_STR = "tx".toByteArray().copyOf(12)
        val GETADDR_STR = "getaddr".toByteArray().copyOf(12)
        val PING_STR = "ping".toByteArray().copyOf(12)
        val PONG_STR = "pong".toByteArray().copyOf(12)

        val GETHEADERS_STR = ba(12, "getheaders")
        val HEADERS_STR = ba(12, "headers")

        val SENDHEADERS_STR = ba(12, "sendheaders")  // Say that we want block headers, not INV
        val REJECT_STR = ba(12, "reject")
        val MEMPOOL_STR = ba(12, "mempool")

        val REQ_TXVAL_STR = ba(12, "req-txval")
        val RES_TXVAL_STR = ba(12, "res-txval")

        val PROTOCONF_STR = ba(12, "protoconf")

        val INVALID_STR = "".toByteArray().copyOf(12)

        fun cvt(data: ByteArray): NetMsgType
        {
            // version msgs
            if (data contentEquals VERSION_STR) return VERSION
            else if (data contentEquals VERACK_STR) return VERACK
            else if (data contentEquals BUVERSION_STR) return BUVERSION
            else if (data contentEquals BUVERACK_STR) return BUVERACK
            else if (data contentEquals XVERSION_STR) return XVERSION
            else if (data contentEquals XUPDATE_STR) return XUPDATE
            else if (data contentEquals SENDCMPCT_STR) return SENDCMPCT

            // object msgs
            else if (data contentEquals ADDR_STR) return ADDR
            else if (data contentEquals INV_STR) return INV
            else if (data contentEquals TX_STR) return TX
            else if (data contentEquals GETADDR_STR) return GETADDR
            else if (data contentEquals GETDATA_STR) return GETDATA
            else if (data contentEquals PING_STR) return PING
            else if (data contentEquals PONG_STR) return PONG
            else if (data contentEquals GETHEADERS_STR) return GETHEADERS
            else if (data contentEquals HEADERS_STR) return HEADERS
            else if (data contentEquals SENDHEADERS_STR) return SENDHEADERS
            else if (data contentEquals MERKLEBLOCK_STR) return MERKLEBLOCK
            else if (data contentEquals BLOCK_STR) return BLOCK

            // filter msgs
            else if (data contentEquals FILTERLOAD_STR) return FILTERLOAD
            else if (data contentEquals FILTERCLEAR_STR) return FILTERCLEAR
            else if (data contentEquals FILTERADD_STR) return FILTERADD
            else if (data contentEquals FEEFILTER_STR) return FEEFILTER

            // Misc
            else if (data contentEquals NOTFOUND_STR) return NOTFOUND
            else if (data contentEquals REJECT_STR) return REJECT
            else if (data contentEquals MEMPOOL_STR) return MEMPOOL
            else if (data contentEquals REQ_TXVAL_STR) return REQ_TXVAL
            else if (data contentEquals RES_TXVAL_STR) return RES_TXVAL

            // SV
            else if (data contentEquals PROTOCONF_STR) return PROTOCONF
            else throw NetMsgTypeCodingError("converting network bytes " + data.decodeUtf8() + " to enum")
        }

        fun cvt(data: NetMsgType): ByteArray
        {
            return when (data)
            {
                // version msgs
                VERSION -> VERSION_STR
                VERACK -> VERACK_STR
                BUVERSION -> BUVERSION_STR
                BUVERACK -> BUVERACK_STR
                XVERSION -> XVERSION_STR
                XUPDATE -> XUPDATE_STR
                SENDCMPCT -> SENDCMPCT_STR

                // object msgs
                ADDR -> ADDR_STR
                INV -> INV_STR
                TX -> TX_STR
                GETADDR -> GETADDR_STR
                PING -> PING_STR
                PONG -> PONG_STR
                GETHEADERS -> GETHEADERS_STR
                HEADERS -> HEADERS_STR
                SENDHEADERS -> SENDHEADERS_STR
                GETDATA -> GETDATA_STR
                NOTFOUND -> NOTFOUND_STR

                MERKLEBLOCK -> MERKLEBLOCK_STR
                BLOCK -> BLOCK_STR

                // filter msgs
                FILTERLOAD -> FILTERLOAD_STR
                FILTERADD -> FILTERADD_STR
                FILTERCLEAR -> FILTERCLEAR_STR
                FEEFILTER -> FEEFILTER_STR

                // misc
                REJECT -> REJECT_STR
                MEMPOOL -> MEMPOOL_STR
                RES_TXVAL -> RES_TXVAL_STR
                REQ_TXVAL -> REQ_TXVAL_STR

                // SV
                PROTOCONF -> PROTOCONF_STR
                else ->
                {
                    throw NetMsgTypeCodingError("converting enum " + data.name + " to network bytes"); /* INVALID_STR */
                }
            }
        }

    }

}


// Helper objects for deserializing TxValidate message
@Serializable
data class TxValMetadata(val size: Long? = null, val txfee:Long? = null, val txfeeneeded: Long? = null)
@Serializable
data class TxValInputsFlags(val isValid: Boolean, val inputs: List<TxValInputs>)
@Serializable
data class TxValInputs(val isValid: Boolean, val metadata: TxValInputMetadata, val errors: List<String>)
@Serializable
data class TxValInputMetadata(val amount: String, val constraint: String, val constraintType: String, val outpoint: String, val satisfier: String, val sequence: String, val spentAmount: String)
@Serializable
data class TxValReply(val txid: String, val isValid: Boolean, val isMineable: Boolean, val isFutureMineable: Boolean, val isStandard: Boolean,
  val metadata: TxValMetadata, val errors: List<String>, val inputs_flags: TxValInputsFlags, val inputs_mandatoryFlags: TxValInputsFlags
)

fun parseTxValReply(s:String):TxValReply
{
    val json = kotlinx.serialization.json.Json { encodeDefaults = true; ignoreUnknownKeys = true }
    val ret = json.decodeFromString(TxValReply.serializer(), s)
    return ret
}


class NodeServices
{
    companion object
    {
        val NODE_NETWORK = (1 shl 0)
        val NODE_GETUTXO = (1 shl 1)
        val NODE_BLOOM = (1 shl 2)
        val NODE_WITNESS = (1 shl 3)
        val NODE_XTHIN = (1 shl 4)
        val NODE_BITCOIN_CASH = (1 shl 5)
    }
}

class Inv(var type: Types = Types.ILLEGAL, var id: Hash256 = Hash256.ZERO) : BCHserializable
{
    enum class Types(val v: Int)  // Ordinals must match specification. See protocol.h MSG_ enum
    {
        ILLEGAL(0),
        TX(1),
        BLOCK(2),
        FILTERED_BLOCK(3),
        CMPCT_BLOCK(4),
        XTHIN_BLOCK(5),
        GRAPHENE_BLOCK(6);

        companion object
        {
            private val values = values()
            fun get(value: Int) = values.first { it.v == value }  // throws exception if the int is not in the enum
        }
    }

    // Serialization
    constructor(buf: BCHserialized) : this()
    {
        val v = buf.deint32()
        type = Types.get(v)
        id = Hash256(buf)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized = BCHserialized.int32(type.ordinal, format) + id
    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        type = Types.get(stream.deint32())
        id = Hash256(stream)
        return stream
    }
}

class ReceivedMessage(cmd: ByteArray, dat: ByteArray)
{
    val command = cmd
    val data = dat
}

@cli(Display.Simple, "A connection to another node over the blockchain peer-to-peer network")
class P2pClient(
  @cli(Display.User, "Which blockchain is being tracked")
  val chainSelector: ChainSelector,
  @cli(Display.Simple, "IP address or FQDN of the peer node")
  val name: String,
  @cli(Display.Simple, "IP port connected to")
  val port: Int,
  @cli(Display.User, "Name of this connection as shown in the log files")
  val logName: String,
  val coScope: CoroutineScope,
    // val cocond: CoCond<Boolean>
)
{
    companion object
    {
        const val AWAIT_TIME = 100.toLong()
        const val CONNECT_TIMEOUT = 20000  //* How long to attempt to connect in milliseconds
        const val INITIAL_MESSAGE_XCHG_TIMEOUT = 50000 //* how long the initial message exchange may take in milliseconds
    }

    @kotlin.time.ExperimentalTime
    override fun toString(): String
    {
        val aliveTime = (millinow() - beginTime).let { if (it / 1000 < 120) (it/1000).toString()+"sec" else (it/60000).toString()+"min" }
        val elapsedRecv = (millinow() - lastReceiveTime)/1000
        val lastTime = if (elapsedRecv < 120) elapsedRecv.toString()+"sec" else (elapsedRecv/60).toString()+"min"
        return name + ":" + port.toString() + " (alive " + aliveTime + " sent " + bytesSent.toString(Units.BestBytes) + " received " + bytesReceived.toString(Units.BestBytes) + " last " + lastTime + " ago)"
    }

    /** When we first attempted to connect to this peer */
    @cli(Display.User, "When the connection first was opened")
    val beginTime: Long = millinow()  // connection happens just after object creation, so marking now is good enough

    /** When we last heard from this peer */

    @cli(Display.User, "When we last heard from this peer")
    var lastReceiveTime: Long = millinow()  // connection happens just after object creation, so marking now is good enough

    var lastPingCount = 0L
    var pingOutstanding = false

    @cli(Display.Simple, "Number of bytes sent to this peer")
    var bytesSent = 0L
    @cli(Display.Simple, "Number of bytes received from this peer")
    var bytesReceived = 0L

    val atomicWriter = Gate()
    var sockBuilder = aSocket(SelectorManager()).tcp() // (ipp, port)
    var sock: Socket? = null

    var inp: ByteReadChannel? = null
    var out: ByteWriteChannel? = null

    var localServices: Long = (NodeServices.NODE_BITCOIN_CASH).toLong()
    @Volatile
    var closed = false

    /** keep track of how often we got a message drop.  If its too many times, drop the node */
    var misbehavingQuietCount = 0
    val MISBEHAVING_QUIET_LIMIT = 25
    var misbehavingBadMessageCount = 0
    val MISBEHAVING_BAD_MESSAGE_LIMIT = 10
    var misbehavingReason: BanReason = BanReason.NOT_BANNED

    var MESSAGE_HEADER_SIZE = 24
    var receivedData = BCHserialized(SerializationType.NETWORK)

    val curMsgCommand = ByteArray(12)
    var curMsgLen: Long = -1
    var curMsgChecksum: Long = 0

    // For messages that pass a nonce to match requests with replies
    var curNonce: Long = 0

    // Version message stuff
    @Volatile
    var version: Int = 0
    @cli(Display.Simple, "Version string")
    var subversion: String = ""
    @cli(Display.Simple, "Supported services ")
    var services: Long = 0
    @cli(Display.Simple, "Height of peer blockchain")
    var currentHeight: Int = 0
    var timeOffset: Long = 0

    var bloomCount: Int = -1

    // We can't really combine these into a dictionary because each has a unique call signature
    var onVersionCallback: MutableList<(P2pClient) -> Unit> = mutableListOf()

    // Callback returns true if this cb should be removed from the callback list.
    // Wants to know about any headers that arrive
    var onHeadersCallback: MutableList<(MutableList<out iBlockHeader>, P2pClient) -> Boolean> = mutableListOf()

    // For the specific requester
    @Volatile
    var headerCallback: ((MutableList<out iBlockHeader>, P2pClient) -> Boolean)? = null
    val exclusiveHeaders = Gate()  // Only do a single headers request at a time due to P2P protocol limitations

    // Application installed handlers for specific messages
    val onInvCallback: MutableList<(MutableList<Inv>, P2pClient) -> Unit> = mutableListOf()
    val onTxCallback: MutableList<(MutableList<out iTransaction>, P2pClient) -> Unit> = mutableListOf()
    val onBlockCallback: MutableList<(iBlock, P2pClient) -> Unit> = mutableListOf()
    val onMerkleBlockCallback: MutableList<(iMerkleBlock, P2pClient) -> Unit> = mutableListOf()
    val onAddrCallback: MutableList<(MutableList<PossibleConnection>, P2pClient) -> Unit> = mutableListOf()

    // Since the txValidation message passes a nonce, we can install a handler for a specific response
    // Note application must time out and clean up a request that ignored
    val txValidation: MutableMap<Long, suspend (String) -> Unit> = mutableMapOf()

    @Volatile
    var incoming = mutableMapOf<NetMsgType, MutableList<BCHserialized>>()

    var wantsTxRelayed = true

    @cli(Display.Simple, "check that this connection is active")
    fun isAlive(): Boolean
    {
        if (closed) return false
        val s = sock
        if (s == null) return true // we call a connection that has not initialized yet alive
        if (s.isClosed) return false
        if (inp?.isClosedForRead == true) return false
        if (out?.isClosedForWrite == true) return false
        return true
    }

    @cli(Display.User, "Establish a connection")
    fun connect(timeout: Int = CONNECT_TIMEOUT): P2pClient
    {
        try
        {
            runBlocking {
                withTimeout(timeout.toLong())
                {
                    sock = sockBuilder.connect(name, port, {
                        this.noDelay = true
                        this.receiveBufferSize = 2 * 1024 * 1024
                        this.sendBufferSize = 2 * 1024 * 1024
                    }) // NOTE: timeout is not included as in libbitcoincashkotlin, timeout)
                    inp = sock!!.openReadChannel()
                    out = sock!!.openWriteChannel()
                }
                LogIt.info(sourceLoc() + " " + logName + ": Connected. Sending version to ${name}:${port}")
                sendVersion()

            }
        }
        catch (e: P2PDisconnectedException)
        {
            LogIt.warning(sourceLoc() + " " + logName + ": CLOSE -- Instant disconnect, likely banned from ${name}:${port}")
            close()
            millisleep(1000U)
        }
        /*
        catch (e: ConnectException)
        {
            LogIt.warning(sourceLoc() + " " + logName + ": CLOSE -- Connection not established (${e.message ?: ""}, likely ${name}:${port} has no server")
            close()
            millisleep(1000U)
            throw e
        }
         */
        catch (e: SocketTimeoutException)
        {
            LogIt.warning(sourceLoc() + " " + logName + ": CLOSE -- Instant socket disconnect, likely banned from ${name}:${port}")
            close()
            millisleep(1000U)
        }
        catch (e: IOException)  // Before Android 4.3
        {
            LogIt.warning(sourceLoc() + " " + logName + ": CLOSE -- I/O exception connecting to ${name}:${port}")
            close()
            millisleep(1000U)
        }
        catch (e: TimeoutCancellationException)  // do not close because the open never happened
        {
            LogIt.warning(sourceLoc() + " " + logName + ": CLOSE -- coroutine timeout connecting to ${name}:${port}")
            throw SocketTimeoutException("coroutine timeout connecting to ${name}:${port}")
        }
        catch(e: IllegalStateException)
        {
            LogIt.warning(sourceLoc() + " " + logName + ": CLOSE -- cannot connect to ${name}:${port}")
            close()
            millisleep(1000U)
        }
        catch (e: Exception)  // unknown problem, move on to another node
        {
            handleThreadException(e, "Could not connect to node")
            LogIt.warning(sourceLoc() + " " + logName + ": CLOSE -- Unknown exception connecting to ${name}:${port}")
            close()
            millisleep(1000U)
        }
        return this
    }

    @cli(Display.User, "Close the connection")
    fun close()
    {
        if (closed == false)
        {
            closed = true
            LogIt.warning(sourceLoc() + " " + logName + ": CLOSE -- Disconnect, close() was called")

            try
            {
                inp?.cancel(null)
            }
            catch (_: IOException)
            {
            }  // possibly already shutdown
            try
            {
                out?.close(null)
            }
            catch (e: IOException)
            {
            }
            try
            {
                sock?.close()
                sock = null
            }
            catch (_: IOException)
            {
            }
        }
    }

    /** clears all incoming messages */
    @cli(Display.User, "Clear all incoming messages")
    fun clear()
    {
        for (v in incoming.values)
        {
            v.clear()
        }
    }

    /** Provide feedback that this node is not responding to our requests */
    fun misbehavingQuiet(howBad: Int = 1)
    {
        misbehavingQuietCount += howBad

        if (misbehavingQuietCount >= MISBEHAVING_QUIET_LIMIT)
        {
            LogIt.info(sourceLoc() + " " + logName + ": CLOSE -- Note, closing nonresponsive connection")
            close()
        }
    }

    /** Provide feedback that this node is not responding to our requests */
    fun misbehavingBadMessage(howBad: Int = 1)
    {
        misbehavingBadMessageCount += howBad

        if (misbehavingBadMessageCount >= MISBEHAVING_BAD_MESSAGE_LIMIT)
        {
            LogIt.info(sourceLoc() + " " + logName + ": CLOSE -- Closing connection -- sent bad messages")
            close()
        }
    }

    fun waitForReady()
    {
        var elapsed = 0
        while (version == 0)
        {
            if (closed) throw P2PDisconnectedException(sourceLoc() + " " + logName + ": I closed during initial message exchange")
            else if (sock?.isClosed == true) throw P2PDisconnectedException(sourceLoc() + " " + logName + ": disconnect during initial message exchange")
            else if (elapsed > INITIAL_MESSAGE_XCHG_TIMEOUT)
            {
                throw P2PDisconnectedException(sourceLoc() + " " + logName + ": too slow during initial message exchange")
            }
            millisleep(50U)
            elapsed += 50
        }
        LogIt.info(sourceLoc() + " " + logName + ": Connection ready")

    }

    /*
    suspend fun coWaitForReady()
    {
        if (cocond.yield({ version != 0 || sock.isClosed }, INITIAL_MESSAGE_XCHG_TIMEOUT) == null)
        {
            if (version == 0)
                throw P2PDisconnectedException(sourceLoc() + " " + logName + ": too slow during initial message exchange")
        }

        if (sock.isClosed)
        {
            throw P2PDisconnectedException(sourceLoc() + " " + logName + ": disconnect during initial message exchange")
        }
    }

     */

    /*
    suspend fun waitFor(msg: NetMsgType): BCHserialized
    {
        cocond.yield({ !((incoming[msg] == null || incoming[msg]?.count() == 0) && !(sock?.isClosed == true)) })
        val lst = incoming[msg]
        if (lst != null && lst.count() != 0)
        {
            val msgbytes = lst.get(0)
            lst.removeAt(0)
            return msgbytes
        }

        assert(sock?.isClosed == true)  // only way to skip the above return
        throw P2PDisconnectedException("disconnect while waiting for message")
    }
     */

    // Helper functions to send various messages
    @cli(Display.Dev, "Send a transaction message")
    fun sendTx(tx: ByteArray)
    {
        send(NetMsgType.TX, BCHserialized(tx, SerializationType.NETWORK))
    }

    /** Send a GETADDR request message */
    @cli(Display.Dev, "Ask this peer for other peers it knows about")
    fun sendGetAddr()
    {
        send(NetMsgType.GETADDR)
    }

    // Send the GETDATA message
    fun sendGetData(invs: Collection<Inv>)
    {
        if (invs.size > 0)
        {
            val msg = BCHserialized.list(invs, SerializationType.NETWORK)
            send(NetMsgType.GETDATA, msg)
        }
    }

    fun sendGetHeaders(loc: BlockLocator, stopAt: Hash256)
    {
        var msg: BCHserialized = BCHserialized(SerializationType.NETWORK) + loc + stopAt
        send(NetMsgType.GETHEADERS, msg)
    }

    fun sendTxVal(tx: iTransaction, nonce: Long? = null, handler: suspend (String) -> Unit)
    {
        val n = atomicWriter.lock { nonce ?: curNonce++ }
        var msg: BCHserialized = BCHserialized(SerializationType.NETWORK).addUint64(n).add(tx)
        txValidation[n] = handler
        send(NetMsgType.REQ_TXVAL, msg)
    }

    /** Async get block headers.
     * This function only allows one outstanding getheaders per node due to protocol issues.
     * Call @cleanupExclusiveHeaders() if you stop waiting (if cb is never called).
     * Its Ok to unnecessarily call cleanupExclusiveHeaders */
    fun getHeaders(loc: BlockLocator, stopAt: Hash256, cb: (MutableList<out iBlockHeader>, P2pClient) -> Boolean)
    {
        // Waiting for a prior request from someone else to finish up.
        // If the request if from "me" (same callback) then allow it to allow retries
        exclusiveHeaders.waitfor({ (headerCallback == null) || (headerCallback == cb)}) {
            headerCallback = cb
            sendGetHeaders(loc, stopAt)
        }
    }

    /** If you issue a getHeaders() but then give up waiting, call this function */
    fun cleanupExclusiveHeaders(cb: (MutableList<iBlockHeader>, P2pClient) -> Boolean)
    {
        synchronized(exclusiveHeaders)
        {
            if (headerCallback == cb)
            {
                headerCallback = null
                exclusiveHeaders.wake()
            }
        }
    }

    // Send a bloom filter that is already serialized as a byte array
    fun sendBloomFilter(filter: ByteArray, inBloomCount: Int = -1)
    {
        if ((inBloomCount == -1) || (bloomCount != inBloomCount))
        {
            LogIt.info(sourceLoc() + " " + logName +": Sent bloom filter")
            var msg = BCHserialized(filter, SerializationType.NETWORK)
            send(NetMsgType.FILTERLOAD, msg)
            sendPing()  // this should by a sync point on Nexa and BU clients
            millisleep(250U)  // Because the filter has GOT to be installed before we do merkle requests
            if (inBloomCount != -1) bloomCount = inBloomCount
        }
    }

    fun sendVersion()
    {
        // BUIP005 add our special subversion string
        // PushMessage(NetMsgType::VERSION, PROTOCOL_VERSION, nLocalServices, nTime, addrYou, addrMe, nLocalHostNonce,
        //        FormatSubVersion(CLIENT_NAME, CLIENT_VERSION, BUComments), nBestHeight,
        //        !GetBoolArg("-blocksonly", DEFAULT_BLOCKSONLY));

        var remoteServices: Long = NodeServices.NODE_NETWORK.toLong()

        val s = sock
        if (s == null) return

        val myAddrs = resolveDomain((s.localAddress as InetSocketAddress).component1(), port)
        val ipbytes = if (myAddrs.size > 0) myAddrs.random().copyOf(16) else ByteArray(16, {0})

        val youAddrs = resolveDomain((s.remoteAddress as InetSocketAddress).component1())

        val addrYouBytes = if (youAddrs.size > 0) youAddrs.random().copyOf(16) else ByteArray(16, {0})

        // some const values to send
        val nonce = 1.toLong()
        val sendBlockHeight = 0.toLong()
        val relayTx = false // We will turn it on when we install a filter

        var now:Long = millinow()
        assert(ipbytes.size == 16)  // the protocol requires 16 bytes for these addresses
        assert(addrYouBytes.size == 16)
        var msg =
          BCHserialized(SerializationType.NETWORK).addInt32(PROTOCOL_VERSION).addUint64(localServices).addUint64(now).addExactBytes(ipbytes).addUint64( localServices).addUint16(port).addUint64(remoteServices).addExactBytes(addrYouBytes)
              .addUint16(port).addUint64(nonce).add(CLIENT_SUBVERSION).addUint32(sendBlockHeight).add(relayTx)

        send(NetMsgType.VERSION, msg)
    }

    fun receive(): Boolean
    {
        val netId = BlockchainNetMagic[chainSelector]!!
        var didSomething = false
        val input = inp ?: return false  // input channel hasn't been opened up yet

        val available = try
        {
            input.availableForRead
        }
        catch (e: IOException)
        {
            // Note: Although the available() documentation (https://docs.oracle.com/javase/8/docs/api/java/io/InputStream.html#available--)
            // implies that this exception is only thrown on close  it appears to be thrown at undetermined non-fatal times.
            // So model the exception as if no data is currently available.
            LogIt.warning(sourceLoc() + ": IO exception getting available bytes on socket")
            millisleep(100U)
            return false
        }

        var dataBuf = ByteArray(available)
        var amtRead = try
        {
            //inp?.readAvailable(buffer)
            runBlocking { input.readAvailable(dataBuf,0,available) }
        }
        catch (e: IOException)
        {
            LogIt.info(sourceLoc() + ": CLOSE -- Disconnecting, IO exception reading bytes on socket")
            close()
            return false
        }
        catch (e: CancellationException)
        {
            LogIt.info(sourceLoc() + ": CLOSE -- Disconnecting, Cancellation reading bytes on socket channel")
            close()
            return false
        }
        if (amtRead == -1) // end of file (connection closed)
        {
            LogIt.info(sourceLoc() + ": CLOSE -- Disconnecting, socket at end")
            close()
            return false
        }

        bytesReceived += amtRead

        if (amtRead > 0)
        {
            receivedData.addExactBytes(dataBuf, amtRead)
            lastReceiveTime = millinow()  // We got something to reset the last receive time
        }
        // LogIt.info(name + ":" + port.toString() + " Received " + amtRead + " Waiting for " + curMsgLen + " Have " + receivedData.size) // + "(" + curMsgCommand.toString(Charset.defaultCharset()) + ":" + (receivedData.availableBytes() + data.size) + "/" + curMsgLen + " bytes: " + data.ToHex())

        val msgStart = ByteArray(4)
        while (true)
        {
            if ((curMsgLen >= 0L) and (curMsgLen <= receivedData.availableBytes()))
            {
                var msg = receivedData.debytes(curMsgLen)
                receivedData.lazyReset()
                // todo validate checksum
                //lock.withLock
                run {
                    try
                    {
                        val msgtype = NetMsgType.cvt(curMsgCommand)
                        if (doIHandleThisMessage(msgtype))
                        {
                            var list = incoming.getOrPut(msgtype, { -> mutableListOf() })
                            // LogIt.info(sourceLoc() + " " + logName + ": received message of type " + curMsgCommand.dropLastWhile({ it == 0.toByte() }).toByteArray().decodeUtf8() + " hex: " + msg.toHex())
                            list.add(BCHserialized.wrap(msg, SerializationType.NETWORK))
                        }
                        if (msgtype == NetMsgType.PONG)  // Handle this easy msg inline
                        {
                            pingOutstanding = false
                        }
                    }
                    catch (e: NetMsgTypeCodingError)
                    {
                        LogIt.warning(sourceLoc() + ": " + e.toString())
                        if (DEBUG) throw e  // If in debug mode let's call attention to this bad message, otherwise ignore it
                    }
                }
                curMsgLen = -1
                curMsgCommand.fill(0)
                didSomething = true

            }
            else if ((curMsgLen == -1L) and (receivedData.availableBytes() >= MESSAGE_HEADER_SIZE))
            {
                receivedData.debytes(msgStart,4)
                if (!msgStart.contentEquals(netId))
                {
                    LogIt.info(sourceLoc() + " " + logName + ": CLOSE -- Invalid message start. Disconnecting")
                    close()
                    return false
                }
                receivedData.debytes(curMsgCommand, 12)
                curMsgLen = receivedData.deuint32()
                curMsgChecksum = receivedData.deuint32()
                didSomething = true
                // LogIt.info(name + ":" + port.toString() + " Initiate msg " + curMsgCommand.toString(Charset.defaultCharset()) + " " + (receivedData.availableBytes()) + "/" + curMsgLen)
            }
            else break  // nothing to do so no need to loop
        }

        return didSomething
    }

    fun processVersionMessage(msg: BCHserialized)
    {
        var curTime = millinow() / 1000
        val ver = msg.deint32()
        services = msg.deuint64()
        var time = msg.deuint64()
        timeOffset = curTime - time

        // AddrYou
        var services2 = msg.deuint64()
        var ip = msg.debytes(16)
        var port = msg.deuint16()  // TODO ntohs
        LogIt.info(sourceLoc() + " " + logName + ": received version info.  Services: " + services2.toString(16) + "  IP: " + ip.toHex() + "  Port: " + port)

        if (msg.availableBytes() > 0)
        {
            // AddrMe
            var services3 = msg.deuint64()
            var ipFrom = msg.debytes(16)
            var portFrom = msg.deuint16()
            var nonce = msg.deuint64()
            LogIt.info(sourceLoc() + " " + logName + ": my reflected info. " + services3.toString(16) + "  IP: " + ipFrom.toHex() + "  Port: " + portFrom + "  Nonce: " + nonce)
        }
        if (msg.availableBytes() > 0)
        {
            subversion = msg.deString()
        }
        if (msg.availableBytes() > 0)
        {
            currentHeight = msg.deint32()
        }
        if (msg.availableBytes() > 0)
        {
            wantsTxRelayed = msg.deboolean()
        }



        version = ver  // set this last because blocking Ready depends on it
        // unused now: cocond.wake(true)

        for (fn in onVersionCallback)
            fn(this)
    }

    fun processHeadersMessage(msg: BCHserialized)
    {
        val hdrs: MutableList<iBlockHeader> = msg.delist({ strm -> blockHeaderFor(chainSelector,strm) })

        synchronized(exclusiveHeaders)
        {
            val hdrs2 = hdrs.toMutableList()  // Make a copy to avoid concurrent mod exception in this callback and the next
            headerCallback?.invoke(hdrs2, this)
            headerCallback = null
            exclusiveHeaders.wake()
        }

        for (fn in onHeadersCallback)
        {
            fn(hdrs, this@P2pClient)
        }
        hdrs.clear()
    }

    @Suppress("UNUSED_PARAMETER")
    fun processGetDataMessage(msg: BCHserialized)
    {
        // nothing to do, this code doesn't implement the server side yet or probably ever
    }

    fun processInvMessage(msg: BCHserialized)
    {
        try
        {
            val invs: MutableList<Inv> = msg.delist({ strm -> Inv(strm) })
            // LogIt.info(sourceLoc() + " " + name + ": Received INV")
            for (fn in onInvCallback) fn(invs, this@P2pClient)
            invs.clear()
        }
        catch(e: NoSuchElementException)
        {
            LogIt.warning(sourceLoc() + " " + name + ": unknown INV element")
        }
    }

    fun processTxMessage(msg: BCHserialized)
    {
        val tx: iTransaction = txFor(chainSelector, msg)
        // LogIt.info(sourceLoc() + " " + name + ": TX arrived ${tx.idem.toHex()}")
        val txl = mutableListOf(tx)
        //for (fn in onTxCallback) coScope.launch { fn(txl, this@P2pClient) }
        for (fn in onTxCallback)
        {
            fn(txl, this@P2pClient)
        }
        txl.clear()
    }

    fun processBlockMessage(msg: BCHserialized)
    {
        val blk: NexaBlock = NexaBlock(chainSelector, msg)
        //for (fn in onBlockCallback) coScope.launch { fn(blk, this@P2pClient) }
        for (fn in onBlockCallback)
        {
            fn(blk, this@P2pClient)
        }
    }

    fun processMerkleBlockMessage(msg: BCHserialized)
    {
        val blk = merkleBlockFor(chainSelector, msg)
        // LogIt.info(sourceLoc() + ": Processing merkle block ${blk.height}:${blk.hash.toHex()} within ${onMerkleBlockCallback.size} callbacks")

        // These MUST be executed synchronously to ensure that the merkle block's state is ready to go before the p2p code processes any subsequent
        // messages (which may be merkle block transactions)
        for (fn in onMerkleBlockCallback) fn(blk, this@P2pClient)
    }

    fun processAddrMessage(msg: BCHserialized)
    {
        val netPeers: MutableList<PossibleConnection> = msg.delist({ strm -> PossibleConnection(strm) });
        LogIt.info(sourceLoc() + ": Peer addresses received: " + netPeers.size)
        for (fn in onAddrCallback)
        {
            fn(netPeers, this@P2pClient)
        }
        netPeers.clear()
    }

    fun processTxValResponse(msg: BCHserialized)
    {
        val nonce: Long = msg.deuint64()
        // LogIt.info(sourceLoc() + ": tx validation response received: " + nonce)
        val requestCallback = txValidation[nonce]
        if (requestCallback != null)
        {
            txValidation.remove(nonce)
            val result = msg.deString()
            coScope.launch { requestCallback(result) }
        }
    }


    suspend fun coProcessForever()
    {
        LogIt.info( logName + ": processing forever (in a coroutine)")
        while (isAlive())
        {
            if (!process()) delay(20)
        }
    }

    fun processForever()
    {
        LogIt.info( logName + ": processing forever (in a thread)")
        while (isAlive())
        {
            if (!process()) millisleep(20U)
            // LogIt.info( logName + ": process loop")
        }
    }

    // Return true for all messages that I handle
    fun doIHandleThisMessage(msgType: NetMsgType): Boolean
    {
        if ((msgType == NetMsgType.VERSION) || (msgType == NetMsgType.BUVERSION) ||
          (msgType == NetMsgType.PING) || (msgType == NetMsgType.ADDR) ||
          (msgType == NetMsgType.PROTOCONF) || (msgType == NetMsgType.HEADERS) ||
          (msgType == NetMsgType.INV) || (msgType == NetMsgType.BLOCK) ||
          (msgType == NetMsgType.MERKLEBLOCK) || (msgType == NetMsgType.NOTFOUND) ||
          (msgType == NetMsgType.REJECT) || (msgType == NetMsgType.TX)
          || (msgType == NetMsgType.RES_TXVAL))
            return true
        return false
    }

    // Process a message
    fun process(): Boolean
    {
        var didSomething = false
        try
        {
            didSomething = receive()

            // Note if you add a handler for a message here you must add it to doIHandleThisMessage()!

            // If I got a version message, autorespond with VERACK
            var list = incoming[NetMsgType.VERSION]
            list?.let {
                if (it.size > 0)
                {
                    send(NetMsgType.VERACK)
                    processVersionMessage(it[0])
                    it.clear() // so I don't verack multiple times per version
                    didSomething = true
                    sendGetAddr()  // for any new connection, let's get who it knows about
                }
            }

            list = incoming[NetMsgType.BUVERSION]
            list?.let {
                if (it.size > 0)
                {
                    send(NetMsgType.BUVERACK, BCHserialized.uint16(0, SerializationType.NETWORK))  // optionally send your port
                    it.clear() // so I only buverack once
                    didSomething = true
                }
            }

            list = incoming[NetMsgType.PING]
            list?.let({
                while (it.size > 0)
                {
                    var msg = it[0]
                    send(NetMsgType.PONG, msg)  // send the nonce back
                    it.removeAt(0)
                    didSomething = true
                }
            })

            list = incoming[NetMsgType.ADDR]
            list?.let {
                while (it.size > 0)
                {
                    var msg = it.removeAt(0)
                    processAddrMessage(msg)
                    didSomething = true
                }


            }

            list = incoming[NetMsgType.RES_TXVAL]
            list?.let {
                while (it.size > 0)
                {
                    var msg = it.removeAt(0)
                    processTxValResponse(msg)
                    didSomething = true
                }


            }

            list = incoming[NetMsgType.PROTOCONF]
            list?.let {
                if (it.size > 0)
                {
                    LogIt.info(sourceLoc() + ": CLOSE -- Disconnecting: connected to SV node")
                    misbehavingReason = BanReason.INCORRECT_BLOCKCHAIN
                    incoming.clear()
                    close()
                }


            }
            // If someone has installed a headers message handler, then consume headers messages
            if (true) // onHeadersCallback.size > 0)
            {
                var hdrlst = incoming[NetMsgType.HEADERS]
                hdrlst?.let {
                    while (it.size > 0)
                    {
                        var msg = it.removeAt(0)
                        processHeadersMessage(msg)
                        didSomething = true
                    }
                }
            }
            // If someone has installed an inv message handler, then consume headers messages
            if (onInvCallback.size > 0)
            {
                var hdrlst = incoming[NetMsgType.INV]
                hdrlst?.let {
                    while (it.size > 0)
                    {
                        var msg = it.removeAt(0)
                        processInvMessage(msg)
                        didSomething = true
                    }
                }
            }

            // If someone has installed a BLOCK message handler, then consume incoming transactions
            if (onBlockCallback.size > 0)
            {
                var hdrlst = incoming[NetMsgType.BLOCK]
                hdrlst?.let {
                    while (it.size > 0)
                    {
                        // LogIt.info("p2p got block")
                        var msg = it.removeAt(0)
                        processBlockMessage(msg)
                        didSomething = true
                    }
                }
            }
            // If someone has installed a MERKLEBLOCK message handler, then consume incoming transactions
            if (onMerkleBlockCallback.size > 0)
            {
                val hdrlst = incoming[NetMsgType.MERKLEBLOCK]
                while ((hdrlst != null) && (hdrlst.size > 0))  // The purpose of this while loop is to process every merkleblock before processing any transactions
                {
                    var msg = hdrlst.removeAt(0)
                    // LogIt.info(sourceLoc() + ": Process Merkle block")
                    processMerkleBlockMessage(msg)
                    didSomething = true
                }
            }

            // Someone could be lying to us about a block or tx being NOTFOUND so drop these messages for now.  However, in the future it may make sense to handle multiple notfound from different sources
            val nflst = incoming[NetMsgType.NOTFOUND]
            if (nflst != null) for (nf in nflst)
            {
                LogIt.warning(sourceLoc() + ": received NOTFOUND message. Dropping it")
            }
            incoming[NetMsgType.NOTFOUND]?.clear()

            val errlst = incoming[NetMsgType.REJECT]
            while ((errlst != null) && (errlst.size > 0))
            {
                // val MAX_REJECT_MESSAGE_LENGTH: Long = 111
                var msg = errlst.removeAt(0)
                val rejectedCommand = msg.deString()
                val rejectCode = msg.deuint8()
                val rejectStr = msg.deString()
                LogIt.warning(sourceLoc() + logName + ": Received reject of ${rejectedCommand}, code: ${rejectCode}, reason: ${rejectStr}")
                didSomething = true
            }

            // If someone has installed a TX message handler, then consume incoming transactions
            // Note that the tx handler must happen after the merkle block handler so that any transactions that arrive after a merkle block arrives are processed in that order
            if (onTxCallback.size > 0)
            {
                var hdrlst = incoming[NetMsgType.TX]
                hdrlst?.let {
                    while (it.size > 0)
                    {
                        var msg = it.removeAt(0)
                        // LogIt.info(sourceLoc() + ": Process TX")
                        processTxMessage(msg)
                        didSomething = true
                    }
                }
            }

            // TODO periodically clean up anything that hasn't been processed
        }
        catch (e: P2PDisconnectedException)
        {
            LogIt.info(sourceLoc() + ": CLOSE -- Disconnecting: " + e.message)
            close()  // once I close my side higher layer software will detect this and reconnect using a different object
        }
        catch (e: CancellationException)  // channel was cancelled
        {
            LogIt.info(sourceLoc() + ": Channel cancelled -- Disconnecting: " + e.message)
            close()
        }

        return didSomething
    }


    /*
    // Ask this node for a list of peers
    fun queryPeers():BCHserialized
    {
        sendPeerRequest()
        var list = incoming.getOrPut(NetMsgType.ADDR, { -> mutableListOf() })
        while (list.size == 0)
        {
            receive()
        }
        return list.removeAt(0)

    }

    fun sendPeerRequest() = send(NetMsgType.GETADDR, BCHserialized())
     */


    fun sendPing()
    {
        lastPingCount += 1
        pingOutstanding = true
        send(NetMsgType.PING, BCHserialized(SerializationType.NETWORK).addUint64(lastPingCount))
    }
    fun sendInv(invs: MutableList<Inv>) = send(NetMsgType.INV, BCHserialized(SerializationType.NETWORK) + invs)

    fun send(command: ByteArray, msg: BCHserialized) = send(command, msg.toByteArray())
    fun send(command: NetMsgType, msg: BCHserialized = BCHserialized(SerializationType.NETWORK)) = send(NetMsgType.cvt(command), msg.toByteArray())


    fun send(command: ByteArray, msg: ByteArray)
    {
        // LogIt.info(sourceLoc() + " " + logName + ": Send " + command.dropLastWhile({ it == 0.toByte() }).toByteArray().toString(Charset.defaultCharset()))

        // example of debugging to trigger on a specific message type
        //if (command.dropLastWhile({ it == 0.toByte() }).toByteArray().toString(Charset.defaultCharset()) == "getheaders")
        //{
        //    LogIt.info("DBG: getheader")
        //}
        var msghash = if (chainSelector == ChainSelector.BCH) libnexa.hash256(msg) else ByteArray(4)
        // without libbitcoincash.so:
        //var msghash = MessageDigest.getInstance("SHA-256").digest(msg)
        //msghash = MessageDigest.getInstance("SHA-256").digest(msghash)

        val msglen = BCHserialized.uint32(msg.size.toLong(), SerializationType.NETWORK).toByteArray()
        /*
        var buf = MutableList<ByteArray>(5,
                { i ->
                    when
                    {
                        i == 0 -> BlockchainNetMagic[chainSelector] ?: BCH_CASH_MAGIC_MAINNET
                        i == 1 -> command
                        i == 2 -> msglen
                        i == 3 -> msghash.sliceArray(IntRange(0, 3))
                        i == 4 -> msg

                        else -> ByteArray(0, { _ -> 0 })
                    }
                }).join()
                */

        val buf = mutableListOf(BlockchainNetMagic[chainSelector] ?: NEXA_NET_ID, command, msglen, msghash.sliceArray(IntRange(0, 3)), msg).join()

        try
        {
            synchronized(atomicWriter)
            {
                val tmp = out
                if (tmp != null)
                {
                    runBlocking { tmp.writeFully(buf, 0, buf.size) }
                    tmp.flush()
                    bytesSent += buf.size
                }
            }
        }
        catch (e: IOException)  // Happens if other side disconnects
        {
            LogIt.warning(sourceLoc() + ": CLOSE -- Disconnecting, other side dropped")
            close()
            throw P2PDisconnectedException("disconnect during initial message exchange")
        }
    }
}

