package org.nexa.libnexakotlin

// Define units for Nexa currency names
const val SAT = 1L
const val NEX = SAT*100L
const val KEX = NEX*1000L
const val MEX = KEX*1000L

/***
 * Identify the cryptocurrency/blockchain.
 */
enum class ChainSelector(val v: Byte)
{
    // Do not change these values; they are serialized
    /** NEXA mainnet blockchain */
    NEXA(1),
    /** NEXA testnet (public testing network) blockchain */
    NEXATESTNET(2),
    /** NEXA regtest (local private network) blockchain */
    NEXAREGTEST(3),
    /** Bitcoin Cash mainnet blockchain */
    BCH(4),
    /** Bitcoin Cash testnet (public testing network) blockchain */
    BCHTESTNET(5),
    /** Bitcoin Cash regtest (local private network) blockchain */
    BCHREGTEST(6);

    companion object
    {
        /** transform a raw byte into a chainselector (used in serialization/deserialization) */
        fun from(v: Byte):ChainSelector = ChainSelectorFromValue(v)
    }

    // Helper functions to determine blockchain functionality

    /** Is this blockchain capable of group tokenization? */
    val hasGroupTokens: Boolean
        get() = (this == NEXA) or (this == NEXATESTNET) or (this == NEXAREGTEST)
    /** Is this blockchain capable of P2SH-style outputs? */
    val hasP2SH: Boolean
        get() = (this == BCH) or (this == BCHTESTNET) or (this == BCHREGTEST)
    /** Is this blockchain capable of P2PKH-style outputs? */
    val hasP2PKH: Boolean
        get() = true
    /** Is this blockchain capable of nexa-style script template outputs? */
    val hasTemplates: Boolean
        get() = (this == NEXA) or (this == NEXATESTNET) or (this == NEXAREGTEST)

    /** Is this a nexa-family blockchain (e.g. mainnet, testnet, or regtest)? */
    val isNexaFamily: Boolean
        get() = (this == NEXA) or (this == NEXATESTNET) or (this == NEXAREGTEST)
    /** Is this a BCH-family blockchain (e.g. mainnet, testnet, or regtest)? */
    val isBchFamily: Boolean
        get() = (this == BCH)  or (this == BCHTESTNET)  or (this == BCHREGTEST)

    /** Returns true if this is any mainnet chain, false if a testnet or regtest chain */
    val isMainNet: Boolean
        get() = (this == BCH) or (this == NEXA)

    /** Is this blockchain block headers compatible with bitcoin? */
    val hasBitcoinLikeHeader: Boolean
        get() = isBchFamily
}

fun ChainSelectorFromValue(v: Byte): ChainSelector
{
    return when (v)
    {
        ChainSelector.BCH.v -> ChainSelector.BCH
        ChainSelector.BCHTESTNET.v -> ChainSelector.BCHTESTNET
        ChainSelector.BCHREGTEST.v -> ChainSelector.BCHREGTEST
        ChainSelector.NEXATESTNET.v -> ChainSelector.NEXATESTNET
        ChainSelector.NEXAREGTEST.v -> ChainSelector.NEXAREGTEST
        ChainSelector.NEXA.v -> ChainSelector.NEXA
        else -> throw UnknownBlockchainException()
    }
}

/** Convert a ChainSelector to its uri and address prefix */
val chainToURI: Map<ChainSelector, String> = mapOf(
  ChainSelector.NEXATESTNET to "nexatest", ChainSelector.NEXAREGTEST to "nexareg", ChainSelector.NEXA to "nexa",
  ChainSelector.BCH to "bitcoincash", ChainSelector.BCHTESTNET to "bchtest", ChainSelector.BCHREGTEST to "bchreg"
)

/** Convert a uri or address prefix to a ChainSelector -- throws exception if not found */
val uriToChain: Map<String, ChainSelector> = chainToURI.invert()

/** Convert a ChainSelector to its approx currency code at 100M units */
val chainToCurrencyCode: Map<ChainSelector, String> = mapOf(
  ChainSelector.NEXATESTNET to "tNEX", ChainSelector.NEXAREGTEST to "rNEX", ChainSelector.NEXA to "NEXA",
  ChainSelector.BCH to "BCH", ChainSelector.BCHTESTNET to "tBCH", ChainSelector.BCHREGTEST to "rBCH"
)

  /** Convert a ChainSelector to its currency code at 100M/1000 units */
val chainToMilliCurrencyCode: Map<ChainSelector, String> = mapOf(
    ChainSelector.NEXATESTNET to "tKEX", ChainSelector.NEXAREGTEST to "rKEX", ChainSelector.NEXA to "KEX",
    ChainSelector.BCH to "mBCH", ChainSelector.BCHTESTNET to "tmBCH", ChainSelector.BCHREGTEST to "rmBCH"
  )


    /** Convert a uri or address prefix to a ChainSelector -- throws exception if not found */
val currencyCodeToChain: Map<String, ChainSelector> = chainToURI.invert()


fun ChainSelectorFromAddress(address: String): ChainSelector
{
    try
    {
        if (address.contains(":"))
        {
            val (chain, _) = address.split(":")
            return uriToChain[chain] ?: throw UnknownBlockchainException()
        }
        else
        {
            if ((address[0] == '1') || (address[0] == '3')) throw UnknownBlockchainException()  // TODO bitcoin address
            // Multiple forks use cash addr address format.  Some of them change how the checksum is calculated, making it very likely that an address
            // will not decode properly for a different blockchain.  User SHOULD use the blockchain prefix to remove all ambiguity, but major services do not.
            if ((address.length > 32) && ((address[0] == 'q') || (address[0] == 'p') || (address[0] == 'n')))  // first start with a very quick filter on things that can't be addresses
            {
                // If multiple blockchains do not change the checksum, best we can do it return the first one we decode -- ChainSelector should order its
                // blockchains based on what's most important to this wallet
                for (chain in ChainSelector.values())
                {
                    try
                    {
                        libnexa.decodeCashAddr(chain, address)
                        return chain
                    }
                    catch (e: Exception)
                    {
                    }
                }

            }
        }
    }
    catch (e: IndexOutOfBoundsException)
    {
        // no colon means missing the blockchain identifier prefix used in bitcoincash
        // TODO handle the case where people forget to use the chain identifier
        // LogIt.info("Bad address, cannot determine blockchain: '$address'")
        throw UnknownBlockchainException()
    }
    throw UnknownBlockchainException()
}