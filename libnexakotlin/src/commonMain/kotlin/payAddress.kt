// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.nexa.libnexakotlin

@cli(Display.Simple, "A means to communicate constraint scripts between people or applications.  You can send coins or tokens to an address.")
data class PayAddress(var blockchain: ChainSelector, var type: PayAddressType, var data: ByteArray) : BCHserializable
{
    constructor(stream: BCHserialized) : this(ChainSelector.NEXA, PayAddressType.P2PUBKEY, ByteArray(0))
    {
        BCHdeserialize(stream)  // Will overwrite the chain selector so any can be used in the default constructor
    }

    @cli(Display.Simple, "Construct from a string address")
    constructor(address: String) : this(ChainSelectorFromAddress(address), PayAddressType.P2PUBKEY, ByteArray(0))
    {
        fromString(address)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        if (format == SerializationType.DISK)
        {
            return BCHserialized(format).add(blockchain.v).add(type.v).addVariableSized(data)
        }
        else if (format == SerializationType.NETWORK)  // in scripts for example, we just place the raw bytes
        {
            throw NotImplementedError()
            // return BCHserialized(format) + exactBytes(data)
        }
        throw NotImplementedError()
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        if (stream.format == SerializationType.DISK)
        {
            blockchain = ChainSelectorFromValue(stream.debyte())
            type = LoadPayAddressType(stream.debyte())
            data = stream.deByteArray()
        }
        else if (stream.format == SerializationType.NETWORK)  // in scripts for example, we just place the raw bytes
        {
            throw NotImplementedError()
            // data = stream.debytes(20)
        }
        return stream
    }

    @cli(Display.Simple, "Load this object from a string address")
    fun fromString(address:String)
    {
        if (address == "") throw PayAddressBlankException("")
        val decoded = libnexa.decodeCashAddr(blockchain, address)
        type = LoadPayAddressType(decoded[0])
        data = decoded.sliceArray(IntRange(1, decoded.size-1))
        /*
        // script template payment text addresses contain the output script serialized as a byte array.  Strip off the serialization for insertion into data.
        // Other stuff in the address (after the first array) can be ignored -- may be app-level data
        if (type == PayAddressType.P2PKT || type == PayAddressType.TEMPLATE)
        {
            val strm = BCHserialized(data, SerializationType.NETWORK)
            data = strm.deByteArray()
        }
         */
    }

    @cli(Display.Simple, "Convert to this address's string representation")
    override fun toString(): String
    {
        if (type == PayAddressType.NONE)
        {
            throw IllegalArgumentException("Invalid address type")
        }
        if (type == PayAddressType.P2PKH || type == PayAddressType.P2SH)
        {
            if (data.size != 20)
            {
                throw IllegalArgumentException("Invalid dataload. Expected 20 bytes in a P2PKH address, got " + data.size)
            }
        }
        if (type.isValid())
            return libnexa.encodeCashAddr(blockchain, type, data)
        else
        {
            throw IllegalArgumentException("Invalid address type ${type.v}")
        }
    }

    /** return the prefix for this address, e.g. "bchreg" in "bchreg:qpvdragqrmvashle90kjjx7hx87aq6xe75jlnlxc9c" */
    @cli(Display.Simple, """return the prefix for this address, e.g. "nexa" in "nexa:qpvdragqrmvashle90kjjx7hx87aq6xe75jlnlxc9c"""")
    val addressUriScheme: String
        get() = chainToURI[blockchain]!!  // !! because its a coding error if this dictionary doesn't contain every ChainSelector enum item

    @cli(Display.Simple, "Create an output script that is constrained to this address")
    fun outputScript(): SatoshiScript
    {
        val script = when (type)
        {
            PayAddressType.NONE -> throw WalletNotSupportedException("Cannot create payment unconstrained by an address")
            PayAddressType.P2PUBKEY -> throw WalletNotSupportedException("Pay to public key outputs not supported")
            PayAddressType.GROUP -> throw WalletNotSupportedException("This denotes a token type, not an address")
            PayAddressType.P2PKH -> SatoshiScript.p2pkh(data, blockchain)
            PayAddressType.P2SH -> SatoshiScript.p2sh(data, blockchain)
            PayAddressType.TEMPLATE, PayAddressType.P2PKT -> SatoshiScript(blockchain, SatoshiScript.Type.TEMPLATE, BCHserialized(data, SerializationType.NETWORK).deByteArray())
            // PayAddressType.P2PKT -> SatoshiScript(blockchain, SatoshiScript.Type.TEMPLATE, BCHserialized(data, SerializationType.NETWORK).deByteArray())
            // unnecessary: else                    -> throw PayAddressException("Payment address not supported")
        }
        return script
    }

    /** synonym for outputScript()
     * @return the script that constrains spending to this address
     */
    @cli(Display.Simple, "Create the constraint script that corresponds to this address")
    fun constraintScript(): SatoshiScript = outputScript()

    @cli(Display.Simple, "Create a grouped constraint script that corresponds to this address, with the passed quantity of tokens")
        /** Create a grouped constraint script that corresponds to this address
         * @param grp Group Identifier
         * @param grpQty Number of tokens
         * @return output/constraint script that constrains [grpQty] tokens of type [grp] with this address
         */
    fun groupedConstraintScript(grp: GroupId, grpQty: Long): SatoshiScript
    {
        val script = when (type)
        {
            PayAddressType.NONE -> throw WalletNotSupportedException("Cannot create payment unconstrained by an address")
            PayAddressType.P2PUBKEY -> throw WalletNotSupportedException("Pay to public key outputs not supported")
            PayAddressType.P2PKH -> throw WalletNotSupportedException("grouped P2PKH not supported")
            PayAddressType.P2SH -> throw WalletNotSupportedException("grouped P2SH not supported")
            PayAddressType.GROUP -> throw WalletNotSupportedException("This denotes a token type, not an address")
            PayAddressType.TEMPLATE, PayAddressType.P2PKT -> {
                // Pull this apart and recreate with a different group and group amount
                val orig = SatoshiScript(blockchain, BCHserialized(data, SerializationType.NETWORK))
                orig.type = SatoshiScript.Type.TEMPLATE
                val pt = orig.parseTemplate(0)
                if (pt == null) throw WalletIncompatibleAddress("Address is indicated as a template, but data is not a template")
                pt.groupInfo = GroupInfo(grp, grpQty)
                pt.gp2t(blockchain)
            }
            /*
            PayAddressType.P2PKT -> {
                // Pull this apart and recreate with a different group and group amount
                val orig = SatoshiScript(blockchain, BCHserialized(data, SerializationType.NETWORK))
                orig.type = SatoshiScript.Type.TEMPLATE
                val pt = orig.parseTemplate(0)
                if (pt == null) throw WalletIncompatibleAddress("Address is indicated as a template, but data is not a template")
                pt.groupInfo = GroupInfo(grp, grpQty)
                pt.gp2t(blockchain)
            }

             */
        }
        return script
    }

    override fun equals(other: Any?): Boolean
    {
        return contentEquals(other)
        //return super.equals(other)
    }

    fun contentEquals(other: Any?): Boolean
    {
        if (other == null) return false
        if (other is PayAddress)
        {
            // Note that I do not compare the type.  This is because some types are specializations of others
            // and so 2 PayAddresses can be equal although the type field may be different.
            // Additionally, the data fully specifies the type (it just may take effort to extract it).
            return ((blockchain == other.blockchain) && (data.contentEquals(other.data)))
        }
        return false
    }

    override fun hashCode(): Int
    {
        // Note that I do not compare the type.  This is because some types are specializations of others
        // and so 2 PayAddresses can be equal although the type field may be different.
        // Additionally, the data fully specifies the type (it just may take effort to extract it).
        val dhc = data.contentHashCode()
        val hc = blockchain.ordinal.toInt().xor(dhc)
        return hc
    }
}
