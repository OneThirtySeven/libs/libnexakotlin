package org.nexa.libnexakotlin

import org.nexa.threads.*
import kotlin.test.*

class LibnexaTests
{
    @Test
    fun verifyCodec()
    {
        println("verifyCodec")
        val libnexa = initializeLibNexa()
        var data = byteArrayOf(1,2,3,4,5,6,7,8,9,10, 0, -1, 127, -127 )
        var str = libnexa.encode64(data)
        // println(str.toString())
        var data2 = libnexa.decode64(str)
        check(data contentEquals data2)
    }
    @OptIn(ExperimentalUnsignedTypes::class)
    @Test
    fun verifyHash()
    {
        // println("PLATFORM: " + getPlatform().name)
        val libnexa = try
        {
            initializeLibNexa()
        }
        catch(e: LibNexaException)
        {
            println("\n*** MISSING SHARED LIBRARY: SKIPPING TESTS ***\n")
            return
        }
        val hash = libnexa.hash256(byteArrayOf(1,2))
        check(hash.toHex() == "76a56aced915d2513dcd84c2c378b2e8aa5cd632b5b71ca2f2ac5b0e3a649bdb")
        val uhash = libnexa.hash256(ubyteArrayOf(1u,2u))
        check(uhash.toHex() == "76a56aced915d2513dcd84c2c378b2e8aa5cd632b5b71ca2f2ac5b0e3a649bdb")

        println("Platform: ${org.nexa.threads.platformName()}  verifyHash of 0: ${libnexa.hash256(byteArrayOf(0)).toHex()}")


        val hash2 = libnexa.sha256(byteArrayOf(1,2))
        check(hash2.toHex() == "a12871fee210fb8619291eaea194581cbd2531e4b23759d225f6806923f63222")
        // println(hash2.toHex())
        val uhash2 = libnexa.sha256(ubyteArrayOf(1u,2u))
        check(uhash2.toHex() == "a12871fee210fb8619291eaea194581cbd2531e4b23759d225f6806923f63222")

        val hash3 = libnexa.hash160(byteArrayOf(1,2))
        check(hash3.toHex() == "15cc49e191cbc520d91944600a5cb77af6aa3291")
        // println(hash3.toHex())
        val uhash3 = libnexa.hash160(ubyteArrayOf(1u,2u))
        check(uhash3.toHex() == "15cc49e191cbc520d91944600a5cb77af6aa3291")
    }

    @Test fun verifyGetWorkFromDifficultyNbits()
    {
        val libnexa = initializeLibNexa()
        if (true)
        {
            var work = libnexa.getWorkFromDifficultyBits(0x1e010000)
            // println(work.toHex())
            check(work.toHex() == "0000000000000000000000000000000000000000000000000000000000ffffff")
            work = libnexa.getWorkFromDifficultyBits(0x1a759c40)
            check(work.toHex() == "00000000000000000000000000000000000000000000000000022d3ad84091b3")
            work = libnexa.getWorkFromDifficultyBits(386366690)
            check(work.toHex() == "00000000000000000000000000000000000000000000223057d36f9c859674aa")
        }

        if (true)
        {
            var work = libnexa.getWorkFromDifficultyBits(0x1e010000u)
            check(work.toHex() == "0000000000000000000000000000000000000000000000000000000000ffffff")
            work = libnexa.getWorkFromDifficultyBits(0x1a759c40u)
            check(work.toHex() == "00000000000000000000000000000000000000000000000000022d3ad84091b3")
            work = libnexa.getWorkFromDifficultyBits(386366690u)
            check(work.toHex() == "00000000000000000000000000000000000000000000223057d36f9c859674aa")
        }

        if (true)
        {
            var work = libnexa.getWorkFromDifficultyBits(0x1e010000uL)
            check(work.toHex() == "0000000000000000000000000000000000000000000000000000000000ffffff")
            work = libnexa.getWorkFromDifficultyBits(0x1a759c40uL)
            check(work.toHex() == "00000000000000000000000000000000000000000000000000022d3ad84091b3")
            work = libnexa.getWorkFromDifficultyBits(386366690uL)
            check(work.toHex() == "00000000000000000000000000000000000000000000223057d36f9c859674aa")
        }
    }

    @OptIn(ExperimentalUnsignedTypes::class)
    @Test
    fun verifyGetPubKey()
    {
        val libnexa = initializeLibNexa()

        var secret = ByteArray(32, { it.toByte() })
        var usecret = UByteArray(32, { it.toUByte() })
        try
        {
            val pk = libnexa.getPubKey(secret)
            val upk = libnexa.getPubKey(usecret)
            check(upk contentEquals pk.toUByteArray())
        }
        catch(e:IllegalStateException)
        {
            check(false)
        }


        // Invalid private key lengths
        secret = ByteArray(31, { it.toByte() })
        try
        {
            libnexa.getPubKey(secret)
            check(false)
        }
        catch(e:IllegalStateException)
        {
            check(true)
        }

        secret = ByteArray(33, { it.toByte() })
        try
        {
            libnexa.getPubKey(secret)
            check(false)
        }
        catch(e:IllegalStateException)
        {
            check(true)
        }

        secret = ByteArray(0, { it.toByte() })
        try
        {
            libnexa.getPubKey(secret)
            check(false)
        }
        catch(e:IllegalStateException)
        {
            check(true)
        }

        // Invalid private key value
        secret = ByteArray(32, { 0 })
        try
        {
            libnexa.getPubKey(secret)
            assert(false)  // because check throws illegalstateexception (and so does libnexa)
        }
        catch(e:IllegalStateException)
        {
            check(true)
        }
    }

    @Test fun verifyDust()
    {
        val libnexa = initializeLibNexa()
        check(libnexa.minDust(ChainSelector.NEXA) == 546L)
        check(libnexa.minDust(ChainSelector.BCH) == 546L)
    }

    @kotlin.ExperimentalUnsignedTypes
    @Test fun verifyTxidemTxid()
    {
        val libnexa = initializeLibNexa()
        val tx1 = "000100696b069545cb31f6a53c822c3efca739a29256a34de181b541381ad22e242d3964222102f2a709a523d3cb03c168acac1e22dfe5c026904860eb3de3a9483858e94cb2e440cd1f5c4a12c89b1aa992b341ac3e167175197e25ac6439e4ead0318f9c09651a11e9b663d9669e98ae98270e816e0538cd0f37ca5cca5496e6f8ad33b8f04f5afeffffff75547f2c000000000201809698000000000017005114a23baa184b33c3ee244ea736d73dd37447f638da011abde62b0000000017005114de5c3eb8b94a27932509df76df63d0fe2ab311be7c000000".fromHex()
        // println(libnexa.txid(tx1).toHex())
        check(libnexa.txid(tx1).reversed().toHex() == "188fc9f1b33afb968753b6cd642afc5a28c59fb244841f7ba7378972c30563c6")
        check(libnexa.txidem(tx1).reversed().toHex() == "882b5de933360a36c1ee38590579de3ee69655bad72bf442a3b4b16a7f196cc5")
        val utx1 = "000100696b069545cb31f6a53c822c3efca739a29256a34de181b541381ad22e242d3964222102f2a709a523d3cb03c168acac1e22dfe5c026904860eb3de3a9483858e94cb2e440cd1f5c4a12c89b1aa992b341ac3e167175197e25ac6439e4ead0318f9c09651a11e9b663d9669e98ae98270e816e0538cd0f37ca5cca5496e6f8ad33b8f04f5afeffffff75547f2c000000000201809698000000000017005114a23baa184b33c3ee244ea736d73dd37447f638da011abde62b0000000017005114de5c3eb8b94a27932509df76df63d0fe2ab311be7c000000".ufromHex()
        check(libnexa.txid(utx1).reversed().utoHex() == "188fc9f1b33afb968753b6cd642afc5a28c59fb244841f7ba7378972c30563c6")
        check(libnexa.txidem(utx1).reversed().utoHex() == "882b5de933360a36c1ee38590579de3ee69655bad72bf442a3b4b16a7f196cc5")
    }

    @kotlin.ExperimentalUnsignedTypes
    @Test fun verifyBlock()
    {
        val libnexa = initializeLibNexa()
        val blk1header = "01c839537ce90782da85aa1423a4dbb16c0fa9eed735df0edd4ebae14f14c7ed0000011e01c839537ce90782da85aa1423a4dbb16c0fa9eed735df0edd4ebae14f14c7ed45b134cbc6014b6193bdbc82d6b5ca3cacbab66ee18abb4916dac5c1647aa23700000000000000000000000000000000000000000000000000000000000000000abab16201feffff0100000000000000000000000000000000000000000000000000000000f70000000000000001000000041821f5c0".fromHex()
        val hash = libnexa.blockHash(blk1header)
        //println(hash.toHex())
        // Note that full node reverses hashes
        check (hash.reversed().toHex() == "9bd9f36759a53aa3dd5d979bd587b6a6808230b4b079e30d93e5da637f2fe59d")
        check (libnexa.blockHash(blk1header.toUByteArray()).toByteArray() contentEquals  hash)

        val blkOk = libnexa.verifyBlockHeader(ChainSelector.NEXA, blk1header)
        check(blkOk == true)
    }

    @kotlin.ExperimentalUnsignedTypes
    @Test fun verifySignHashSchnorr()
    {
        val libnexa = initializeLibNexa()
        val secret = ByteArray(32, { it.toByte() })
        val data = "test".toByteArray()
        val udata = "test".toByteArray().toUByteArray()
        val hashdata = libnexa.hash256(data)
        val uhashdata = libnexa.hash256(udata)
        try
        {
            libnexa.signHashSchnorr(data, secret)
            assert(false)  // data is the wrong size because not signing the hash of the data
        }
        catch (e:IllegalStateException)
        {
            check(true)
        }

        val sig = libnexa.signHashSchnorr(hashdata, secret)
        val usig = libnexa.signHashSchnorr(uhashdata, secret.toUByteArray())
        // println("sig: " + sig.toHex())
        check(sig contentEquals usig.toByteArray())
    }

    @kotlin.ExperimentalUnsignedTypes
    @Test fun verifyCreateBloomFilter()
    {
        val libnexa = initializeLibNexa()
        val data: Array<Any> = arrayOf(byteArrayOf(1), byteArrayOf(2))
        var bloom = libnexa.createBloomFilter(data, 0.1, 10, MAX_BLOOM_FILTER_SIZE, BloomFlags.BLOOM_UPDATE_ALL.v)
        check(bloom.size == 15)
        check(bloom.toHex() == "050020018004020000000100000001")

        val udata: Array<Any> = arrayOf(ubyteArrayOf(1u), byteArrayOf(2))
        bloom = libnexa.createBloomFilter(udata, 0.1, 10, MAX_BLOOM_FILTER_SIZE, BloomFlags.BLOOM_UPDATE_ALL.v)
        check(bloom.size == 15)
        check(bloom.toHex() == "050020018004020000000100000001")
    }

    @Test fun verifySignTxOneInputUsingSchnorr()
    {
        val libnexa = initializeLibNexa()
        val secret = ByteArray(32, { it.toByte() })

        if (true)
        {
            // None of these values make any sense except that they are parsable
            val tx =
                "000100696b069545cb31f6a53c822c3efca739a29256a34de181b541381ad22e242d3964222102f2a709a523d3cb03c168acac1e22dfe5c026904860eb3de3a9483858e94cb2e440cd1f5c4a12c89b1aa992b341ac3e167175197e25ac6439e4ead0318f9c09651a11e9b663d9669e98ae98270e816e0538cd0f37ca5cca5496e6f8ad33b8f04f5afeffffff75547f2c000000000201809698000000000017005114a23baa184b33c3ee244ea736d73dd37447f638da011abde62b0000000017005114de5c3eb8b94a27932509df76df63d0fe2ab311be7c000000".fromHex()
            val aScript = "005114b98039671cfbda088a2f4f845cce1627a0043199".fromHex()
            val sighashtype = byteArrayOf()

            val sig =
                libnexa.signTxOneInputUsingSchnorr(tx, sighashtype, 0, 10000, aScript, secret)
            // println(sig.toHex())
            //check(sig != null)
            check(sig.size == 64)  // not for every sig, but true for this one because all/all sighashtype
        }

        if (true)
        {
            // This tx won't decode
            val tx =
                "010101000100696b069545cb31f6a53c822c3efca739a29256a34de181b541381ad22e242d3964222102f2a709a523d3cb03c168acac1e22dfe5c026904860eb3de3a9483858e94cb2e440cd1f5c4a12c89b1aa992b341ac3e167175197e25ac6439e4ead0318f9c09651a11e9b663d9669e98ae98270e816e0538cd0f37ca5cca5496e6f8ad33b8f04f5afeffffff75547f2c000000000201809698000000000017005114a23baa184b33c3ee244ea736d73dd37447f638da011abde62b0000000017005114de5c3eb8b94a27932509df76df63d0fe2ab311be7c000000".fromHex()
            val aScript = "005114b98039671cfbda088a2f4f845cce1627a0043199".fromHex()
            val sighashtype = byteArrayOf()

            try
            {
                libnexa.signTxOneInputUsingSchnorr(tx, sighashtype, 0, 10000, aScript, secret)
                assert(false)
            }
            catch (e: IllegalStateException)
            {
                check(true)
            }
        }

    }

    @Test fun verifySignBchTxOneInputUsingECDSA()
    {
    }

    @Test fun verifySignBchTxOneInputUsingSchnorr()
    {
    }

    @Test
    fun verifySignMessage()
    {
        val libnexa = initializeLibNexa()
        if (true)
        {
            val secret = ByteArray(32, { it.toByte() })
            val pub = initializeLibNexa().getPubKey(secret)
            val addrbytes = initializeLibNexa().hash160(pub)
            val sig = libnexa.signMessage("test".toByteArray(), secret)
            check(sig != null)
            if (true)
            {
                // println("SIG: " + sig.toHex())
                check(sig.size >= 64)
                check(sig.toHex() == "1f846d24527e3dea00bf8eb2a585a426290a00254e81fd6f9233be4cefae7d88b878f7cb23c460193211e03a628e12993c49f8ad0f68fe66ab1023e095dbac271c")
                val vfy = libnexa.verifyMessage("test".toByteArray(), addrbytes, sig)
                // println("VFY: " + vfy?.toHex())
                check(vfy != null)
                check(vfy contentEquals pub)
                //val vfy2 = libnexa.verifyMessage("test".toByteArray(), pub, sig)
                //check(vfy2 != null)

                var badvfy = libnexa.verifyMessage("test1".toByteArray(), addrbytes, sig)
                check (badvfy == null)

                sig[0] = (sig[0]+1).toByte()
                badvfy = libnexa.verifyMessage("test".toByteArray(), addrbytes, sig)
                check (badvfy == null)

            }
        }
    }

    @Test fun verifyEncodeDecodeCashAddr()
    {
        val libnexa = initializeLibNexa()
        if (true)
        {
            val decoded = libnexa.decodeCashAddr(ChainSelector.NEXAREGTEST, "qpaj30le3wqz04ldwnsj94x75u9kv782kvh6hgf5e0")
            val type = LoadPayAddressType(decoded[0])
            check(type == PayAddressType.P2PKH)
            val data = decoded.sliceArray(IntRange(1, decoded.size - 1))
            val addr = libnexa.encodeCashAddr(ChainSelector.NEXAREGTEST, PayAddressType.P2PKH, data)
            // println( addr)
            check(addr == "nexareg:qpaj30le3wqz04ldwnsj94x75u9kv782kvh6hgf5e0")
        }
        if (true)
        {
            val decoded = libnexa.decodeCashAddr(ChainSelector.NEXA, "nexa:qzdf59mdl28fe869x5rjf69swdhvt5snmqwvwemr3e")
            val decoded1 = libnexa.decodeCashAddr(ChainSelector.NEXA, "qzdf59mdl28fe869x5rjf69swdhvt5snmqwvwemr3e")
            check(decoded contentEquals decoded1)
            val type = LoadPayAddressType(decoded[0])
            check(type == PayAddressType.P2PKH)
            val data = decoded.sliceArray(IntRange(1, decoded.size - 1))
            val addr = libnexa.encodeCashAddr(ChainSelector.NEXA, PayAddressType.P2PKH, data)
            check(addr == "nexa:qzdf59mdl28fe869x5rjf69swdhvt5snmqwvwemr3e")
        }
        if (true)
        {
            val decoded = libnexa.decodeCashAddr(ChainSelector.NEXA, "nexa:nqtsq5g5scatjzp7mx2k890px6k9c5wqe3x5zk4dcu7hwrfn")
            val decoded1 = libnexa.decodeCashAddr(ChainSelector.NEXA, "nqtsq5g5scatjzp7mx2k890px6k9c5wqe3x5zk4dcu7hwrfn")
            check(decoded contentEquals decoded1)
            val type = LoadPayAddressType(decoded[0])
            // println(type)
            check(type == PayAddressType.TEMPLATE)
            val data = decoded.sliceArray(IntRange(1, decoded.size - 1))
            val addr = libnexa.encodeCashAddr(ChainSelector.NEXA, PayAddressType.TEMPLATE, data)
            check(addr == "nexa:nqtsq5g5scatjzp7mx2k890px6k9c5wqe3x5zk4dcu7hwrfn")
        }

        if (true)
        {
            val decoded = libnexa.decodeCashAddr(ChainSelector.BCH, "bitcoincash:qqvu9hd57fftx3lmhq37e5wnvrpth63edq7g3p7k6r")
            val decoded1 = libnexa.decodeCashAddr(ChainSelector.BCH, "qqvu9hd57fftx3lmhq37e5wnvrpth63edq7g3p7k6r")
            check(decoded contentEquals decoded1)
            val type = LoadPayAddressType(decoded[0])
            check(type == PayAddressType.P2PKH)
            val data = decoded.sliceArray(IntRange(1, decoded.size - 1))
            val addr = libnexa.encodeCashAddr(ChainSelector.BCH, PayAddressType.P2PKH, data)
            check(addr == "bitcoincash:qqvu9hd57fftx3lmhq37e5wnvrpth63edq7g3p7k6r")
        }

        if (true)
        {
            val decoded = libnexa.decodeCashAddr(ChainSelector.NEXAREGTEST, "nexareg:qpaj30le3wqz04ldwnsj94x75u9kv782kvh6hgf5e0")
            val type = LoadPayAddressType(decoded[0])
            check(type == PayAddressType.P2PKH)
            val data = decoded.sliceArray(IntRange(1, decoded.size - 1))
            val addr = libnexa.encodeCashAddr(ChainSelector.NEXAREGTEST, PayAddressType.P2PKH, data)
            check(addr == "nexareg:qpaj30le3wqz04ldwnsj94x75u9kv782kvh6hgf5e0")
        }
        if (true)
        {
            // This NEXAREG address is not correct as NEXA so results in NONE
            val decoded = libnexa.decodeCashAddr(ChainSelector.NEXA, "nexareg:qpaj30le3wqz04ldwnsj94x75u9kv782kvh6hgf5e0")
            val type = LoadPayAddressType(decoded[0])
            check(type == PayAddressType.NONE)
        }

        if (true)
        {
            // This NEXAREG address is not correct as BCH so results in NONE
            val decoded = libnexa.decodeCashAddr(ChainSelector.BCH, "qpaj30le3wqz04ldwnsj94x75u9kv782kvh6hgf5e0")
            val type = LoadPayAddressType(decoded[0])
            check(type == PayAddressType.NONE)
        }
    }

    @Test fun verifyHd44()
    {
        val libnexa = initializeLibNexa()
        val masterSecret = ByteArray(32,{it.toByte()})

        val childKey = libnexa.deriveHd44ChildKey(masterSecret, 0, 0, 0, false, 0)
        check(childKey.first.toHex() == "1cce5762593d8ac5aba74102d1252c6139005e67cb20fe16f17a2a11f5b806fe")
    }

    @Test fun verifySeed()
    {
        val ret = generateBip39Seed("timber sword where noodle joy eagle admit tuna vibrant museum gossip river", "", 64)
        println(ret.toHex())
        check(ret.toHex() == "e6e1704c58bbb27d8eadd448489b32dfdb9388fd28af24cf79d40353b0e5d729b1bc30eab460a60493b6c4caab3ea1f2edfab52dedd2f962fa4a45620ee4f238")
    }

    @Test fun verifyWifPrivateKey()
    {
        val libnexa = initializeLibNexa()
        val key = libnexa.decodeWifPrivateKey(ChainSelector.BCH, "L5EZftvrYaSudiozVRzTqLcHLNDoVn7H5HSfM9BAN6tMJX8oTWz6")
        check(key.toHex() == "ef235aacf90d9f4aadd8c92e4b2562e1d9eb97f0df9ba3b508258739cb013db2")

        try
        {
            // Wrong blockchain
            libnexa.decodeWifPrivateKey(ChainSelector.NEXA, "L5EZftvrYaSudiozVRzTqLcHLNDoVn7H5HSfM9BAN6tMJX8oTWz6")
            assert(false)
        }
        catch (e: IllegalStateException)
        {
            check(true)
        }
    }

    @Test fun verifyGroupIdAddrEncodeDecode()
    {
        val libnexa = initializeLibNexa()
        val fakegroupid32 = ByteArray(32, { it.toByte() } )
        val fakegroupid31 = ByteArray(31, { it.toByte() })
        val fakegroupid520 = ByteArray(520, { it.toByte() })
        val fakegroupid521 = ByteArray(521, { it.toByte() })

        var addr = libnexa.groupIdToAddr(ChainSelector.NEXA, fakegroupid32)
        check(addr == "nexa:tqqqzqsrqszsvpcgpy9qkrqdpc83qygjzv2p29shrqv35xcur50p7j7y9s6g6")
        var bin = libnexa.groupIdFromAddr(ChainSelector.NEXA, addr)
        check (bin contentEquals fakegroupid32)
        addr = libnexa.groupIdToAddr(ChainSelector.NEXA, fakegroupid520)
        check(addr == "nexa:tqqqzqsrqszsvpcgpy9qkrqdpc83qygjzv2p29shrqv35xcur50p7gppyg3jgffxyu5zj23t9skjutesxyerxdp4xcmnswf68v7r603lgpq5ys6yg4rywjzfff95cn2wfag9z5jn2324v46ct9d9khzate0kqctzvdjx2en8dp5k56mvd4hx7ur3wfehgatkwau8j7nm037huluqsxpg8py9s6rc3zv23wxgmr50jzge9yu5jktf0xyen2dee8v7n7s2rg4r5jj6dfag4x42ht9d46hmpvdjkw6ttd4hhzum4wauhkltlsxpctpuf3wxclyvnjktenxuan7s68fd84x46mta3kw6m0wdmhklurs79clyuhnw068fat47em0walc0ruhn7n6ldalcl8a0hl8almlsqqgzqvzq2ps8pqys5zcvp58q7yq3zgf3g9gkzuvpjxsmrsw3u8eqyy3zxfp9ycnjs2f29vkz6t30xqcnyve5x5mrwwpe8ganc0f78aqyzsjrg3z5v36gf99yknzdfe84q52j2d2924jhtpv45k6ut4097crpvf3kgetxva5xj6ntd3kkummsw9e8xar4wemhs7t60d786lnlszqc9quyskrg0zyf329cervw37gfry5njj2ed9ucnxdfh8yan606pgdz5wj2tf484z5642av4kh2lv93k2emfddkk7utnw4mhj7ma07qc8pv83x9cmru3jw2e0xvmnk06rga95756htd0kxemtdaehw7mlswrchrunj7delga84whm8damhlpu0j7060tahhlrul47lulhl07qqpqgpsgpgxquxaqmgg5r")
        bin = libnexa.groupIdFromAddr(ChainSelector.NEXA, addr)
        check (bin contentEquals fakegroupid520)
        try
        {
            libnexa.groupIdToAddr(ChainSelector.NEXA, fakegroupid31)
            assert(false)
        }
        catch(e: IllegalStateException)
        {
            check(true)
        }

        try
        {
            libnexa.groupIdToAddr(ChainSelector.NEXA, fakegroupid521)
            assert(false)
        }
        catch(e: IllegalStateException)
        {
            check(true)
        }
    }

}
