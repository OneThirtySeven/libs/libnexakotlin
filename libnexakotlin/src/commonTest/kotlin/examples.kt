import org.nexa.libnexakotlin.*
import kotlin.test.*
import com.ionspin.kotlin.bignum.integer.BigInteger
import com.ionspin.kotlin.bignum.integer.Sign

class BlockchainObjTests
{
    companion object
    {
        init
        {
            initializeLibNexa()
        }
    }

    // @Test
    fun createATxWithoutAWallet()
    {
        val outpointHex = "put your hex encoded outpoint hash here"
        val secret = UnsecuredSecret("hex encoded 32 bytes".fromHex())
        val outpointAmount = 2000L
        val pto = PayAddress("nexa:pay to this address")
        val s = Pay2PubKeyTemplateDestination(ChainSelector.NEXA, secret,0)
        val fee = 300L

        val script = s.outputScript()
        println("P2PKT script ASM:" + script.toAsm())
        println("pubkey: " + s.pubkeyOrThrow().toHex())
        println("hash160 pubkey: " + libnexa.hash160(s.pubkeyOrThrow()).toHex())
        val scr = SatoshiScript(ChainSelector.NEXA, SatoshiScript.Type.SATOSCRIPT, OP.push(s.pubkeyOrThrow()))
        println("args script: " + scr.toByteArray().toHex() + " or ASM " + scr.toAsm())
        println("hash160 of script pubkey: " + libnexa.hash160(scr.toByteArray()).toHex())
        val tx = txFor(ChainSelector.NEXA)
        val spend = Spendable(ChainSelector.NEXA)
        spend.outpoint = NexaTxOutpoint(outpointHex)
        spend.amount = outpointAmount
        spend.backingPayDestination = s

        val txIn = NexaTxInput(spend)
        txIn.sequence = 4294967294
        //tx.lockTime = locktime
        tx.add(txIn)
        val out = NexaTxOutput(ChainSelector.NEXA)
        out.type = NexaTxOutput.Type.TEMPLATE
        out.script = pto.outputScript()
        out.amount = outpointAmount - fee
        tx.add(out)
        signTransaction(tx)
        val txhex = tx.toHex()
        println("hex tx: " + txhex)
    }

}