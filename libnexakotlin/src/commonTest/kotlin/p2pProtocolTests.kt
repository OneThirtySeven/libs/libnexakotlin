import com.ionspin.kotlin.bignum.decimal.BigDecimal
import kotlin.test.*
import kotlin.coroutines.CoroutineContext
import kotlinx.coroutines.channels.*

import kotlinx.coroutines.*
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonObject
import org.nexa.libnexakotlin.*
import org.nexa.threads.Gate
import com.ionspin.kotlin.bignum.integer.BigInteger
import kotlinx.atomicfu.AtomicInt
import kotlinx.atomicfu.atomic
import org.nexa.threads.Thread
import org.nexa.nexarpc.*

private val LogIt = GetLog("P2pProtocolTests.kt")

private val FULL_NODE_IP = "127.0.0.1"

class P2pProtocolTests
{
    val chainSelector = ChainSelector.NEXAREGTEST
    val port = NexaRegtestPort

    @BeforeTest
    fun beforeMethod()
    {
        LogIt.info("Test starting")
        initializeLibNexa()
    }

    @Test
    fun connectToP2P()
    {
        LogIt.info("This test requires a bitcoind full node running on regtest at ${FULL_NODE_IP}:${port}")

        val coCtxt: CoroutineContext = newFixedThreadPoolContext(4, "testP2p")
        val coScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(coCtxt)

        var cnxn = P2pClient(chainSelector, FULL_NODE_IP, port, "regtest@${FULL_NODE_IP}",coScope).connect(2000)
        Thread("p2p receive processor") { cnxn.processForever() }

        var txValResponses:AtomicInt = atomic(0)

        cnxn.waitForReady()
        cnxn.send(NetMsgType.PING, BCHserialized(SerializationType.NETWORK).addUint64(1))

        val loc = BlockLocator()
        loc.add(Hash256())  // This will ask for the genesis block because no hashes will match
        val stop = Hash256()
        var headers: MutableList<out iBlockHeader>? = null
        val waiter = Gate()
        cnxn.getHeaders(loc, stop, { lst, _  -> headers = lst; waiter.wake(); true})

        waiter.delayuntil(5000) { headers != null }
        check(headers != null)  // If its null we didn't get the headers

        for (hdr in headers!!)
        {
            // LogIt.info(hdr.height.toString() + " hash: " + hdr.hash.toHex() + " prev: " + hdr.hashPrevBlock.toHex())
        }

        var tx : NexaTransaction = NexaTransaction(chainSelector)
        cnxn.sendTxVal(tx) {
            s -> LogIt.info("tx Validation response: " + s)
            check("bad-txns-vout-empty" in s)
            check("txn-undersize" in s)
            txValResponses += 1
        }

        var rawAddr: ByteArray = ByteArray(20)
        tx._outputs.add(NexaTxOutput(10000, SatoshiScript.p2pkh(rawAddr, chainSelector)))
        cnxn.sendTxVal(tx) {
            s -> LogIt.info("tx2 Validation response: " + s)
            check("txn-undersize" in s)
            check("Coinbase is only valid in a block" in s)
            check("txpool min fee not met" in s)
            txValResponses += 1
        }

        tx._inputs.add(NexaTxInput(chainSelector, Spendable(chainSelector, NexaTxOutpoint(Hash256()), 10000),SatoshiScript(chainSelector) + OP.PUSHTRUE))
        //tx.outputs[0] = BCHoutput(10000, BCHscript.p2pkh(rawAddr, chain))
        cnxn.sendTxVal(tx) {
            s -> LogIt.info("tx3 Validation response: " + s)
            check("input-does-not-exist" in s)
            check("inputs-are-missing" in s)
            txValResponses += 1
        }

        /*
        // set up a quick way to get blocks back from a node
        var blk: iBlock? = null
        val blkChannel = Channel<iBlock>()
        cnxn.onBlockCallback.add({ incomingblk, _ -> blkChannel.send(incomingblk) })
        // Spend an immature coinbase
        cnxn.sendGetData(listOf(Inv(Inv.Types.BLOCK, headers!![headers!!.size - 1].hash)))
        val g = Gate()
        launch { blk = blkChannel.receive(); g.wake() }
        if (g.delayuntil(10000) { blk != null} == false)
            check(blk != null, {"never received the block"})

         */

        // set up a quick way to get blocks back from a node
        var blk: iBlock? = null
        val g = Gate()
        cnxn.onBlockCallback.add(
            { incomingblk, _ ->
                println("block callback")
                g.wake {
                    blk=incomingblk
                }
            }
        )

        val blkhash = headers!![headers!!.size - 1].hash
        println("Get block ${blkhash.toHex()}")
        cnxn.sendGetData(listOf(Inv(Inv.Types.BLOCK, blkhash)))
        if (g.delayuntil(10000) { blk != null} == false)
            check(blk != null, {"never received the block"})
        check(blk!!.hash == blkhash)

        // Spend the coinbase of a block that can't be spent yet
        tx._inputs[0] = NexaTxInput(chainSelector, Spendable(chainSelector, NexaTxOutpoint(blk!!.txes[0].idem, 0), 5000000000),SatoshiScript(chainSelector) + OP.PUSHTRUE)
        //tx.outputs[0] = BCHoutput(10000, BCHscript.p2pkh(rawAddr, chain))
        cnxn.sendTxVal(tx) {
            s -> LogIt.info("tx4 Validation response: " + s)
            check("input-amount-mismatch" in s)
            check("mandatory-script-verify-flag-failed" in s)
            txValResponses += 1
        }

        // Since I'm looking now at the coinbase 100 blocks ago, this must be spendable and unspent
        blk = null
        cnxn.sendGetData(listOf(Inv(Inv.Types.BLOCK, headers!![headers!!.size - 100].hash)))
        if (g.delayuntil(10000) { blk != null} == false)
            check(false, {"never received the block"})

        // Spend the coinbase of a block that just matured, so we know it can't have been spent.  Spend too much to ensure that we still check sigs
        tx._outputs[0].amount = 10000000000
        tx._inputs[0] = NexaTxInput(chainSelector, Spendable(chainSelector, NexaTxOutpoint(blk!!.txes[0].idem, 0), 1000000000),SatoshiScript(chainSelector) + OP.PUSHTRUE)

        val respCnt = txValResponses.value + 1
        cnxn.sendTxVal(tx) {
            s -> LogIt.info("tx5 Validation response: " + s)
            check("mandatory-script-verify-flag-failed (Bad template operation)" in s)
            check("bad-txns-in-belowout" in s)
            txValResponses += 1
            waiter.wake()
        }
        waiter.delayuntil(5000) { respCnt == txValResponses.value }

        if (txValResponses.value == 0) LogIt.error("TX validation request is being completely ignored.  DID YOU ENABLE IT IN THE FULL NODE? (by setting net.allowp2pTxVal=1)")
        check(txValResponses.value == respCnt, {"incorrect number of txval responses $txValResponses"})
        LogIt.info("shutting down")
        cnxn.close()
        LogIt.info("TestCompleted")
    }



    @Test
    fun testRpc()
    {
        LogIt.info("This test requires a full node running on regtest at ${EMULATOR_HOST_IP} and port ${NexaRegtestRpcPort}")

        // Set up RPC connection
        val rpcConnection = "http://" + FULL_NODE_IP + ":" + NexaRegtestRpcPort
        LogIt.info("Connecting to: " + rpcConnection)

        val nexaRpc: NexaRpc = NexaRpcFactory.create(rpcConnection)

        // Try the hard way (manual parsing returned parameter)
        val retje = nexaRpc.callje("listunspent")
        val result = (retje as JsonObject)["result"] as JsonArray
        check(result.size > 0)  // This could fail if you haven't created at least 101 blocks

        // Try the easy way
        val ret = nexaRpc.listunspent()
        check(ret.size > 0)
    }

    /*
    @Test
    fun rpcAndWalletSetup()
    {
        LogIt.info("This test requires a bitcoind full node running on " +
                "regtest at ${EMULATOR_HOST_IP} and ports ${port} and ${NexaRegtestRpcPort}")

        val rpcConnection = "http://" + FULL_NODE_IP + ":" + NexaRegtestRpcPort
        LogIt.info("Connecting to: " + rpcConnection)
        val rpc = NexaRpcFactory.create(rpcConnection)
        var unspent = rpc.listunspent()
        println(unspent.toString())

        // Generate blocks until we get coins to spend. This is needed inside the ci testing.
        // But the code checks first so that lots of extra blocks aren't created during dev testing
        val peerinfo:String = rpc.calls("getpeerinfo")
        println(peerinfo)

        // Generate blocks until there is some mature balance in this wallet
        var rpcBalance = rpc.getbalance()
        LogIt.info(rpcBalance.toPlainString())
        while (rpcBalance < BigDecimal.fromInt(50))
        {
            rpc.generate(1)
            rpcBalance = rpc.getbalance()
        }


        // Set up connection between full and light nodes
        val coCtxt: CoroutineContext = newFixedThreadPoolContext(2, "testP2p")
        val coScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(coCtxt)
        val coCond = CoCond<Boolean>(coScope)
        var client = P2pClient(chainSelector, EMULATOR_HOST_IP, port, "regtest@${EMULATOR_HOST_IP}",coScope,coCond).connect()
        GlobalScope.launch { client.processForever() }
        client.waitForReady()
        client.send(NetMsgType.PING, BCHserialized(SerializationType.NETWORK) + 1L)

        // Set up blockchain
        val cnxnMgr: CnxnMgr = MultiNodeCnxnMgr("regtest", chainSelector, arrayOf(EMULATOR_HOST_IP))
        val chain = Blockchain(
                chainSelector,
                "regtest",
                cnxnMgr,
                // If checkpointing the genesis block, set the prior block id to the genesis block as well
                Hash256(),
                Hash256(),
                Hash256(),
                0,
                BigInteger.fromInt(0),
                dbPrefix
        )
        val chainThread = Thread("blockchain") {
            println("Starting blockchain")
            chain.start()
        }

        // Set up wallet
        val kvp = OpenKvpDB(ctxt, "db")
        val wallet = Bip44Wallet(kvp!!, "test", chainSelector, "secret word")
        wallet.addBlockchain(chain, chain.checkpointHeight, 0)
        val walletThread = Thread("wallet") {
            println("Starting wallet")
            wallet.run()
        }

        // Generate new address
        val addr = wallet.getnewaddress()

        // Add address to bloom filter and send it to full node
        val data = Array<Any>(1, { Unit })
        data[0] = addr.data;
        val bloom = libnexa.createBloomFilter(data, 0.1, data.size * 10, Wallet.MAX_BLOOM_SIZE, Wallet.Companion.BloomFlags.BLOOM_UPDATE_ALL.v, 1)
        client.sendBloomFilter(bloom, 1)

        // Full node sends 1000 satoshi to light
        val spend = BigDecimal.fromInt(1000)
        LogIt.info("Sending ${spend} to address ${addr.toString()}")
        rpc.sendtoaddress(addr.toString(), spend)

        // Full node generates a block
        val generated: List<HashId> = rpc.generate(1)
        val latestBlock = Hash256(generated[0].hash)

        // Light client asks for header of new block
        val loc = BlockLocator()
        loc.add(Hash256())  // This will ask for the genesis block because no hashes will match
        val stop = latestBlock
        var headers: MutableList<out iBlockHeader>? = null
        val waiter = Gate()
        client.getHeaders(loc, stop, { lst, _  -> headers = lst; waiter.wake(); true})
        waiter.delayuntil(5000) { headers != null}
        check(headers != null)  // If its null we didn't get the headers

        for (hdr in headers!!)
        {
            LogIt.info(hdr.height.toString() + " hash: " + hdr.hash.toHex() + " prev: " + hdr.hashPrevBlock.toHex())
        }

        // Cleanup
        LogIt.info("shutting down")
        wallet.stop()
        chain.stop()
        client.close()
        walletThread.join()
        chainThread.join()
        LogIt.info("TestCompleted")
    }

     */
}
