package org.nexa.libnexakotlin
import kotlin.test.*

import com.ionspin.kotlin.bignum.integer.BigInteger
import com.ionspin.kotlin.bignum.integer.Sign
import org.nexa.threads.microsleep
import org.nexa.threads.millisleep

private var FULL_NODE_IP = "192.168.1.5" //"127.0.0.1"

private val LogIt = GetLog("BlockchainTests.kt")

/** Database name prefix, empty string for mainnet, set for testing */
private var dbPrefix = "nexa_regtest_"

class BlockchainTests
{
    companion object
    {
        init
        {
            initializeLibNexa()
        }
    }

    @BeforeTest
    fun setup()
    {
        REG_TEST_ONLY = true
    }
    @Test
    fun create()
    {
        org.nexa.threads.DefaultThreadExceptionHandler = {
            println(it)
            handleThreadException(it)
        }
        try
        {
            deleteDatabase("unittest_regtest.db")
        }
        catch(e: org.nexa.libnexakotlin.LibNexaException)
        {
            if (e.message?.contains("with the Android application context") ?: false) return
            else throw e
        }
        val cm = GetCnxnMgr(ChainSelector.NEXAREGTEST, "regtest", true)
        cm.exclusiveNodes(setOf(FULL_NODE_IP))
        val bc = GetBlockchain(ChainSelector.NEXAREGTEST, cm, "regtest", true)
        var i = 0
        while(i < 5)  // loop for 5 seconds grabbing headers
        {
            println("$i Height: ${bc.curHeight} Cnxns: ${bc.net.p2pCnxns.size}")
            millisleep(1000U)
            i++
        }
        val tip = bc.nearTip
        assertTrue { tip != null }
        val tipheight = tip!!.height
        assertTrue { tipheight > 0 }
        println("tip height ${tip.height}")

        val blk1 = bc.getBlockHeader(1)
        println("block 1 hash ${blk1.hash.toHex()}")
        assertEquals(blk1.hashPrevBlock.toHex(), "d71ee431e307d12dfef31a6b21e071f1d5652c0eb6155c04e3222612c9d0b371")  // regtest genesis block hash
        val startClose = millinow()
        println("close at ${startClose}")
        bc.stop()
        val elapsed = millinow() - startClose
        println("complete close took $elapsed ms.")
        check(elapsed < 3000)  // the close should be done in a few seconds (or the blockchain run thread is not responding quickly enough)
    }
}