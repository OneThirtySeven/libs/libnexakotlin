package org.nexa.libnexakotlin
import app.cash.sqldelight.db.QueryResult
import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.db.SqlSchema
import app.cash.sqldelight.driver.native.NativeSqliteDriver
import co.touchlab.sqliter.DatabaseFileContext
import platform.posix.*
import kotlinx.cinterop.*
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1
import com.ionspin.kotlin.bignum.decimal.BigDecimal
import com.ionspin.kotlin.bignum.decimal.DecimalMode
import com.ionspin.kotlin.bignum.decimal.RoundingMode
import kotlinx.datetime.Instant
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import okio.internal.commonAsUtf8ToByteArray
import okio.internal.commonToUtf8String

var DEFAULT_DB_PATH = cwd() + "/ndb"

actual fun appContext():Any?
{
    return null
}

actual val lineSeparator: String = "\n"

actual fun show(p: KProperty1<*, *>, obj: Any): String
{
    return obj.toString()
}
actual fun showProperties(obj: Any, cls: KClass<*>, level: Display, style: DisplayStyles, depth: Int, indent: Int): String
{
    return ""
}

actual fun ByteArray.decodeUtf8():String
{
     return this.commonToUtf8String()
}

/** Convert a string into a utf-8 encoded byte array. */
@OptIn(ExperimentalForeignApi::class)
actual fun String.encodeUtf8():ByteArray
{
    // this is broken: return this.utf8.getBytes()
    return this.commonAsUtf8ToByteArray()
}


actual class DecimalFormat actual constructor(val fmtSpec: String)
{
    var fmtMode: DecimalMode
    init {
        val nDigits = (fmtSpec.filter { it == '#' || it == '0' }).length.toLong()
        val nDecimals = fmtSpec.split('.')[1].length.toLong()
        fmtMode = DecimalMode(nDigits, RoundingMode.ROUND_HALF_AWAY_FROM_ZERO, nDecimals )
    }

    actual fun format(num: BigDecimal): String
    {
        val bg = BigDecimal.fromBigDecimal(num, fmtMode)
        return bg.toStringExpanded()
    }
}

// TODO handle locale based numeral markup (commas and periods)
actual fun String.toCurrency(chainSelector: ChainSelector?): BigDecimal
{
    return BigDecimal.parseStringWithMode(this, CurrencyMath)
}

/** Return the directory that this application may use for its files */
@Deprecated("all file io should be already relative to this")
actual fun getFilesDir(): String?
{
    return null
}

@OptIn(ExperimentalForeignApi::class)
fun cwd(): String
{
      val cwd = memScoped {
        val result = this.allocArray<ByteVar>(1000)
        val cwd = getcwd(result, 1000U)
        result.toKStringFromUtf8()
    }
    // println("cwd $cwd")
    return cwd
}

actual fun deleteDatabase(name: String):Boolean?
{
    DatabaseFileContext.databasePath(name + ".db", DEFAULT_DB_PATH)  // This is ignored right now on Linux
    DatabaseFileContext.deleteDatabase(name + ".db", null)
    /*
    val pathWithExt = cwd() + "/" + path + ".db"
    val result = unlink(pathWithExt)
    if (result == -1)
    {
        val err = errno  // note Errno is thread-safe on modern systems (its thread-local)
        if ((err == EACCES)||(err==EROFS)||(err==EPERM)) throw PermissionDeniedException("deleting file $pathWithExt")
        if (err == ENOENT)
        {
            println("file $pathWithExt not found")
            return false
        } // Its ok to delete something that doesn't exist because the end state is correct
        if (err == EBUSY) throw InUseException(pathWithExt)
        else throw PlatformException("cannot remove $pathWithExt", err)
    }
    return true
     */
    return null
}

/** given a byte array, convert it into some string form of IP address
 * Should support ipv4 and ipv6 at a minimum
 */
actual fun ipAsString(ba:ByteArray):String
{
    if (ba.size == 4)  // IPv4 address
    {
        return "${ba[0].toPositiveInt().toString()}.${ba[1].toPositiveInt().toString()}.${ba[2].toPositiveInt().toString(10)}.${ba[3].toPositiveInt().toString()}"
    }
    else if (ba.size == 16)
    {
        var str = ""
        for (i in 0 until 16 step 2)
        {
            //val tmp = ba.slice(IntRange(i,i+1))
            val tmp = ba[i].toPositiveInt() * 256 + ba[i+1].toPositiveInt()
            str = str + tmp.toString(16) + if (i<14) ":" else ""
        }
        return str
    }
    else  // some other format
    {
        return "unknown IP address format"
    }
}

actual fun epochToDate(epochSeconds: Long): String
{
    val timept = Instant.Companion.fromEpochSeconds(epochSeconds,0)
    val dt = timept.toLocalDateTime(TimeZone.currentSystemDefault())
    return dt.toString()
}

@OptIn(ExperimentalForeignApi::class)
actual fun resolveDomain(domainName: String, port: Int?): List<ByteArray>
{
    // Try to parse numerical domain names ourselves
    try
    {
        val quad = domainName.split(".")
        if (quad.size == 4)
        {
            val b0 = quad[0].toByte()
            val b1 = quad[1].toByte()
            val b2 = quad[2].toByte()
            val b3 = quad[3].toByte()
            return listOf(byteArrayOf(b0, b1, b2, b3))
        }
    }
    catch(e: Exception)
    {}

    memScoped {
        val aip = alloc<CPointerVar<addrinfo>>()
        getaddrinfo(domainName, null, null, aip.ptr )

        var cur = aip
        val ret = mutableListOf<ByteArray>()
        while (true)
        {
            val ai = cur.pointed?.ai_addr
            if (ai != null)
            {
                val sa = ai.pointed
                val ba = if (sa.sa_family.toInt()  == AF_INET)
                {
                    val portThenIP = sa.sa_data.readBytes(2 + 4)
                    portThenIP.sliceArray(2 until 6)
                }
                else if (sa.sa_family.toInt()  == AF_INET6)
                {
                    val IP6 = sa.sa_data.readBytes(2 + 2 + 4 + 16 + 4)
                    IP6.sliceArray((2+4) until (2 + 4 + 16))
                }
                else null
                if (ba != null)
                {
                    if (ret.size == 0 || (!(ret.last() contentEquals ba))) ret.add(ba)
                }
            }
            if (cur.pointed?.ai_next == null) break
            cur.value = cur.pointed?.ai_next
        }
        freeaddrinfo(aip.value)
        return ret
    }
}


actual fun createDbDriver(dbname: String, schema: SqlSchema<QueryResult.Value<Unit>>): SqlDriver
{
    val nm = dbname
    DatabaseFileContext.databasePath(nm, DEFAULT_DB_PATH)  // This is ignored right now on Linux, DB end up in your home dir
    // println("opening $nm")
    return NativeSqliteDriver(schema, nm)
}

/*
actual fun createNexaDbDriver(dbname: String): SqlDriver
{
    return NativeSqliteDriver(NexaDb.Schema, dbname)
}

actual fun createKvpDbDriver(dbname: String): SqlDriver
{
    return NativeSqliteDriver(NexaDb.Schema, dbname)
}

 */