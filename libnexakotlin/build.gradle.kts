import com.android.ide.common.util.toPathString
import org.gradle.model.internal.core.ModelNodes.withType
import org.jetbrains.dokka.base.DokkaBase
import org.jetbrains.dokka.base.DokkaBaseConfiguration
import org.jetbrains.dokka.gradle.DokkaTask
import org.jetbrains.kotlin.gradle.plugin.KotlinExecution
import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget
import org.jetbrains.kotlin.gradle.plugin.mpp.NativeBuildType
import java.util.Properties
import java.io.FileInputStream
import java.net.URL

// Dependency versions
val serializationVersion = "1.6.2"  // https://github.com/Kotlin/kotlinx.serialization
// see https://kotlinlang.org/docs/multiplatform-mobile-concurrency-and-coroutines.html#multithreaded-coroutines
// but does not exist
//val coroutinesVersion = "1.7.1-native-mt"     // https://github.com/Kotlin/kotlinx.coroutines
val coroutinesVersion = "1.7.3"     // https://github.com/Kotlin/kotlinx.coroutines
val ktorVersion = "2.3.7" // https://github.com/ktorio/ktor/releases
val bigNumVersion = "0.3.8" // https://github.com/ionspin/kotlin-multiplatform-bignum
val mpThreadsVersion = "0.1.9"
val nexaRpcVersion = "1.1.4"
val sqlDelightVersion = "2.0.1"  // https://github.com/cashapp/sqldelight

// Host determination
val LINUX = System.getProperty("os.name").lowercase().contains("linux")
val MAC = System.getProperty("os.name").lowercase().contains("mac")
val MSWIN = System.getProperty("os.name").lowercase().contains("windows")

// NOTE on your primary (first publish) system, you need to specify ALL targets as targets, even if this host does not
// publish them.  If they are not specified, the published .module file will not contain a definition for that target
// and so it will be as if it does not exist from a dependency perspective, even if you later publish the library from
// another host.
val LINUX_TARGETS = LINUX
val MAC_TARGETS = MAC || LINUX
// ktor network does not support ms windows so we cannot produce NATIVE (jvm will work) MSWIN right now
var MSWIN_TARGETS = false
var ANDROID_TARGETS = MAC || LINUX  // I want to build android on linux and mac only

if (MAC) println("Host is a MAC, MacOS and iOS targets are enabled")
if (LINUX) println("Host is LINUX, Android, JVM, and LinuxNative targets are enabled")
else println("Linux target is disabled")

if (MSWIN) { println("Host is MS-WINDOWS"); MSWIN_TARGETS = true }


if (!LINUX_TARGETS) println("Linux targets are disabled")
if (!MAC_TARGETS) println("MacOS and iOS targets are disabled")
if (!MSWIN_TARGETS) println("Ms-windows Mingw64 target is disabled")
if (!ANDROID_TARGETS) println("Android target is disabled")

val NATIVE_BUILD_CHOICE:NativeBuildType = NativeBuildType.DEBUG

fun org.jetbrains.kotlin.gradle.dsl.KotlinNativeBinaryContainer.libnexaBinCfg() {
    /* This is a library only
    executable {
        NATIVE_BUILD_CHOICE
    }
     */
    sharedLib { NATIVE_BUILD_CHOICE }
    staticLib { NATIVE_BUILD_CHOICE }
}

val prop = Properties().apply {
    load(FileInputStream(File(rootProject.rootDir, "local.properties")))
}

// All plugin versions are in the root project
plugins {
    kotlin("multiplatform")
    id("com.android.library")
    kotlin("plugin.serialization")
    id("app.cash.sqldelight")
    id("maven-publish")
    id("org.jetbrains.dokka")
    idea
}

// Stop android studio from indexing the contrib folder
idea {
    module {
        excludeDirs.add(File(projectDir,"libnexa"))
        excludeDirs.add(File("/fast/nexa/nexa"))  // Painful but the IDE can't deal with symlinks in this exclude
    }
}

fun getCompileSdk():Int
{
    return rootProject.extensions.findByType<com.android.build.gradle.BaseExtension>()!!.compileSdkVersion!!.split("-")[1].toInt()
}

fun mustExist(dir:String):File
{
    val f = file(dir)
    if (!f.exists()) throw Exception("missing $f")
    return f
}

fun prjFileMustExist(path:String):File
{
    val f = project.file(path)
    if (!f.exists()) throw Exception("missing $f")
    return f
}

// To regenerate use: ./gradlew generateSqlDelightInterface
sqldelight {
    databases {
        create("WalletDb") {
            //srcDirs.setFrom(prjFileMustExist("src/commonMain/sqldelight/WalletDb/KvpDatabase.sq"))
            srcDirs.setFrom(prjFileMustExist("src/sqlWallet"))
            packageName.set("WalletDb") // packageName is now a Property<String>
        }
        create("NexaDb") {
            //srcDirs.setFrom(prjFileMustExist("src/commonMain/sqldelight/NexaDb/NexaHeaderDatabase.sq"))
            srcDirs.setFrom(prjFileMustExist("src/sqlNexa"))
            packageName.set("NexaDb") // packageName is now a Property<String>
        }
        create("BchDb") {
            //srcDirs.setFrom(prjFileMustExist("src/commonMain/sqldelight/NexaDb/NexaHeaderDatabase.sq"))
            srcDirs.setFrom(prjFileMustExist("src/sqlBch"))
            packageName.set("BchDb") // packageName is now a Property<String>
        }
        create("KvDb") {
            //srcDirs.setFrom(prjFileMustExist("src/commonMain/sqldelight/NexaDb/NexaHeaderDatabase.sq"))
            srcDirs.setFrom(prjFileMustExist("src/sqlKvp"))
            packageName.set("KvDb") // packageName is now a Property<String>
        }
    }
    // This adds -lsqlite3 into the link line.  Not needed, I'm providing sqlite3 in libnexalight.so: linkSqlite.set(true)
    linkSqlite.set(false)
}


@OptIn(org.jetbrains.kotlin.gradle.ExperimentalKotlinGradlePluginApi::class)
kotlin {
    targetHierarchy.default()

    // Create a function that adds a dependency to the libnexa project (sibling directory to this one)
    fun KotlinNativeTarget.libnexa(compilationTask: String) {
        compilations["main"].cinterops {
            val libnexanative by creating {
                println("libnexalight being generated with task $compilationTask")
                defFile(rootProject.file("libnexa/libnexalight.def"))
                includeDirs.headerFilterOnly(rootProject.file("libnexa/nexa/src/cashlib/"))
                tasks[interopProcessingTaskName].dependsOn(":libnexa:$compilationTask")
            }
        }
    }

    /*  No longer needed because I put sqlite into libnexalight
    if (LINUX_TARGETS)
    {
        // Somehow the sqldelight project doesn't include the sqlite3 library!  On linux this must be built with the non-host compiler
        // (the kotlin native c compiler (in ~/.konan).  So this ugly hack produces that & we jam it into the final link with flags
        task("libsqlite3_linux") {
            project.exec {
                    workingDir(prjFileMustExist("src/linuxMain/sqlite"))
                    commandLine("bash", prjFileMustExist("src/linuxMain/sqlite/build.sh"))
            }
            //    doLast { println("libsqlite3_linux finished") }
        }
    }

     */

    jvm {
        compilations.all {
            kotlinOptions {
                jvmTarget = "17"
            }
        }
    }

    if (ANDROID_TARGETS)
    {
        androidTarget {
            compilations.all {
                kotlinOptions {
                    jvmTarget = "17"
                }
            }
            publishLibraryVariants("release", "debug")
        }
    }

    if (MAC_TARGETS)
    {
        val iosX64def = iosX64 {
            compilations.getByName("main") {
                if (MAC) libnexa("libnexalightIosX64")
                compilerOptions.options.freeCompilerArgs.add("-verbose")
                binaries.libnexaBinCfg()
            }
        }
        val iosArm64def = iosArm64 {
            compilations.getByName("main") {
                if (MAC) libnexa("libnexalightIosArm64")
                compilerOptions.options.freeCompilerArgs.add("-verbose")
                binaries.libnexaBinCfg()
            }
        }

        /* commented out because in libnexalight.def doesn't know how to point to the .a file */
        val iosSimArm64def = iosSimulatorArm64 {
            compilations.getByName("main") {
                if (MAC) libnexa("libnexalightIosSimulatorArm64")
                compilerOptions.options.freeCompilerArgs.add("-verbose")
                binaries.libnexaBinCfg()
            }
        }

        listOf(iosX64def, iosArm64def, iosSimArm64def).forEach {
            // it.binaries.libnexaBinCfg()
            it.binaries.framework {
                baseName = "shared"
            }
        }

        macosX64 {
            compilations.getByName("main") {
                if (MAC) libnexa("libnexalightMacosX64")
                compilerOptions.options.freeCompilerArgs.add("-verbose")
                binaries.libnexaBinCfg()
            }
        }

        macosArm64 {
            compilations.getByName("main") {
                if (MAC) libnexa("libnexalightMacosArm64")
                compilerOptions.options.freeCompilerArgs.add("-verbose")
                binaries.libnexaBinCfg()
            }
        }
    }


    /*  These targets have no threading
    wasm {
        // browser()
        nodejs()
        //d8()
    }
    js(IR) {
        nodejs()
        binaries.executable()
    }
     */


    if (LINUX_TARGETS)
    {
        linuxX64 {
            compilations.getByName("main") {
                // No longer needed because I put sqlite into libnexalight: tasks[this.compileKotlinTaskName].dependsOn("libsqlite3_linux")
                libnexa("libnexalightLinuxX64")
                compilerOptions.options.freeCompilerArgs.add("-verbose")
                //compilerOptions.options.freeCompilerArgs.addAll("-include-binary", prjFileMustExist("src/linuxMain/sqlite/sqlite3.o").toString())

                binaries.libnexaBinCfg()
            }
        }
    }

    if (MSWIN_TARGETS)
    {
        // MS windows
        mingwX64 {
            compilations.getByName("main") {
                libnexa("libnexalightMingwX64")
                compilerOptions.options.freeCompilerArgs.add("-verbose")
                binaries.libnexaBinCfg()
            }
        }
    }


    sourceSets {
        // All these "variable" definitions need corresponding directories (that's what "by getting" does)
        val commonMain by getting {
            dependencies {
                // core language features
                // To support IOS multithreaded coroutines, we need to use a specific version
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
                // { version { strictly(coroutinesVersion) } }
                implementation(kotlin("stdlib-common"))
                implementation(kotlin("reflect"))
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.5.0") // https://github.com/Kotlin/kotlinx-datetime/tags

                // multiplatform replacements

                // files
                // implementation("org.jetbrains.kotlinx:kotlinx-io-core:0.2.1")
                // database
                implementation("app.cash.sqldelight:runtime:$sqlDelightVersion")
                implementation("app.cash.sqldelight:coroutines-extensions:$sqlDelightVersion")

                // for bigintegers
                implementation("com.ionspin.kotlin:bignum:$bigNumVersion")
                implementation("com.ionspin.kotlin:bignum-serialization-kotlinx:$bigNumVersion")
                // for network
                implementation("io.ktor:ktor-client-core:$ktorVersion")
                // implementation("io.ktor:ktor-client-websockets:$ktorVersion")
                // implementation("io.ktor:ktor-client-logging:$ktorVersion")
                implementation("io.ktor:ktor-client-serialization:$ktorVersion")
                implementation("io.ktor:ktor-client-content-negotiation:$ktorVersion")
                // implementation("io.ktor:ktor-utils:$ktorVersion")
                implementation("io.ktor:ktor-serialization-kotlinx-json:$ktorVersion")

                // These deps don't exist on the mingw64 native target, which is why its disabled right now
                implementation("io.ktor:ktor-network:$ktorVersion")
                // implementation("io.ktor:ktor-network-tls:$ktorVersion")
                implementation("io.ktor:ktor-client-cio:$ktorVersion")

                implementation("com.squareup.okio:okio:3.7.0")  // https://mvnrepository.com/artifact/com.squareup.okio/okio
                implementation("org.jetbrains.kotlinx:atomicfu:0.23.1")  // https://github.com/Kotlin/kotlinx-atomicfu

                // Nexa
                implementation("org.nexa:mpthreads:$mpThreadsVersion")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
                //implementation(kotlin("LibNexaTests"))
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:$coroutinesVersion")
                implementation("org.nexa:nexarpc:$nexaRpcVersion")
            }
        }

        // Common to all "native" targets
        val nativeMain by getting {
            dependsOn(sourceSets.named("commonMain").get())
            dependencies {
                implementation("com.squareup.okio:okio:3.7.0")
                implementation("app.cash.sqldelight:native-driver:2.0.1")
            }
        }

        // Common to all JVM targets
        create("commonJvm") {
            kotlin.srcDir(mustExist("src/commonJvm/kotlin"))
            dependsOn(sourceSets.named("commonMain").get())
            dependencies {
                implementation("com.squareup.okio:okio:3.7.0")
            }
        }


        if (ANDROID_TARGETS)
        {
            val androidMain by getting {
                //dependsOn(commonMain)
                // Android studio's syntax highlighting compiler can't handle additional srcdirs like this (although the compile works)
                // for now, use symlinks.
                // kotlin.srcDirs(mustExist("src/commonJvm/kotlin"))
                dependsOn(sourceSets.named("commonJvm").get())
                dependencies {
                    implementation(kotlin("stdlib-jdk8"))

                    // This calls your own startup code with the app context (see AndroidManifest.xml)
                    implementation("androidx.startup:startup-runtime:1.1.1")
                    implementation("io.ktor:ktor-network:$ktorVersion")

                    // Nexa
                    implementation("org.nexa:mpthreads:$mpThreadsVersion")
                    implementation("app.cash.sqldelight:android-driver:2.0.1")
                    // implementation(project(":aars",configuration = "default"))
                }
            }
        }

        val jvmMain by getting {
            dependsOn(sourceSets.named("commonJvm").get())
            dependencies {
                // Nexa
                implementation("org.nexa:mpthreads:$mpThreadsVersion")
                implementation("app.cash.sqldelight:sqlite-driver:2.0.1")
            }
        }

        if (MAC_TARGETS)
        {
            val macosX64Main by getting {
                // dependsOn(sourceSets.named("commonNative").get())
                dependencies {
                    // implementation("app.cash.sqldelight:native-driver:2.0.0")
                }
            }

            val iosX64Main by getting {
                dependencies {
                }
            }
            val iosArm64Main by getting {
                dependencies {
                }
            }

            val iosMain by getting {
                dependencies {
                    // implementation("app.cash.sqldelight:native-driver:2.0.0")
                    // implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-ios:$coroutinesVersion")
                }
            }
        }


        if (LINUX_TARGETS)
        {
            val linuxMain by getting {
                dependencies {
                    implementation("app.cash.sqldelight:native-driver:2.0.1")
                }
            }
            val linuxX64Main by getting {
                dependsOn(sourceSets.named("linuxMain").get())
                dependencies {
                    implementation("app.cash.sqldelight:native-driver:2.0.1")
                }
            }
        }


        if (MSWIN_TARGETS)
        {
            val mingwMain by getting {
                dependencies {
                    implementation("app.cash.sqldelight:native-driver:2.0.1")
                }
            }
        }


        if (ANDROID_TARGETS)
        {
            val androidInstrumentedTest by getting {
                dependencies {
                    implementation(kotlin("test-junit"))
                    implementation("org.nexa:nexarpc:$nexaRpcVersion")
                    implementation("androidx.test:core:1.5.0")
                    implementation("androidx.test:core-ktx:1.5.0")
                    implementation("androidx.test.ext:junit:1.1.5")
                    implementation("androidx.test.ext:junit-ktx:1.1.5")

                    implementation("androidx.test.espresso:espresso-core:3.5.1")
                    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:$coroutinesVersion")
                    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutinesVersion")
                }
            }
        }

        all {
            nexaLangSettings()
            languageSettings {
                optIn("kotlinx.coroutines.ExperimentalCoroutinesApi")
                optIn("kotlinx.coroutines.ExperimentalUnsignedTypes")
            }
        }
    }

    // Stop publication duplication
    val publicationsFromLinuxOnly:MutableList<String> =
        mutableListOf(jvm(), androidTarget()).map { it.name }.toMutableList()
    if (LINUX_TARGETS) publicationsFromLinuxOnly.add(linuxX64().name)
    publicationsFromLinuxOnly.add("kotlinMultiplatform")
    if (ANDROID_TARGETS) publicationsFromLinuxOnly.add("androidDebug")
    if (ANDROID_TARGETS) publicationsFromLinuxOnly.add("androidRelease")
    if (MSWIN_TARGETS) publicationsFromLinuxOnly.add(mingwX64().name)

    publishing {
        publications {
            matching { val name = it.name; publicationsFromLinuxOnly.filter { it in name }.size > 0 }.all {
                tasks.withType<AbstractPublishToMaven>()
                    .matching {
                        val pub = it.publication
                        if (pub != null) {
                            pub.name in publicationsFromLinuxOnly
                        } else false
                    }
                    .configureEach { onlyIf { LINUX } }
            }
        }
    }
}

/* No longer needed because sqlite was put in libnexa.so
if (LINUX_TARGETS)
{
    // Add our custom libsqlite3.so file into the link (if its missing, go to that directory and build it)
    // tasks.named("linkDebugTestLinuxX64") {
    //tasks.filter { it.name.contains( "linkDebugTestLinuxX64")}.forEach {

    listOf("linkDebugTestLinuxX64","linkDebugSharedLinuxX64","linkReleaseSharedLinuxX64").forEach { taskName ->
        tasks.named(taskName) {
            // val co = (this as org.jetbrains.kotlin.gradle.tasks.KotlinCompile).compilerOptions
            val knl = (this as org.jetbrains.kotlin.gradle.tasks.KotlinNativeLink)
            val co = knl.kotlinOptions
            co.freeCompilerArgs = listOf(
                "-linker-option",
                "-L" + prjFileMustExist("src/linuxMain/sqlite")
            ) + co.freeCompilerArgs
        }
    }
}

 */

var androidNdkDir:File? = null

android {
    namespace = "org.nexa.libnexakotlin"
    // ndkVersion = "25.2.9519653"
    // ndkVersion = rootProject.ext.ndkVersion
    // Go get the compile SDK from the root project
    compileSdk = rootProject.extensions.findByType<com.android.build.gradle.BaseExtension>()!!.compileSdkVersion!!.split("-")[1].toInt()
    defaultConfig {
        minSdk = 29
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        externalNativeBuild {
            cmake {
                cppFlags.add("-std=c++17")
                targets.add("nexalight")

                if (isRingSignaturesEnabled())
                {
                    println("Experimental ring signatures enabled.")
                    val ndkPath = (rootProject.ext["androidNdkDir"] as File).toString()
                    targets.add("ringsigandroid")
                    arguments.add("-DENABLE_RINGSIG=ON")
                    arguments.add("-DNDK_ROOT=" + ndkPath)
                }

                // See: https://developer.android.com/reference/tools/gradle-api/4.1/com/android/build/api/dsl/ExternalNativeBuildOptions
                arguments.add("-DANDROID_STL=c++_shared")
                arguments.add("-DANDROID=1")
            }
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    externalNativeBuild {
        cmake {
            path("src/androidMain/cpp/CMakeLists.txt")
            this.version = "3.14.4+"
        }
    }
    androidNdkDir = android.ndkDirectory
}



if (MAC_TARGETS)
{
    task("iosTest") {
        val device = project.findProperty("iosDevice")?.toString() ?: "iPhone 14 Pro Max"
        dependsOn(kotlin.iosX64().binaries.getTest("DEBUG").linkTaskName)
        group = JavaBasePlugin.VERIFICATION_GROUP
        description = "Runs tests for target 'ios' on an iOS simulator"

        doLast {
            val binary = kotlin.iosX64().binaries.getTest("DEBUG").outputFile
            exec {
                commandLine = listOf("xcrun", "simctl", "spawn", device, binary.absolutePath)
            }
        }
    }
}

fun isRingSignaturesEnabled():Boolean
{
    val enableRingsigProperty = prop.getProperty("ringsignatures")
    val enableRingsig = enableRingsigProperty != null &&
        (enableRingsigProperty == "true" || enableRingsigProperty == "on")
    return enableRingsig
}

fun org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet.nexaLangSettings()
{
    languageSettings {
        languageVersion = "1.9"
    }
}

// Grab my libraries from the jars directory (for local development)
fun org.jetbrains.kotlin.gradle.plugin.KotlinDependencyHandler.localOrReleased(localFile: String, dependencyNotation: Any, forceLocal:Boolean? = null)
{
    val localJars = project.property("localJars").toString().trim()
    var always = false

    val fl = if (forceLocal==null) {
        if (localJars == "always") {
            always = true
            true
        }
        else localJars.toBooleanStrict()
    } else forceLocal

    val hasLocal = if (fl)
    {
        //val f = localFile.forEach { File(rootProject.rootDir, it) }
        val f = File(rootProject.rootDir, localFile)

        if (f.exists())
        {
            println("Using local library: $localFile")
            implementation(files(f))
            true
        }
        else
        {
            if (always) {
                println("ERROR: Cannot find local library file $localFile")
                throw GradleException("Cannot find local library file $localFile")
            }

            logger.warn("w: Even though LOCAL_JARS is true $localFile pulled from its released version because it" +
                " does not exist at {f.absolutePath}.")
            false
        }
    } else false

    if (!hasLocal)
    {
        println("Using release library for: $localFile")
        implementation(dependencyNotation)
    }
}

// PUBLISHING
// Deployment constants
group = "org.nexa"
version = "0.1.25"

publishing {
    repositories {
        maven {
            // Project ID number is shown just below the project name in the project's home screen
            url = uri("https://gitlab.com/api/v4/projects/48545045/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Deploy-Token"
                value = prop.getProperty("LibNexaKotlinDeployTokenValue")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}

// DOCUMENTATION

tasks.dokkaHtml {
    outputDirectory.set(rootDir.resolve("public"))
}

tasks.withType<DokkaTask>().configureEach {

    pluginConfiguration<DokkaBase, DokkaBaseConfiguration> {
        customAssets = listOf(file("src/doc/nexa-64x64.png"), file("src/doc/logo-icon.svg"))
        customStyleSheets = listOf(file("src/doc/logo-styles.css"))
        footerMessage = "(c) 2023 Bitcoin Unlimited"
    }

    dokkaSourceSets {
        named("commonMain")
        {
            //displayName.set("libnexakotlin")
            // displayName.set("Nexa Kotlin Library")
            // includes.from("src/doc/Module.md")
            // platform.set(org.jetbrains.dokka.Platform.jvm)
            //sourceRoots.from(kotlin.sourceSets.getByName("jvmMain").kotlin.srcDirs)
            sourceRoots.from(kotlin.sourceSets.getByName("commonMain").kotlin.srcDirs)
            /*
            sourceLink {
                val path = "src/commonMain/kotlin"
                localDirectory.set(file(path))
                val url = "https://nexa.gitlab.io/libnexakotlin"
                remoteUrl.set(URL("$url/tree/${moduleVersion.get()}/lib/$path"))
            }

             */
        }

        forEach {
            it.run {
                //displayName.set("Nexa Kotlin Library")
                includes.from("src/doc/Module.md")
                sourceLink {
                    localDirectory.set(project.file("libnexakotlin/src/commonMain/kotlin"))
                    remoteUrl.set(URL("https://gitlab.com/nexa/libnexakotlin/-/tree/main/"))
                    remoteLineSuffix.set("#L")
                }
                includeNonPublic.set(false)
            }
        }
    }

    dokkaSourceSets.configureEach {
        perPackageOption {
            matchingRegex.set("bitcoinunlimited.*")
            suppress.set(true)
        }
    }
}
