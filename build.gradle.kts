import java.util.Properties
import java.io.FileInputStream
import java.net.URL
import org.jetbrains.dokka.base.DokkaBase
import org.jetbrains.dokka.base.DokkaBaseConfiguration
import org.jetbrains.dokka.gradle.DokkaTask


plugins {
    //trick: for the same plugin versions in all sub-modules
    id("com.android.library").version("8.2.0")
    kotlin("multiplatform").version("1.9.10").apply(false)
    kotlin("plugin.serialization") version "1.9.10"
    id("app.cash.sqldelight").version("2.0.1").apply(false)
    id("maven-publish")
    id("org.jetbrains.dokka").version("1.9.10").apply(false)
    idea
}

val prop = Properties().apply {
    try {
        load(FileInputStream(File(rootProject.rootDir, "local.properties")))
    } catch(e: java.io.FileNotFoundException)
    {
        File(rootProject.rootDir, "local.properties").writeText("### This file must NOT be checked into version control, since it contains local configuration.")
        load(FileInputStream(File(rootProject.rootDir, "local.properties")))
    }
}

// Define a few local variables
ext {
    var androidNdkDir = ""
    var linuxToolchain = prop["linux.toolchain"]
    var linuxTarget = prop["linux.target"]
}


android {
    namespace = "org.nexa"
    compileSdk = 34
    //onError(InvalidUserDataException containing "NDK is not installed" printAndExit
    //    "Install the Android Native Development Kit (NDK) first") {

    try {
        rootProject.ext["androidNdkDir"] = android.ndkDirectory
    }
    catch (e:org.gradle.api.InvalidUserDataException)
    {
        val msg = e.message
        if (msg != null && ("NDK is not installed" in msg))
        {
            println("Install the Android Native Development kit, in Tools->SDK Manager->plugins")
        }
        throw e
    }

}

buildscript {
    repositories {
        google()
        mavenCentral()
    }
    dependencies {
        //classpath("com.android.tools.build:gradle:YOUR_CURRENT_ANDROID_PLUGIN_VERSION")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.9.20")
        // classpath("com.squareup.sqldelight:gradle-plugin:1.5.5")
        classpath("org.jetbrains.dokka:dokka-base:1.9.10")
    }
}


// Stop android studio from indexing the contrib folder
idea {
    module {
        excludeDirs.add(File(projectDir,"libnexa/nexa"))
        excludeDirs.add(File("/fast/nexa/nexa"))  // Painful but the IDE can't deal with symlinks in this exclude
    }
}

/*
tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}
*/
